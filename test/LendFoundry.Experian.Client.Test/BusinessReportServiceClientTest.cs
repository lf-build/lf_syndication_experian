﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Experian;
using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using Moq;
using RestSharp;
using Xunit;

namespace LendFoundry.Experian.Client.Test
{
    public class BusinessReportServiceClientTest
    {
        public BusinessReportServiceClientTest()
        {
            Client = new Mock<IServiceClient>();
            businessReportServiceClient = new BusinessReportService(Client.Object);
        }

        public BusinessReportService businessReportServiceClient { get; }

        private Mock<IServiceClient> Client { get; set; }
        private IRestRequest restRequest { get; set; }

        [Fact]
        public async void Client_GetBusinessSearch()
        {
            GetSearchBusinessResponse businessResponse = new GetSearchBusinessResponse();
            businessResponse.Products = null;
            Client.Setup(s => s.ExecuteAsync<GetSearchBusinessResponse>(It.IsAny<IRestRequest>()))
             .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new GetSearchBusinessResponse());
            var response = await businessReportServiceClient.GetBusinessSearch("application", "12345", It.IsAny<IGetBusinessSearchRequest>());
            Assert.Equal("business/{entitytype}/{entityid}/search", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        [Fact]
        public async void Client_GetBusinessReport()
        {
            GetBusinessReportResponse businessReportResponse = new GetBusinessReportResponse();
            businessReportResponse.BusinessProfile = null;
            Client.Setup(s => s.ExecuteAsync<GetBusinessReportResponse>(It.IsAny<IRestRequest>()))
             .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new GetBusinessReportResponse());
            var response = await businessReportServiceClient.GetBusinessReport("application", "12345", It.IsAny<IGetBusinessReportRequest>());
            Assert.Equal("business/{entitytype}/{entityid}/report", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        [Fact]
        public async void Client_GetDirectHitReport()
        {
            GetDirectHitReportResponse businessReportResponse = new GetDirectHitReportResponse();
            businessReportResponse.PremierProfile = null;
            Client.Setup(s => s.ExecuteAsync<GetDirectHitReportResponse>(It.IsAny<IRestRequest>()))
             .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new GetDirectHitReportResponse());
            var response = await businessReportServiceClient.GetDirectHitReport("application", "12345", It.IsAny<IGetDirectHitReportRequest>());
            Assert.Equal("business/{entitytype}/{entityid}/directhitreport", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        
    }
}