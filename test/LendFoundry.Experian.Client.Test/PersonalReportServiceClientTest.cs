﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Experian;
using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using Moq;
using RestSharp;
using Xunit;

namespace LendFoundry.Experian.Client.Test
{
    public class PersonalReportServiceClient
    {
        public PersonalReportServiceClient()
        {
            Client = new Mock<IServiceClient>();
            personalReportServiceClient = new PersonalReportService(Client.Object);
        }

        public PersonalReportService personalReportServiceClient { get; }

        private Mock<IServiceClient> Client { get; set; }
        private IRestRequest restRequest { get; set; }

        [Fact]
        public void Client_GetPersonalReport()
        {
            GetPersonalReportResponse personalReportResponse = new GetPersonalReportResponse();
            personalReportResponse.ProductCreditProfile = null;
            Client.Setup(s => s.ExecuteAsync<GetPersonalReportResponse>(It.IsAny<IRestRequest>()))
             .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new GetPersonalReportResponse());
            var response = personalReportServiceClient.GetPersonalReportResponse("application", "12345", It.IsAny<IPersonalReportRequest>());
            Assert.Equal("personal/{entitytype}/{entityid}/report", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }
    }
}