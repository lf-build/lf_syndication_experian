﻿using LendFoundry.Experian.Api.Controller;
using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;

namespace LendFoundry.Syndication.Experian.Api.Test
{
    public class PersonalReportController
    {
        private Mock<IBusinessReportService> BusinessReportService { get; }
        private Mock<IPersonalReportService> PersonalReportService { get; }

        private PersonalReportApiController GetController(IBusinessReportService businessReportService, IPersonalReportService personalReportService)
        {
            return new PersonalReportApiController(businessReportService, personalReportService);
        }

        private Mock<IBusinessReportService> GetBusinessReportService()
        {
            return new Mock<IBusinessReportService>();
        }

        private Mock<IPersonalReportService> GetPersonalReportService()
        {
            return new Mock<IPersonalReportService>();
        }

        [Fact]
        public async void GetPersonalReportReturnOnSucess()
        {
            var businessReportService = GetBusinessReportService();
            var personalReportService = GetPersonalReportService();
            personalReportService.Setup(x => x.GetPersonalReportResponse(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IPersonalReportRequest>())).ReturnsAsync(new GetPersonalReportResponse());
            var response = (HttpOkObjectResult)await GetController(businessReportService.Object, personalReportService.Object).GetPersonalReport("application", "12345", It.IsAny<GetPersonalReportRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetPersonalReportReturnOnError()
        {
            var businessReportService = GetBusinessReportService();
            var personalReportService = GetPersonalReportService();
            personalReportService.Setup(x => x.GetPersonalReportResponse(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IPersonalReportRequest>())).Throws(new ExperianException());
            var response = (Foundation.Services.ErrorResult)await GetController(businessReportService.Object, personalReportService.Object).GetPersonalReport("", "", null);
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }
    }
}