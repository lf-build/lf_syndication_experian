﻿using LendFoundry.Experian.Api.Controller;
using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;

namespace LendFoundry.Syndication.Experian.Api.Test
{
    public class ExperianControllerTest
    {
        private Mock<IBusinessReportService> BusinessReportService { get; }
        private Mock<IPersonalReportService> PersonalReportService { get; }

        private BusinessReportApiController GetController(IBusinessReportService businessReportService, IPersonalReportService personalReportService)
        {
            return new BusinessReportApiController(businessReportService, personalReportService);
        }

        private Mock<IBusinessReportService> GetBusinessReportService()
        {
            return new Mock<IBusinessReportService>();
        }

        private Mock<IPersonalReportService> GetPersonalReportService()
        {
            return new Mock<IPersonalReportService>();
        }

        [Fact]
        public async void GetBusinessReportReturnOnSucess()
        {
            var businessReportService = GetBusinessReportService();
            var personalReportService = GetPersonalReportService();
            businessReportService.Setup(x => x.GetBusinessReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IGetBusinessReportRequest>())).ReturnsAsync(new GetBusinessReportResponse());
            var response = (HttpOkObjectResult)await GetController(businessReportService.Object, personalReportService.Object).GetBusinessReport("application", "12345", It.IsAny<GetBusinessReportRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetBusinessReportReturnOnError()
        {
            var businessReportService = GetBusinessReportService();
            var personalReportService = GetPersonalReportService();
            businessReportService.Setup(x => x.GetBusinessReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IGetBusinessReportRequest>())).Throws(new ExperianException());
            var response = (Foundation.Services.ErrorResult)await GetController(businessReportService.Object, personalReportService.Object).GetBusinessReport("", "", null);
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void GetDirectHitReportReturnOnSucess()
        {
            var businessReportService = GetBusinessReportService();
            var personalReportService = GetPersonalReportService();
            businessReportService.Setup(x => x.GetDirectHitReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IGetDirectHitReportRequest>())).ReturnsAsync(new GetDirectHitReportResponse());
            var response = (HttpOkObjectResult)await GetController(businessReportService.Object, personalReportService.Object).GetDirectHitReport("application", "12345", It.IsAny<GetDirectHitReportRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetDirectHitReportReturnOnError()
        {
            var businessReportService = GetBusinessReportService();
            var personalReportService = GetPersonalReportService();
            businessReportService.Setup(x => x.GetDirectHitReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IGetDirectHitReportRequest>())).Throws(new ExperianException());
            var response = (Foundation.Services.ErrorResult)await GetController(businessReportService.Object, personalReportService.Object).GetDirectHitReport("", "", null);
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }
    }
}