﻿using LendFoundry.Syndication.Experian.Proxy;
using System;
using Xunit;

namespace LendFoundry.Syndication.Experian.Test
{
    public class BusinessReportConfigurationTest
    {
        public ExperianConfiguration experianConfiguration = new ExperianConfiguration();

        [Fact]
        public void ThrowArgumentExceptionWithNullConfiguration()
        {
            Assert.Throws<ArgumentNullException>(() => new BusinessReportProxy(null));
        }

        [Fact]
        public void ThrowArgumentExceptionWithMissingConfiguration()
        {
            var NetConnectSubscriberNullException = new BusinessReportConfiguration
            {
                NetConnectSubscriber = "",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1",
                BusinessVerbose = "1"
            };
            experianConfiguration.businessReportConfiguration = NetConnectSubscriberNullException;
            Assert.Throws<ArgumentException>(() => new BusinessReportProxy(experianConfiguration));
            var NetConnectPasswordNullException = new BusinessReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1",
                BusinessVerbose = "1"
            };
            experianConfiguration.businessReportConfiguration = NetConnectPasswordNullException;
            Assert.Throws<ArgumentException>(() => new BusinessReportProxy(experianConfiguration));
            var NetConnectRequestUrlNullException = new BusinessReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "",
                Eai = "1",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1",
                BusinessVerbose = "1"
            };
            experianConfiguration.businessReportConfiguration = NetConnectRequestUrlNullException;

            Assert.Throws<ArgumentException>(() => new BusinessReportProxy(experianConfiguration));
            var EaiNullException = new BusinessReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1",
                BusinessVerbose = "1"
            };
            experianConfiguration.businessReportConfiguration = EaiNullException;

            Assert.Throws<ArgumentException>(() => new BusinessReportProxy(experianConfiguration));
            var PreambleNullException = new BusinessReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1",
                BusinessVerbose = "1"
            };

            experianConfiguration.businessReportConfiguration = PreambleNullException;

            Assert.Throws<ArgumentException>(() => new BusinessReportProxy(experianConfiguration));
            var OpInitialsNullException = new BusinessReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1",
                BusinessVerbose = "1"
            };

            experianConfiguration.businessReportConfiguration = OpInitialsNullException;
            Assert.Throws<ArgumentException>(() => new BusinessReportProxy(experianConfiguration));
            var ArfVersionNullException = new BusinessReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "",
                VendorNumber = "123",
                VendorVersion = "1",
                BusinessVerbose = "1"
            };
            experianConfiguration.businessReportConfiguration = ArfVersionNullException;
            Assert.Throws<ArgumentException>(() => new BusinessReportProxy(experianConfiguration));
            var DbHostNullException = new BusinessReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1",
                BusinessVerbose = "1"
            };

            experianConfiguration.businessReportConfiguration = DbHostNullException;
            Assert.Throws<ArgumentException>(() => new BusinessReportProxy(experianConfiguration));
            var SubCodeNullException = new BusinessReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "Test",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1",
                BusinessVerbose = "1"
            };

            experianConfiguration.businessReportConfiguration = SubCodeNullException;

            Assert.Throws<ArgumentException>(() => new BusinessReportProxy(experianConfiguration));
        }
    }
}