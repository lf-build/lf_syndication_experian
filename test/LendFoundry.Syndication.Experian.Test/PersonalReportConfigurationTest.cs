﻿using LendFoundry.Syndication.Experian.Proxy;
using System;
using Xunit;

namespace LendFoundry.Syndication.Experian.Test
{
    public class PersonalReportConfigurationTest
    {
        public ExperianConfiguration experianConfiguration = new ExperianConfiguration();

        [Fact]
        public void ThrowArgumentExceptionWithNullConfiguration()
        {
            Assert.Throws<ArgumentNullException>(() => new PersonalReportProxy(null));
        }

        [Fact]
        public void ThrowArgumentExceptionWithMissingConfiguration()
        {
            var NetConnectSubscriberNullException = new PersonalReportConfiguration
            {
                NetConnectSubscriber = "",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1"
            };

            experianConfiguration.personalReportConfiguration = NetConnectSubscriberNullException;

            Assert.Throws<ArgumentException>(() => new PersonalReportProxy(experianConfiguration));
            var NetConnectPasswordNullException = new PersonalReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1"
            };

            experianConfiguration.personalReportConfiguration = NetConnectSubscriberNullException;

            Assert.Throws<ArgumentException>(() => new PersonalReportProxy(experianConfiguration));
            var NetConnectRequestUrlNullException = new PersonalReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "",
                Eai = "1",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1"
            };

            experianConfiguration.personalReportConfiguration = NetConnectRequestUrlNullException;

            Assert.Throws<ArgumentException>(() => new PersonalReportProxy(experianConfiguration));
            var EaiNullException = new PersonalReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1"
            };

            experianConfiguration.personalReportConfiguration = EaiNullException;

            Assert.Throws<ArgumentException>(() => new PersonalReportProxy(experianConfiguration));
            var PreambleNullException = new PersonalReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1"
            };
            experianConfiguration.personalReportConfiguration = PreambleNullException;

            Assert.Throws<ArgumentException>(() => new PersonalReportProxy(experianConfiguration));
            var OpInitialsNullException = new PersonalReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1"
            };
            experianConfiguration.personalReportConfiguration = OpInitialsNullException;

            Assert.Throws<ArgumentException>(() => new PersonalReportProxy(experianConfiguration));
            var ArfVersionNullException = new PersonalReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "1",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "",
                VendorNumber = "123",
                VendorVersion = "1"
            };
            experianConfiguration.personalReportConfiguration = ArfVersionNullException;

            Assert.Throws<ArgumentException>(() => new PersonalReportProxy(experianConfiguration));
            var DbHostNullException = new PersonalReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "123",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1"
            };
            experianConfiguration.personalReportConfiguration = DbHostNullException;

            Assert.Throws<ArgumentException>(() => new PersonalReportProxy(experianConfiguration));
            var SubCodeNullException = new PersonalReportConfiguration
            {
                NetConnectSubscriber = "www.sigma.net",
                NetConnectPassword = "123456",
                NetConnectRequestUrl = "www.abc.net",
                Eai = "1",
                DbHost = "Test",
                ReferenceId = "123",
                Preamble = "Test",
                OpInitials = "Test",
                SubCode = "",
                ArfVersion = "1",
                VendorNumber = "123",
                VendorVersion = "1"
            };
            experianConfiguration.personalReportConfiguration = SubCodeNullException;

            Assert.Throws<ArgumentException>(() => new PersonalReportProxy(experianConfiguration));
        }
    }
}