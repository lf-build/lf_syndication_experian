﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Syndication.Experian.Proxy;
using LendFoundry.Syndication.Experian.Proxy.BusinessReportRequest;
using LendFoundry.Syndication.Experian.Request;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Syndication.Experian.Test.Live
{
    public class BusinessReportServiceLiveTest
    {
        private BusinessReportService BusinessReportService { get; }
        private Mock<IBusinessReportProxy> BusinessReportProxy { get; }
        private Mock<IEventHubClient> EventHub { get; }
        private Mock<ILookupService> Lookup { get; }
        private ExperianConfiguration experianConfiguration { get; }

        public BusinessReportServiceLiveTest()
        {
            EventHub = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            BusinessReportProxy = new Mock<IBusinessReportProxy>();
            BusinessReportConfiguration BusinessReportConfiguration = new BusinessReportConfiguration
            {
                NetConnectSubscriber = "capitalalliance_bisdemo",
                NetConnectPassword = "Sigma@1234",
                NetConnectRequestUrl = "https://dm1.experian.com/netconnect2_0Demo/servlets/NetConnectServlet",
                Eai = "OXOEYRDW",
                DbHost = "BISTEST",
                ReferenceId = null,
                Preamble = "TBD1",
                OpInitials = "OP",
                SubCode = "0487130",
                ArfVersion = "07",
                VendorNumber = "BIS183",
                VendorVersion = null,
                BusinessVerbose = "Y"
            };
            experianConfiguration = new ExperianConfiguration
            {
                businessReportConfiguration = BusinessReportConfiguration,
                personalReportConfiguration = null
            };
            BusinessReportService = new BusinessReportService(experianConfiguration, BusinessReportProxy.Object,EventHub.Object, Lookup.Object);
        }

        [Fact]
        public void GetBusinessReport_Response()
        {
            var response = BusinessReportService.GetBusinessReport("application","123456",new GetBusinessReportRequest
            {
                     BusinessName= "EXPERIAN INFORMATION SOLUTIONS",
                     AlternateName= null,
                      Street= "475 ANTON BLVD",
                     City= "COSTA MESA",
                      State= "CA",
                      Zip= "92626",
                     AddOnsList= null,
                      TransactionNumber= "C011839507",
                      BisListNumber="70044177300",
                      AddOnsBUSP= null
                
            });
            Assert.NotNull(response);
        }

        [Fact]
        public void GetDirectHitReport_Response()
        {
            var response = BusinessReportService.GetDirectHitReport("application","123456",new GetDirectHitReportRequest
            {
                   BusinessName= "CHARLIE BROWNS OF TINTON FALLS INC",
                  AlternateName= null,
                  Street= "1450 US HIGHWAY 22 STE 4",
                  City= "MOUNTAINSIDE ",
                  State= "NJ",
                  Zip= "07092",
                  AddOnsList="Y",
                  AddOnsBUSP=""
            });
            Assert.NotNull(response);
        }

        [Fact]
        public void GetBusinessSearch_Response()
        {
            var response = BusinessReportService.GetBusinessSearch("application", "123456", new GetBusinessSearchRequest
            {
                BusinessName = "MORGAN STANLEY SMITH BARNEY LLC",
                AlternateName = "MORGAN STANLEY SMITH BARNEY LLC",
                Street = "2000 WESTCHESTER AVE",
                City = "PURCHASE",
                State = "NY",
                Zip = "10577",
                TransactionNumber = null,
                BisListNumber = null,
                AddOnsBUSP = ""
            });
            Assert.NotNull(response);
        }
    }
}