﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Syndication.Experian.Proxy;
using LendFoundry.Syndication.Experian.Proxy.BusinessReportRequest;
using LendFoundry.Syndication.Experian.Proxy.BusinessReportResponse;
using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Syndication.Experian.Test
{
    public class BusinessReportServiceTest
    {
        private ExperianConfiguration BusinessReportConfiguration { get; }
        private FakeBusinessReportProxy FakeBusinessReportProxy { get; }
        private BusinessReportService BusinessReportService { get; }
        private Mock<IEventHubClient> EventHub { get; }
        private Mock<ILookupService> Lookup { get; }
        public Dictionary<string, string> categoryLookup = new Dictionary<string, string>();

        public BusinessReportServiceTest()
        {
            EventHub = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            BusinessReportConfiguration = new ExperianConfiguration();
            BusinessReportConfiguration.businessReportConfiguration = new BusinessReportConfiguration
            {
                NetConnectSubscriber = "capitalalliance_bisdemo",
                NetConnectPassword = "Sigma@123",
                NetConnectRequestUrl = "https://dm1.experian.com/netconnect2_0Demo/servlets/NetConnectServlet",
                Eai = "OXOEYRDW",
                DbHost = "BISTEST",
                ReferenceId = null,
                Preamble = "TBD1",
                OpInitials = "OP",
                SubCode = "0487130",
                ArfVersion = "07",
                VendorNumber = "BIS183",
                VendorVersion = null,
                BusinessVerbose = "Y"
            };
            FakeBusinessReportProxy = new FakeBusinessReportProxy();
            BusinessReportService = new BusinessReportService(BusinessReportConfiguration, FakeBusinessReportProxy, EventHub.Object, Lookup.Object);
        }

        [Fact]
        public void VerifyThrowsArgumentExceptionWithNullGetBusinessReportRequest()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => BusinessReportService.GetBusinessReport("", "", null));
        }

        [Fact]
        public void VerifyThrowsArgumentExceptionWithMissingBusinessName()
        {
            var BusinessNameNullrequest = new GetBusinessSearchRequest
            {
                BusinessName = "",
                AlternateName = "Test",
                Street = "ABC",
                City = "Irvine",
                State = "CA",
                Zip = "12345",
                TransactionNumber = "123123",
                BisListNumber = "12345",
                AddOnsBUSP = "Test"
            };
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);

            Assert.ThrowsAsync<ArgumentException>(() => BusinessReportService.GetBusinessSearch("application", "12345", BusinessNameNullrequest));
        }

        [Fact]
        public void VerifyThrowsArgumentExceptionWithMissingCity()
        {
            var CityNullrequest = new GetBusinessSearchRequest
            {
                BusinessName = "Test",
                AlternateName = "Test",
                Street = "ABC",
                City = "",
                State = "CA",
                Zip = "12345",
                TransactionNumber = "123123",
                BisListNumber = "12345",
                AddOnsBUSP = "Test"
            };
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);

            Assert.ThrowsAsync<ArgumentException>(() => BusinessReportService.GetBusinessSearch("application", "12345", CityNullrequest));
        }

        [Fact]
        public void VerifyThrowsArgumentExceptionWithMissingStreet()
        {
            var StreetNullrequest = new GetBusinessSearchRequest
            {
                BusinessName = "Test",
                AlternateName = "Test",
                Street = "",
                City = "Irvine",
                State = "CA",
                Zip = "12345",
                TransactionNumber = "123123",
                BisListNumber = "12345",
                AddOnsBUSP = "Test"
            };
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentException>(() => BusinessReportService.GetBusinessSearch("application", "12345", StreetNullrequest));
        }

        [Fact]
        public void VerifyThrowsArgumentExceptionWithMissingState()
        {
            var StateNullrequest = new GetBusinessSearchRequest
            {
                BusinessName = "Test",
                AlternateName = "Test",
                Street = "ABC",
                City = "Irvine",
                State = "",
                Zip = "12345",
                TransactionNumber = "123123",
                BisListNumber = "12345",
                AddOnsBUSP = "Test"
            };
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);

            Assert.ThrowsAsync<ArgumentException>(() => BusinessReportService.GetBusinessSearch("application", "12345", StateNullrequest));
        }

        [Fact]
        public void VerifyThrowsArgumentExceptionWithMissingZip()
        {
            var ZipNullrequest = new GetBusinessSearchRequest
            {
                BusinessName = "Test",
                AlternateName = "Test",
                Street = "ABC",
                City = "Irvine",
                State = "CA",
                Zip = "",
                TransactionNumber = "123123",
                BisListNumber = "12345",
                AddOnsBUSP = "Test"
            };
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);

            Assert.ThrowsAsync<ArgumentException>(() => BusinessReportService.GetBusinessSearch("application", "12345", ZipNullrequest));
        }

        [Fact]
        public void GetBusinessReport_ArgumentNullException()
        {
            var TransactionNumberNullrequest = new GetBusinessSearchRequest
            {
                BusinessName = "Test",
                AlternateName = "Test",
                Street = "ABC",
                City = "Irvine",
                State = "CA",
                Zip = "12345",
                TransactionNumber = "",
                BisListNumber = "12345",
                AddOnsBUSP = "Test"
            };

            var BisListNumberNullrequest = new GetBusinessSearchRequest
            {
                BusinessName = "Test",
                AlternateName = "Test",
                Street = "ABC",
                City = "Irvine",
                State = "CA",
                Zip = "12345",
                TransactionNumber = "123123",
                BisListNumber = "",
                AddOnsBUSP = "Test"
            };
            Assert.ThrowsAsync<ArgumentNullException>(() => BusinessReportService.GetBusinessReport("application", "12345", It.IsAny<IGetBusinessReportRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => BusinessReportService.GetBusinessReport("", "12345", It.IsAny<IGetBusinessReportRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => BusinessReportService.GetBusinessReport("", "", It.IsAny<IGetBusinessReportRequest>()));
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentException>(() => BusinessReportService.GetBusinessSearch("application", "12345", TransactionNumberNullrequest));
            Assert.ThrowsAsync<ArgumentException>(() => BusinessReportService.GetBusinessSearch("application", "12345", BisListNumberNullrequest));
        }

        [Fact]
        public void MappingSearchResponse()
        {
            var porxyresponse = FakeBusinessReportProxy.GetFakeSearchResponse();
            GetBusinessSearchRequest searchRequest = new GetBusinessSearchRequest
            {
                BusinessName = "EXPERIAN INFORMATION SOLUTIONS",
                Street = "475 ANTON BLVD",
                City = "COSTA MESA",
                State = "CA",
                Zip = "92626",
            };
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            var serviceresponse = BusinessReportService.GetBusinessSearch("application", "1234", searchRequest);
            Assert.NotNull(serviceresponse);
            Assert.NotNull(serviceresponse.Result.Products);
            Assert.NotNull(serviceresponse.Result.Products.PremierProfile);
            CompareSearchPremierProfile(serviceresponse.Result.Products.PremierProfile.FirstOrDefault(), porxyresponse.Products.PremierProfile.FirstOrDefault());
        }

        private void CompareSearchPremierProfile(

          SearchBusinessResponse.IProductsPremierProfile productsPremierProfile, Proxy.SearchBusinessResponse.NetConnectResponseProductsPremierProfile fakePremierProfile)
        {
            Assert.NotNull(productsPremierProfile);
            Assert.NotNull(productsPremierProfile.BillingIndicator);
            CompareSearchBillingIndicator(productsPremierProfile.BillingIndicator, fakePremierProfile.BillingIndicator);
            Assert.NotNull(productsPremierProfile.BusinessNameAndAddress);
            CompareSearchBusinessNameAndAddress(productsPremierProfile.BusinessNameAndAddress.FirstOrDefault(), fakePremierProfile.BusinessNameAndAddress.FirstOrDefault());

            Assert.NotNull(productsPremierProfile.InputSummary);
            CompareSearchInputSummary(productsPremierProfile.InputSummary.FirstOrDefault(),
                fakePremierProfile.InputSummary.FirstOrDefault());

            Assert.NotNull(productsPremierProfile.ListOfSimilars);
            CompareSearchListOfSimilars(

productsPremierProfile.ListOfSimilars.FirstOrDefault(), fakePremierProfile.ListOfSimilars.FirstOrDefault());
        }

        private void CompareSearchBillingIndicator(SearchBusinessResponse.IBillingIndicator billingIndicator, Proxy.SearchBusinessResponse.BillingIndicator fakeBillingIndicator)
        {
            Assert.Equal(billingIndicator.Code, fakeBillingIndicator.Code);
            Assert.Equal(billingIndicator.Text, fakeBillingIndicator.Text);
        }

        private void CompareSearchBusinessNameAndAddress(SearchBusinessResponse.IBusinessNameAndAddress businessNameAndAddress,
           Proxy.SearchBusinessResponse.NetConnectResponseProductsPremierProfileBusinessNameAndAddress fakeBusinessNameAndAddress
          )
        {
            Assert.Equal(businessNameAndAddress.ProfileDate, fakeBusinessNameAndAddress.ProfileDate);
            Assert.Equal(businessNameAndAddress.ProfileTime, fakeBusinessNameAndAddress.ProfileTime);
            Assert.Equal(businessNameAndAddress.TerminalNumber, fakeBusinessNameAndAddress.TerminalNumber);

            Assert.NotNull(businessNameAndAddress.ProfileType);
            Assert.Equal(businessNameAndAddress.ProfileType.Code, fakeBusinessNameAndAddress.ProfileType.Code);
            Assert.Equal(businessNameAndAddress.ProfileType.Text, fakeBusinessNameAndAddress.ProfileType.Text);

            Assert.NotNull(businessNameAndAddress.FileEstablishFlag);
            Assert.Equal(businessNameAndAddress.FileEstablishFlag.Code, fakeBusinessNameAndAddress.FileEstablishFlag.code);
            Assert.Equal(businessNameAndAddress.FileEstablishFlag.Text, fakeBusinessNameAndAddress.FileEstablishFlag.Text);
        }

        private void CompareSearchInputSummary(SearchBusinessResponse.IInputSummary inputSummary, Proxy.SearchBusinessResponse.NetConnectResponseProductsPremierProfileInputSummary fakeInputSummary)
        {
            Assert.Equal(inputSummary.SubscriberNumber, fakeInputSummary.SubscriberNumber);
            Assert.Equal(inputSummary.InquiryTransactionNumber, fakeInputSummary.InquiryTransactionNumber);
            Assert.Equal(inputSummary.CpuVersion, fakeInputSummary.CPUVersion);
            Assert.Equal(inputSummary.Inquiry, fakeInputSummary.Inquiry);
        }

        private void CompareSearchListOfSimilars(SearchBusinessResponse.IListOfSimilars listOfSimilars, Proxy.SearchBusinessResponse.NetConnectResponseProductsPremierProfileListOfSimilars
             fakeListOfSimilars)
        {
            Assert.Equal(listOfSimilars.Zip, fakeListOfSimilars.Zip);
            Assert.Equal(listOfSimilars.BusinessName, fakeListOfSimilars.BusinessName);
            Assert.Equal(listOfSimilars.City, fakeListOfSimilars.City);

            Assert.NotNull(listOfSimilars.DataContent);
            Assert.Equal(listOfSimilars.DataContent.BankDataIndicator, fakeListOfSimilars.DataContent.BankDataIndicator);
            Assert.Equal(listOfSimilars.DataContent.CollectionIndicator, fakeListOfSimilars.DataContent.CollectionIndicator);
            Assert.Equal(listOfSimilars.DataContent.ExecutiveSummaryIndicator, fakeListOfSimilars.DataContent.ExecutiveSummaryIndicator);
            Assert.Equal(listOfSimilars.DataContent.FileEstablishDate, fakeListOfSimilars.DataContent.FileEstablishDate);
            Assert.Equal(listOfSimilars.DataContent.GovernmentDataIndicator, fakeListOfSimilars.DataContent.GovernmentDataIndicator);
            Assert.Equal(listOfSimilars.DataContent.IndustryPremierIndicator, fakeListOfSimilars.DataContent.IndustryPremierIndicator);
            Assert.Equal(listOfSimilars.DataContent.InquiryIndicator, fakeListOfSimilars.DataContent.InquiryIndicator);
            Assert.Equal(listOfSimilars.DataContent.KeyFactsIndicator, fakeListOfSimilars.DataContent.KeyFactsIndicator);
            Assert.Equal(listOfSimilars.DataContent.NumberOfTradeLines, fakeListOfSimilars.DataContent.NumberOfTradeLines);
            Assert.Equal(listOfSimilars.DataContent.PublicRecordDataIndicator, fakeListOfSimilars.DataContent.PublicRecordDataIndicator);
            Assert.Equal(listOfSimilars.DataContent.SAndPIndicator, fakeListOfSimilars.DataContent.SAndPIndicator);
            Assert.Equal(listOfSimilars.DataContent.UccIndicator, fakeListOfSimilars.DataContent.UCCIndicator);
            Assert.NotNull(listOfSimilars.DataContent.FileEstablishFlag);
            Assert.Equal(listOfSimilars.DataContent.FileEstablishFlag.Code, fakeListOfSimilars.DataContent.FileEstablishFlag.code);
            Assert.Equal(listOfSimilars.DataContent.FileEstablishFlag.Text, fakeListOfSimilars.DataContent.FileEstablishFlag.Text);

            Assert.NotNull(listOfSimilars.BusinessAssociationType);
            Assert.Equal(listOfSimilars.BusinessAssociationType.Code, fakeListOfSimilars.BusinessAssociationType.Code);
            Assert.Equal(listOfSimilars.BusinessAssociationType.Text, fakeListOfSimilars.BusinessAssociationType.Text);
        }

        [Fact]
        public void GetBusinessSearch_ArgumentNullException()
        {
            var BusinessNameNullrequest = new GetBusinessSearchRequest
            {
                BusinessName = "",
                AlternateName = "Test",
                Street = "ABC",
                City = "Irvine",
                State = "CA",
                Zip = "12345",
                TransactionNumber = "123123",
                BisListNumber = "12345",
                AddOnsBUSP = "Test"
            };
            var CityNullrequest = new GetBusinessSearchRequest
            {
                BusinessName = "Test",
                AlternateName = "Test",
                Street = "ABC",
                City = "",
                State = "CA",
                Zip = "12345",
                TransactionNumber = "123123",
                BisListNumber = "12345",
                AddOnsBUSP = "Test"
            };
            var StreetNullrequest = new GetBusinessSearchRequest
            {
                BusinessName = "Test",
                AlternateName = "Test",
                Street = "",
                City = "Irvine",
                State = "CA",
                Zip = "12345",
                TransactionNumber = "123123",
                BisListNumber = "12345",
                AddOnsBUSP = "Test"
            };
            var StateNullrequest = new GetBusinessSearchRequest
            {
                BusinessName = "Test",
                AlternateName = "Test",
                Street = "ABC",
                City = "Irvine",
                State = "",
                Zip = "12345",
                TransactionNumber = "123123",
                BisListNumber = "12345",
                AddOnsBUSP = "Test"
            };
            var ZipNullrequest = new GetBusinessSearchRequest
            {
                BusinessName = "Test",
                AlternateName = "Test",
                Street = "ABC",
                City = "Irvine",
                State = "CA",
                Zip = "",
                TransactionNumber = "123123",
                BisListNumber = "12345",
                AddOnsBUSP = "Test"
            };
            Assert.ThrowsAsync<ArgumentNullException>(() => BusinessReportService.GetBusinessSearch("application", "12345", It.IsAny<IGetBusinessSearchRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => BusinessReportService.GetBusinessSearch("", "12345", It.IsAny<IGetBusinessSearchRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => BusinessReportService.GetBusinessSearch("", "", It.IsAny<IGetBusinessSearchRequest>()));
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentException>(() => BusinessReportService.GetBusinessSearch("application", "12345", It.IsAny<IGetBusinessSearchRequest>()));
            Assert.ThrowsAsync<ArgumentException>(() => BusinessReportService.GetBusinessSearch("application", "12345", BusinessNameNullrequest));
            Assert.ThrowsAsync<ArgumentException>(() => BusinessReportService.GetBusinessSearch("application", "12345", CityNullrequest));
            Assert.ThrowsAsync<ArgumentException>(() => BusinessReportService.GetBusinessSearch("application", "12345", StreetNullrequest));
            Assert.ThrowsAsync<ArgumentException>(() => BusinessReportService.GetBusinessSearch("application", "12345", StateNullrequest));
            Assert.ThrowsAsync<ArgumentException>(() => BusinessReportService.GetBusinessSearch("application", "12345", ZipNullrequest));
        }

        [Fact]
        public void GetDirectHitReport_ArgumentNullException()
        {
            var TransactionNumberNullrequest = new GetDirectHitReportRequest
            {
                BusinessName = "Test",
                AlternateName = "Test",
                Street = "ABC",
                City = "Irvine",
                State = "CA",
                Zip = "12345",
                AddOnsList = "Test",
                AddOnsBUSP = "Test"
            };
            Assert.ThrowsAsync<ArgumentNullException>(() => BusinessReportService.GetDirectHitReport("application", "12345", It.IsAny<IGetDirectHitReportRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => BusinessReportService.GetDirectHitReport("", "12345", It.IsAny<IGetDirectHitReportRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => BusinessReportService.GetDirectHitReport("", "", It.IsAny<IGetDirectHitReportRequest>()));
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Assert.ThrowsAsync<ArgumentNullException>(() => BusinessReportService.GetDirectHitReport("application", "", It.IsAny<IGetDirectHitReportRequest>()));
        }
    }
}