﻿namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public interface IDataContent
    {
        string BankDataIndicator { get; set; }
        string CollectionIndicator { get; set; }
        string ExecutiveSummaryIndicator { get; set; }
        string FileEstablishDate { get; set; }
        IFileEstablishFlag FileEstablishFlag { get; set; }
        string GovernmentDataIndicator { get; set; }
        string IndustryPremierIndicator { get; set; }
        string InquiryIndicator { get; set; }
        string KeyFactsIndicator { get; set; }
        string NumberOfTradeLines { get; set; }
        string PublicRecordDataIndicator { get; set; }
        string SAndPIndicator { get; set; }
        string UccIndicator { get; set; }
    }
}