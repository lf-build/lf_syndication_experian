﻿namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public interface IListOfSimilars
    {
        IBusinessAssociationType BusinessAssociationType { get; set; }
        string BusinessName { get; set; }
        string City { get; set; }
        IDataContent DataContent { get; set; }
        string ExperianFileNumber { get; set; }
        string PhoneNumber { get; set; }
        string RecordSequenceNumber { get; set; }
        string State { get; set; }
        string StreetAddress { get; set; }
        string Zip { get; set; }
    }
}