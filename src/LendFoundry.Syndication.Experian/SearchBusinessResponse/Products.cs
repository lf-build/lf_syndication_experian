﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public class Products : IProducts
    {
        public Products()
        {
        }

        public Products(Proxy.SearchBusinessResponse.Products products)
        {
            if (products?.PremierProfile != null)
                PremierProfile =
                    products.PremierProfile.Select(p => new ProductsPremierProfile(p))
                        .ToList<IProductsPremierProfile>();
        }

        [JsonConverter(typeof(InterfaceListConverter<IProductsPremierProfile,ProductsPremierProfile>))]
        public List<IProductsPremierProfile> PremierProfile { get; set; }
    }
}