﻿namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public interface IBusinessNameAndAddress
    {
        IFileEstablishFlag FileEstablishFlag { get; set; }
        string ProfileDate { get; set; }
        string ProfileTime { get; set; }
        IProfileType ProfileType { get; set; }
        string TerminalNumber { get; set; }
    }
}