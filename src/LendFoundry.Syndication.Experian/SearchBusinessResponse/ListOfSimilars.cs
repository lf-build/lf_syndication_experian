using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public class ListOfSimilars : IListOfSimilars
    {
        public ListOfSimilars()
        {
        }

        public ListOfSimilars(Proxy.SearchBusinessResponse.NetConnectResponseProductsPremierProfileListOfSimilars listOfSimilars)
        {
            if (listOfSimilars != null)
            {
                if (listOfSimilars.BusinessAssociationType != null)
                    BusinessAssociationType = new BusinessAssociationType(listOfSimilars.BusinessAssociationType);
                BusinessName = listOfSimilars.BusinessName;
                City = listOfSimilars.City;
                if (listOfSimilars.DataContent != null)
                    DataContent = new DataContent(listOfSimilars.DataContent);
                ExperianFileNumber = listOfSimilars.ExperianFileNumber;
                PhoneNumber = listOfSimilars.PhoneNumber;
                RecordSequenceNumber = listOfSimilars.RecordSequenceNumber;
                State = listOfSimilars.State;
                StreetAddress = listOfSimilars.StreetAddress;
                Zip = listOfSimilars.Zip;
            }
        }

        public string ExperianFileNumber { get; set; }
        public string RecordSequenceNumber { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IBusinessAssociationType,BusinessAssociationType>))]
        public IBusinessAssociationType BusinessAssociationType { get; set; }
        public string BusinessName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumber { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDataContent,DataContent>))]
        public IDataContent DataContent { get; set; }
    }
}