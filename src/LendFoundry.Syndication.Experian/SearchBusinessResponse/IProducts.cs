﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public interface IProducts
    {
        List<IProductsPremierProfile> PremierProfile { get; set; }
    }
}