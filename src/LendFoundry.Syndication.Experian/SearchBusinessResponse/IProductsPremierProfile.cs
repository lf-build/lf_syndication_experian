﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public interface IProductsPremierProfile
    {
        IBillingIndicator BillingIndicator { get; set; }
        List<IBusinessNameAndAddress> BusinessNameAndAddress { get; set; }
        List<IInputSummary> InputSummary { get; set; }
        List<IListOfSimilars> ListOfSimilars { get; set; }
    }
}