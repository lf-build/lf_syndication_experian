﻿namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public interface IBillingIndicator
    {
        string Code { get; set; }
        string Text { get; set; }
    }
}