using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public class DataContent : IDataContent
    {
        public DataContent()
        {
        }

        public DataContent(Proxy.SearchBusinessResponse.NetConnectResponseProductsPremierProfileListOfSimilarsDataContent dataContent)
        {
            if (dataContent != null)
            {
                BankDataIndicator = dataContent.BankDataIndicator;
                CollectionIndicator = dataContent.CollectionIndicator;
                ExecutiveSummaryIndicator = dataContent.ExecutiveSummaryIndicator;
                FileEstablishDate = dataContent.FileEstablishDate;
                if (dataContent.FileEstablishFlag != null)
                    FileEstablishFlag = new FileEstablishFlag(dataContent.FileEstablishFlag);
                GovernmentDataIndicator = dataContent.GovernmentDataIndicator;
                IndustryPremierIndicator = dataContent.IndustryPremierIndicator;
                InquiryIndicator = dataContent.InquiryIndicator;
                KeyFactsIndicator = dataContent.KeyFactsIndicator;
                NumberOfTradeLines = dataContent.NumberOfTradeLines;
                PublicRecordDataIndicator = dataContent.PublicRecordDataIndicator;
                SAndPIndicator = dataContent.SAndPIndicator;
                UccIndicator = dataContent.UCCIndicator;
            }
        }

        public string NumberOfTradeLines { get; set; }
        public string FileEstablishDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IFileEstablishFlag,FileEstablishFlag>))]
        public IFileEstablishFlag FileEstablishFlag { get; set; }
        public string SAndPIndicator { get; set; }
        public string KeyFactsIndicator { get; set; }
        public string InquiryIndicator { get; set; }
        public string PublicRecordDataIndicator { get; set; }
        public string BankDataIndicator { get; set; }
        public string GovernmentDataIndicator { get; set; }
        public string CollectionIndicator { get; set; }
        public string ExecutiveSummaryIndicator { get; set; }
        public string IndustryPremierIndicator { get; set; }

        public string UccIndicator { get; set; }
    }
}