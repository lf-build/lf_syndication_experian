﻿namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public class BusinessAssociationType : IBusinessAssociationType
    {
        public BusinessAssociationType()
        {
        }

        public BusinessAssociationType(Proxy.SearchBusinessResponse.BusinessAssociationType businessAssociationType)
        {
            Code = businessAssociationType.Code;
            Text = businessAssociationType.Text;
        }

        public string Code { get; set; }
        public string Text { get; set; }
    }
}