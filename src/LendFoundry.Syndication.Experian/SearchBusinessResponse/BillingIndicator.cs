﻿namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public class BillingIndicator : IBillingIndicator
    {
        public BillingIndicator()
        {
        }

        public BillingIndicator(Proxy.SearchBusinessResponse.BillingIndicator billingIndicator)
        {
            if (billingIndicator != null)
            {
                Code = billingIndicator.Code;
                Text = billingIndicator.Text;
            }
        }

        public string Code { get; set; }
        public string Text { get; set; }
    }
}