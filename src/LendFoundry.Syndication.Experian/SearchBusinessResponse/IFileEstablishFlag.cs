﻿namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public interface IFileEstablishFlag
    {
        string Code { get; set; }

        string Text { get; set; }
    }
}