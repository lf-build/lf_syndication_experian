using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Experian.Proxy.SearchBusinessResponse;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public class ProductsPremierProfile : IProductsPremierProfile
    {
        public ProductsPremierProfile()
        {
        }

        public ProductsPremierProfile(NetConnectResponseProductsPremierProfile productsPremierProfile)
        {
            if (productsPremierProfile != null)
            {
                if (productsPremierProfile.BillingIndicator != null)
                    BillingIndicator = new BillingIndicator(productsPremierProfile.BillingIndicator);
                if (productsPremierProfile.BusinessNameAndAddress != null)
                    BusinessNameAndAddress =
                        productsPremierProfile.BusinessNameAndAddress.Select(
                            p => new BusinessNameAndAddress(p))
                            .ToList<IBusinessNameAndAddress>();
                if (productsPremierProfile.InputSummary != null)
                    InputSummary =
                        productsPremierProfile.InputSummary.Select(p => new InputSummary(p))
                            .ToList<IInputSummary>();

                if (productsPremierProfile.ListOfSimilars != null)
                    ListOfSimilars =
                        productsPremierProfile.ListOfSimilars.Select(p => new ListOfSimilars(p))
                            .ToList<IListOfSimilars>();
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IInputSummary, InputSummary>))]
        public List<IInputSummary> InputSummary { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBusinessNameAndAddress, BusinessNameAndAddress>))]
        public List<IBusinessNameAndAddress> BusinessNameAndAddress { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IListOfSimilars, ListOfSimilars>))]
        public List<IListOfSimilars> ListOfSimilars { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IBillingIndicator, BillingIndicator>))]
        public IBillingIndicator BillingIndicator { get; set; }
    }
}