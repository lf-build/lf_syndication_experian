namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public class FileEstablishFlag : IFileEstablishFlag
    {
        public FileEstablishFlag()
        {
        }

        public FileEstablishFlag(Proxy.SearchBusinessResponse.FileEstablishFlag fileEstablishFlag)
        {
            if (fileEstablishFlag != null)
            {
                Code = fileEstablishFlag.code;
                Text = fileEstablishFlag.Text;
            }
        }

        public string Code { get; set; }
        public string Text { get; set; }
    }
}