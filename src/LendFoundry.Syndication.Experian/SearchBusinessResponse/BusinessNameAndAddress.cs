using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Experian.Proxy.SearchBusinessResponse;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public class BusinessNameAndAddress : IBusinessNameAndAddress
    {
        public BusinessNameAndAddress()
        {
        }

        public BusinessNameAndAddress(NetConnectResponseProductsPremierProfileBusinessNameAndAddress businessNameAndAddress)
        {
            if (businessNameAndAddress != null)
            {
                if (businessNameAndAddress.FileEstablishFlag != null)
                    FileEstablishFlag = new FileEstablishFlag(businessNameAndAddress.FileEstablishFlag);
                ProfileDate = businessNameAndAddress.ProfileDate;
                ProfileTime = businessNameAndAddress.ProfileTime;
                if (businessNameAndAddress.ProfileType != null)
                    ProfileType = new ProfileType(businessNameAndAddress.ProfileType);
                TerminalNumber = businessNameAndAddress.TerminalNumber;
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IProfileType,ProfileType>))]
        public IProfileType ProfileType { get; set; }
        public string ProfileDate { get; set; }
        public string ProfileTime { get; set; }
        public string TerminalNumber { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IFileEstablishFlag,FileEstablishFlag>))]
        public IFileEstablishFlag FileEstablishFlag { get; set; }
    }
}