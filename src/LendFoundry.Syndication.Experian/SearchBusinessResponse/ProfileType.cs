﻿namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public class ProfileType : IProfileType
    {
        public ProfileType()
        {
        }

        public ProfileType(Proxy.SearchBusinessResponse.ProfileType profileType)
        {
            if (profileType != null)
            {
                Code = profileType.Code;
                Text = profileType.Text;
            }
        }

        public string Code { get; set; }
        public string Text { get; set; }
    }
}