﻿namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public interface IProfileType
    {
        string Code { get; set; }
        string Text { get; set; }
    }
}