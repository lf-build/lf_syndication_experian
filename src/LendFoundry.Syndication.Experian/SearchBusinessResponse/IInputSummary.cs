﻿namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public interface IInputSummary
    {
        string CpuVersion { get; set; }
        string Inquiry { get; set; }
        string InquiryTransactionNumber { get; set; }
        string SubscriberNumber { get; set; }
    }
}