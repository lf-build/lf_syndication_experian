namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public class InputSummary : IInputSummary
    {
        public InputSummary()
        {
        }

        public InputSummary(Proxy.SearchBusinessResponse.NetConnectResponseProductsPremierProfileInputSummary inputSummary)
        {
            if (inputSummary != null)
            {
                CpuVersion = inputSummary.CPUVersion;
                Inquiry = inputSummary.Inquiry;
                InquiryTransactionNumber = inputSummary.InquiryTransactionNumber;
                SubscriberNumber = inputSummary.SubscriberNumber;
            }
        }

        public string SubscriberNumber { get; set; }
        public string InquiryTransactionNumber { get; set; }

        public string CpuVersion { get; set; }

        public string Inquiry { get; set; }
    }
}