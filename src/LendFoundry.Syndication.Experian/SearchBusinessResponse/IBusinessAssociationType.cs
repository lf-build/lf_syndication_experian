﻿namespace LendFoundry.Syndication.Experian.SearchBusinessResponse
{
    public interface IBusinessAssociationType
    {
        string Code { get; set; }
        string Text { get; set; }
    }
}