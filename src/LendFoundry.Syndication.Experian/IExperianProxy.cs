﻿using System.Threading.Tasks;
using RestSharp;

namespace LendFoundry.Syndication.Experian.Proxy
{
    public interface IExperianProxy
    {
        string EcalsTransactionCheck();
        Task<TResponse> ExecuteRequest<TResponse>(string url, Method method, string request);
        string Hash(string password);
        string RequestNewPassword();
        string ResetPassword(string password);
    }
}