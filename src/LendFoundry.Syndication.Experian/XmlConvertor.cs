﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Experian
{
    //TODO: Remove it.
    public static class XmlConvertor
    {
        public static string Serialize<T>(T obj, Encoding encoding)
        {
            if (obj != null)
            {
                Type objType = obj.GetType();
                XmlSerializer serializer = new XmlSerializer(objType);
                string xml = string.Empty;
                using (var stringWriter = new ExtentedStringWriter(encoding))
                {
                    using (XmlWriter writer = XmlWriter.Create(stringWriter))
                    {
                        serializer.Serialize(writer, obj);
                        xml = stringWriter.ToString();
                    }
                }
                return xml;
            }
            return string.Empty;
        }
        public static T Deserialize<T>(string xmlString)
        {
            Type objType = typeof(T);
            XmlSerializer serializer = new XmlSerializer(objType);
            var objectResult = (T)Activator.CreateInstance(objType);
            using (var stringReader = new StringReader(xmlString))
            {
                using (XmlReader reader = XmlReader.Create(stringReader))
                {
                    objectResult = (T)serializer.Deserialize(reader);
                }
            }
            return objectResult;
        }
    }
}
