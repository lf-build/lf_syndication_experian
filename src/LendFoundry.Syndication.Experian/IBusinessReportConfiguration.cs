﻿namespace LendFoundry.Syndication.Experian
{
    public interface IBusinessReportConfiguration
    {
     
        string NetConnectSubscriber { get; set; }

        string NetConnectPassword { get; set; }

        string NetConnectPasswordHash { get; set; }
        
        string Eai { get; set; }

        string DbHost { get; set; }

        string ReferenceId { get; set; }

        string Preamble { get; set; }

        string OpInitials { get; set; }

        string SubCode { get; set; }

        string ArfVersion { get; set; }

        string VendorNumber { get; set; }

        string VendorVersion { get; set; }

        string BusinessVerbose { get; set; }        
         string AddOnsList { get; set; }
         string AddOnsBUSP { get; set; }
    }
}