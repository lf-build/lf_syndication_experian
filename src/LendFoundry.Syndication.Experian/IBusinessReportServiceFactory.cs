﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Experian
{
    public interface IBusinessReportServiceFactory
    {
        IBusinessReportService Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}
