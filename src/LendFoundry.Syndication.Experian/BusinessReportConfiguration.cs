﻿namespace LendFoundry.Syndication.Experian
{
    public class BusinessReportConfiguration : IBusinessReportConfiguration
    {       
        public string NetConnectSubscriber { get; set; }

        public string NetConnectPassword { get; set; }

        public string NetConnectPasswordHash { get; set; }
     
        public string Eai { get; set; }

        public string DbHost { get; set; }

        public string ReferenceId { get; set; }

        public string Preamble { get; set; }

        public string OpInitials { get; set; }

        public string SubCode { get; set; }

        public string ArfVersion { get; set; }

        public string VendorNumber { get; set; }

        public string VendorVersion { get; set; }

        public string BusinessVerbose { get; set; }
        public bool UseProxy { get; set; }
        public string ProxyUrl { get; set; }

        // list of similars fields
        public string AddOnsList { get; set; }
        public string AddOnsBUSP { get; set; }

    }
}