﻿using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian
{
    public interface IPersonalReportService
    {
        Task<IPersonalReportResponse> GetPersonalReportResponse(string entityType, string entityId, IPersonalReportRequest personalReportRequest, bool isHardpull=false);

        Task<string> ResetPassword();
    }
}