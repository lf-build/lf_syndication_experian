﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.Experian.Events
{
    public class ExperianDirecthitReportFetched : SyndicationCalledEvent
    {
    }
}