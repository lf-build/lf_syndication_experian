﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.Experian.Events
{
    public class ExperianBusinessReportFetched : SyndicationCalledEvent
    {
        public object ResponseObject { get; set; }
    }
}