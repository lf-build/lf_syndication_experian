﻿using LendFoundry.Syndication.Experian.PersonalReportResponse;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian
{
    public interface IPersonalReportConfiguration
    {
      
        string NetConnectSubscriber { get; set; }

        string NetConnectPassword { get; set; }

        string NetConnectPasswordHash { get; set; }
      
        string Eai { get; set; }

        string DbHost { get; set; }

        string ReferenceId { get; set; }

        string Preamble { get; set; }

        string OpInitials { get; set; }

        string SubCode { get; set; }

        string ArfVersion { get; set; }
        string Verbose { get; set; }
        string Demographics { get; set; }
        string Y2K { get; set; }
        string Segment130 { get; set; }
        string VendorNumber { get; set; }

        string VendorVersion { get; set; }
        string AccountType { get; set; }
        RiskModelIndicators RiskModelIndicators { get; set; }
        string CustomRRDashKeyword { get; set; }
        string HardPullSubCode { get; set; }
        string DirectCheck { get; set; }
    }
}