﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian
{
    public class ExperianConfiguration : IExperianConfiguration , IDependencyConfiguration
    {
        [JsonConverter(typeof(InterfaceConverter<IBusinessReportConfiguration, BusinessReportConfiguration>))]
        public IBusinessReportConfiguration businessReportConfiguration { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPersonalReportConfiguration, PersonalReportConfiguration>))]
        public IPersonalReportConfiguration personalReportConfiguration { get; set; }

        public string PasswordResetURL { get; set; }

        public string EcalsTransactionUrl { get; set; }

        public string ExperianHostName { get; set; }
        public bool RequiredCertificateVerification { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}