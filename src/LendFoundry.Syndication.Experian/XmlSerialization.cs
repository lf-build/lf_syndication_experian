﻿using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Experian
{
    internal sealed class ExtentedStringWriter : StringWriter
    {
        public ExtentedStringWriter(Encoding desiredEncoding)
        {
            Encoding = desiredEncoding;
        }

        public override Encoding Encoding { get; }
    }

    public static class XmlSerialization
    {
        public static string Serialize<T>(T @object)
        {
            string xmlContent;
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (var sww = new ExtentedStringWriter(Encoding.UTF8))
            using (var writer = XmlWriter.Create(sww, new XmlWriterSettings() { NamespaceHandling = NamespaceHandling.OmitDuplicates, OmitXmlDeclaration = true }))
            {
                var namespaces = new XmlSerializerNamespaces();
                namespaces.Add(string.Empty, string.Empty);
                xmlSerializer.Serialize(writer, @object, namespaces);
                xmlContent = sww.ToString();
            }
            var doc = XDocument.Parse(xmlContent);
            doc.Descendants().Attributes().Where(a => a.IsNamespaceDeclaration).Remove();
            var xmlWithoutNamespaces = doc.ToString();
            return xmlWithoutNamespaces;
        }

        public static T Deserialize<T>(string xmlString)
        {
            var serializer = new XmlSerializer(typeof(T));
            T objectResult;
            using (var stringReader = new StringReader(xmlString))
            {
                using (var reader = XmlReader.Create(stringReader))
                {
                    objectResult = (T)serializer.Deserialize(reader);
                }
            }
            return objectResult;
        }
    }
}