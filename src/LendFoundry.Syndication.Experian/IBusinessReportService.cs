﻿using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian
{
    public interface IBusinessReportService
    {
        Task<ISearchBusinessResponse> GetBusinessSearch(string entityType, string entityId, IBusinessSearchRequest searchRequest);

        Task<IBusinessReportResponse> GetBusinessReport(string entityType, string entityId, IBusinessReportRequest reportRequest);

        Task<IDirectHitReportResponse> GetDirectHitReport(string entityType, string entityId, IDirectHitReportRequest directhitRequest);

        Task<string> ResetPassword();
    }
}