using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.Request
{
    public class GetPersonalReportRequest : IPersonalReportRequest
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Middlename { get; set; }
        public string Gen { get; set; }
        public string Ssn { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmploymentDetail, EmploymentDetail>))]
        public IEmploymentDetail Employment { get; set; }

        public string PhoneNumber { get; set; }
        public string PhoneType { get; set; }
        public string Dob { get; set; }
        public string OwnerId { get; set; }
    }
}