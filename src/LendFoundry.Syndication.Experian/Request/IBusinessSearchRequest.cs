﻿namespace LendFoundry.Syndication.Experian.Request
{
    public interface IBusinessSearchRequest
    {
        string BusinessName { get; set; }

        string AlternateName { get; set; }

        string Street { get; set; }

        string City { get; set; }

        string State { get; set; }

        string Zip { get; set; }

      
        // list of similars fields

        string TransactionNumber { get; set; }

        string BisListNumber { get; set; }

        string AddOnsBUSP { get; set; }
    }
}