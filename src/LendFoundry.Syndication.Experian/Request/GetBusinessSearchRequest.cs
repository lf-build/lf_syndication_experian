namespace LendFoundry.Syndication.Experian.Request
{
    public class GetBusinessSearchRequest : IBusinessSearchRequest
    {
        public string BusinessName { get; set; }
        public string AlternateName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string TransactionNumber { get; set; }
        public string BisListNumber { get; set; }
        public string AddOnsBUSP { get; set; }
    }
}