﻿namespace LendFoundry.Syndication.Experian.Request
{
    public class GetDirectHitReportRequest : IDirectHitReportRequest
    {
        public string BusinessName { get; set; }
        public string AlternateName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string AddOnsList { get; set; }
        public string AddOnsBUSP { get; set; }
    }
}