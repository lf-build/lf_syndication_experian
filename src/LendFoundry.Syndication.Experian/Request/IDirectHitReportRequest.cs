﻿namespace LendFoundry.Syndication.Experian.Request
{
    public interface IDirectHitReportRequest
    {
        string BusinessName { get; set; }

        string AlternateName { get; set; }

        string Street { get; set; }

        string City { get; set; }

        string State { get; set; }

        string Zip { get; set; }

        string AddOnsList { get; set; }

        // list of similars fields

        string AddOnsBUSP { get; set; }
    }
}