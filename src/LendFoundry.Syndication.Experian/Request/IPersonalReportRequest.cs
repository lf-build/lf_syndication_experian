﻿namespace LendFoundry.Syndication.Experian.Request
{
    public interface IPersonalReportRequest
    {
        string Firstname { get; set; }
        string Lastname { get; set; }
        string Middlename { get; set; }
        string Gen { get; set; }

        string Ssn { get; set; }
        string Street { get; set; }
        string City { get; set; }

        string State { get; set; }

        string Zip { get; set; }

        IEmploymentDetail Employment { get; set; }
        string PhoneNumber { get; set; }
        string PhoneType { get; set; }

        string Dob { get; set; }
        string OwnerId { get; set; }
    }
}