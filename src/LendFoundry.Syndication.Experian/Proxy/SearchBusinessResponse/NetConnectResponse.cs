﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Experian.Proxy.SearchBusinessResponse
{
    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/NetConnectResponse")]
    [XmlRoot(Namespace = "http://www.experian.com/NetConnectResponse", IsNullable = false)]
    public partial class NetConnectResponse
    {
        public int Id { get; set; }
        public List<string> Message { get; set; }
        public bool IsSucceeded { get; set; }
        public string CompletionCode { get; set; }
        public string ReferenceId { get; set; }
        public string TransactionId { get; set; }

        [XmlElement(Namespace = "http://www.experian.com/ARFResponse")]
        public Products Products { get; set; }

        public string ErrorMessage { get; set; }
        public string ErrorTag { get; set; }
    }

    [XmlRoot(Namespace = "http://www.experian.com/ARFResponse", IsNullable = false)]
    public partial class Products
    {
        [XmlElement("PremierProfile")]
        public List<NetConnectResponseProductsPremierProfile> PremierProfile { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfile
    {
        [XmlElement("InputSummary")]
        public NetConnectResponseProductsPremierProfileInputSummary[] InputSummary { get; set; }

        [XmlElement("BusinessNameAndAddress")]
        public NetConnectResponseProductsPremierProfileBusinessNameAndAddress[] BusinessNameAndAddress { get; set; }

        [XmlElement("ListOfSimilars")]
        public List<NetConnectResponseProductsPremierProfileListOfSimilars> ListOfSimilars { get; set; }

        public BillingIndicator BillingIndicator { get; set; }
    }

    [XmlRoot(ElementName = "BillingIndicator", Namespace = "http://www.experian.com/ARFResponse")]
    public class BillingIndicator
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileInputSummary
    {
        public string SubscriberNumber { get; set; }
        public string InquiryTransactionNumber { get; set; }
        public string CPUVersion { get; set; }
        public string Inquiry { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileBusinessNameAndAddress
    {
        public ProfileType ProfileType { get; set; }
        public string ProfileDate { get; set; }
        public string ProfileTime { get; set; }
        public string TerminalNumber { get; set; }
        public FileEstablishFlag FileEstablishFlag { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class FileEstablishFlag
    {
        private string codeField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        // TODO: Manually added as per sample response.
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "ProfileType", Namespace = "http://www.experian.com/ARFResponse")]
    public class ProfileType
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileListOfSimilars
    {
        public string ExperianFileNumber { get; set; }
        public string RecordSequenceNumber { get; set; }
        public BusinessAssociationType BusinessAssociationType { get; set; }
        public string BusinessName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumber { get; set; }
        public NetConnectResponseProductsPremierProfileListOfSimilarsDataContent DataContent { get; set; }
    }

    [XmlRoot(ElementName = "BusinessAssociationType", Namespace = "http://www.experian.com/ARFResponse")]
    public class BusinessAssociationType
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileListOfSimilarsDataContent
    {
        public string NumberOfTradeLines { get; set; }
        public string FileEstablishDate { get; set; }
        public FileEstablishFlag FileEstablishFlag { get; set; }
        public string SAndPIndicator { get; set; }
        public string KeyFactsIndicator { get; set; }
        public string InquiryIndicator { get; set; }
        public string PublicRecordDataIndicator { get; set; }
        public string BankDataIndicator { get; set; }
        public string GovernmentDataIndicator { get; set; }
        public string CollectionIndicator { get; set; }
        public string ExecutiveSummaryIndicator { get; set; }
        public string IndustryPremierIndicator { get; set; }
        public string UCCIndicator { get; set; }
    }
}