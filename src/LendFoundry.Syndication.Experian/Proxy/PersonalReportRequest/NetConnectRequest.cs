﻿using System.Xml.Linq;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Experian.Proxy.PersonalReportRequest
{
    [XmlRoot(ElementName = "Subscriber", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class Subscriber
    {
        [XmlElement(ElementName = "Preamble", Namespace = "http://www.experian.com/WebDelivery")]
        public string Preamble { get; set; }

        [XmlElement(ElementName = "OpInitials", Namespace = "http://www.experian.com/WebDelivery")]
        public string OpInitials { get; set; }

        [XmlElement(ElementName = "SubCode", Namespace = "http://www.experian.com/WebDelivery")]
        public string SubCode { get; set; }
    }

    [XmlRoot(ElementName = "Name", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class Name
    {
        [XmlElement(ElementName = "Surname", Namespace = "http://www.experian.com/WebDelivery")]
        public string Surname { get; set; }

        [XmlElement(ElementName = "First", Namespace = "http://www.experian.com/WebDelivery")]
        public string First { get; set; }

        [XmlElement(ElementName = "Middle", Namespace = "http://www.experian.com/WebDelivery")]
        public string Middle { get; set; }

        [XmlElement(ElementName = "Gen", Namespace = "http://www.experian.com/WebDelivery")]
        public string Gen { get; set; }
    }

    [XmlRoot(ElementName = "Phone", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class Phone
    {
        [XmlElement(ElementName = "Number", Namespace = "http://www.experian.com/WebDelivery")]
        public string Number { get; set; }

        [XmlElement(ElementName = "Type", Namespace = "http://www.experian.com/WebDelivery")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "AddOns", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class AddOns
    {
        [XmlElement(ElementName = "RiskModels", Namespace = "http://www.experian.com/WebDelivery")]
        public RiskModels RiskModels { get; set; }

        [XmlElement(ElementName = "CustomRRDashKeyword", Namespace = "http://www.experian.com/WebDelivery")]
        public string CustomRRDashKeyword { get; set; }

        [XmlElement(ElementName = "DirectCheck", Namespace = "http://www.experian.com/WebDelivery")]
        public string DirectCheck { get; set; }

        [XmlElement(ElementName = "ProfileSummary", Namespace = "http://www.experian.com/WebDelivery")]
        public string ProfileSummary { get; set; }

        [XmlElement(ElementName = "DemographicBand", Namespace = "http://www.experian.com/WebDelivery")]
        public DemographicBand DemographicBand { get; set; }
    }

    [XmlRoot(ElementName = "DemographicBand", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class DemographicBand
    {
        [XmlElement(ElementName = "DemographicsAll", Namespace = "http://www.experian.com/WebDelivery")]
        public string DemographicsAll { get; set; }

        [XmlElement(ElementName = "DemographicPhone", Namespace = "http://www.experian.com/WebDelivery")]
        public string DemographicPhone { get; set; }

        [XmlElement(ElementName = "DemographicDriverLicense", Namespace = "http://www.experian.com/WebDelivery")]
        public string DemographicDriverLicense { get; set; }

        [XmlElement(ElementName = "DemographicGeoCode", Namespace = "http://www.experian.com/WebDelivery")]
        public string DemographicGeoCode { get; set; }
    }

    [XmlRoot(ElementName = "RiskModels", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class RiskModels
    {
        [XmlElement(ElementName = "FICO8", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICO8 { get; set; }
        [XmlElement(ElementName = "VantageScore3", Namespace = "http://www.experian.com/WebDelivery")]
        public string VantageScore3 { get; set; }
        [XmlElement(ElementName = "FraudShield", Namespace = "http://www.experian.com/WebDelivery")]
        public string FraudShield { get; set; }
        [XmlElement(ElementName = "BankruptcyPLUS", Namespace = "http://www.experian.com/WebDelivery")]
        public string BankruptcyPLUS { get; set; }
        [XmlElement(ElementName = "TotalDebtToIncome", Namespace = "http://www.experian.com/WebDelivery")]
        public string TotalDebtToIncome { get; set; }
        [XmlElement(ElementName = "IncomeInsight", Namespace = "http://www.experian.com/WebDelivery")]
        public string IncomeInsight { get; set; }
        [XmlElement(ElementName = "AssetInsight", Namespace = "http://www.experian.com/WebDelivery")]
        public string AssetInsight { get; set; }
        [XmlElement(ElementName = "BankruptcyPLUSRescaled", Namespace = "http://www.experian.com/WebDelivery")]
        public string BankruptcyPLUSRescaled { get; set; }
        [XmlElement(ElementName = "BustOutScore", Namespace = "http://www.experian.com/WebDelivery")]
        public string BustOutScore { get; set; }
        [XmlElement(ElementName = "CollectScore", Namespace = "http://www.experian.com/WebDelivery")]
        public string CollectScore { get; set; }

        [XmlElement(ElementName = "FirstMtgDebtToIncome", Namespace = "http://www.experian.com/WebDelivery")]
        public string FirstMtgDebtToIncome { get; set; }
        [XmlElement(ElementName = "TotalMtgDebtToIncome", Namespace = "http://www.experian.com/WebDelivery")]
        public string TotalMtgDebtToIncome { get; set; }
        [XmlElement(ElementName = "TotalDebtToIncomeW2", Namespace = "http://www.experian.com/WebDelivery")]
        public string TotalDebtToIncomeW2 { get; set; }
        [XmlElement(ElementName = "TotalDebtToIncomeJointW2", Namespace = "http://www.experian.com/WebDelivery")]
        public string TotalDebtToIncomeJointW2 { get; set; }
        [XmlElement(ElementName = "FirstMtgDebtToIncomeW2", Namespace = "http://www.experian.com/WebDelivery")]
        public string FirstMtgDebtToIncomeW2 { get; set; }
        [XmlElement(ElementName = "TotalMtgDebtToIncomeW2", Namespace = "http://www.experian.com/WebDelivery")]
        public string TotalMtgDebtToIncomeW2 { get; set; }
        [XmlElement(ElementName = "FICOAdvanced", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOAdvanced { get; set; }
        [XmlElement(ElementName = "FICOAdvanced2", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOAdvanced2 { get; set; }
        [XmlElement(ElementName = "FICOAuto2", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOAuto2 { get; set; }
        [XmlElement(ElementName = "FICOAuto3", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOAuto3 { get; set; }
        [XmlElement(ElementName = "FICO8Auto", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICO8Auto { get; set; }
        [XmlElement(ElementName = "FICO9Auto", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICO9Auto { get; set; }
        [XmlElement(ElementName = "FICOBank2", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOBank2 { get; set; }
        [XmlElement(ElementName = "FICOBank3", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOBank3 { get; set; }
        [XmlElement(ElementName = "FICO8Bank", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICO8Bank { get; set; }
        [XmlElement(ElementName = "FICO9Bank", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICO9Bank { get; set; }
        [XmlElement(ElementName = "FICOBankruptcy", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOBankruptcy { get; set; }
        [XmlElement(ElementName = "FICOInstall2", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOInstall2 { get; set; }
        [XmlElement(ElementName = "FICOInstall3", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOInstall3 { get; set; }
        [XmlElement(ElementName = "FICOInsHomeownerF35", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOInsHomeownerF35 { get; set; }
        [XmlElement(ElementName = "FICOInsHomeownerF4", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOInsHomeownerF4 { get; set; }
        [XmlElement(ElementName = "FICOInsPrefAutoGT", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOInsPrefAutoGT { get; set; }
        [XmlElement(ElementName = "FICOInsStandardAutoGT", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOInsStandardAutoGT { get; set; }
        [XmlElement(ElementName = "FICOInsStandardAutoMin", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOInsStandardAutoMin { get; set; }
        [XmlElement(ElementName = "FICOInsNonstandardAuto", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOInsNonstandardAuto { get; set; }
        [XmlElement(ElementName = "FICOFinance2", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOFinance2 { get; set; }
        [XmlElement(ElementName = "FICOFinance3", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOFinance3 { get; set; }
        [XmlElement(ElementName = "FICO2", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICO2 { get; set; }
        [XmlElement(ElementName = "FICO3", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICO3 { get; set; }
        [XmlElement(ElementName = "FICO9", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICO9 { get; set; }
        [XmlElement(ElementName = "IncomeInsightW2", Namespace = "http://www.experian.com/WebDelivery")]
        public string IncomeInsightW2 { get; set; }
        [XmlElement(ElementName = "NeverPay", Namespace = "http://www.experian.com/WebDelivery")]
        public string NeverPay { get; set; }
        [XmlElement(ElementName = "RecoveryScoreBank", Namespace = "http://www.experian.com/WebDelivery")]
        public string RecoveryScoreBank { get; set; }
        [XmlElement(ElementName = "RecoveryScoreRetail", Namespace = "http://www.experian.com/WebDelivery")]
        public string RecoveryScoreRetail { get; set; }
        [XmlElement(ElementName = "ROI1Digit", Namespace = "http://www.experian.com/WebDelivery")]
        public string ROI1Digit { get; set; }
        [XmlElement(ElementName = "ROI3Digit", Namespace = "http://www.experian.com/WebDelivery")]
        public string ROI3Digit { get; set; }
        [XmlElement(ElementName = "ScorexPLUS", Namespace = "http://www.experian.com/WebDelivery")]
        public string ScorexPLUS { get; set; }
        [XmlElement(ElementName = "ScorexPLUS2", Namespace = "http://www.experian.com/WebDelivery")]
        public string ScorexPLUS2 { get; set; }
        [XmlElement(ElementName = "TEC", Namespace = "http://www.experian.com/WebDelivery")]
        public string TEC { get; set; }
        [XmlElement(ElementName = "TeleRisk", Namespace = "http://www.experian.com/WebDelivery")]
        public string TeleRisk { get; set; }
        [XmlElement(ElementName = "VantageScore", Namespace = "http://www.experian.com/WebDelivery")]
        public string VantageScore { get; set; }
        [XmlElement(ElementName = "VantageScore2", Namespace = "http://www.experian.com/WebDelivery")]
        public string VantageScore2 { get; set; }
        [XmlElement(ElementName = "VantageScore3Positive", Namespace = "http://www.experian.com/WebDelivery")]
        public string VantageScore3Positive { get; set; }
        [XmlElement(ElementName = "Auto", Namespace = "http://www.experian.com/WebDelivery")]
        public string Auto { get; set; }
        [XmlElement(ElementName = "BankruptcyWatch", Namespace = "http://www.experian.com/WebDelivery")]
        public string BankruptcyWatch { get; set; }
        [XmlElement(ElementName = "CreditUnion", Namespace = "http://www.experian.com/WebDelivery")]
        public string CreditUnion { get; set; }
        [XmlElement(ElementName = "FICOAuto", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOAuto { get; set; }
        [XmlElement(ElementName = "FICOBank", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOBank { get; set; }
        [XmlElement(ElementName = "Bankruptcy", Namespace = "http://www.experian.com/WebDelivery")]
        public string Bankruptcy { get; set; }
        [XmlElement(ElementName = "FICOInstall", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOInstall { get; set; }
        [XmlElement(ElementName = "FICOFinance", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICOFinance { get; set; }
        [XmlElement(ElementName = "FICO", Namespace = "http://www.experian.com/WebDelivery")]
        public string FICO { get; set; }
        [XmlElement(ElementName = "NationalEquiv", Namespace = "http://www.experian.com/WebDelivery")]
        public string NationalEquiv { get; set; }
        [XmlElement(ElementName = "NationalRisk", Namespace = "http://www.experian.com/WebDelivery")]
        public string NationalRisk { get; set; }
        [XmlElement(ElementName = "Retail", Namespace = "http://www.experian.com/WebDelivery")]
        public string Retail { get; set; }

    }

    [XmlRoot(ElementName = "AccountType", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class AccountType
    {
        [XmlElement(ElementName = "Type", Namespace = "http://www.experian.com/WebDelivery")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "Options", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class Options
    {
        [XmlElement(ElementName = "ReferenceNumber", Namespace = "http://www.experian.com/WebDelivery")]
        public string ReferenceNumber { get; set; }
    }

    [XmlRoot(ElementName = "CurrentAddress", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class CurrentAddress
    {
        [XmlElement(ElementName = "Street", Namespace = "http://www.experian.com/WebDelivery")]
        public string Street { get; set; }

        [XmlElement(ElementName = "City", Namespace = "http://www.experian.com/WebDelivery")]
        public string City { get; set; }

        [XmlElement(ElementName = "State", Namespace = "http://www.experian.com/WebDelivery")]
        public string State { get; set; }

        [XmlElement(ElementName = "Zip", Namespace = "http://www.experian.com/WebDelivery")]
        public string Zip { get; set; }
    }

    [XmlRoot(ElementName = "Employment", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class Employment
    {
        [XmlElement(ElementName = "Company", Namespace = "http://www.experian.com/WebDelivery")]
        public string Company { get; set; }

        [XmlElement(ElementName = "Address", Namespace = "http://www.experian.com/WebDelivery")]
        public string Address { get; set; }

        [XmlElement(ElementName = "City", Namespace = "http://www.experian.com/WebDelivery")]
        public string City { get; set; }

        [XmlElement(ElementName = "State", Namespace = "http://www.experian.com/WebDelivery")]
        public string State { get; set; }

        [XmlElement(ElementName = "Zip", Namespace = "http://www.experian.com/WebDelivery")]
        public string Zip { get; set; }
    }

    [XmlRoot(ElementName = "PrimaryApplicant", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class PrimaryApplicant
    {
        [XmlElement(ElementName = "Name", Namespace = "http://www.experian.com/WebDelivery")]
        public Name Name { get; set; }

        [XmlElement(ElementName = "SSN", Namespace = "http://www.experian.com/WebDelivery")]
        public string SSN { get; set; }

        [XmlElement(ElementName = "CurrentAddress", Namespace = "http://www.experian.com/WebDelivery")]
        public CurrentAddress CurrentAddress { get; set; }

        [XmlElement(ElementName = "Employment", Namespace = "http://www.experian.com/WebDelivery")]
        public Employment Employment { get; set; }

        [XmlElement(ElementName = "Phone", Namespace = "http://www.experian.com/WebDelivery")]
        public Phone Phone { get; set; }

        [XmlElement(ElementName = "DOB", Namespace = "http://www.experian.com/WebDelivery")]
        public string Dob { get; set; }
    }

    [XmlRoot(ElementName = "XML", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class XML
    {
        [XmlElement(ElementName = "ARFVersion", Namespace = "http://www.experian.com/WebDelivery")]
        public string ARFVersion { get; set; }

        [XmlElement(ElementName = "Verbose", Namespace = "http://www.experian.com/WebDelivery")]
        public string Verbose { get; set; }

        [XmlElement(ElementName = "Demographics", Namespace = "http://www.experian.com/WebDelivery")]
        public string Demographics { get; set; }

        [XmlElement(ElementName = "Y2K", Namespace = "http://www.experian.com/WebDelivery")]
        public string Y2K { get; set; }

        [XmlElement(ElementName = "Segment130", Namespace = "http://www.experian.com/WebDelivery")]
        public string Segment130 { get; set; }
    }

    [XmlRoot(ElementName = "OutputType", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class OutputType
    {
        [XmlElement(ElementName = "XML", Namespace = "http://www.experian.com/WebDelivery")]
        public XML XML { get; set; }
    }

    [XmlRoot(ElementName = "Vendor", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class Vendor
    {
        [XmlElement(ElementName = "VendorNumber", Namespace = "http://www.experian.com/WebDelivery")]
        public string VendorNumber { get; set; }

        [XmlElement(ElementName = "VendorVersion", Namespace = "http://www.experian.com/WebDelivery")]
        public string VendorVersion { get; set; }
    }

    [XmlRoot(ElementName = "CustomSolution", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class CustomSolution
    {
        [XmlElement(ElementName = "Subscriber", Namespace = "http://www.experian.com/WebDelivery")]
        public Subscriber Subscriber { get; set; }

        [XmlElement(ElementName = "PrimaryApplicant", Namespace = "http://www.experian.com/WebDelivery")]
        public PrimaryApplicant PrimaryApplicant { get; set; }

        [XmlElement(ElementName = "AccountType", Namespace = "http://www.experian.com/WebDelivery")]
        public AccountType AccountType { get; set; }

        [XmlElement(ElementName = "AddOns", Namespace = "http://www.experian.com/WebDelivery")]
        public AddOns AddOns { get; set; }

        [XmlElement(ElementName = "OutputType", Namespace = "http://www.experian.com/WebDelivery")]
        public OutputType OutputType { get; set; }

        [XmlElement(ElementName = "Vendor", Namespace = "http://www.experian.com/WebDelivery")]
        public Vendor Vendor { get; set; }

        [XmlElement(ElementName = "Options", Namespace = "http://www.experian.com/WebDelivery")]
        public Options Options { get; set; }
    }

    [XmlRoot(ElementName = "Products", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class Products
    {
        [XmlElement(ElementName = "CustomSolution", Namespace = "http://www.experian.com/WebDelivery")]
        public CustomSolution CustomSolution { get; set; }
    }

    [XmlRoot(ElementName = "Request", Namespace = "http://www.experian.com/WebDelivery")]
    public partial class Request
    {
        [XmlElement(ElementName = "Products", Namespace = "http://www.experian.com/WebDelivery")]
        public Products Products { get; set; }

        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }

        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; } = "1.0";
    }

    [XmlRoot(ElementName = "NetConnectRequest", Namespace = "http://www.experian.com/NetConnect")]
    public partial class NetConnectRequest
    {
        [XmlElement(ElementName = "EAI")]
        public string Eai { get; set; }

        [XmlElement(ElementName = "DBHost")]
        public string DbHost { get; set; }

        [XmlElement(ElementName = "Request", Namespace = "http://www.experian.com/WebDelivery")]
        public Request Request { get; set; }

        [XmlAttribute(AttributeName = "schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string SchemaLocation { get; set; } = "http://www.experian.com/NetConnect";
    }
}