﻿using LendFoundry.Syndication.Experian.Request;
using System;

namespace LendFoundry.Syndication.Experian.Proxy.PersonalReportRequest
{
    public partial class CustomSolution
    {
        public CustomSolution()
        {
        }

        public CustomSolution(IPersonalReportConfiguration personalReportConfiguration, IPersonalReportRequest personalReportRequest)
        {
            if (personalReportConfiguration == null)
                throw new ArgumentNullException(nameof(personalReportConfiguration));
            if (personalReportRequest == null)
                throw new ArgumentNullException(nameof(personalReportRequest));
            Subscriber = new Subscriber(personalReportConfiguration);
            PrimaryApplicant = new PrimaryApplicant(personalReportRequest);
            AccountType = new AccountType(personalReportConfiguration);
            AddOns = new AddOns(personalReportConfiguration);
            OutputType = new OutputType(personalReportConfiguration);
            Vendor = new Vendor(personalReportConfiguration);
            Options = new Options(personalReportConfiguration);
        }
    }

    public partial class Subscriber
    {
        public Subscriber()
        {
        }

        public Subscriber(IPersonalReportConfiguration personalReportConfiguration)
        {
            if (personalReportConfiguration == null)
                throw new ArgumentNullException(nameof(personalReportConfiguration));

            Preamble = personalReportConfiguration.Preamble;
            OpInitials = personalReportConfiguration.OpInitials;
            SubCode = personalReportConfiguration.SubCode;
        }
    }

    public partial class OutputType
    {
        public OutputType()
        {
        }

        public OutputType(IPersonalReportConfiguration personalReportConfiguration)
        {
            if (personalReportConfiguration == null)
                throw new ArgumentNullException(nameof(personalReportConfiguration));
            XML = new XML(personalReportConfiguration);
        }
    }

    public partial class PrimaryApplicant
    {
        public PrimaryApplicant()
        {
        }

        public PrimaryApplicant(IPersonalReportRequest personalReportRequest)
        {
            if (personalReportRequest == null)
                throw new ArgumentNullException(nameof(personalReportRequest));
            Name = new Name(personalReportRequest);
            SSN = personalReportRequest.Ssn;
            CurrentAddress = new CurrentAddress(personalReportRequest);
            if (personalReportRequest.Employment != null)
            {
                Employment = new Employment(personalReportRequest);
            }
            if (personalReportRequest.PhoneNumber != null && personalReportRequest.PhoneType != null)
            {
                Phone = new Phone(personalReportRequest);
            }
            if (personalReportRequest.Dob != null)
            {
                Dob = personalReportRequest.Dob;
            }
        }
    }

    public partial class CurrentAddress
    {
        public CurrentAddress()
        {
        }

        public CurrentAddress(IPersonalReportRequest personalReportRequest)
        {
            if (personalReportRequest == null)
                throw new ArgumentNullException(nameof(personalReportRequest));
            Street = personalReportRequest.Street;
            City = personalReportRequest.City;
            State = personalReportRequest.State;
            Zip = personalReportRequest.Zip;
        }
    }

    public partial class Employment
    {
        public Employment()
        {
        }

        public Employment(IPersonalReportRequest personalReportRequest)
        {
            if (personalReportRequest.Employment == null)
                throw new ArgumentNullException(nameof(personalReportRequest.Employment));
            Company = personalReportRequest.Employment.Company;
            Address = personalReportRequest.Employment.Address;
            City = personalReportRequest.Employment.City;
            State = personalReportRequest.Employment.State;
            Zip = personalReportRequest.Employment.Zip;
        }
    }

    public partial class Name
    {
        public Name()
        {
        }

        public Name(IPersonalReportRequest personalReportRequest)
        {
            if (personalReportRequest == null)
                throw new ArgumentNullException(nameof(personalReportRequest));
            Surname = personalReportRequest.Lastname;
            First = personalReportRequest.Firstname;
            Middle = personalReportRequest.Middlename;
            Gen = personalReportRequest.Gen;
        }
    }

    public partial class Phone
    {
        public Phone()
        {
        }

        public Phone(IPersonalReportRequest personalReportRequest)
        {
            if (personalReportRequest == null)
                throw new ArgumentNullException(nameof(personalReportRequest));
            Number = personalReportRequest.PhoneNumber;
            Type = personalReportRequest.PhoneType;
        }
    }

    public partial class AccountType
    {
        public AccountType()
        {
        }

        public AccountType(IPersonalReportConfiguration personalReportConfiguration)
        {
            if (personalReportConfiguration == null)
                throw new ArgumentNullException(nameof(personalReportConfiguration));
            Type = personalReportConfiguration.AccountType;
        }
    }

    public partial class AddOns
    {
        public AddOns()
        {
        }

        public AddOns(IPersonalReportConfiguration personalReportConfiguration)
        {
            if (personalReportConfiguration == null)
                throw new ArgumentNullException(nameof(personalReportConfiguration));
            RiskModels = new RiskModels(personalReportConfiguration);
            DemographicBand = new DemographicBand();
            DemographicBand.DemographicsAll = "Y";
            DemographicBand.DemographicPhone = "Y";
            DemographicBand.DemographicGeoCode = "Y";
            DemographicBand.DemographicDriverLicense = "Y";
            CustomRRDashKeyword = personalReportConfiguration.CustomRRDashKeyword;
            ProfileSummary = "Y";
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.DirectCheck) && personalReportConfiguration.DirectCheck.Equals("Y"))
                DirectCheck = personalReportConfiguration.DirectCheck;
        }
    }

    public partial class RiskModels
    {
        public RiskModels()
        {

        }
        public RiskModels(IPersonalReportConfiguration personalReportConfiguration)
        {
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.AssetInsight) && personalReportConfiguration.RiskModelIndicators.AssetInsight.Equals("Y"))
                AssetInsight = personalReportConfiguration.RiskModelIndicators.AssetInsight;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.BankruptcyPLUS) && personalReportConfiguration.RiskModelIndicators.BankruptcyPLUS.Equals("Y"))
                BankruptcyPLUS = personalReportConfiguration.RiskModelIndicators.BankruptcyPLUS;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.BankruptcyPLUSRescaled) && personalReportConfiguration.RiskModelIndicators.BankruptcyPLUSRescaled.Equals("Y"))
                BankruptcyPLUSRescaled = personalReportConfiguration.RiskModelIndicators.BankruptcyPLUSRescaled;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.BustOutScore) && personalReportConfiguration.RiskModelIndicators.BustOutScore.Equals("Y"))
                BustOutScore = personalReportConfiguration.RiskModelIndicators.BustOutScore;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.CollectScore) && personalReportConfiguration.RiskModelIndicators.CollectScore.Equals("Y"))
                CollectScore = personalReportConfiguration.RiskModelIndicators.CollectScore;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.TotalDebtToIncome) && personalReportConfiguration.RiskModelIndicators.TotalDebtToIncome.Equals("Y"))
                TotalDebtToIncome = personalReportConfiguration.RiskModelIndicators.TotalDebtToIncome;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FirstMtgDebtToIncome) && personalReportConfiguration.RiskModelIndicators.FirstMtgDebtToIncome.Equals("Y"))
                FirstMtgDebtToIncome = personalReportConfiguration.RiskModelIndicators.FirstMtgDebtToIncome;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.TotalMtgDebtToIncome) && personalReportConfiguration.RiskModelIndicators.TotalMtgDebtToIncome.Equals("Y"))
                TotalMtgDebtToIncome = personalReportConfiguration.RiskModelIndicators.TotalMtgDebtToIncome;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.TotalDebtToIncomeW2) && personalReportConfiguration.RiskModelIndicators.TotalDebtToIncomeW2.Equals("Y"))
                TotalDebtToIncomeW2 = personalReportConfiguration.RiskModelIndicators.TotalDebtToIncomeW2;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.TotalDebtToIncomeJointW2) && personalReportConfiguration.RiskModelIndicators.TotalDebtToIncomeJointW2.Equals("Y"))
                TotalDebtToIncomeJointW2 = personalReportConfiguration.RiskModelIndicators.TotalDebtToIncomeJointW2;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FirstMtgDebtToIncomeW2) && personalReportConfiguration.RiskModelIndicators.FirstMtgDebtToIncomeW2.Equals("Y"))
                FirstMtgDebtToIncomeW2 = personalReportConfiguration.RiskModelIndicators.FirstMtgDebtToIncomeW2;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.TotalMtgDebtToIncomeW2) && personalReportConfiguration.RiskModelIndicators.TotalMtgDebtToIncomeW2.Equals("Y"))
                TotalMtgDebtToIncomeW2 = personalReportConfiguration.RiskModelIndicators.TotalMtgDebtToIncomeW2;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOAdvanced) && personalReportConfiguration.RiskModelIndicators.FICOAdvanced.Equals("Y"))
                FICOAdvanced = personalReportConfiguration.RiskModelIndicators.FICOAdvanced;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOAdvanced2) && personalReportConfiguration.RiskModelIndicators.FICOAdvanced2.Equals("Y"))
                FICOAdvanced2 = personalReportConfiguration.RiskModelIndicators.FICOAdvanced2;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOAuto2) && personalReportConfiguration.RiskModelIndicators.FICOAuto2.Equals("Y"))
                FICOAuto2 = personalReportConfiguration.RiskModelIndicators.FICOAuto2;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOAuto3) && personalReportConfiguration.RiskModelIndicators.FICOAuto3.Equals("Y"))
                FICOAuto3 = personalReportConfiguration.RiskModelIndicators.FICOAuto3;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICO8Auto) && personalReportConfiguration.RiskModelIndicators.FICO8Auto.Equals("Y"))
                FICO8Auto = personalReportConfiguration.RiskModelIndicators.FICO8Auto;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICO9Auto) && personalReportConfiguration.RiskModelIndicators.FICO9Auto.Equals("Y"))
                FICO9Auto = personalReportConfiguration.RiskModelIndicators.FICO9Auto;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOBank2) && personalReportConfiguration.RiskModelIndicators.FICOBank2.Equals("Y"))
                FICOBank2 = personalReportConfiguration.RiskModelIndicators.FICOBank2;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOBank3) && personalReportConfiguration.RiskModelIndicators.FICOBank3.Equals("Y"))
                FICOBank3 = personalReportConfiguration.RiskModelIndicators.FICOBank3;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICO8Bank) && personalReportConfiguration.RiskModelIndicators.FICO8Bank.Equals("Y"))
                FICO8Bank = personalReportConfiguration.RiskModelIndicators.FICO8Bank;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICO9Bank) && personalReportConfiguration.RiskModelIndicators.FICO9Bank.Equals("Y"))
                FICO9Bank = personalReportConfiguration.RiskModelIndicators.FICO9Bank;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOBankruptcy) && personalReportConfiguration.RiskModelIndicators.FICOBankruptcy.Equals("Y"))
                FICOBankruptcy = personalReportConfiguration.RiskModelIndicators.FICOBankruptcy;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOInstall2) && personalReportConfiguration.RiskModelIndicators.FICOInstall2.Equals("Y"))
                FICOInstall2 = personalReportConfiguration.RiskModelIndicators.FICOInstall2;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOInstall3) && personalReportConfiguration.RiskModelIndicators.FICOInstall3.Equals("Y"))
                FICOInstall3 = personalReportConfiguration.RiskModelIndicators.FICOInstall3;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOInsHomeownerF35) && personalReportConfiguration.RiskModelIndicators.FICOInsHomeownerF35.Equals("Y"))
                FICOInsHomeownerF35 = personalReportConfiguration.RiskModelIndicators.FICOInsHomeownerF35;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOInsHomeownerF4) && personalReportConfiguration.RiskModelIndicators.FICOInsHomeownerF4.Equals("Y"))
                FICOInsHomeownerF4 = personalReportConfiguration.RiskModelIndicators.FICOInsHomeownerF4;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOInsPrefAutoGT) && personalReportConfiguration.RiskModelIndicators.FICOInsPrefAutoGT.Equals("Y"))
                FICOInsPrefAutoGT = personalReportConfiguration.RiskModelIndicators.FICOInsPrefAutoGT;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOInsStandardAutoGT) && personalReportConfiguration.RiskModelIndicators.FICOInsStandardAutoGT.Equals("Y"))
                FICOInsStandardAutoGT = personalReportConfiguration.RiskModelIndicators.FICOInsStandardAutoGT;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOInsStandardAutoMin) && personalReportConfiguration.RiskModelIndicators.FICOInsStandardAutoMin.Equals("Y"))
                FICOInsStandardAutoMin = personalReportConfiguration.RiskModelIndicators.FICOInsStandardAutoMin;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOInsNonstandardAuto) && personalReportConfiguration.RiskModelIndicators.FICOInsNonstandardAuto.Equals("Y"))
                FICOInsNonstandardAuto = personalReportConfiguration.RiskModelIndicators.FICOInsNonstandardAuto;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOFinance2) && personalReportConfiguration.RiskModelIndicators.FICOFinance2.Equals("Y"))
                FICOFinance2 = personalReportConfiguration.RiskModelIndicators.FICOFinance2;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOFinance3) && personalReportConfiguration.RiskModelIndicators.FICOFinance3.Equals("Y"))
                FICOFinance3 = personalReportConfiguration.RiskModelIndicators.FICOFinance3;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICO2) && personalReportConfiguration.RiskModelIndicators.FICO2.Equals("Y"))
                FICO2 = personalReportConfiguration.RiskModelIndicators.FICO2;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICO3) && personalReportConfiguration.RiskModelIndicators.FICO3.Equals("Y"))
                FICO3 = personalReportConfiguration.RiskModelIndicators.FICO3;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICO8) && personalReportConfiguration.RiskModelIndicators.FICO8.Equals("Y"))
                FICO8 = personalReportConfiguration.RiskModelIndicators.FICO8;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICO9) && personalReportConfiguration.RiskModelIndicators.FICO9.Equals("Y"))
                FICO9 = personalReportConfiguration.RiskModelIndicators.FICO9;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FraudShield) && personalReportConfiguration.RiskModelIndicators.FraudShield.Equals("Y"))
                FraudShield = personalReportConfiguration.RiskModelIndicators.FraudShield;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.IncomeInsight) && personalReportConfiguration.RiskModelIndicators.IncomeInsight.Equals("Y"))
                IncomeInsight = personalReportConfiguration.RiskModelIndicators.IncomeInsight;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.IncomeInsightW2) && personalReportConfiguration.RiskModelIndicators.IncomeInsightW2.Equals("Y"))
                IncomeInsightW2 = personalReportConfiguration.RiskModelIndicators.IncomeInsightW2;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.NeverPay) && personalReportConfiguration.RiskModelIndicators.NeverPay.Equals("Y"))
                NeverPay = personalReportConfiguration.RiskModelIndicators.NeverPay;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.RecoveryScoreBank) && personalReportConfiguration.RiskModelIndicators.RecoveryScoreBank.Equals("Y"))
                RecoveryScoreBank = personalReportConfiguration.RiskModelIndicators.RecoveryScoreBank;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.RecoveryScoreRetail) && personalReportConfiguration.RiskModelIndicators.RecoveryScoreRetail.Equals("Y"))
                RecoveryScoreRetail = personalReportConfiguration.RiskModelIndicators.RecoveryScoreRetail;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.ROI1Digit) && personalReportConfiguration.RiskModelIndicators.ROI1Digit.Equals("Y"))
                ROI1Digit = personalReportConfiguration.RiskModelIndicators.ROI1Digit;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.ROI3Digit) && personalReportConfiguration.RiskModelIndicators.ROI3Digit.Equals("Y"))
                ROI3Digit = personalReportConfiguration.RiskModelIndicators.ROI3Digit;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.ScorexPLUS) && personalReportConfiguration.RiskModelIndicators.ScorexPLUS.Equals("Y"))
                ScorexPLUS = personalReportConfiguration.RiskModelIndicators.ScorexPLUS;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.ScorexPLUS2) && personalReportConfiguration.RiskModelIndicators.ScorexPLUS2.Equals("Y"))
                ScorexPLUS2 = personalReportConfiguration.RiskModelIndicators.ScorexPLUS2;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.TEC) && personalReportConfiguration.RiskModelIndicators.TEC.Equals("Y"))
                TEC = personalReportConfiguration.RiskModelIndicators.TEC;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.TeleRisk) && personalReportConfiguration.RiskModelIndicators.TeleRisk.Equals("Y"))
                TeleRisk = personalReportConfiguration.RiskModelIndicators.TeleRisk;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.VantageScore) && personalReportConfiguration.RiskModelIndicators.VantageScore.Equals("Y"))
                VantageScore = personalReportConfiguration.RiskModelIndicators.VantageScore;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.VantageScore2) && personalReportConfiguration.RiskModelIndicators.VantageScore2.Equals("Y"))
                VantageScore2 = personalReportConfiguration.RiskModelIndicators.VantageScore2;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.VantageScore3) && personalReportConfiguration.RiskModelIndicators.VantageScore3.Equals("Y"))
                VantageScore3 = personalReportConfiguration.RiskModelIndicators.VantageScore3;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.VantageScore3Positive) && personalReportConfiguration.RiskModelIndicators.VantageScore3Positive.Equals("Y"))
                VantageScore3Positive = personalReportConfiguration.RiskModelIndicators.VantageScore3Positive;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.Auto) && personalReportConfiguration.RiskModelIndicators.Auto.Equals("Y"))
                Auto = personalReportConfiguration.RiskModelIndicators.Auto;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.BankruptcyWatch) && personalReportConfiguration.RiskModelIndicators.BankruptcyWatch.Equals("Y"))
                BankruptcyWatch = personalReportConfiguration.RiskModelIndicators.BankruptcyWatch;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.CreditUnion) && personalReportConfiguration.RiskModelIndicators.CreditUnion.Equals("Y"))
                CreditUnion = personalReportConfiguration.RiskModelIndicators.CreditUnion;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOAuto) && personalReportConfiguration.RiskModelIndicators.FICOAuto.Equals("Y"))
                FICOAuto = personalReportConfiguration.RiskModelIndicators.FICOAuto;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOBank) && personalReportConfiguration.RiskModelIndicators.FICOBank.Equals("Y"))
                FICOBank = personalReportConfiguration.RiskModelIndicators.FICOBank;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.Bankruptcy) && personalReportConfiguration.RiskModelIndicators.Bankruptcy.Equals("Y"))
                Bankruptcy = personalReportConfiguration.RiskModelIndicators.Bankruptcy;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOInstall) && personalReportConfiguration.RiskModelIndicators.FICOInstall.Equals("Y"))
                FICOInstall = personalReportConfiguration.RiskModelIndicators.FICOInstall;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICOFinance) && personalReportConfiguration.RiskModelIndicators.FICOFinance.Equals("Y"))
                FICOFinance = personalReportConfiguration.RiskModelIndicators.FICOFinance;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.FICO) && personalReportConfiguration.RiskModelIndicators.FICO.Equals("Y"))
                FICO = personalReportConfiguration.RiskModelIndicators.FICO;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.NationalEquiv) && personalReportConfiguration.RiskModelIndicators.NationalEquiv.Equals("Y"))
                NationalEquiv = personalReportConfiguration.RiskModelIndicators.NationalEquiv;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.NationalRisk) && personalReportConfiguration.RiskModelIndicators.NationalRisk.Equals("Y"))
                NationalRisk = personalReportConfiguration.RiskModelIndicators.NationalRisk;
            if (!string.IsNullOrWhiteSpace(personalReportConfiguration.RiskModelIndicators.Retail) && personalReportConfiguration.RiskModelIndicators.Retail.Equals("Y"))
                Retail = personalReportConfiguration.RiskModelIndicators.Retail;
        }
    }

    public partial class Products
    {
        public Products()
        {
        }

        public Products(IPersonalReportConfiguration personalReportConfiguration, IPersonalReportRequest personalReportRequest)
        {
            if (personalReportConfiguration == null)
                throw new ArgumentNullException(nameof(personalReportConfiguration));
            if (personalReportRequest == null)
                throw new ArgumentNullException(nameof(personalReportRequest));

            CustomSolution = new CustomSolution(personalReportConfiguration, personalReportRequest);
        }
    }

    public partial class Request
    {
        public Request()
        {
        }

        public Request(IPersonalReportConfiguration personalReportConfiguration, IPersonalReportRequest personalReportRequest)
        {
            if (personalReportConfiguration == null)
                throw new ArgumentNullException(nameof(personalReportConfiguration));
            if (personalReportRequest == null)
                throw new ArgumentNullException(nameof(personalReportRequest));

            Products = new Products(personalReportConfiguration, personalReportRequest);
        }
    }

    public partial class Vendor
    {
        public Vendor()
        {
        }

        public Vendor(IPersonalReportConfiguration personalReportConfiguration)
        {
            if (personalReportConfiguration == null)
                throw new ArgumentNullException(nameof(personalReportConfiguration));

            VendorNumber = personalReportConfiguration.VendorNumber;
            VendorVersion = personalReportConfiguration.VendorVersion;
        }
    }

    public partial class Options
    {
        public Options()
        {
        }

        public Options(IPersonalReportConfiguration personalReportConfiguration)
        {
            if (personalReportConfiguration == null)
                throw new ArgumentNullException(nameof(personalReportConfiguration));

            ReferenceNumber = personalReportConfiguration.ReferenceId;
        }
    }

    public partial class XML
    {
        public XML()
        {
        }

        public XML(IPersonalReportConfiguration personalReportConfiguration)
        {
            if (personalReportConfiguration == null)
                throw new ArgumentNullException(nameof(personalReportConfiguration));
            if (personalReportConfiguration.ArfVersion != null)
            {
                ARFVersion = personalReportConfiguration.ArfVersion;
            }
            if (personalReportConfiguration.Verbose != null)
            {
                Verbose = personalReportConfiguration.Verbose;
            }
            if (personalReportConfiguration.Demographics != null)
            {
                Demographics = personalReportConfiguration.Demographics;
            }
            if (personalReportConfiguration.Y2K != null)
            {
                Y2K = personalReportConfiguration.Y2K;
            }
            if (personalReportConfiguration.Segment130 != null)
            {
                Segment130 = personalReportConfiguration.Segment130;
            }
        }
    }

    public partial class NetConnectRequest
    {
        public NetConnectRequest()
        {
        }

        public NetConnectRequest(IPersonalReportConfiguration personalReportConfiguration, IPersonalReportRequest personalReportRequest)
        {
            if (personalReportConfiguration == null)
                throw new ArgumentNullException(nameof(personalReportConfiguration));
            if (personalReportRequest == null)
                throw new ArgumentNullException(nameof(personalReportRequest));

            if (string.IsNullOrWhiteSpace(personalReportConfiguration.NetConnectSubscriber))
                throw new ArgumentException("Net connect subscriber is required",
                    nameof(personalReportConfiguration.NetConnectSubscriber));

            if (string.IsNullOrWhiteSpace(personalReportConfiguration.NetConnectPassword))
                throw new ArgumentException("Net connect password is required",
                    nameof(personalReportConfiguration.NetConnectPassword));

            if (string.IsNullOrWhiteSpace(personalReportConfiguration.Eai))
                throw new ArgumentException("Eai is required", nameof(personalReportConfiguration.Eai));

            if (string.IsNullOrWhiteSpace(personalReportConfiguration.ArfVersion))
                throw new ArgumentException("ArfVersion is required", nameof(personalReportConfiguration.ArfVersion));

            if (string.IsNullOrWhiteSpace(personalReportConfiguration.DbHost))
                throw new ArgumentException("DbHost is required", nameof(personalReportConfiguration.DbHost));
            if (string.IsNullOrWhiteSpace(personalReportConfiguration.OpInitials))
                throw new ArgumentException("OpInitials is required", nameof(personalReportConfiguration.OpInitials));

            if (string.IsNullOrWhiteSpace(personalReportConfiguration.Preamble))
                throw new ArgumentException("Preamble is required", nameof(personalReportConfiguration.Preamble));

            if (string.IsNullOrWhiteSpace(personalReportConfiguration.SubCode))
                throw new ArgumentException("SubCode is required", nameof(personalReportConfiguration.SubCode));

            if (string.IsNullOrWhiteSpace(personalReportConfiguration.VendorNumber))
                throw new ArgumentException("VendorNumber is required", nameof(personalReportConfiguration.VendorNumber));

            if (string.IsNullOrWhiteSpace(personalReportRequest.Firstname))
                throw new ArgumentException("Firstname is required", nameof(personalReportRequest.Firstname));

            if (string.IsNullOrWhiteSpace(personalReportRequest.Lastname))
                throw new ArgumentException("Lastname is required", nameof(personalReportRequest.Lastname));

            if (string.IsNullOrWhiteSpace(personalReportRequest.Street))
                throw new ArgumentException("Street is required", nameof(personalReportRequest.Street));

            if (string.IsNullOrWhiteSpace(personalReportRequest.Zip))
                throw new ArgumentException("Zip is required", nameof(personalReportRequest.Zip));

            if (string.IsNullOrWhiteSpace(personalReportRequest.Ssn))
                throw new ArgumentException("Ssn is required", nameof(personalReportRequest.Ssn));

            if (personalReportRequest.Ssn.Replace("-", "").Length > 9)
                throw new ArgumentException("Ssn should not more than 9 digits", nameof(personalReportRequest.Ssn));

            Eai = personalReportConfiguration.Eai;
            DbHost = personalReportConfiguration.DbHost;
            Request = new Request(personalReportConfiguration, personalReportRequest);
        }
    }
}