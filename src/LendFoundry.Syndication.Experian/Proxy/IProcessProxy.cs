using System;
using System.Diagnostics;
using System.IO;

namespace LendFoundry.Syndication.Experian.Proxy
{
    public interface IProcessProxy : IDisposable
    {
        int ExitCode { get; }
        ProcessStartInfo StartInfo { get; set; }
        bool EnableRaisingEvents { get; set; }
        StreamWriter StandardInput { get; }
        bool HasExited { get; }
        event EventHandler Exited;
        event DataReceivedEventHandler OutputDataReceived;
        event DataReceivedEventHandler ErrorDataReceived;
        bool Start();
        void Kill();
        void BeginOutputReadLine();
        void BeginErrorReadLine();
        bool WaitForExit(int milliseconds);
    }
}