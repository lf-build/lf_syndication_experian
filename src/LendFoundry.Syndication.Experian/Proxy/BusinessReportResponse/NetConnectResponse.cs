﻿using System.Xml.Serialization;

namespace LendFoundry.Syndication.Experian.Proxy.BusinessReportResponse
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class Modifier
    {
        private string codeField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class RecentHighCredit
    {
        public string Amount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Modifier")]
        public Modifier[] Modifier { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class AccountBalance
    {
        public string Amount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Modifier")]
        public Modifier[] Modifier { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class TradeLineFlag
    {
        private string codeField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NewlyReportedIndicator
    {
        private string codeField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class TotalHighCreditAmount
    {
        public string Amount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Modifier")]
        public Modifier[] Modifier { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class TotalAccountBalance
    {
        public string Amount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Modifier")]
        public Modifier[] Modifier { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class CurrentMonth
    {
        public string Date { get; set; }
        public string DBT { get; set; }
        public string CurrentPercentage { get; set; }
        public string DBT30 { get; set; }
        public string DBT60 { get; set; }
        public string DBT90 { get; set; }
        public string DBT120 { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalAccountBalance")]
        public TotalAccountBalance[] TotalAccountBalance { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class PriorMonth
    {
        public string Date { get; set; }
        public string DBT { get; set; }
        public string CurrentPercentage { get; set; }
        public string DBT30 { get; set; }
        public string DBT60 { get; set; }
        public string DBT90 { get; set; }
        public string DBT120 { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalAccountBalance")]
        public TotalAccountBalance[] TotalAccountBalance { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class MostRecentQuarter
    {
        public string DBT { get; set; }
        public string CurrentPercentage { get; set; }
        public string DBT30 { get; set; }
        public string DBT60 { get; set; }
        public string DBT90 { get; set; }
        public string DBT120 { get; set; }
        public string YearOfQuarter { get; set; }
        public string Quarter { get; set; }
        public string Score { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalAccountBalance")]
        public TotalAccountBalance[] TotalAccountBalance { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("QuarterWithinYear", IsNullable = true)]
        public QuarterWithinYear[] QuarterWithinYear { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class QuarterWithinYear
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class PriorQuarter
    {
        public string Date { get; set; }
        public string DBT { get; set; }
        public string CurrentPercentage { get; set; }
        public string DBT30 { get; set; }
        public string DBT60 { get; set; }
        public string DBT90 { get; set; }
        public string DBT120 { get; set; }
        public string YearOfQuarter { get; set; }
        public string Quarter { get; set; }
        public string Score { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalAccountBalance")]
        public TotalAccountBalance[] TotalAccountBalance { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("QuarterWithinYear", IsNullable = true)]
        public QuarterWithinYear[] QuarterWithinYear { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class LegalType
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class LegalAction
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class Statement
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class MatchingBusinessIndicator
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/NetConnectResponse")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.experian.com/NetConnectResponse", IsNullable = false)]
    public partial class NetConnectResponse
    {
        public string CompletionCode { get; set; }
        public string ReferenceId { get; set; }
        public string TransactionId { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.experian.com/ARFResponse")]
        public Products Products { get; set; }

        public string ErrorMessage { get; set; }
        public string ErrorTag { get; set; }
    }

    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.experian.com/ARFResponse", IsNullable = false)]
    public partial class Products
    {
        public NetConnectResponseProductsPremierProfile PremierProfile { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfile
    {
        [System.Xml.Serialization.XmlElementAttribute("InputSummary")]
        public NetConnectResponseProductsPremierProfileInputSummary[] InputSummary { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ExpandedCreditSummary")]
        public NetConnectResponseProductsPremierProfileExpandedCreditSummary[] ExpandedCreditSummary { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ExpandedBusinessNameAndAddress")]
        public NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddress[] ExpandedBusinessNameAndAddress { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("DoingBusinessAs")]
        public NetConnectResponseProductsPremierProfileDoingBusinessAs[] DoingBusinessAs { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ExecutiveSummary")]
        public NetConnectResponseProductsPremierProfileExecutiveSummary[] ExecutiveSummary { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("CollectionData")]
        public NetConnectResponseProductsPremierProfileCollectionData[] CollectionData { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TradePaymentExperiences")]
        public NetConnectResponseProductsPremierProfileTradePaymentExperiences[] TradePaymentExperiences { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("AdditionalPaymentExperiences")]
        public NetConnectResponseProductsPremierProfileAdditionalPaymentExperiences[] AdditionalPaymentExperiences { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("PaymentTotals")]
        public NetConnectResponseProductsPremierProfilePaymentTotals[] PaymentTotals { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("PaymentTrends")]
        public NetConnectResponseProductsPremierProfilePaymentTrends[] PaymentTrends { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("IndustryPaymentTrends")]
        public NetConnectResponseProductsPremierProfileIndustryPaymentTrends[] IndustryPaymentTrends { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("QuarterlyPaymentTrends")]
        public NetConnectResponseProductsPremierProfileQuarterlyPaymentTrends[] QuarterlyPaymentTrends { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TaxLien")]
        public NetConnectResponseProductsPremierProfileTaxLien[] TaxLien { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("JudgmentOrAttachmentLien")]
        public NetConnectResponseProductsPremierProfileJudgmentOrAttachmentLien[] JudgmentOrAttachmentLien { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("UCCFilingsSummaryCounts")]
        public NetConnectResponseProductsPremierProfileUCCFilingsSummaryCounts[] UCCFilingsSummaryCounts { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("UCCFilings")]
        public NetConnectResponseProductsPremierProfileUCCFilings[] UCCFilings { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("CorporateRegistration")]
        public NetConnectResponseProductsPremierProfileCorporateRegistration CorporateRegistration { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("DemographicInformation")]
        public NetConnectResponseProductsPremierProfileDemographicInformation DemographicInformation { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("KeyPersonnelExecutiveInformation")]
        public NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformation[] KeyPersonnelExecutiveInformation { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("CorporateLinkage")]
        public NetConnectResponseProductsPremierProfileCorporateLinkage[] CorporateLinkage { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ExecutiveElements")]
        public NetConnectResponseProductsPremierProfileExecutiveElements ExecutiveElements { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("BusinessFacts")]
        public NetConnectResponseProductsPremierProfileBusinessFacts[] BusinessFacts { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("SICCodes", typeof(NetConnectResponseProductsPremierProfileSICCodesSIC))]
        public NetConnectResponseProductsPremierProfileSICCodesSIC[] SICCodes { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("NAICS", typeof(NetConnectResponseProductsPremierProfileNAICSCodesNAICS))]
        public NetConnectResponseProductsPremierProfileNAICSCodesNAICS[] NAICSCodes { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Competitors")]
        public NetConnectResponseProductsPremierProfileCompetitors[] Competitors { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("CommercialFraudShieldSummary")]
        public NetConnectResponseProductsPremierProfileCommercialFraudShieldSummary[] CommercialFraudShieldSummary { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Inquiry")]
        public NetConnectResponseProductsPremierProfileInquiry[] Inquiry { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("IntelliscoreScoreInformation")]
        public NetConnectResponseProductsPremierProfileIntelliscoreScoreInformation[] IntelliscoreScoreInformation { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ScoreFactors")]
        public NetConnectResponseProductsPremierProfileScoreFactors[] ScoreFactors { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ScoreTrendsCreditLimit")]
        public NetConnectResponseProductsPremierProfileScoreTrendsCreditLimit[] ScoreTrendsCreditLimit { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("BillingIndicator", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileBillingIndicator[] BillingIndicator { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ProcessingMessage")]
        public ProcessingMessage ProcessingMessage { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Bankruptcy")]
        public NetConnectResponseProductsPremierProfileBankruptcy[] Bankruptcy { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProcessingMessage
    {
        public string ProcessingAction { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileInputSummary
    {
        public string SubscriberNumber { get; set; }
        public string InquiryTransactionNumber { get; set; }
        public string CPUVersion { get; set; }
        public string Inquiry { get; set; }
    }

    [XmlRoot(ElementName = "ExecutiveElements")]
    public class NetConnectResponseProductsPremierProfileExecutiveElements
    {
        [XmlElement(ElementName = "BankruptcyCount")]
        public string BankruptcyCount { get; set; }
        [XmlElement(ElementName = "TaxLienCount")]
        public string TaxLienCount { get; set; }
        [XmlElement(ElementName = "EarliestTaxLienDate")]
        public string EarliestTaxLienDate { get; set; }
        [XmlElement(ElementName = "MostRecentTaxLienDate")]
        public string MostRecentTaxLienDate { get; set; }
        [XmlElement(ElementName = "JudgmentCount")]
        public string JudgmentCount { get; set; }
        [XmlElement(ElementName = "EarliestJudgmentDate")]
        public string EarliestJudgmentDate { get; set; }
        [XmlElement(ElementName = "MostRecentJudgmentDate")]
        public string MostRecentJudgmentDate { get; set; }
        [XmlElement(ElementName = "CollectionCount")]
        public string CollectionCount { get; set; }
        [XmlElement(ElementName = "LegalBalance")]
        public string LegalBalance { get; set; }
        [XmlElement(ElementName = "UCCFilings")]
        public string UCCFilings { get; set; }
        [XmlElement(ElementName = "UCCDerogatoryCount")]
        public string UCCDerogatoryCount { get; set; }
        [XmlElement(ElementName = "CurrentAccountBalance")]
        public string CurrentAccountBalance { get; set; }
        [XmlElement(ElementName = "CurrentTradelineCount")]
        public string CurrentTradelineCount { get; set; }
        [XmlElement(ElementName = "MonthlyAverageDBT")]
        public string MonthlyAverageDBT { get; set; }
        [XmlElement(ElementName = "HighestDBT6Months")]
        public string HighestDBT6Months { get; set; }
        [XmlElement(ElementName = "HighestDBT5Quarters")]
        public string HighestDBT5Quarters { get; set; }
        [XmlElement(ElementName = "ActiveTradelineCount")]
        public string ActiveTradelineCount { get; set; }
        [XmlElement(ElementName = "AllTradelineBalance")]
        public string AllTradelineBalance { get; set; }
        [XmlElement(ElementName = "AllTradelineCount")]
        public string AllTradelineCount { get; set; }
        [XmlElement(ElementName = "AverageBalance5Quarters")]
        public string AverageBalance5Quarters { get; set; }
        [XmlElement(ElementName = "SingleLineHighCredit")]
        public string SingleLineHighCredit { get; set; }
        [XmlElement(ElementName = "LowBalance6Months")]
        public string LowBalance6Months { get; set; }
        [XmlElement(ElementName = "HighBalance6Months")]
        public string HighBalance6Months { get; set; }
        [XmlElement(ElementName = "EarliestCollectionDate")]
        public string EarliestCollectionDate { get; set; }
        [XmlElement(ElementName = "MostRecentCollectionDate")]
        public string MostRecentCollectionDate { get; set; }
        [XmlElement(ElementName = "CurrentDBT")]
        public string CurrentDBT { get; set; }
        [XmlElement(ElementName = "YearofIncorporation")]
        public string YearofIncorporation { get; set; }
        [XmlElement(ElementName = "TaxID")]
        public string TaxID { get; set; }
        [XmlElement(ElementName = "JudgmentFlag")]
        public string JudgmentFlag { get; set; }
        [XmlElement(ElementName = "TaxLienFlag")]
        public string TaxLienFlag { get; set; }
    }

    [XmlRoot(ElementName = "BusinessType")]
    public class BusinessType
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "StatusFlag")]
    public class StatusFlag
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "ProfitFlag")]
    public class ProfitFlag
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "AgentInformation")]
    public class AgentInformation
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "StreetAddress")]
        public string StreetAddress { get; set; }
        [XmlElement(ElementName = "City")]
        public string City { get; set; }
        [XmlElement(ElementName = "State")]
        public string State { get; set; }
    }

    [XmlRoot(ElementName = "CorporateInformation")]
    public class NetConnectResponseProductsPremierProfileCorporateRegistration
    {
        [XmlElement(ElementName = "StateOfOrigin")]
        public string StateOfOrigin { get; set; }
        [XmlElement(ElementName = "OriginalFilingDate")]
        public string OriginalFilingDate { get; set; }
        [XmlElement(ElementName = "RecentFilingDate")]
        public string RecentFilingDate { get; set; }
        [XmlElement(ElementName = "IncorporatedDate")]
        public string IncorporatedDate { get; set; }
        [XmlElement(ElementName = "BusinessType")]
        public BusinessType BusinessType { get; set; }
        [XmlElement(ElementName = "StatusFlag")]
        public StatusFlag StatusFlag { get; set; }
        [XmlElement(ElementName = "StatusDescription")]
        public string StatusDescription { get; set; }
        [XmlElement(ElementName = "ProfitFlag")]
        public ProfitFlag ProfitFlag { get; set; }
        [XmlElement(ElementName = "CharterNumber")]
        public string CharterNumber { get; set; }
        [XmlElement(ElementName = "AgentInformation")]
        public AgentInformation AgentInformation { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExpandedCreditSummary
    {
        public string BankruptcyFilingCount { get; set; }
        public string TaxLienFilingCount { get; set; }
        public string OldestTaxLienDate { get; set; }
        public string MostRecentTaxLienDate { get; set; }
        public string JudgmentFilingCount { get; set; }
        public string OldestJudgmentDate { get; set; }
        public string MostRecentJudgmentDate { get; set; }
        public string CollectionCount { get; set; }
        public string CollectionBalance { get; set; }
        public string CollectionCountPast24Months { get; set; }
        public string LegalBalance { get; set; }
        public string UCCFilings { get; set; }
        public string UCCDerogCount { get; set; }
        public string CurrentAccountBalance { get; set; }
        public string CurrentTradelineCount { get; set; }
        public string MonthlyAverageDBT { get; set; }
        public string HighestDBT6Months { get; set; }
        public string HighestDBT5Quarters { get; set; }
        public string ActiveTradelineCount { get; set; }
        public string AllTradelineBalance { get; set; }
        public string AllTradelineCount { get; set; }
        public string AverageBalance5Quarters { get; set; }
        public string SingleHighCredit { get; set; }
        public string LowBalance6Months { get; set; }
        public string HighBalance6Months { get; set; }
        public string OldestCollectionDate { get; set; }
        public string MostRecentCollectionDate { get; set; }
        public string CurrentDBT { get; set; }
        public string OldestUCCFilingDate { get; set; }
        public string MostRecentUCCFilingDate { get; set; }
        public string JudgmentFlag { get; set; }
        public string TaxLienFlag { get; set; }
        public string TradeCollectionCount { get; set; }
        public string TradeCollectionBalance { get; set; }
        public string OpenCollectionCount { get; set; }
        public string OpenCollectionBalance { get; set; }
        public string MostRecentBankruptcyDate { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("OFACMatch")]
        public NetConnectResponseProductsPremierProfileExpandedCreditSummaryOFACMatch[] OFACMatch { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("VictimStatement", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileExpandedCreditSummaryVictimStatement[] VictimStatement { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExpandedCreditSummaryOFACMatch
    {
        private string codeField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExpandedCreditSummaryVictimStatement
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddress
    {
        public string ExperianBIN { get; set; }
        public string ProfileDate { get; set; }
        public string ProfileTime { get; set; }
        public string TerminalNumber { get; set; }
        public string BusinessName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ZipExtension { get; set; }
        public string PhoneNumber { get; set; }
        public string TaxID { get; set; }
        public string WebsiteURL { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ProfileType")]
        public NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddressProfileType[] ProfileType { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("MatchingBranchAddress")]
        public NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddressMatchingBranchAddress[] MatchingBranchAddress { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("CorporateLink")]
        public NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddressCorporateLink[] CorporateLink { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddressProfileType
    {
        private string codeField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddressMatchingBranchAddress
    {
        public string MatchingBranchBIN { get; set; }
        public string MatchingStreetAddress { get; set; }
        public string MatchingCity { get; set; }
        public string MatchingState { get; set; }
        public string MatchingZip { get; set; }
        public string MatchingZipExtension { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddressCorporateLink
    {
        public string CorporateLinkageIndicator { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileDoingBusinessAs
    {
        public string DBAName { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExecutiveSummary
    {
        public string PredictedDBT { get; set; }
        public string PredictedDBTDate { get; set; }
        public string IndustryDBT { get; set; }
        public string AllIndustryDBT { get; set; }
        public string HighCreditAmountExtended { get; set; }
        public string MedianCreditAmountExtended { get; set; }
        public string IndustryDescription { get; set; }
        public string CommonTerms { get; set; }
        public string CommonTerms2 { get; set; }
        public string CommonTerms3 { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("BusinessDBT", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileExecutiveSummaryBusinessDBT[] BusinessDBT { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("LowestTotalAccountBalance")]
        public NetConnectResponseProductsPremierProfileExecutiveSummaryLowestTotalAccountBalance[] LowestTotalAccountBalance { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("HighestTotalAccountBalance")]
        public NetConnectResponseProductsPremierProfileExecutiveSummaryHighestTotalAccountBalance[] HighestTotalAccountBalance { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("CurrentTotalAccountBalance")]
        public NetConnectResponseProductsPremierProfileExecutiveSummaryCurrentTotalAccountBalance[] CurrentTotalAccountBalance { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("IndustryPaymentComparison", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileExecutiveSummaryIndustryPaymentComparison[] IndustryPaymentComparison { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("PaymentTrendIndicator", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileExecutiveSummaryPaymentTrendIndicator[] PaymentTrendIndicator { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExecutiveSummaryBusinessDBT
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExecutiveSummaryLowestTotalAccountBalance
    {
        public string Amount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Modifier")]
        public Modifier[] Modifier { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExecutiveSummaryHighestTotalAccountBalance
    {
        public string Amount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Modifier")]
        public Modifier[] Modifier { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExecutiveSummaryCurrentTotalAccountBalance
    {
        public string Amount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Modifier")]
        public Modifier[] Modifier { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExecutiveSummaryIndustryPaymentComparison
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileExecutiveSummaryPaymentTrendIndicator
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileCollectionData
    {
        public string DatePlacedForCollection { get; set; }
        public string DateClosed { get; set; }
        public string AmountPlacedForCollection { get; set; }
        public string AmountPaid { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("AccountStatus", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileCollectionDataAccountStatus[] AccountStatus { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("CollectionAgencyInfo")]
        public NetConnectResponseProductsPremierProfileCollectionDataCollectionAgencyInfo[] CollectionAgencyInfo { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileCollectionDataAccountStatus
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileCollectionDataCollectionAgencyInfo
    {
        public string AgencyName { get; set; }
        public string PhoneNumber { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileTradePaymentExperiences
    {
        public string BusinessCategory { get; set; }
        public string DateReported { get; set; }
        public string DateLastActivity { get; set; }
        public string Terms { get; set; }
        public string CurrentPercentage { get; set; }
        public string DBT30 { get; set; }
        public string DBT60 { get; set; }
        public string DBT90 { get; set; }
        public string DBT90Plus { get; set; }
        public string Comments { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("PaymentIndicator")]
        public NetConnectResponseProductsPremierProfileTradePaymentExperiencesPaymentIndicator[] PaymentIndicator { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("RecentHighCredit")]
        public RecentHighCredit[] RecentHighCredit { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("AccountBalance")]
        public AccountBalance[] AccountBalance { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TradeLineFlag")]
        public TradeLineFlag[] TradeLineFlag { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("NewlyReportedIndicator")]
        public NewlyReportedIndicator[] NewlyReportedIndicator { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileTradePaymentExperiencesPaymentIndicator
    {
        private string codeField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileAdditionalPaymentExperiences
    {
        public string BusinessCategory { get; set; }
        public string DateReported { get; set; }
        public string DateLastActivity { get; set; }
        public string Terms { get; set; }
        public string CurrentPercentage { get; set; }
        public string DBT30 { get; set; }
        public string DBT60 { get; set; }
        public string DBT90 { get; set; }
        public string DBT90Plus { get; set; }
        public string Comments { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("RecentHighCredit")]
        public RecentHighCredit[] RecentHighCredit { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("AccountBalance")]
        public AccountBalance[] AccountBalance { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TradeLineFlag")]
        public TradeLineFlag[] TradeLineFlag { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("NewlyReportedIndicator")]
        public NewlyReportedIndicator[] NewlyReportedIndicator { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfilePaymentTotals
    {
        [System.Xml.Serialization.XmlElementAttribute("NewlyReportedTradeLines")]
        public NetConnectResponseProductsPremierProfilePaymentTotalsNewlyReportedTradeLines[] NewlyReportedTradeLines { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ContinouslyReportedTradeLines")]
        public NetConnectResponseProductsPremierProfilePaymentTotalsContinouslyReportedTradeLines[] ContinouslyReportedTradeLines { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("CombinedTradeLines")]
        public NetConnectResponseProductsPremierProfilePaymentTotalsCombinedTradeLines[] CombinedTradeLines { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("AdditionalTradeLines")]
        public NetConnectResponseProductsPremierProfilePaymentTotalsAdditionalTradeLines[] AdditionalTradeLines { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TradeLines")]
        public NetConnectResponseProductsPremierProfilePaymentTotalsTradeLines[] TradeLines { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfilePaymentTotalsNewlyReportedTradeLines
    {
        public string NumberOfLines { get; set; }
        public string DBT { get; set; }
        public string CurrentPercentage { get; set; }
        public string DBT30 { get; set; }
        public string DBT60 { get; set; }
        public string DBT90 { get; set; }
        public string DBT120 { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalHighCreditAmount")]
        public TotalHighCreditAmount[] TotalHighCreditAmount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalAccountBalance")]
        public TotalAccountBalance[] TotalAccountBalance { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfilePaymentTotalsContinouslyReportedTradeLines
    {
        public string NumberOfLines { get; set; }
        public string DBT { get; set; }
        public string CurrentPercentage { get; set; }
        public string DBT30 { get; set; }
        public string DBT60 { get; set; }
        public string DBT90 { get; set; }
        public string DBT120 { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalHighCreditAmount")]
        public TotalHighCreditAmount[] TotalHighCreditAmount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalAccountBalance")]
        public TotalAccountBalance[] TotalAccountBalance { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfilePaymentTotalsCombinedTradeLines
    {
        public string NumberOfLines { get; set; }
        public string DBT { get; set; }
        public string CurrentPercentage { get; set; }
        public string DBT30 { get; set; }
        public string DBT60 { get; set; }
        public string DBT90 { get; set; }
        public string DBT120 { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalHighCreditAmount")]
        public TotalHighCreditAmount[] TotalHighCreditAmount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalAccountBalance")]
        public TotalAccountBalance[] TotalAccountBalance { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfilePaymentTotalsAdditionalTradeLines
    {
        public string NumberOfLines { get; set; }
        public string CurrentPercentage { get; set; }
        public string DBT30 { get; set; }
        public string DBT60 { get; set; }
        public string DBT90 { get; set; }
        public string DBT120 { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalHighCreditAmount")]
        public TotalHighCreditAmount[] TotalHighCreditAmount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalAccountBalance")]
        public TotalAccountBalance[] TotalAccountBalance { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfilePaymentTotalsTradeLines
    {
        public string NumberOfLines { get; set; }
        public string CurrentPercentage { get; set; }
        public string DBT30 { get; set; }
        public string DBT60 { get; set; }
        public string DBT90 { get; set; }
        public string DBT120 { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalHighCreditAmount")]
        public TotalHighCreditAmount[] TotalHighCreditAmount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("TotalAccountBalance")]
        public TotalAccountBalance[] TotalAccountBalance { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfilePaymentTrends
    {
        [System.Xml.Serialization.XmlElementAttribute("CurrentMonth")]
        public CurrentMonth[] CurrentMonth { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("PriorMonth")]
        public PriorMonth[] PriorMonth { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileIndustryPaymentTrends
    {
        public string SIC { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("CurrentMonth")]
        public CurrentMonth[] CurrentMonth { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("PriorMonth")]
        public PriorMonth[] PriorMonth { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileQuarterlyPaymentTrends
    {
        [System.Xml.Serialization.XmlElementAttribute("MostRecentQuarter")]
        public MostRecentQuarter[] MostRecentQuarter { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("PriorQuarter")]
        public PriorQuarter[] PriorQuarter { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileTaxLien
    {
        public string DateFiled { get; set; }
        public string DocumentNumber { get; set; }
        public string FilingLocation { get; set; }
        public string Owner { get; set; }
        public string LiabilityAmount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("LegalType", IsNullable = true)]
        public LegalType[] LegalType { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("LegalAction", IsNullable = true)]
        public LegalAction[] LegalAction { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileJudgmentOrAttachmentLien
    {
        public string DateFiled { get; set; }
        public string DocumentNumber { get; set; }
        public string FilingLocation { get; set; }
        public string LiabilityAmount { get; set; }
        public string PlaintiffName { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("LegalType", IsNullable = true)]
        public LegalType[] LegalType { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("LegalAction", IsNullable = true)]
        public LegalAction[] LegalAction { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileUCCFilingsSummaryCounts
    {
        public string UCCFilingsTotal { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("MostRecent6Months")]
        public NetConnectResponseProductsPremierProfileUCCFilingsSummaryCountsMostRecent6Months[] MostRecent6Months { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Previous6Months")]
        public NetConnectResponseProductsPremierProfileUCCFilingsSummaryCountsPrevious6Months[] Previous6Months { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileUCCFilingsSummaryCountsMostRecent6Months
    {
        public string StartDate { get; set; }
        public string FilingsTotal { get; set; }
        public string FilingsWithDerogatoryCollateral { get; set; }
        public string ReleasesAndTerminationsTotal { get; set; }
        public string ContinuationsTotal { get; set; }
        public string AmendedAndAssignedTotal { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileUCCFilingsSummaryCountsPrevious6Months
    {
        public string StartDate { get; set; }
        public string FilingsTotal { get; set; }
        public string FilingsWithDerogatoryCollateral { get; set; }
        public string ReleasesAndTerminationsTotal { get; set; }
        public string ContinuationsTotal { get; set; }
        public string AmendedAndAssignedTotal { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileUCCFilings
    {
        public string DateFiled { get; set; }
        public string DocumentNumber { get; set; }
        public string FilingLocation { get; set; }
        public string SecuredParty { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("LegalType", IsNullable = true)]
        public LegalType[] LegalType { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("LegalAction", IsNullable = true)]
        public LegalAction[] LegalAction { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("OriginalUCCFilingsInfo")]
        public NetConnectResponseProductsPremierProfileUCCFilingsOriginalUCCFilingsInfo[] OriginalUCCFilingsInfo { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("CollateralCodes", typeof(NetConnectResponseProductsPremierProfileUCCFilingsCollateralCodesCollateral))]
        public NetConnectResponseProductsPremierProfileUCCFilingsCollateralCodesCollateral CollateralCodes { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileBankruptcy
    {
        public string DateFiled { get; set; }
        public string DocumentNumber { get; set; }
        public string FilingLocation { get; set; }
        public string Owner { get; set; }
        public string LiabilityAmount { get; set; }
        public string AssetAmount { get; set; }
        public string ExemptAmount { get; set; }
        public string CustomerDispute { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("LegalType", IsNullable = true)]
        public LegalType[] LegalType { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("LegalAction", IsNullable = true)]
        public LegalAction[] LegalAction { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Statement", IsNullable = true)]
        public Statement[] Statement { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileUCCFilingsOriginalUCCFilingsInfo
    {
        public string FilingState { get; set; }
        public string DocumentNumber { get; set; }
        public string DateFiled { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("LegalAction", IsNullable = true)]
        public LegalAction[] LegalAction { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileUCCFilingsCollateralCodesCollateral
    {
        [System.Xml.Serialization.XmlElementAttribute("Collateral", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileUCCFilingsCollateralCodesCollateralValues[] Collateral { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileUCCFilingsCollateralCodesCollateralValues
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformation
    {
        public string Name { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("NameFlag", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformationNameFlag[] NameFlag { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Title", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformationTitle[] Title { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformationNameFlag
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformationTitle
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileCorporateLinkage
    {
        public string LinkageRecordBIN { get; set; }
        public string LinkageCompanyName { get; set; }
        public string LinkageCompanyCity { get; set; }
        public string LinkageCompanyState { get; set; }
        public string LinkageCountryCode { get; set; }
        public string LinkageCompanyAddress { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("LinkageRecordType", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileCorporateLinkageLinkageRecordType[] LinkageRecordType { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ReturnLimitExceeded")]
        public NetConnectResponseProductsPremierProfileCorporateLinkageReturnLimitExceeded[] ReturnLimitExceeded { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("MatchingBusinessIndicator", IsNullable = true)]
        public MatchingBusinessIndicator[] MatchingBusinessIndicator { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileCorporateLinkageLinkageRecordType
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileCorporateLinkageReturnLimitExceeded
    {
        private string codeField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileBusinessFacts
    {
        public string FileEstablishedDate { get; set; }
        public string YearsInBusiness { get; set; }
        public string SalesRevenue { get; set; }
        public string EmployeeSize { get; set; }
        public string StateOfIncorporation { get; set; }
        public string DateOfIncorporation { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("FileEstablishedFlag", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileBusinessFactsFileEstablishedFlag[] FileEstablishedFlag { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("BusinessTypeIndicator", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileBusinessFactsBusinessTypeIndicator[] BusinessTypeIndicator { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("YearsInBusinessIndicator", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileBusinessFactsYearsInBusinessIndicator[] YearsInBusinessIndicator { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("SalesIndicator")]
        public NetConnectResponseProductsPremierProfileBusinessFactsSalesIndicator[] SalesIndicator { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("EmployeeSizeIndicator")]
        public NetConnectResponseProductsPremierProfileBusinessFactsEmployeeSizeIndicator[] EmployeeSizeIndicator { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("PublicIndicator")]
        public NetConnectResponseProductsPremierProfileBusinessFactsPublicIndicator[] PublicIndicator { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("NonProfitIndicator", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileBusinessFactsNonProfitIndicator[] NonProfitIndicator { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileBusinessFactsFileEstablishedFlag
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileBusinessFactsBusinessTypeIndicator
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileBusinessFactsYearsInBusinessIndicator
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileBusinessFactsSalesIndicator
    {
        private string codeField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileBusinessFactsEmployeeSizeIndicator
    {
        private string codeField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileBusinessFactsPublicIndicator
    {
        private string codeField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileBusinessFactsNonProfitIndicator
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileSICCodesSIC
    {
        public NetConnectResponseProductsPremierProfileSICCodesSICValues SIC { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileSICCodesSICValues
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileNAICSCodesNAICS
    {
        public NetConnectResponseProductsPremierProfileNAICSCodesNAICSValues NAICS { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileNAICSCodesNAICSValues
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileCompetitors
    {
        public string CompetitorName { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileCommercialFraudShieldSummary
    {
        public string BusinessBIN { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("MatchingBusinessIndicator", IsNullable = true)]
        public MatchingBusinessIndicator[] MatchingBusinessIndicator { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ActiveBusinessIndicator", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryActiveBusinessIndicator[] ActiveBusinessIndicator { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("OFACMatchCode")]
        public NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryOFACMatchCode[] OFACMatchCode { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("BusinessVictimStatementIndicator", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryBusinessVictimStatementIndicator[] BusinessVictimStatementIndicator { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("BusinessRiskTriggersIndicator", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryBusinessRiskTriggersIndicator[] BusinessRiskTriggersIndicator { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("NameAddressVerificationIndicator", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryNameAddressVerificationIndicator[] NameAddressVerificationIndicator { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryActiveBusinessIndicator
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryOFACMatchCode
    {
        private string codeField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryBusinessVictimStatementIndicator
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryBusinessRiskTriggersIndicator
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryNameAddressVerificationIndicator
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileInquiry
    {
        public string InquiryBusinessCategory { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("InquiryCount")]
        public NetConnectResponseProductsPremierProfileInquiryInquiryCount[] InquiryCount { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileInquiryInquiryCount
    {
        public string Date { get; set; }
        public string Count { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileIntelliscoreScoreInformation
    {
        public string Filler { get; set; }
        public string ProfileNumber { get; set; }
        public string PercentileRanking { get; set; }
        public string Action { get; set; }
        public string RiskClass { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("PubliclyHeldCompany")]
        public NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationPubliclyHeldCompany[] PubliclyHeldCompany { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("LimitedProfile")]
        public NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationLimitedProfile[] LimitedProfile { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ScoreInfo")]
        public NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationScoreInfo[] ScoreInfo { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ModelInformation")]
        public NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationModelInformation[] ModelInformation { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationPubliclyHeldCompany
    {
        private string codeField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationLimitedProfile
    {
        private string codeField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationScoreInfo
    {
        public string Score { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationModelInformation
    {
        public string ModelCode { get; set; }
        public string ModelTitle { get; set; }
        public string CustomModelCode { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileScoreFactors
    {
        public string ModelCode { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ScoreFactor", IsNullable = true)]
        public NetConnectResponseProductsPremierProfileScoreFactorsScoreFactor[] ScoreFactor { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileScoreFactorsScoreFactor
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileScoreTrendsCreditLimit
    {
        public string CreditLimitAmount { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("MostRecentQuarter")]
        public MostRecentQuarter[] MostRecentQuarter { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("PriorQuarter")]
        public PriorQuarter[] PriorQuarter { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class NetConnectResponseProductsPremierProfileBillingIndicator
    {
        private string codeField;
        private string valueField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }


    [XmlRoot(ElementName = "AdditionalSICCodes")]
    public class DemographicInformationAdditionalSICCodes
    {
        [XmlElement(ElementName = "SICCode")]
        public string SICCode { get; set; }
    }

    [XmlRoot(ElementName = "YearsInBusinessIndicator")]
    public class DemographicInformationYearsInBusinessIndicator
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "SalesIndicator")]
    public class DemographicInformationSalesIndicator
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "ProfitLossIndicator")]
    public class DemographicInformationProfitLossIndicator
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "ProfitLossCode")]
    public class DemographicInformationProfitLossCode
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "NetWorthIndicator")]
    public class DemographicInformationNetWorthIndicator
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "Modifier")]
    public class DemographicInformationModifier
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "NetWorthAmountOrLowRange")]
    public class DemographicInformationNetWorthAmountOrLowRange
    {
        [XmlElement(ElementName = "Modifier")]
        public DemographicInformationModifier Modifier { get; set; }
        [XmlElement(ElementName = "Amount")]
        public string Amount { get; set; }
    }

    [XmlRoot(ElementName = "HighRangeOrNetWorth")]
    public class DemographicInformationHighRangeOrNetWorth
    {
        [XmlElement(ElementName = "Modifier")]
        public DemographicInformationModifier Modifier { get; set; }
        [XmlElement(ElementName = "Amount")]
        public string Amount { get; set; }
    }

    [XmlRoot(ElementName = "EmployeeIndicator")]
    public class DemographicInformationEmployeeIndicator
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "BuildingOwnership")]
    public class DemographicInformationBuildingOwnership
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "Ownership")]
    public class DemographicInformationOwnership
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "Location")]
    public class DemographicInformationLocation
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }

    [XmlRoot(ElementName = "BusinessType")]
    public class DemographicInformationBusinessType
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "DemographicInformation")]
    public class NetConnectResponseProductsPremierProfileDemographicInformation
    {
        [XmlElement(ElementName = "PrimarySICCode")]
        public string PrimarySICCode { get; set; }
        [XmlElement(ElementName = "SICDescription")]
        public string SICDescription { get; set; }
        [XmlElement(ElementName = "SecondarySICCode")]
        public string SecondarySICCode { get; set; }
        [XmlElement(ElementName = "SecondarySICDescription")]
        public string SecondarySICDescription { get; set; }
        [XmlElement(ElementName = "AdditionalSICCodes")]
        public DemographicInformationAdditionalSICCodes AdditionalSICCodes { get; set; }
        [XmlElement(ElementName = "YearsInBusinessIndicator")]
        public DemographicInformationYearsInBusinessIndicator YearsInBusinessIndicator { get; set; }
        [XmlElement(ElementName = "YearsInBusinessOrLowRange")]
        public string YearsInBusinessOrLowRange { get; set; }
        [XmlElement(ElementName = "HighRangeYears")]
        public string HighRangeYears { get; set; }
        [XmlElement(ElementName = "SalesIndicator")]
        public DemographicInformationSalesIndicator SalesIndicator { get; set; }
        [XmlElement(ElementName = "SalesRevenueOrLowRange")]
        public string SalesRevenueOrLowRange { get; set; }
        [XmlElement(ElementName = "HighRangeOfSales")]
        public string HighRangeOfSales { get; set; }
        [XmlElement(ElementName = "ProfitLossIndicator")]
        public DemographicInformationProfitLossIndicator ProfitLossIndicator { get; set; }
        [XmlElement(ElementName = "ProfitLossCode")]
        public DemographicInformationProfitLossCode ProfitLossCode { get; set; }
        [XmlElement(ElementName = "ProfitAmountOrLowRange")]
        public string ProfitAmountOrLowRange { get; set; }
        [XmlElement(ElementName = "HighRangeOfProfit")]
        public string HighRangeOfProfit { get; set; }
        [XmlElement(ElementName = "NetWorthIndicator")]
        public DemographicInformationNetWorthIndicator NetWorthIndicator { get; set; }
        [XmlElement(ElementName = "NetWorthAmountOrLowRange")]
        public DemographicInformationNetWorthAmountOrLowRange NetWorthAmountOrLowRange { get; set; }
        [XmlElement(ElementName = "HighRangeOrNetWorth")]
        public DemographicInformationHighRangeOrNetWorth HighRangeOrNetWorth { get; set; }
        [XmlElement(ElementName = "EmployeeIndicator")]
        public DemographicInformationEmployeeIndicator EmployeeIndicator { get; set; }
        [XmlElement(ElementName = "EmployeeSizeOrLowRange")]
        public string EmployeeSizeOrLowRange { get; set; }
        [XmlElement(ElementName = "HighEmployeeRange")]
        public string HighEmployeeRange { get; set; }
        [XmlElement(ElementName = "InBuildingDate")]
        public string InBuildingDate { get; set; }
        [XmlElement(ElementName = "BuildingSize")]
        public string BuildingSize { get; set; }
        [XmlElement(ElementName = "BuildingOwnership")]
        public DemographicInformationBuildingOwnership BuildingOwnership { get; set; }
        [XmlElement(ElementName = "Ownership")]
        public DemographicInformationOwnership Ownership { get; set; }
        [XmlElement(ElementName = "Location")]
        public DemographicInformationLocation Location { get; set; }
        [XmlElement(ElementName = "BusinessType")]
        public DemographicInformationBusinessType BusinessType { get; set; }
        [XmlElement(ElementName = "CustomerCount")]
        public string CustomerCount { get; set; }
        [XmlElement(ElementName = "DateFounded")]
        public string DateFounded { get; set; }
    }


    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.experian.com/ARFResponse", IsNullable = false)]
    public partial class NewDataSet
    {
        private object[] itemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AccountBalance", typeof(AccountBalance))]
        [System.Xml.Serialization.XmlElementAttribute("CurrentMonth", typeof(CurrentMonth))]
        [System.Xml.Serialization.XmlElementAttribute("LegalAction", typeof(LegalAction), IsNullable = true)]
        [System.Xml.Serialization.XmlElementAttribute("LegalType", typeof(LegalType), IsNullable = true)]
        [System.Xml.Serialization.XmlElementAttribute("MatchingBusinessIndicator", typeof(MatchingBusinessIndicator), IsNullable = true)]
        [System.Xml.Serialization.XmlElementAttribute("Modifier", typeof(Modifier))]
        [System.Xml.Serialization.XmlElementAttribute("MostRecentQuarter", typeof(MostRecentQuarter))]
        [System.Xml.Serialization.XmlElementAttribute("NetConnectResponse", typeof(NetConnectResponse))]
        [System.Xml.Serialization.XmlElementAttribute("NewlyReportedIndicator", typeof(NewlyReportedIndicator))]
        [System.Xml.Serialization.XmlElementAttribute("PriorMonth", typeof(PriorMonth))]
        [System.Xml.Serialization.XmlElementAttribute("PriorQuarter", typeof(PriorQuarter))]
        [System.Xml.Serialization.XmlElementAttribute("QuarterWithinYear", typeof(QuarterWithinYear), IsNullable = true)]
        [System.Xml.Serialization.XmlElementAttribute("RecentHighCredit", typeof(RecentHighCredit))]
        [System.Xml.Serialization.XmlElementAttribute("TotalAccountBalance", typeof(TotalAccountBalance))]
        [System.Xml.Serialization.XmlElementAttribute("TotalHighCreditAmount", typeof(TotalHighCreditAmount))]
        [System.Xml.Serialization.XmlElementAttribute("TradeLineFlag", typeof(TradeLineFlag))]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }
}