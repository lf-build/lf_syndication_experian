﻿using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
using RestSharp;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian.Proxy
{
    public class ExperianProxy : IExperianProxy
    {
        public ExperianProxy(IExperianConfiguration reportConfiguration, IConfigurationService<ExperianConfiguration> configurationServiceFactory, IProcessProxy processProxy, string userName, string password, ILogger logger)
        {
            ConfigurationServiceFactory = configurationServiceFactory;
            UserName = userName;
            Password = password;
            ReportConfiguration = reportConfiguration;
            ProcessProxy = processProxy;
            Logger = logger;
        }

        public IExperianConfiguration ReportConfiguration { get; }
        private IConfigurationService<ExperianConfiguration> ConfigurationServiceFactory { get; }
        private string UserName { get; }
        private string Password { get; set; }
        private IProcessProxy ProcessProxy { get; }
        private ILogger Logger { get; }

        public string ResetPassword(string password)
        {
            var postData = "&newpassword=" + password + "& command=resetpassword&application=netconnect";
            return ExecuteRequest<string>(ReportConfiguration.PasswordResetURL, Method.POST, postData).Result;
        }

        public string RequestNewPassword()
        {
            var postData = "command=requestnewpassword&application=netconnect";
            return ExecuteRequest<string>(ReportConfiguration.PasswordResetURL, Method.POST, postData).Result;
        }

        public async Task<TResponse> ExecuteRequest<TResponse>(string url, Method method, string request)
        {
            var restRequest = new RestRequest(method);
            var uri = new Uri(url);

            var userAuthorization = UserName + ":" + Password;
            restRequest.AddHeader("Authorization", "BASIC " + Convert.ToBase64String(Encoding.ASCII.GetBytes(userAuthorization)));

            if (method == Method.POST && !string.IsNullOrEmpty(request))
                restRequest.AddParameter("application/x-www-form-urlencoded", request, ParameterType.RequestBody);

            var client = new RestClient(uri);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            ServicePointManager.ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) =>
            {
                if (sslPolicyErrors != SslPolicyErrors.None)
                    return false;

                var certDetails = new X509Certificate2(cert);
                if (certDetails.NotAfter.Date < DateTime.Now.Date || certDetails.NotBefore.Date >= DateTime.Now.Date)
                    return false;
                var sn = certDetails.GetNameInfo(X509NameType.SimpleName, false);
                var uriFromCert = new Uri(url);
                if (sn != uriFromCert.Host)
                    return false;

                return true;
            };


            var response = await client.ExecuteTaskAsync(restRequest);

            if (response.ErrorException != null)
                throw new Exception(response.ErrorMessage);

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK)
                throw new Exception(response.ErrorMessage, new Exception(response.Content));

            if (typeof(TResponse) == typeof(string))
            {
                return (TResponse)Convert.ChangeType(response.Content, typeof(TResponse));
            }
            return XmlSerialization.Deserialize<TResponse>(response.Content);
        }

        public string EcalsTransactionCheck()
        {
            var result = ExecuteRequest<string>(ReportConfiguration.EcalsTransactionUrl, Method.POST, null).Result;
            var uri = new Uri(result);

            if (!uri.Host.EndsWith("." + ReportConfiguration.ExperianHostName))
                throw new InvalidDataException("Can not proceed the request, ECALSTransactionCheck is not validated");
            if (ReportConfiguration.RequiredCertificateVerification)
            {
                if (!VerifyCertificate(uri.Host).Result)
                    throw new InvalidDataException("Certificate is not validated");
            }
            return result;
        }

        public void ResetBusinessPasswordIfMisMatch(string password, string hashPassword)
        {
            if (hashPassword != Hash(password))
            {
                var newPassword = RequestNewPassword();
                ResetPassword(newPassword);
                var configurationdata = ConfigurationServiceFactory.Get();
                configurationdata.businessReportConfiguration.NetConnectPassword = newPassword;
                configurationdata.businessReportConfiguration.NetConnectPasswordHash = Hash(newPassword);
                ConfigurationServiceFactory.Set(configurationdata);
                Password = newPassword;
            }
        }

        public void ResetPersonalPasswordIfMisMatch(string password, string hashPassword)
        {
            if (hashPassword != Hash(password))
            {
                var newPassword = RequestNewPassword();
                ResetPassword(newPassword);
                var configurationdata = ConfigurationServiceFactory.Get();
                configurationdata.personalReportConfiguration.NetConnectPassword = newPassword;
                configurationdata.personalReportConfiguration.NetConnectPasswordHash = Hash(newPassword);
                ConfigurationServiceFactory.Set(configurationdata);
                Password = newPassword;
            }
        }

        public string Hash(string password)
        {
            var bytes = new UTF8Encoding().GetBytes(password);
            var hashBytes = System.Security.Cryptography.MD5.Create().ComputeHash(bytes);
            return Convert.ToBase64String(hashBytes);
        }

        public async Task<bool> VerifyCertificate(string host)
        {
            ProcessProxy.StartInfo = new ProcessStartInfo
            {
                FileName = "openssl",
                Arguments = $"s_client -showcerts -connect {host}:443",
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                RedirectStandardInput = true
            };

            ProcessProxy.EnableRaisingEvents = true;

            var output = "";
            ProcessProxy.OutputDataReceived += (s, e) => output = output + e.Data;
            ProcessProxy.ErrorDataReceived += (s, e) => Logger.Error(e.Data);

            var started = ProcessProxy.Start();

            if (!started)
            {
                throw new InvalidOperationException($"Could not start process {ProcessProxy}");
            }

            ProcessProxy.BeginOutputReadLine();
            ProcessProxy.BeginErrorReadLine();

            var noOfTries = 0;
            var certificateVerified = false;
            await Task.Run(() =>
            {
                while (true)
                {
                    if (output.Contains("Verify return code: 0 (ok)"))
                    {
                        using (var stream = ProcessProxy.StandardInput)
                        {
                            var buffer = Encoding.UTF8.GetBytes("Q");
                            stream.BaseStream.Write(buffer, 0, buffer.Length);
                            stream.WriteLine();
                        }
                        if (!ProcessProxy.HasExited)
                            ProcessProxy.WaitForExit(5000);

                        certificateVerified = true;
                        break;
                    }
                    noOfTries++;
                    if (noOfTries == 3)
                        break;
                    Task.Delay(5000).Wait();
                }
            });


            return certificateVerified;

        }
    }
}
