﻿using System;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.EventHub;
using LendFoundry.Configuration;

namespace LendFoundry.Syndication.Experian.Proxy
{
    public class BusinessReportProxyFactory : IBusinessReportProxyFactory
    {
        public BusinessReportProxyFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public IBusinessReportProxy Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();

            var configurationService = configurationServiceFactory.Create<ExperianConfiguration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var processProxyFactory = Provider.GetService<IProcessProxyFactory>();
            var processProxy = processProxyFactory.Create();

            return new BusinessReportProxy(configuration, configurationService, processProxy, logger);
        }
    }
}



