﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Experian.Proxy
{
    public interface IProcessProxyFactory
    {
        IProcessProxy Create();
    }
}
