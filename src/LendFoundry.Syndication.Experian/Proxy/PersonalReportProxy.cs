using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
using LendFoundry.Syndication.Experian.Proxy.PersonalReportRequest;
using LendFoundry.Syndication.Experian.Proxy.PersonalReportResponse;
using RestSharp;
using System;
using System.Net;
using System.Text;

namespace LendFoundry.Syndication.Experian.Proxy
{
    public class PersonalReportProxy : ExperianProxy, IPersonalReportProxy
    {
        public PersonalReportProxy(IExperianConfiguration reportConfiguration, IConfigurationService<ExperianConfiguration> configurationService, IProcessProxy processProxy, ILogger logger)
  : base(reportConfiguration, configurationService, processProxy, reportConfiguration.personalReportConfiguration.NetConnectSubscriber, reportConfiguration.personalReportConfiguration.NetConnectPassword, logger)
        {
            if (reportConfiguration == null)
                throw new ArgumentNullException(nameof(reportConfiguration));

            if (reportConfiguration.personalReportConfiguration == null)
                throw new ArgumentNullException(nameof(reportConfiguration.personalReportConfiguration));

            if (string.IsNullOrWhiteSpace(reportConfiguration.personalReportConfiguration.NetConnectSubscriber))
                throw new ArgumentException("Net connect subscriber is required",
                    nameof(reportConfiguration.personalReportConfiguration.NetConnectSubscriber));

            if (string.IsNullOrWhiteSpace(reportConfiguration.personalReportConfiguration.NetConnectPassword))
                throw new ArgumentException("Net connect password is required",
                    nameof(reportConfiguration.personalReportConfiguration.NetConnectPassword));

            if (string.IsNullOrWhiteSpace(reportConfiguration.personalReportConfiguration.NetConnectPasswordHash))
                throw new ArgumentException("Net Connect Password Hash is required",
                    nameof(reportConfiguration.personalReportConfiguration.NetConnectPasswordHash));

            if (string.IsNullOrWhiteSpace(reportConfiguration.personalReportConfiguration.Eai))
                throw new ArgumentException("Eai is required", nameof(reportConfiguration.personalReportConfiguration.Eai));

            if (string.IsNullOrWhiteSpace(reportConfiguration.personalReportConfiguration.ArfVersion))
                throw new ArgumentException("ArfVersion is required", nameof(reportConfiguration.personalReportConfiguration.ArfVersion));

            if (string.IsNullOrWhiteSpace(reportConfiguration.personalReportConfiguration.DbHost))
                throw new ArgumentException("DbHost is required", nameof(reportConfiguration.personalReportConfiguration.DbHost));

            if (string.IsNullOrWhiteSpace(reportConfiguration.personalReportConfiguration.OpInitials))
                throw new ArgumentException("OpInitials is required", nameof(reportConfiguration.personalReportConfiguration.OpInitials));

            if (string.IsNullOrWhiteSpace(reportConfiguration.personalReportConfiguration.Preamble))
                throw new ArgumentException("Preamble is required", nameof(reportConfiguration.personalReportConfiguration.Preamble));

            if (string.IsNullOrWhiteSpace(reportConfiguration.personalReportConfiguration.SubCode))
                throw new ArgumentException("SubCode is required", nameof(reportConfiguration.personalReportConfiguration.SubCode));

            if (string.IsNullOrWhiteSpace(reportConfiguration.personalReportConfiguration.VendorNumber))
                throw new ArgumentException("VendorNumber is required", nameof(reportConfiguration.personalReportConfiguration.VendorNumber));

            ProcessProxy = processProxy;
            Logger = logger;
        }

        private IProcessProxy ProcessProxy { get; set; }
        private ILogger Logger { get; set; }
        public NetConnectResponse GetPersonalReport(NetConnectRequest personalReportRequest)
        {
            if (personalReportRequest == null)
                throw new ArgumentNullException(nameof(personalReportRequest));

            ResetPersonalPasswordIfMisMatch(ReportConfiguration.personalReportConfiguration.NetConnectPassword, ReportConfiguration.personalReportConfiguration.NetConnectPasswordHash);

            var netConnectUrl = EcalsTransactionCheck();

            var xml = XmlConvertor.Serialize(personalReportRequest, Encoding.UTF8);
            var postData = "NETCONNECT_TRANSACTION=" + WebUtility.UrlEncode(xml);

            var response = ExecuteRequest<NetConnectResponse>(netConnectUrl, Method.POST, postData).Result;

            return response;


        }

    }
}
