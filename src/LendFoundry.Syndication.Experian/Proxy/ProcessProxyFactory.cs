using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using System.Diagnostics;
using System.IO;

namespace LendFoundry.Syndication.Experian.Proxy
{
    public class ProcessProxyFactory : IProcessProxyFactory
    {
        public ProcessProxyFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public IProcessProxy Create()
        {
            return new ProcessProxy();
        }
    }
}