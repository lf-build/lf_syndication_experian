using System;
using System.Diagnostics;
using System.IO;

namespace LendFoundry.Syndication.Experian.Proxy
{
    public class ProcessProxy : IProcessProxy
    {
        private bool _disposed;

        public ProcessProxy()
        {
            Process = new Process();
        }

        private Process Process { get; }

        public event DataReceivedEventHandler ErrorDataReceived
        {
            add { Process.ErrorDataReceived += value; }
            remove { Process.ErrorDataReceived -= value; }
        }

        public int ExitCode => Process.ExitCode;

        public ProcessStartInfo StartInfo
        {
            get { return Process.StartInfo; }
            set { Process.StartInfo = value; }
        }

        public StreamWriter StandardInput => Process.StandardInput;

        public bool HasExited => Process.HasExited;

        public bool EnableRaisingEvents
        {
            get { return Process.EnableRaisingEvents; }
            set { Process.EnableRaisingEvents = value; }
        }

        public event EventHandler Exited
        {
            add { Process.Exited += value; }
            remove { Process.Exited -= value; }
        }

        public event DataReceivedEventHandler OutputDataReceived
        {
            add { Process.OutputDataReceived += value; }
            remove { Process.OutputDataReceived -= value; }
        }

        public bool Start()
        {
            return Process.Start();
        }

        public void Kill()
        {
            Process.Kill();
        }

        public void BeginOutputReadLine()
        {
            Process.BeginOutputReadLine();
        }

        public void BeginErrorReadLine()
        {
            Process.BeginErrorReadLine();
        }

        public bool WaitForExit(int milliseconds)
        {
            return Process.WaitForExit(milliseconds);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Process?.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public override string ToString()
        {
            return Process.ToString();
        }
    }
}