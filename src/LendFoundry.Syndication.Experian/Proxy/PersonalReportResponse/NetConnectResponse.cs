﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Experian.Proxy.PersonalReportResponse
{
    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/NetConnectResponse")]
    [XmlRoot(Namespace = "http://www.experian.com/NetConnectResponse", IsNullable = false)]
    public partial class NetConnectResponse
    {
        public string CompletionCode { get; set; }
        public string ReferenceId { get; set; }
        public string TransactionId { get; set; }

        [XmlElement(Namespace = "http://www.experian.com/ARFResponse")]
        public Products Products { get; set; }

        public string ErrorMessage { get; set; }
        public string ErrorTag { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    [XmlRoot(Namespace = "http://www.experian.com/ARFResponse", IsNullable = false)]
    public partial class Products
    {
        public ProductsCreditProfile CustomSolution { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfile
    {
        public ProductsCreditProfileHeader Header { get; set; }

        [XmlElement("RiskModel")]
        public ProductsCreditProfileRiskModel[] RiskModel { get; set; }

        [XmlElement("SSN")]
        public ProductsCreditProfileSSN[] SSN { get; set; }

        [XmlElement("ConsumerIdentity")]
        public ProductsCreditProfileConsumerIdentity[] ConsumerIdentity { get; set; }

        [XmlElement("AddressInformation")]
        public ProductsCreditProfileAddressInformation[] AddressInformation { get; set; }

        [XmlElement("EmploymentInformation")]
        public ProductsCreditProfileEmploymentInformation[] EmploymentInformation { get; set; }

        [XmlElement("TradeLine")]
        public ProductsCreditProfileTradeLine[] TradeLine { get; set; }

        public ProductsCreditProfileConsumerAssistanceReferralAddress ConsumerAssistanceReferralAddress { get; set; }

        [XmlElement("PublicRecord")]
        public ProductsCreditProfilePublicRecord[] PublicRecord { get; set; }

        [XmlElement("Inquiry")]
        public ProductsCreditProfileInquiry[] Inquiry { get; set; }

        [XmlElement("Statement")]
        public ProductsCreditProfileStatement Statement { get; set; }

        [XmlElement("InformationalMessage")]
        public ProductsCreditProfileInformationalMessage[] InformationalMessage { get; set; }

        [XmlElement("FraudServices")]
        public ProductsCreditProfileFraudServices[] FraudServices { get; set; }

        [XmlElement("ProfileSummary")]
        public ProductsCreditProfileProfileSummary ProfileSummary { get; set; }


        [XmlElement("PremierAttributes")]
        public ProductsCreditProfilePremierAttributes PremierAttributes { get; set; }

        [XmlElement("ProcessingMessage")]
        public ProcessingMessage ProcessingMessage { get; set; }

        [XmlElement("DirectCheck")]
        public DirectCheck[] DirectCheck { get; set; }

        [XmlElement("Error")]
        public Error Error { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProcessingMessage
    {
        public string ProcessingAction { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfilePremierAttributes
    {
        [XmlElement("Message")]
        public Message Message { get; set; }
        [XmlElement("Attributes")]
        public Attributes Attributes { get; set; }

    }
    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]

    public partial class Attributes
    {
        [XmlElement("Attribute")]

        public List<Attribute> Attribute { get; set; }

    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class Attribute
    {
        [XmlElement("Name")]
        public string Name { get; set; }

        [XmlElement("Value")]
        public string Value { get; set; }

    }
    public class Message
    {
        private string codeField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

    }
    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileHeader
    {
        public string ReportDate { get; set; }
        public string ReportTime { get; set; }
        public string Preamble { get; set; }
        public string ARFVersion { get; set; }
        public string ReferenceNumber { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileRiskModel
    {
        public ProductsCreditProfileRiskModelModelIndicator ModelIndicator { get; set; }
        public string Score { get; set; }
        public string ScoreFactorCodeOne { get; set; }
        public string ScoreFactorCodeTwo { get; set; }
        public string ScoreFactorCodeThree { get; set; }
        public string ScoreFactorCodeFour { get; set; }
        public ProductsCreditProfileRiskModelEvaluation Evaluation { get; set; }
        public string ScorePercentile { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileRiskModelModelIndicator
    {
        private string codeField;
        private string valueField;

        /// <remarks/>
        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileRiskModelEvaluation
    {
        private string codeField;
        private string valueField;

        /// <remarks/>
        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileSSN
    {
        public ProductsCreditProfileSSNVariationIndicator VariationIndicator { get; set; }
        public string Number { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileSSNVariationIndicator
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileConsumerIdentity
    {
        public ProductsCreditProfileConsumerIdentityName Name { get; set; }
        public string YOB { get; set; }
        public string DOB { get; set; }
        public ProductsCreditProfileConsumerIdentityPhone Phone { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ConsumerIdentityPhoneSource
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }
    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileConsumerIdentityName
    {
        public string Surname { get; set; }
        public string First { get; set; }
        public string Middle { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileConsumerIdentityPhone
    {
        public ProductsCreditProfileConsumerIdentityPhoneType Type { get; set; }
        public string Number { get; set; }
        public ProductsCreditProfileConsumerIdentityPhoneSource Source { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileConsumerIdentityNameType
    {
        private string codeField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }
    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileConsumerIdentityPhoneType
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
        [XmlText()]
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }
    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileConsumerIdentityPhoneSource
    {
        private string codeField;
        private string valueField;


        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
        [XmlText()]
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileAddressInformation
    {
        public string FirstReportedDate { get; set; }
        public string LastUpdatedDate { get; set; }
        public ProductsCreditProfileAddressInformationOrigination Origination { get; set; }
        public string TimesReported { get; set; }
        public string LastReportingSubcode { get; set; }
        public ProductsCreditProfileAddressInformationDwellingType DwellingType { get; set; }
        public ProductsCreditProfileAddressInformationHomeOwnership HomeOwnership { get; set; }
        public string StreetPrefix { get; set; }
        public string StreetName { get; set; }
        public string StreetSuffix { get; set; }
        public string UnitType { get; set; }
        public string UnitID { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string CensusGeoCode { get; set; }
        public string CountyCode { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileAddressInformationOrigination
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileAddressInformationDwellingType
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileAddressInformationHomeOwnership
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileEmploymentInformation
    {
        public string FirstReportedDate { get; set; }
        public string LastUpdatedDate { get; set; }
        public ProductsCreditProfileEmploymentInformationOrigination Origination { get; set; }
        public string Name { get; set; }
        public string AddressFirstLine { get; set; }
        public string AddressSecondLine { get; set; }
        public string AddressExtraLine { get; set; }
        public string Zip { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileEmploymentInformationOrigination
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    public class MonthlyPaymentType
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLine
    {
        public ProductsCreditProfileTradeLineSpecialComment SpecialComment { get; set; }
        public ProductsCreditProfileTradeLineEvaluation Evaluation { get; set; }
        public string OpenDate { get; set; }
        public string StatusDate { get; set; }
        public string MaxDelinquencyDate { get; set; }
        public ProductsCreditProfileTradeLineAccountType AccountType { get; set; }
        public ProductsCreditProfileTradeLineTermsDuration TermsDuration { get; set; }
        public ProductsCreditProfileTradeLineECOA ECOA { get; set; }

        [XmlElement("Amount")]
        public ProductsCreditProfileTradeLineAmount[] Amount { get; set; }

        public string BalanceDate { get; set; }
        public string BalanceAmount { get; set; }
        public ProductsCreditProfileTradeLineStatus Status { get; set; }
        public string AmountPastDue { get; set; }
        public ProductsCreditProfileTradeLineOpenOrClosed OpenOrClosed { get; set; }
        public ProductsCreditProfileTradeLineRevolvingOrInstallment RevolvingOrInstallment { get; set; }
        public string ConsumerComment { get; set; }
        public string AccountNumber { get; set; }
        public string MonthsHistory { get; set; }
        public string DelinquenciesOver30Days { get; set; }
        public string DelinquenciesOver60Days { get; set; }
        public string DelinquenciesOver90Days { get; set; }
        public string DerogCounter { get; set; }
        public string PaymentProfile { get; set; }
        public string MonthlyPaymentAmount { get; set; }
        public MonthlyPaymentType MonthlyPaymentType { get; set; }
        public string LastPaymentDate { get; set; }
        public string Subcode { get; set; }
        public ProductsCreditProfileTradeLineKOB KOB { get; set; }
        public string SubscriberDisplayName { get; set; }
        public ProductsCreditProfileTradeLineEnhancedPaymentData EnhancedPaymentData { get; set; }
        public string OriginalCreditorName { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineSpecialComment
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineEvaluation
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineAccountType
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineTermsDuration
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineECOA
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineAmount
    {
        public ProductsCreditProfileTradeLineAmountQualifier Qualifier { get; set; }
        public string Value { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineAmountQualifier
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineStatus
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineOpenOrClosed
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineRevolvingOrInstallment
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineKOB
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineEnhancedPaymentData
    {
        public string InitialPaymentLevelDate { get; set; }
        public ProductsCreditProfileTradeLineEnhancedPaymentDataAccountCondition AccountCondition { get; set; }
        public ProductsCreditProfileTradeLineEnhancedPaymentDataPaymentStatus PaymentStatus { get; set; }
        public ProductsCreditProfileTradeLineEnhancedPaymentDataAccountType AccountType { get; set; }
        public ProductsCreditProfileTradeLineEnhancedPaymentDataSpecialComment SpecialComment { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineEnhancedPaymentDataAccountCondition
    {
        private string codeField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineEnhancedPaymentDataPaymentStatus
    {
        private string codeField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineEnhancedPaymentDataAccountType
    {
        private string codeField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileTradeLineEnhancedPaymentDataSpecialComment
    {
        private string codeField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileDirectCheck
    {
        public string SubscriberNumber { get; set; }
        public string SubscriberName { get; set; }
        public string SubscriberAddress { get; set; }
        public string SubscriberCity { get; set; }
        public string SubscriberState { get; set; }
        public string SubscriberZip { get; set; }
        public string SubscriberPhone { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileConsumerAssistanceReferralAddress
    {
        public string OfficeName { get; set; }
        public string StreetName { get; set; }
        public string POBox { get; set; }
        public string CityStateZip { get; set; }
        public string Phone { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileProfileSummary
    {
        public string DisputedAccountsExcluded { get; set; }
        public string PublicRecordsCount { get; set; }
        public string InstallmentBalance { get; set; }
        public string RealEstateBalance { get; set; }
        public string RevolvingBalance { get; set; }
        public string PastDueAmount { get; set; }
        public string MonthlyPayment { get; set; }
        public ProductsCreditProfileProfileSummaryMonthlyPaymentPartialFlag MonthlyPaymentPartialFlag { get; set; }
        public string RealEstatePayment { get; set; }
        public ProductsCreditProfileProfileSummaryRealEstatePaymentPartialFlag RealEstatePaymentPartialFlag { get; set; }
        public string RevolvingAvailablePercent { get; set; }
        public ProductsCreditProfileProfileSummaryRevolvingAvailablePartialFlag RevolvingAvailablePartialFlag { get; set; }
        public string TotalInquiries { get; set; }
        public string InquiriesDuringLast6Months { get; set; }
        public string TotalTradeItems { get; set; }
        public string PaidAccounts { get; set; }
        public string SatisfactoryAccounts { get; set; }
        public string NowDelinquentDerog { get; set; }
        public string WasDelinquentDerog { get; set; }
        public string OldestTradeOpenDate { get; set; }
        public string DelinquenciesOver30Days { get; set; }
        public string DelinquenciesOver60Days { get; set; }
        public string DelinquenciesOver90Days { get; set; }
        public string DerogCounter { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileProfileSummaryMonthlyPaymentPartialFlag
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileProfileSummaryRealEstatePaymentPartialFlag
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileProfileSummaryRevolvingAvailablePartialFlag
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfilePublicRecord
    {
        public ProductsCreditProfilePublicRecordStatus Status { get; set; }
        public string StatusDate { get; set; }
        public string FilingDate { get; set; }
        public ProductsCreditProfilePublicRecordEvaluation Evaluation { get; set; }
        public string Amount { get; set; }
        public string ConsumerComment { get; set; }
        public ProductsCreditProfilePublicRecordCourt Court { get; set; }
        public string ReferenceNumber { get; set; }
        public ProductsCreditProfilePublicRecordECOA ECOA { get; set; }
        public string BookPageSequence { get; set; }
        public string PlaintiffName { get; set; }
        public ProductsCreditProfilePublicRecordBankruptcy Bankruptcy { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfilePublicRecordBankruptcy
    {
        public ProductsCreditProfilePublicRecordBankruptcyType Type { get; set; }
        public string AssetAmount { get; set; }
        public string LiabilitiesAmount { get; set; }
        public string RepaymentPercent { get; set; }
        public string AdjustmentPercent { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfilePublicRecordBankruptcyType
    {
        private string codeField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfilePublicRecordStatus
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText]
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfilePublicRecordEvaluation
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText]
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfilePublicRecordCourt
    {
        private string codeField;
        private string nameField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfilePublicRecordECOA
    {
        private string codeField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileStatement
    {
        public ProductsCreditProfileStatementType Type { get; set; }
        public string DateReported { get; set; }
        public ProductsCreditProfileStatementStatementText StatementText { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileInformationalMessage
    {
        public string MessageNumber { get; set; }
        public string MessageText { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileStatementType
    {
        private string codeField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileStatementStatementText
    {
        public string MessageText { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileInquiry
    {
        public string Date { get; set; }
        public string Amount { get; set; }
        public ProductsCreditProfileInquiryType Type { get; set; }
        public ProductsCreditProfileInquiryTerms Terms { get; set; }
        public string Subcode { get; set; }
        public ProductsCreditProfileInquiryKOB KOB { get; set; }
        public string SubscriberDisplayName { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileInquiryType
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText]
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileInquiryTerms
    {
        private string codeField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileInquiryKOB
    {
        private string codeField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileFraudServices
    {
        public ProductsCreditProfileFraudServicesType Type { get; set; }
        public ProductsCreditProfileFraudServicesSIC SIC { get; set; }
        public string Text { get; set; }
        public string SocialDate { get; set; }
        public string SocialCount { get; set; }
        public ProductsCreditProfileFraudServicesSocialErrorCode SocialErrorCode { get; set; }
        public string AddressDate { get; set; }
        public string AddressCount { get; set; }
        public ProductsCreditProfileFraudServicesAddressErrorCode AddressErrorCode { get; set; }
        public string SSNFirstPossibleIssuanceYear { get; set; }
        public string SSNLastPossibleIssuanceYear { get; set; }
        public string DateOfBirth { get; set; }
        public string DateOfDeath { get; set; }
        public string Indicator { get; set; }

    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileFraudServicesType
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText]
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileFraudServicesSIC
    {
        private string codeField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileFraudServicesSocialErrorCode
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText]
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsCreditProfileFraudServicesAddressErrorCode
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText]
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    [XmlRoot(Namespace = "http://www.experian.com/ARFResponse", IsNullable = false)]
    public class DirectCheck
    {
        [XmlElement(ElementName = "SubscriberNumber")]
        public string SubscriberNumber { get; set; }
        [XmlElement(ElementName = "SubscriberName")]
        public string SubscriberName { get; set; }
        [XmlElement(ElementName = "SubscriberAddress")]
        public string SubscriberAddress { get; set; }
        [XmlElement(ElementName = "SubscriberCity")]
        public string SubscriberCity { get; set; }
        [XmlElement(ElementName = "SubscriberState")]
        public string SubscriberState { get; set; }
        [XmlElement(ElementName = "SubscriberZip")]
        public string SubscriberZip { get; set; }
        [XmlElement(ElementName = "SubscriberPhone")]
        public string SubscriberPhone { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    [XmlRoot(Namespace = "http://www.experian.com/ARFResponse", IsNullable = false)]
    public class Error
    {
        public string HostID { get; set; }
        public string ApplicationID { get; set; }
        public string ReportDate { get; set; }
        public string ReportTime { get; set; }
        public string ReportType { get; set; }
        public string Preamble { get; set; }
        public string RegionCode { get; set; }
        public string ARFVersion { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string ErrorCode { get; set; }
        public ProductsErrorActionIndicator ActionIndicator { get; set; }
        public string ModuleID { get; set; }
        public string ReferenceNumber { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://www.experian.com/ARFResponse")]
    public partial class ProductsErrorActionIndicator
    {
        private string codeField;
        private string valueField;

        [XmlAttribute()]
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        [XmlText]
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }
}