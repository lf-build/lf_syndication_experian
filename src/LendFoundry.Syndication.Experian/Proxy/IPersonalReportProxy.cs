using LendFoundry.Syndication.Experian.Proxy.PersonalReportRequest;
using LendFoundry.Syndication.Experian.Proxy.PersonalReportResponse;

namespace LendFoundry.Syndication.Experian.Proxy
{
    public interface IPersonalReportProxy: IExperianProxy
    {
        NetConnectResponse GetPersonalReport(NetConnectRequest personalReportRequest);
    }
}