﻿using LendFoundry.Syndication.Experian.Request;
using System;

namespace LendFoundry.Syndication.Experian.Proxy.BusinessReportRequest
{
    public partial class AddOns
    {
        public AddOns()
        {
        }

        public AddOns(IBusinessReportConfiguration reportConfiguration)
        {
            if (reportConfiguration != null)
            {
                BUSP = reportConfiguration.AddOnsBUSP;
            }
        }
    }

    public partial class BusinessApplicant
    {
        public BusinessApplicant()
        {
        }

        public BusinessApplicant(IBusinessReportRequest reportRequest)
        {
            if (reportRequest != null)
            {
                BISListNumber = reportRequest.BisListNumber;
                TransactionNumber = reportRequest.TransactionNumber;
            }
        }
    }

    public partial class ListOfSimilars
    {
        public ListOfSimilars()
        {
        }

        public ListOfSimilars(IBusinessReportConfiguration reportConfiguration, IBusinessReportRequest reportRequest)
        {
            if (reportConfiguration == null)
                throw new ArgumentNullException(nameof(reportConfiguration));
            if (reportRequest != null)
            {
                Subscriber = new Subscriber(reportConfiguration);
                BusinessApplicant = new BusinessApplicant(reportRequest);
                OutputType = new OutputType(reportConfiguration);
                AddOns = new AddOns(reportConfiguration);
                Vendor = new Vendor(reportConfiguration);
            }
        }
    }

    public partial class OutputType
    {
        public OutputType()
        {
        }

        public OutputType(IBusinessReportConfiguration reportConfiguration)
        {
            if (reportConfiguration == null)
                throw new ArgumentNullException(nameof(reportConfiguration));

            XML = new XML(reportConfiguration);
        }
    }

    public partial class Products
    {
        public Products()
        {
        }

        public Products(IBusinessReportConfiguration reportConfiguration, IBusinessReportRequest reportRequest)
        {
            if (reportConfiguration == null)
                throw new ArgumentNullException(nameof(reportConfiguration));
            if (reportRequest != null)
            {
                ListOfSimilars = new ListOfSimilars(reportConfiguration, reportRequest);
            }
        }
    }

    public partial class Request
    {
        public Request()
        {
        }

        public Request(IBusinessReportConfiguration reportConfiguration, IBusinessReportRequest reportRequest)
        {
            if (reportConfiguration == null)
                throw new ArgumentNullException(nameof(reportConfiguration));
            if (reportRequest != null)
            {
                Products = new Products(reportConfiguration, reportRequest);
            }
        }
    }

    public partial class Subscriber
    {
        public Subscriber()
        {
        }

        public Subscriber(IBusinessReportConfiguration reportConfiguration)
        {
            if (reportConfiguration == null)
                throw new ArgumentNullException(nameof(reportConfiguration));

            OpInitials = reportConfiguration.OpInitials;
            SubCode = reportConfiguration.SubCode;
        }
    }

    public partial class Vendor
    {
        public Vendor()
        {
        }

        public Vendor(IBusinessReportConfiguration reportConfiguration)
        {
            if (reportConfiguration == null)
                throw new ArgumentNullException(nameof(reportConfiguration));
            VendorNumber = reportConfiguration.VendorNumber;
        }
    }

    public partial class XML
    {
        public XML()
        {
        }

        public XML(IBusinessReportConfiguration reportConfiguration)
        {
            if (reportConfiguration == null)
                throw new ArgumentNullException(nameof(reportConfiguration));

            Verbose = reportConfiguration.BusinessVerbose;
        }
    }

    public partial class NetConnectRequest
    {
        public NetConnectRequest()
        {
        }

        public NetConnectRequest(IBusinessReportConfiguration reportConfiguration, IBusinessReportRequest reportRequest)
        {
            if (reportConfiguration == null)
                throw new ArgumentNullException(nameof(reportConfiguration));
            if (reportRequest == null)
                throw new ArgumentNullException(nameof(reportRequest));

            if (string.IsNullOrWhiteSpace(reportConfiguration.NetConnectSubscriber))
                throw new ArgumentException("Net connect subscriber is required",
                    nameof(reportConfiguration.NetConnectSubscriber));

            if (string.IsNullOrWhiteSpace(reportConfiguration.NetConnectPassword))
                throw new ArgumentException("Net connect password is required",
                    nameof(reportConfiguration.NetConnectPassword));
          
            if (string.IsNullOrWhiteSpace(reportConfiguration.Eai))
                throw new ArgumentException("Eai is required", nameof(reportConfiguration.Eai));

            if (string.IsNullOrWhiteSpace(reportConfiguration.ArfVersion))
                throw new ArgumentException("ArfVersion is required", nameof(reportConfiguration.ArfVersion));

            if (string.IsNullOrWhiteSpace(reportConfiguration.DbHost))
                throw new ArgumentException("DbHost is required", nameof(reportConfiguration.DbHost));
            if (string.IsNullOrWhiteSpace(reportConfiguration.OpInitials))
                throw new ArgumentException("OpInitials is required", nameof(reportConfiguration.OpInitials));

            if (string.IsNullOrWhiteSpace(reportConfiguration.Preamble))
                throw new ArgumentException("Preamble is required", nameof(reportConfiguration.Preamble));

            if (string.IsNullOrWhiteSpace(reportConfiguration.SubCode))
                throw new ArgumentException("SubCode is required", nameof(reportConfiguration.SubCode));

            if (string.IsNullOrWhiteSpace(reportConfiguration.VendorNumber))
                throw new ArgumentException("VendorNumber is required", nameof(reportConfiguration.VendorNumber));
            if (string.IsNullOrWhiteSpace(reportRequest.TransactionNumber))
                throw new ArgumentException("TransactionNumber is required",
                    nameof(reportRequest.TransactionNumber));

            Eai = reportConfiguration.Eai;
            DbHost = reportConfiguration.DbHost;
            ReferenceId = reportConfiguration.ReferenceId;

            Request = new Request(reportConfiguration, reportRequest);
        }
    }
}