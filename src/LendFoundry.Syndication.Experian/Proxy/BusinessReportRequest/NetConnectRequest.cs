﻿using System.Xml.Serialization;

namespace LendFoundry.Syndication.Experian.Proxy.BusinessReportRequest
{
    [XmlRoot(ElementName = "Subscriber")]
    public partial class Subscriber
    {
        [XmlElement(ElementName = "OpInitials")]
        public string OpInitials { get; set; }

        [XmlElement(ElementName = "SubCode")]
        public string SubCode { get; set; }
    }

    [XmlRoot(ElementName = "BusinessApplicant")]
    public partial class BusinessApplicant
    {
        [XmlElement(ElementName = "TransactionNumber")]
        public string TransactionNumber { get; set; }

        [XmlElement(ElementName = "BISListNumber")]
        public string BISListNumber { get; set; }
    }

    [XmlRoot(ElementName = "AddOns")]
    public partial class AddOns
    {
        [XmlElement(ElementName = "BUSP")]
        public string BUSP { get; set; }
    }

    [XmlRoot(ElementName = "XML")]
    public partial class XML
    {
        [XmlElement(ElementName = "Verbose")]
        public string Verbose { get; set; }
    }

    [XmlRoot(ElementName = "OutputType")]
    public partial class OutputType
    {
        [XmlElement(ElementName = "XML")]
        public XML XML { get; set; }
    }

    [XmlRoot(ElementName = "Vendor")]
    public partial class Vendor
    {
        [XmlElement(ElementName = "VendorNumber")]
        public string VendorNumber { get; set; }
    }

    [XmlRoot(ElementName = "ListOfSimilars")]
    public partial class ListOfSimilars
    {
        [XmlElement(ElementName = "Subscriber")]
        public Subscriber Subscriber { get; set; }

        [XmlElement(ElementName = "BusinessApplicant")]
        public BusinessApplicant BusinessApplicant { get; set; }

        [XmlElement(ElementName = "AddOns")]
        public AddOns AddOns { get; set; }

        [XmlElement(ElementName = "OutputType")]
        public OutputType OutputType { get; set; }

        [XmlElement(ElementName = "Vendor")]
        public Vendor Vendor { get; set; }
    }

    [XmlRoot(ElementName = "Products")]
    public partial class Products
    {
        [XmlElement(ElementName = "ListOfSimilars")]
        public ListOfSimilars ListOfSimilars { get; set; }
    }

    [XmlRoot(ElementName = "Request")]
    public partial class Request
    {
        [XmlElement(ElementName = "Products")]
        public Products Products { get; set; }
    }

    [XmlRoot(ElementName = "NetConnectRequest", Namespace = "http://www.experian.com/NetConnect")]
    public partial class NetConnectRequest
    {
        [XmlElement(ElementName = "EAI")]
        public string Eai { get; set; }

        [XmlElement(ElementName = "DBHost")]
        public string DbHost { get; set; }

        [XmlElement(ElementName = "ReferenceId")]
        public string ReferenceId { get; set; }

        [XmlElement(ElementName = "Request", Namespace = "http://www.experian.com/WebDelivery")]
        public Request Request { get; set; }
    }
}