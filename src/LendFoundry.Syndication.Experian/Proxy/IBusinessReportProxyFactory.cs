﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Experian.Proxy
{
    public interface IBusinessReportProxyFactory
    {
        IBusinessReportProxy Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}
