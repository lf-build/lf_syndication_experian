﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Experian.Proxy
{
    public interface IPersonalReportProxyFactory
    {
        IPersonalReportProxy Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}
