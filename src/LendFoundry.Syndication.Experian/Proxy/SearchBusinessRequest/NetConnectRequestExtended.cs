﻿using LendFoundry.Syndication.Experian.Request;
using System;

namespace LendFoundry.Syndication.Experian.Proxy.SearchBusinessRequest
{
    public partial class AddOns
    {
        public AddOns()
        {
        }

        public AddOns(IBusinessReportConfiguration searchConfiguration)
        {
            if (searchConfiguration != null)

                List = searchConfiguration.AddOnsList;
        }
    }

    public partial class BusinessApplicant
    {
        public BusinessApplicant()
        {
        }

        public BusinessApplicant(IBusinessSearchRequest searchRequest)
        {
            if (searchRequest != null)
            {
                BusinessName = searchRequest.BusinessName;
                CurrentAddress = new CurrentAddress(searchRequest);
            }
        }
    }

    public partial class CurrentAddress
    {
        public CurrentAddress()
        {
        }

        public CurrentAddress(IBusinessSearchRequest searchRequest)
        {
            if (searchRequest != null)
            {
                Street = searchRequest.Street;
                City = searchRequest.City;
                State = searchRequest.State;
                Zip = searchRequest.Zip;
            }
        }
    }

    public partial class NetConnectRequest
    {
        public NetConnectRequest()
        {
        }

        public NetConnectRequest(IBusinessReportConfiguration searchConfiguration, IBusinessSearchRequest searchRequest)
        {
            if (searchConfiguration == null)
                throw new ArgumentNullException(nameof(searchConfiguration));
            if (searchRequest == null)
                throw new ArgumentNullException(nameof(searchRequest));

            if (string.IsNullOrWhiteSpace(searchConfiguration.NetConnectSubscriber))
                throw new ArgumentException("Net connect subscriber is required",
                    nameof(searchConfiguration.NetConnectSubscriber));

            if (string.IsNullOrWhiteSpace(searchConfiguration.NetConnectPassword))
                throw new ArgumentException("Net connect password is required",
                    nameof(searchConfiguration.NetConnectPassword));
          

            if (string.IsNullOrWhiteSpace(searchConfiguration.Eai))
                throw new ArgumentException("Eai is required", nameof(searchConfiguration.Eai));

            if (string.IsNullOrWhiteSpace(searchConfiguration.ArfVersion))
                throw new ArgumentException("ArfVersion is required", nameof(searchConfiguration.ArfVersion));

            if (string.IsNullOrWhiteSpace(searchConfiguration.DbHost))
                throw new ArgumentException("DbHost is required", nameof(searchConfiguration.DbHost));
            if (string.IsNullOrWhiteSpace(searchConfiguration.OpInitials))
                throw new ArgumentException("OpInitials is required", nameof(searchConfiguration.OpInitials));

            if (string.IsNullOrWhiteSpace(searchConfiguration.Preamble))
                throw new ArgumentException("Preamble is required", nameof(searchConfiguration.Preamble));

            if (string.IsNullOrWhiteSpace(searchConfiguration.SubCode))
                throw new ArgumentException("SubCode is required", nameof(searchConfiguration.SubCode));

            if (string.IsNullOrWhiteSpace(searchConfiguration.VendorNumber))
                throw new ArgumentException("VendorNumber is required", nameof(searchConfiguration.VendorNumber));

            EAI = searchConfiguration.Eai;
            DBHost = searchConfiguration.DbHost;
            ReferenceId = searchConfiguration.ReferenceId;

            Request = new Request(searchConfiguration, searchRequest);
        }
    }

    public partial class OutputType
    {
        public OutputType()
        {
        }

        public OutputType(IBusinessReportConfiguration searchConfiguration)
        {
            if (searchConfiguration == null)
                throw new ArgumentNullException(nameof(searchConfiguration));

            XML = new XML(searchConfiguration);
        }
    }

    public partial class Request
    {
        public Request()
        {
        }

        public Request(IBusinessReportConfiguration searchConfiguration, IBusinessSearchRequest searchRequest)
        {
            if (searchConfiguration == null)
                throw new ArgumentNullException(nameof(searchConfiguration));
            if (searchRequest != null)
            {
                Products = new Products(searchConfiguration, searchRequest);
            }
        }
    }

    public partial class PremierProfile
    {
        public PremierProfile()
        {
        }

        public PremierProfile(IBusinessReportConfiguration searchConfiguration, IBusinessSearchRequest searchRequest)
        {
            if (searchConfiguration == null)
                throw new ArgumentNullException(nameof(searchConfiguration));
            if (searchRequest == null)
                throw new ArgumentNullException(nameof(searchRequest));
            Subscriber = new Subscriber(searchConfiguration);
            BusinessApplicant = new BusinessApplicant(searchRequest);
            OutputType = new OutputType(searchConfiguration);
            AddOns = new AddOns(searchConfiguration);
            Vendor = new Vendor(searchConfiguration);
        }
    }

    public partial class Products
    {
        public Products()
        {
        }

        public Products(IBusinessReportConfiguration searchConfiguration, IBusinessSearchRequest searchRequest)
        {
            if (searchConfiguration == null)
                throw new ArgumentNullException(nameof(searchConfiguration));
            if (searchRequest == null)
                throw new ArgumentNullException(nameof(searchRequest));

            PremierProfile = new PremierProfile(searchConfiguration, searchRequest);
        }
    }

    public partial class Subscriber
    {
        public Subscriber()
        {
        }

        public Subscriber(IBusinessReportConfiguration searchConfiguration)
        {
            if (searchConfiguration == null)
                throw new ArgumentNullException(nameof(searchConfiguration));

            OpInitials = searchConfiguration.OpInitials;
            SubCode = searchConfiguration.SubCode;
        }
    }

    public partial class Vendor
    {
        public Vendor()
        {
        }

        public Vendor(IBusinessReportConfiguration searchConfiguration)
        {
            if (searchConfiguration == null)
                throw new ArgumentNullException(nameof(searchConfiguration));
            VendorNumber = searchConfiguration.VendorNumber;
        }
    }

    public partial class XML
    {
        public XML()
        {
        }

        public XML(IBusinessReportConfiguration searchConfiguration)
        {
            if (searchConfiguration == null)
                throw new ArgumentNullException(nameof(searchConfiguration));
            Verbose = searchConfiguration.BusinessVerbose;
        }
    }
}