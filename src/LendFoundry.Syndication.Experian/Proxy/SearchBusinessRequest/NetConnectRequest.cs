﻿using System.Xml.Serialization;

namespace LendFoundry.Syndication.Experian.Proxy.SearchBusinessRequest
{
    [XmlRoot(ElementName = "Subscriber")]
    public partial class Subscriber
    {
        [XmlElement(ElementName = "OpInitials")]
        public string OpInitials { get; set; }

        [XmlElement(ElementName = "SubCode")]
        public string SubCode { get; set; }
    }

    [XmlRoot(ElementName = "CurrentAddress")]
    public partial class CurrentAddress
    {
        [XmlElement(ElementName = "Street")]
        public string Street { get; set; }

        [XmlElement(ElementName = "City")]
        public string City { get; set; }

        [XmlElement(ElementName = "State")]
        public string State { get; set; }

        [XmlElement(ElementName = "Zip")]
        public string Zip { get; set; }
    }

    [XmlRoot(ElementName = "BusinessApplicant")]
    public partial class BusinessApplicant
    {
        [XmlElement(ElementName = "BusinessName")]
        public string BusinessName { get; set; }

        [XmlElement(ElementName = "CurrentAddress")]
        public CurrentAddress CurrentAddress { get; set; }
    }

    [XmlRoot(ElementName = "AddOns")]
    public partial class AddOns
    {
        [XmlElement(ElementName = "List")]
        public string List { get; set; }
    }

    [XmlRoot(ElementName = "XML")]
    public partial class XML
    {
        [XmlElement(ElementName = "Verbose")]
        public string Verbose { get; set; }
    }

    [XmlRoot(ElementName = "OutputType")]
    public partial class OutputType
    {
        [XmlElement(ElementName = "XML")]
        public XML XML { get; set; }
    }

    [XmlRoot(ElementName = "Vendor")]
    public partial class Vendor
    {
        [XmlElement(ElementName = "VendorNumber")]
        public string VendorNumber { get; set; }
    }

    [XmlRoot(ElementName = "PremierProfile")]
    public partial class PremierProfile
    {
        [XmlElement(ElementName = "Subscriber")]
        public Subscriber Subscriber { get; set; }

        [XmlElement(ElementName = "BusinessApplicant")]
        public BusinessApplicant BusinessApplicant { get; set; }

        [XmlElement(ElementName = "AddOns")]
        public AddOns AddOns { get; set; }

        [XmlElement(ElementName = "OutputType")]
        public OutputType OutputType { get; set; }

        [XmlElement(ElementName = "Vendor")]
        public Vendor Vendor { get; set; }
    }

    [XmlRoot(ElementName = "Products")]
    public partial class Products
    {
        [XmlElement(ElementName = "PremierProfile")]
        public PremierProfile PremierProfile { get; set; }
    }

    [XmlRoot(ElementName = "Request")]
    public partial class Request
    {
        [XmlElement(ElementName = "Products")]
        public Products Products { get; set; }
    }

    [XmlRoot(ElementName = "NetConnectRequest",Namespace = "http://www.experian.com/NetConnect")]
    public partial class NetConnectRequest
    {
        [XmlElement(ElementName = "EAI")]
        public string EAI { get; set; }

        [XmlElement(ElementName = "DBHost")]
        public string DBHost { get; set; }

        [XmlElement(ElementName = "ReferenceId")]
        public string ReferenceId { get; set; }

        [XmlElement(ElementName = "Request", Namespace = "http://www.experian.com/WebDelivery")]
        public Request Request { get; set; }
    }
}