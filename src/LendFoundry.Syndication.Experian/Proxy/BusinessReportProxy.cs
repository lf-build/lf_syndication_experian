using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
using LendFoundry.Syndication.Experian.Proxy.SearchBusinessRequest;
using LendFoundry.Syndication.Experian.Proxy.SearchBusinessResponse;
using RestSharp;
using System;
using System.Net;
using System.Text;

namespace LendFoundry.Syndication.Experian.Proxy
{
    public class BusinessReportProxy : ExperianProxy, IBusinessReportProxy
    {
        public BusinessReportProxy(IExperianConfiguration reportConfiguration, IConfigurationService<ExperianConfiguration> configurationService, IProcessProxy processProxy, ILogger logger)
            : base(reportConfiguration, configurationService, processProxy, reportConfiguration.businessReportConfiguration.NetConnectSubscriber, reportConfiguration.businessReportConfiguration.NetConnectPassword, logger)
        {
            if (reportConfiguration == null)
                throw new ArgumentNullException(nameof(reportConfiguration));
            if (string.IsNullOrWhiteSpace(reportConfiguration.businessReportConfiguration.NetConnectSubscriber))
                throw new ArgumentException("Net connect subscriber is required",
                    nameof(reportConfiguration.businessReportConfiguration.NetConnectSubscriber));

            if (string.IsNullOrWhiteSpace(reportConfiguration.businessReportConfiguration.NetConnectPassword))
                throw new ArgumentException("Net connect password is required",
                    nameof(reportConfiguration.businessReportConfiguration.NetConnectPassword));

            if (string.IsNullOrWhiteSpace(reportConfiguration.businessReportConfiguration.NetConnectPasswordHash))
                throw new ArgumentException("Net Connect Password Hash is required",
                    nameof(reportConfiguration.businessReportConfiguration.NetConnectPasswordHash));

            if (string.IsNullOrWhiteSpace(reportConfiguration.businessReportConfiguration.Eai))
                throw new ArgumentException("Eai is required", nameof(reportConfiguration.businessReportConfiguration.Eai));

            if (string.IsNullOrWhiteSpace(reportConfiguration.businessReportConfiguration.ArfVersion))
                throw new ArgumentException("ArfVersion is required", nameof(reportConfiguration.businessReportConfiguration.ArfVersion));

            if (string.IsNullOrWhiteSpace(reportConfiguration.businessReportConfiguration.DbHost))
                throw new ArgumentException("DbHost is required", nameof(reportConfiguration.businessReportConfiguration.DbHost));
            if (string.IsNullOrWhiteSpace(reportConfiguration.businessReportConfiguration.OpInitials))
                throw new ArgumentException("OpInitials is required", nameof(reportConfiguration.businessReportConfiguration.OpInitials));

            if (string.IsNullOrWhiteSpace(reportConfiguration.businessReportConfiguration.Preamble))
                throw new ArgumentException("Preamble is required", nameof(reportConfiguration.businessReportConfiguration.Preamble));

            if (string.IsNullOrWhiteSpace(reportConfiguration.businessReportConfiguration.SubCode))
                throw new ArgumentException("SubCode is required", nameof(reportConfiguration.businessReportConfiguration.SubCode));
            if (string.IsNullOrWhiteSpace(reportConfiguration.businessReportConfiguration.AddOnsList))
                throw new ArgumentException("AddOnsList is required", nameof(reportConfiguration.businessReportConfiguration.AddOnsList));

            if (string.IsNullOrWhiteSpace(reportConfiguration.businessReportConfiguration.VendorNumber))
                throw new ArgumentException("VendorNumber is required", nameof(reportConfiguration.businessReportConfiguration.VendorNumber));

            ProcessProxy = processProxy;
            Logger = logger;
        }

        private IProcessProxy ProcessProxy { get; set; }
        private ILogger Logger { get; set; }

        public NetConnectResponse GetBusinessSearch(NetConnectRequest searchRequest)
        {
            if (searchRequest == null)
                throw new ArgumentNullException(nameof(searchRequest));
            ResetBusinessPasswordIfMisMatch(ReportConfiguration.businessReportConfiguration.NetConnectPassword, ReportConfiguration.businessReportConfiguration.NetConnectPasswordHash);

            var netConnectUrl = EcalsTransactionCheck();
            var xml = XmlConvertor.Serialize(searchRequest, Encoding.UTF8);
            var postData = "NETCONNECT_TRANSACTION=" + WebUtility.UrlEncode(xml);
            var response = ExecuteRequest<NetConnectResponse>(netConnectUrl, Method.POST, postData).Result;
            return response;
        }

        public string GetBusinessReport(BusinessReportRequest.NetConnectRequest reportRequest)
        {

            if (reportRequest == null)
                throw new ArgumentNullException(nameof(reportRequest));

            ResetBusinessPasswordIfMisMatch(ReportConfiguration.businessReportConfiguration.NetConnectPassword, ReportConfiguration.businessReportConfiguration.NetConnectPasswordHash);

            var netConnectUrl = EcalsTransactionCheck();

            var xml = XmlConvertor.Serialize(reportRequest, Encoding.UTF8);
            var postData = "NETCONNECT_TRANSACTION=" + WebUtility.UrlEncode(xml);

            var response = ExecuteRequest<string>(netConnectUrl, Method.POST, postData).Result;

            return response;

        }

        public DirectHitReportResponse.NetConnectResponse GetDirectHitReport(DirectHitReportRequest.NetConnectRequest reportRequest)
        {
            if (reportRequest == null)
                throw new ArgumentNullException(nameof(reportRequest));

            ResetBusinessPasswordIfMisMatch(ReportConfiguration.businessReportConfiguration.NetConnectSubscriber, ReportConfiguration.businessReportConfiguration.NetConnectPasswordHash);

            var netConnectUrl = EcalsTransactionCheck();

            var xml = XmlConvertor.Serialize(reportRequest, Encoding.UTF8);
            var postData = "NETCONNECT_TRANSACTION=" + WebUtility.UrlEncode(xml);

            var response = ExecuteRequest<DirectHitReportResponse.NetConnectResponse>(netConnectUrl, Method.POST, postData).Result;

            return response;
        }


    }
}
