using LendFoundry.Syndication.Experian.Proxy.SearchBusinessRequest;
using LendFoundry.Syndication.Experian.Proxy.SearchBusinessResponse;

namespace LendFoundry.Syndication.Experian.Proxy
{
    public interface IBusinessReportProxy : IExperianProxy
    {
        NetConnectResponse GetBusinessSearch(NetConnectRequest searchRequest);

        string GetBusinessReport(Proxy.BusinessReportRequest.NetConnectRequest reportRequest);

        DirectHitReportResponse.NetConnectResponse GetDirectHitReport(Proxy.DirectHitReportRequest.NetConnectRequest reportRequest);

    }
}