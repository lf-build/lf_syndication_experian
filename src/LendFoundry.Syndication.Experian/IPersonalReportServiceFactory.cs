﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Experian
{
    public interface IPersonalReportServiceFactory
    {
        IPersonalReportService Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}
