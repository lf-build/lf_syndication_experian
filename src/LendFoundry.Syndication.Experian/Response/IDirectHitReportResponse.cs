using LendFoundry.Syndication.Experian.DirectHitReportResponse;

namespace LendFoundry.Syndication.Experian.Response
{
    public interface IDirectHitReportResponse
    {
       IPremierProfile PremierProfile { get; set; }
    }
}