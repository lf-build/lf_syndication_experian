using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Experian.BusinessReportResponse;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.Response
{
    public class BusinessReportResponse : IBusinessReportResponse
    {
        public BusinessReportResponse()
        {
        }

        public BusinessReportResponse(Proxy.BusinessReportResponse.NetConnectResponse netConnectResponse)
        {
            if (netConnectResponse?.Products?.PremierProfile != null)
                PremierProfile = new PremierProfile(netConnectResponse.Products.PremierProfile);
        }

        [JsonConverter(typeof(InterfaceConverter<IPremierProfile, PremierProfile>))]
        public IPremierProfile PremierProfile { get; set; }
    }
}