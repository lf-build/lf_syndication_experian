﻿using LendFoundry.Syndication.Experian.PersonalReportResponse;

namespace LendFoundry.Syndication.Experian.Response
{
    public interface IPersonalReportResponse
    {
        IProductCreditProfile ProductCreditProfile { get; set; }
    }
}