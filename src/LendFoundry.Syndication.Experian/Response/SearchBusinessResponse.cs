using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Experian.Proxy.SearchBusinessResponse;
using LendFoundry.Syndication.Experian.SearchBusinessResponse;
using Newtonsoft.Json;
using Products = LendFoundry.Syndication.Experian.SearchBusinessResponse.Products;

namespace LendFoundry.Syndication.Experian.Response
{
    public class SearchBusinessResponse : ISearchBusinessResponse
    {
        public SearchBusinessResponse()
        {
        }

        public SearchBusinessResponse(NetConnectResponse netConnectResponse)
        {
            if (netConnectResponse != null)
                Products = new Products(netConnectResponse.Products);
        }

        [JsonConverter(typeof(InterfaceConverter<IProducts, Products>))]
        public IProducts Products { get; set; }
    }
}