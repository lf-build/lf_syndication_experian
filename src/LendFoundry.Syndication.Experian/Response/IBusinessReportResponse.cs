using LendFoundry.Syndication.Experian.BusinessReportResponse;

namespace LendFoundry.Syndication.Experian.Response
{
    public interface IBusinessReportResponse
    {
        IPremierProfile PremierProfile { get; set; }
    }
}