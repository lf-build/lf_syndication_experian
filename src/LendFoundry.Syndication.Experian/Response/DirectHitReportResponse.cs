using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Experian.DirectHitReportResponse;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.Response
{
    public class DirectHitReportResponse : IDirectHitReportResponse
    {
        public DirectHitReportResponse()
        {
        }

        public DirectHitReportResponse(Proxy.DirectHitReportResponse.NetConnectResponse netConnectResponse)
        {
            if (netConnectResponse?.Products?.PremierProfile != null)
                PremierProfile = new PremierProfile(netConnectResponse.Products.PremierProfile);
        }

        [JsonConverter(typeof(InterfaceConverter<IPremierProfile, PremierProfile>))]
        public IPremierProfile PremierProfile { get; set; }
    }
}