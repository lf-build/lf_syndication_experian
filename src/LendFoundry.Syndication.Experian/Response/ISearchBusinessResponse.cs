using LendFoundry.Syndication.Experian.SearchBusinessResponse;

namespace LendFoundry.Syndication.Experian.Response
{
    public interface ISearchBusinessResponse
    {
        IProducts Products { get; set; }
    }
}