﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Experian.PersonalReportResponse;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.Response
{
    public class PersonalReportResponse : IPersonalReportResponse
    {
        public PersonalReportResponse()
        {
        }

        public PersonalReportResponse(Proxy.PersonalReportResponse.NetConnectResponse netConnectResponse)
        {
            if (netConnectResponse?.Products?.CustomSolution != null)
                ProductCreditProfile = new ProductCreditProfile(netConnectResponse.Products.CustomSolution);
        }

        [JsonConverter(typeof(InterfaceConverter<IProductCreditProfile, ProductCreditProfile>))]
        public IProductCreditProfile ProductCreditProfile { get; set; }
    }
}