﻿using LendFoundry.Syndication.Experian.PersonalReportResponse;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian
{
    public class PersonalReportConfiguration : IPersonalReportConfiguration
    {      
        public string NetConnectSubscriber { get; set; }

        public string NetConnectPassword { get; set; }

        public string NetConnectPasswordHash { get; set; }      
        public string Eai { get; set; }

        public string DbHost { get; set; }

        public string ReferenceId { get; set; }

        public string Preamble { get; set; }

        public string OpInitials { get; set; }

        public string SubCode { get; set; }

        public string ArfVersion { get; set; }
        public string Verbose { get; set; }
        public string Demographics { get; set; }
        public string Y2K { get; set; }
        public string Segment130 { get; set; }
        public string VendorNumber { get; set; }
        public string VendorVersion { get; set; }
        public bool UseProxy { get; set; }
        public string ProxyUrl { get; set; }
        public string AccountType { get; set; }
        public RiskModelIndicators RiskModelIndicators { get; set; }
        public string CustomRRDashKeyword { get; set; }
        public string HardPullSubCode { get; set; }
        public string DirectCheck { get; set; }
    }
}