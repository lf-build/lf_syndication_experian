﻿using System;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.EventHub;
using LendFoundry.Configuration;
using LendFoundry.Syndication.Experian.Proxy;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Syndication.Experian
{
    public class PersonalReportServiceFactory : IPersonalReportServiceFactory
    {
        public PersonalReportServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public IPersonalReportService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var personalProxyFactory = Provider.GetService<IPersonalReportProxyFactory>();
            var personalProxy = personalProxyFactory.Create(reader, handler, logger);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var lookupFactory = Provider.GetService<ILookupClientFactory>();
            var lookup = lookupFactory.Create(reader);


            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
          
            var configurationService = configurationServiceFactory.Create<ExperianConfiguration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            return new PersonalReportService(configuration, personalProxy, eventHub, lookup, logger, tenantTime);
        }
    }
}
