﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
namespace LendFoundry.Syndication.Experian
{
    public interface IExperianConfiguration : IDependencyConfiguration
    {
        IBusinessReportConfiguration businessReportConfiguration { get; set; }
        IPersonalReportConfiguration personalReportConfiguration { get; set; }
        string PasswordResetURL { get; set; }

        string EcalsTransactionUrl { get; set; }     

       string ExperianHostName { get; set; }
       bool RequiredCertificateVerification { get; set; }
       string ConnectionString { get; set; }

    }
}