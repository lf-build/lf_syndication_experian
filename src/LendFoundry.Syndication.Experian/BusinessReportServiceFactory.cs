﻿using System;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration;
using LendFoundry.Syndication.Experian.Proxy;
using LendFoundry.Foundation.Lookup.Client;

namespace LendFoundry.Syndication.Experian
{
    public class BusinessReportServiceFactory : IBusinessReportServiceFactory
    {
        public BusinessReportServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public IBusinessReportService Create(ITokenReader reader,ITokenHandler handler, ILogger logger)
        {
                    

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);

            var lookupFactory = Provider.GetService<ILookupClientFactory>();
            var lookup = lookupFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();        

            var configurationService = configurationServiceFactory.Create<ExperianConfiguration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);           

            var businessProxyFactory = Provider.GetService<IBusinessReportProxyFactory>();
            var businessProxy = businessProxyFactory.Create(reader, handler,logger);


            return new BusinessReportService(configuration, businessProxy, eventHub, lookup,logger, tenantTime);
        }
    }
}
