namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IProducts
    {
        IProductCreditProfile CreditProfile { get; set; }
    }
}