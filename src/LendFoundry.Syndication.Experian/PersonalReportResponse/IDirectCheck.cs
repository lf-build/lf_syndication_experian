﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IDirectCheck
    {
        string SubscriberNumber { get; set; }
        string SubscriberName { get; set; }
        string SubscriberAddress { get; set; }
        string SubscriberCity { get; set; }
        string SubscriberState { get; set; }
        string SubscriberZip { get; set; }
        string SubscriberPhone { get; set; }
    }
}
