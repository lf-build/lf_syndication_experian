using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Experian.Proxy.PersonalReportResponse;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class RiskModelIndicators : IRiskModelIndicators
    {
        public string AssetInsight { get; set; }
        public string BankruptcyPLUS { get; set; }
        public string BankruptcyPLUSRescaled { get; set; }
        public string BustOutScore { get; set; }
        public string CollectScore { get; set; }
        public string TotalDebtToIncome { get; set; }
        public string FirstMtgDebtToIncome { get; set; }
        public string TotalMtgDebtToIncome { get; set; }
        public string TotalDebtToIncomeW2 { get; set; }
        public string TotalDebtToIncomeJointW2 { get; set; }
        public string FirstMtgDebtToIncomeW2 { get; set; }
        public string TotalMtgDebtToIncomeW2 { get; set; }
        public string FICOAdvanced { get; set; }
        public string FICOAdvanced2 { get; set; }
        public string FICOAuto2 { get; set; }
        public string FICOAuto3 { get; set; }
        public string FICO8Auto { get; set; }
        public string FICO9Auto { get; set; }
        public string FICOBank2 { get; set; }
        public string FICOBank3 { get; set; }
        public string FICO8Bank { get; set; }
        public string FICO9Bank { get; set; }
        public string FICOBankruptcy { get; set; }
        public string FICOInstall2 { get; set; }
        public string FICOInstall3 { get; set; }
        public string FICOInsHomeownerF35 { get; set; }
        public string FICOInsHomeownerF4 { get; set; }
        public string FICOInsPrefAutoGT { get; set; }
        public string FICOInsStandardAutoGT { get; set; }
        public string FICOInsStandardAutoMin { get; set; }
        public string FICOInsNonstandardAuto { get; set; }
        public string FICOFinance2 { get; set; }
        public string FICOFinance3 { get; set; }
        public string FICO2 { get; set; }
        public string FICO3 { get; set; }
        public string FICO8 { get; set; }
        public string FICO9 { get; set; }
        public string FraudShield { get; set; }
        public string IncomeInsight { get; set; }
        public string IncomeInsightW2 { get; set; }
        public string NeverPay { get; set; }
        public string RecoveryScoreBank { get; set; }
        public string RecoveryScoreRetail { get; set; }
        public string ROI1Digit { get; set; }
        public string ROI3Digit { get; set; }
        public string ScorexPLUS { get; set; }
        public string ScorexPLUS2 { get; set; }
        public string TEC { get; set; }
        public string TeleRisk { get; set; }
        public string VantageScore { get; set; }
        public string VantageScore2 { get; set; }
        public string VantageScore3 { get; set; }
        public string VantageScore3Positive { get; set; }
        public string Auto { get; set; }
        public string BankruptcyWatch { get; set; }
        public string CreditUnion { get; set; }
        public string FICOAuto { get; set; }
        public string FICOBank { get; set; }
        public string Bankruptcy { get; set; }
        public string FICOInstall { get; set; }
        public string FICOFinance { get; set; }
        public string FICO { get; set; }
        public string NationalEquiv { get; set; }
        public string NationalRisk { get; set; }
        public string Retail { get; set; }        
    }
}