namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class ConsumerIdentityName : IConsumerIdentityName
    {
        public ConsumerIdentityName()
        {
        }

        public ConsumerIdentityName(Proxy.PersonalReportResponse.ProductsCreditProfileConsumerIdentityName consumerIdentityName)
        {
            if (consumerIdentityName != null)
            {
                First = consumerIdentityName.First;
                Middle = consumerIdentityName.Middle;
                Surname = consumerIdentityName.Surname;
            }
        }

        public string Surname { get; set; }
        public string First { get; set; }
        public string Middle { get; set; }
    }
}