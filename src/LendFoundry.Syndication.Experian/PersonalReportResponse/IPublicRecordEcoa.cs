﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IPublicRecordEcoa
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}