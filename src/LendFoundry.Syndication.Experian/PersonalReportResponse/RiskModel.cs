using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class RiskModel : IRiskModel
    {
        public RiskModel()
        {
        }

        public RiskModel(Proxy.PersonalReportResponse.ProductsCreditProfileRiskModel productsCreditProfileRiskModel)
        {
            if (productsCreditProfileRiskModel != null)
            {
                if (productsCreditProfileRiskModel.Evaluation != null)
                    Evaluation = new RiskModelEvaluation(productsCreditProfileRiskModel.Evaluation.code, productsCreditProfileRiskModel.Evaluation.Value);
                if (productsCreditProfileRiskModel.ModelIndicator != null)
                    ModelIndicator =
                        new ModelIndicator(productsCreditProfileRiskModel.ModelIndicator.code, productsCreditProfileRiskModel.ModelIndicator.Value);
                Score = productsCreditProfileRiskModel.Score;
                ScoreFactorCodeFour = productsCreditProfileRiskModel.ScoreFactorCodeFour;
                ScoreFactorCodeOne = productsCreditProfileRiskModel.ScoreFactorCodeOne;
                ScoreFactorCodeThree = productsCreditProfileRiskModel.ScoreFactorCodeThree;
                ScoreFactorCodeTwo = productsCreditProfileRiskModel.ScoreFactorCodeTwo;
                ScorePercentile = productsCreditProfileRiskModel.ScorePercentile;
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IModelIndicator, ModelIndicator>))]
        public IModelIndicator ModelIndicator { get; set; }

        public string Score { get; set; }
        public string ScoreFactorCodeOne { get; set; }
        public string ScoreFactorCodeTwo { get; set; }
        public string ScoreFactorCodeThree { get; set; }
        public string ScoreFactorCodeFourDesc { get; set; }
        public string ScoreFactorCodeOneDesc { get; set; }
        public string ScoreFactorCodeTwoDesc { get; set; }
        public string ScoreFactorCodeThreeDesc { get; set; }
        public string ScoreFactorCodeFour { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IRiskModelEvaluation, RiskModelEvaluation>))]
        public IRiskModelEvaluation Evaluation { get; set; }

        public string ScorePercentile { get; set; }
    }
}