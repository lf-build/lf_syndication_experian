namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class FraudServicesType : IFraudServicesType
    {
        public FraudServicesType()
        {
        }

        public FraudServicesType(Proxy.PersonalReportResponse.ProductsCreditProfileFraudServicesType fraudServicesType)
        {
            if (fraudServicesType != null)
            {
                Code = fraudServicesType.code;
                Value = fraudServicesType.value;
            }
        }

        public string Code { get; set; }
        public string Value { get; set; }
    }
}