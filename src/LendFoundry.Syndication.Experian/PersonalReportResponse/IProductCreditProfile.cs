﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IProductCreditProfile
    {
        List<IAddressInformation> AddressInformation { get; set; }
        IConsumerAssistanceReferralAddress ConsumerAssistanceReferralAddress { get; set; }
        List<IConsumerIdentity> ConsumerIdentity { get; set; }
        List<IEmploymentInformation> EmploymentInformation { get; set; }
        List<IFraudServices> FraudServices { get; set; }
        IProfileHeader Header { get; set; }
        List<IInformationalMessage> InformationalMessage { get; set; }
        List<IInquiry> Inquiry { get; set; }
        IProcessingMessage ProcessingMessage { get; set; }
        IProfileSummary ProfileSummary { get; set; }
        List<IPublicRecord> PublicRecord { get; set; }
        List<IRiskModel> RiskModel { get; set; }
        List<ISsn> Ssn { get; set; }
        IStatement Statement { get; set; }
        List<ITradeLine> TradeLine { get; set; }
    }
}