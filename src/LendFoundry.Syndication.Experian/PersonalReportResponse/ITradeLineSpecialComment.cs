﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ITradeLineSpecialComment
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}