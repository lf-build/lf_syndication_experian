namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class ProcessingMessage : IProcessingMessage
    {
        public ProcessingMessage()
        {
        }

        public ProcessingMessage(Proxy.PersonalReportResponse.ProcessingMessage processingMessage)
        {
            if (processingMessage != null)
            {
                ProcessingAction = processingMessage.ProcessingAction;
            }
        }

        public string ProcessingAction { get; set; }
    }
}