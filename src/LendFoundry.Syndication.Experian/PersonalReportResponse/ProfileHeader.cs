namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class ProfileHeader : IProfileHeader
    {
        public ProfileHeader()
        {
        }

        public ProfileHeader(Proxy.PersonalReportResponse.ProductsCreditProfileHeader profileHeader)
        {
            if (profileHeader != null)
            {
                ArfVersion = profileHeader.ARFVersion;
                Preamble = profileHeader.Preamble;
                ReferenceNumber = profileHeader.ReferenceNumber;
                ReportDate = profileHeader.ReportDate;
                ReportTime = profileHeader.ReportTime;
            }
        }

        public string ReportDate { get; set; }
        public string ReportTime { get; set; }
        public string Preamble { get; set; }

        public string ArfVersion { get; set; }

        public string ReferenceNumber { get; set; }
    }
}