﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IRecordStatus
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}