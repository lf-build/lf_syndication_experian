using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class EmploymentInformation : IEmploymentInformation
    {
        public EmploymentInformation()
        {
        }

        public EmploymentInformation(Proxy.PersonalReportResponse.ProductsCreditProfileEmploymentInformation employmentInformation)
        {
            if (employmentInformation != null)
            {
                AddressExtraLine = employmentInformation.AddressExtraLine;
                AddressFirstLine = employmentInformation.AddressFirstLine;
                AddressSecondLine = employmentInformation.AddressSecondLine;
                FirstReportedDate = employmentInformation.FirstReportedDate;
                LastUpdatedDate = employmentInformation.LastUpdatedDate;
                Name = employmentInformation.Name;
                if (employmentInformation.Origination != null)
                    Origination =
                        new Origination(
                            employmentInformation.Origination.code, employmentInformation.Origination.Value);
                Zip = employmentInformation.Zip;
            }
        }

        public string FirstReportedDate { get; set; }
        public string LastUpdatedDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IOrigination, Origination>))]
        public IOrigination Origination { get; set; }

        public string Name { get; set; }
        public string AddressFirstLine { get; set; }
        public string AddressSecondLine { get; set; }
        public string AddressExtraLine { get; set; }
        public string Zip { get; set; }
    }
}