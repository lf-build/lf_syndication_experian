﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class Attributes:IAttributes
    {
        public Attributes()
        {
            
        }

        public Attributes(Proxy.PersonalReportResponse.Attribute attribute)
        {
            if (attribute != null)
            {
                Name = attribute.Name;
                Value = attribute.Value;
            }
        }
      public  string Name { get; set; }
      public  string Value { get; set; }
    }
}