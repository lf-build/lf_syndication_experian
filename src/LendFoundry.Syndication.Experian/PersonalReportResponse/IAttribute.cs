﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IAttributes
    {
         string Name { get; set; }

         string Value { get; set; }
    }
}