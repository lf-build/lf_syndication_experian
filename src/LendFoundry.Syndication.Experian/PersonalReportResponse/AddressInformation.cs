using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class AddressInformation : IAddressInformation
    {
        public AddressInformation()
        {
        }

        public AddressInformation(Proxy.PersonalReportResponse.ProductsCreditProfileAddressInformation addressInformation)
        {
            if (addressInformation != null)
            {
                CensusGeoCode = addressInformation.CensusGeoCode;
                City = addressInformation.City;
                CountyCode = addressInformation.CountyCode;
                if (addressInformation.DwellingType != null)
                    DwellingType =
                        new DwellingType(
                            addressInformation.DwellingType);
                FirstReportedDate = addressInformation.FirstReportedDate;
                if (addressInformation.HomeOwnership != null)
                    HomeOwnership =
                        new HomeOwnership(
                            addressInformation.HomeOwnership);
                LastReportingSubcode = addressInformation.LastReportingSubcode;
                LastUpdatedDate = addressInformation.LastUpdatedDate;
                if (addressInformation.Origination != null)
                    Origination =
                        new Origination(
                            addressInformation.Origination.code, addressInformation.Origination.Value);
                State = addressInformation.State;
                StreetName = addressInformation.StreetName;
                StreetPrefix = addressInformation.StreetPrefix;
                StreetSuffix = addressInformation.StreetSuffix;
                TimesReported = addressInformation.TimesReported;
                UnitId = addressInformation.UnitID;
                UnitType = addressInformation.UnitType;
                Zip = addressInformation.Zip;
            }
        }

        public string FirstReportedDate { get; set; }
        public string LastUpdatedDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IOrigination, Origination>))]
        public IOrigination Origination { get; set; }

        public string TimesReported { get; set; }
        public string LastReportingSubcode { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDwellingType, DwellingType>))]
        public IDwellingType DwellingType { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IHomeOwnership, HomeOwnership>))]
        public IHomeOwnership HomeOwnership { get; set; }

        public string StreetPrefix { get; set; }
        public string StreetName { get; set; }
        public string StreetSuffix { get; set; }
        public string UnitType { get; set; }

        public string UnitId { get; set; }

        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string CensusGeoCode { get; set; }
        public string CountyCode { get; set; }
    }
}