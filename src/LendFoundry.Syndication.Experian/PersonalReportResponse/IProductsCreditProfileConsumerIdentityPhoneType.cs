﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IProductsCreditProfileConsumerIdentityPhoneType
    {
         string Code { get; set; }
         string Value { get; set; }
    }
}