namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class InquiryType : IInquiryType
    {
        public InquiryType()
        {
        }

        public InquiryType(Proxy.PersonalReportResponse.ProductsCreditProfileInquiryType inquiryType)
        {
            if (inquiryType != null)
            {
                Code = inquiryType.code;
                Value = inquiryType.value;
            }
        }

        public string Code { get; set; }
        public string Value { get; set; }
    }
}