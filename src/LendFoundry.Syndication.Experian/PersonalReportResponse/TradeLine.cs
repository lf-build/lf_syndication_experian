using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class TradeLine : ITradeLine
    {
        public TradeLine()
        {
        }

        public TradeLine(Proxy.PersonalReportResponse.ProductsCreditProfileTradeLine productsCreditProfileTradeLine)
        {
            if (productsCreditProfileTradeLine != null)
            {
                AccountNumber = productsCreditProfileTradeLine.AccountNumber;
                if (productsCreditProfileTradeLine.AccountType != null)
                    AccountType =
                        new AccountType(productsCreditProfileTradeLine.AccountType.code, productsCreditProfileTradeLine.AccountType.Value);
                if (productsCreditProfileTradeLine.Amount != null)
                    Amount =
                        productsCreditProfileTradeLine.Amount.Select(p => new TradeLineAmount(p))
                            .ToList<ITradeLineAmount>();
                AmountPastDue = productsCreditProfileTradeLine.AmountPastDue;
                BalanceAmount = productsCreditProfileTradeLine.BalanceAmount;
                BalanceDate = productsCreditProfileTradeLine.BalanceDate;
                ConsumerComment = productsCreditProfileTradeLine.ConsumerComment;
                DelinquenciesOver30Days = productsCreditProfileTradeLine.DelinquenciesOver30Days;
                DelinquenciesOver60Days = productsCreditProfileTradeLine.DelinquenciesOver60Days;
                DelinquenciesOver90Days = productsCreditProfileTradeLine.DelinquenciesOver90Days;
                DerogCounter = productsCreditProfileTradeLine.DerogCounter;
                if (productsCreditProfileTradeLine.ECOA != null)
                    Ecoa = new TradeLineEcoa(productsCreditProfileTradeLine.ECOA.code, productsCreditProfileTradeLine.ECOA.Value);
                if (productsCreditProfileTradeLine.EnhancedPaymentData != null)
                    EnhancedPaymentData =
                        new TradeLineEnhancedPaymentData(
                            productsCreditProfileTradeLine.EnhancedPaymentData);
                if (productsCreditProfileTradeLine.Evaluation != null)
                    Evaluation = new TradeLineEvaluation(productsCreditProfileTradeLine.Evaluation.code, productsCreditProfileTradeLine.Evaluation.Value);
                if (productsCreditProfileTradeLine.KOB != null)
                    Kob = new TradeLineKob(productsCreditProfileTradeLine.KOB.code, productsCreditProfileTradeLine.KOB.Value);
                LastPaymentDate = productsCreditProfileTradeLine.LastPaymentDate;
                MaxDelinquencyDate = productsCreditProfileTradeLine.MaxDelinquencyDate;
                MonthlyPaymentAmount = productsCreditProfileTradeLine.MonthlyPaymentAmount;

                if(productsCreditProfileTradeLine.MonthlyPaymentType !=null)
                      MonthlyPaymentType = new MonthlyPaymentType(productsCreditProfileTradeLine.MonthlyPaymentType.code, productsCreditProfileTradeLine.MonthlyPaymentType.Value);

                MonthsHistory = productsCreditProfileTradeLine.MonthsHistory;
                OpenDate = productsCreditProfileTradeLine.OpenDate;
                if (productsCreditProfileTradeLine.OpenOrClosed != null)
                    OpenOrClosed =
                        new TradeLineOpenOrClosed(productsCreditProfileTradeLine.OpenOrClosed.code, productsCreditProfileTradeLine.OpenOrClosed.Value);
                OriginalCreditorName = productsCreditProfileTradeLine.OriginalCreditorName;
                PaymentProfile = productsCreditProfileTradeLine.PaymentProfile;
                if (productsCreditProfileTradeLine.RevolvingOrInstallment != null)
                    RevolvingOrInstallment =
                        new RevolvingOrInstallment(
                            productsCreditProfileTradeLine.RevolvingOrInstallment.code, productsCreditProfileTradeLine.RevolvingOrInstallment.Value);
                if (productsCreditProfileTradeLine.SpecialComment != null)
                    SpecialComment =
                        new TradeLineSpecialComment(productsCreditProfileTradeLine.SpecialComment.code, productsCreditProfileTradeLine.SpecialComment.Value);
                if (productsCreditProfileTradeLine.Status != null)
                    Status = new TradeLineStatus(productsCreditProfileTradeLine.Status.code, productsCreditProfileTradeLine.Status.Value);
                StatusDate = productsCreditProfileTradeLine.StatusDate;
                Subcode = productsCreditProfileTradeLine.Subcode;
                SubscriberDisplayName = productsCreditProfileTradeLine.SubscriberDisplayName;
                if (productsCreditProfileTradeLine.TermsDuration != null)
                    TermsDuration =
                        new TradeLineTermsDuration(productsCreditProfileTradeLine.TermsDuration.code, productsCreditProfileTradeLine.TermsDuration.Value);
            }
        }

        [JsonConverter(typeof(InterfaceConverter<ITradeLineSpecialComment, TradeLineSpecialComment>))]
        public ITradeLineSpecialComment SpecialComment { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ITradeLineEvaluation, TradeLineEvaluation>))]
        public ITradeLineEvaluation Evaluation { get; set; }

        public string OpenDate { get; set; }
        public string StatusDate { get; set; }
        public string MaxDelinquencyDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAccountType, AccountType>))]
        public IAccountType AccountType { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ITradeLineTermsDuration, TradeLineTermsDuration>))]
        public ITradeLineTermsDuration TermsDuration { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ITradeLineEcoa, TradeLineEcoa>))]
        public ITradeLineEcoa Ecoa { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITradeLineAmount, TradeLineAmount>))]
        public List<ITradeLineAmount> Amount { get; set; }

        public string BalanceDate { get; set; }
        public string BalanceAmount { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ITradeLineStatus, TradeLineStatus>))]
        public ITradeLineStatus Status { get; set; }

        public string AmountPastDue { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ITradeLineOpenOrClosed, TradeLineOpenOrClosed>))]
        public ITradeLineOpenOrClosed OpenOrClosed { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IRevolvingOrInstallment, RevolvingOrInstallment>))]
        public IRevolvingOrInstallment RevolvingOrInstallment { get; set; }

        public string ConsumerComment { get; set; }
        public string AccountNumber { get; set; }
        public string MonthsHistory { get; set; }
        public string DelinquenciesOver30Days { get; set; }
        public string DelinquenciesOver60Days { get; set; }
        public string DelinquenciesOver90Days { get; set; }
        public string DerogCounter { get; set; }
        public string PaymentProfile { get; set; }
        public string MonthlyPaymentAmount { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IMonthlyPaymentType, MonthlyPaymentType>))]
        public IMonthlyPaymentType MonthlyPaymentType { get; set; }

        public string LastPaymentDate { get; set; }
        public string Subcode { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ITradeLineKob, TradeLineKob>))]
        public ITradeLineKob Kob { get; set; }

        public string SubscriberDisplayName { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ITradeLineEnhancedPaymentData, TradeLineEnhancedPaymentData>))]
        public ITradeLineEnhancedPaymentData EnhancedPaymentData { get; set; }

        public string OriginalCreditorName { get; set; }
    }
}