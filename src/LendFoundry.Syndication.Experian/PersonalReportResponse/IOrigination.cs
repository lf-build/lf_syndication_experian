﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IOrigination
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}