﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IMonthlyPaymentType
    {
        string Code { get; set; }
    }
}