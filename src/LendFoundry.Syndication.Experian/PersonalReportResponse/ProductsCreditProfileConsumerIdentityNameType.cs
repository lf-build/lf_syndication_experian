namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class ProductsCreditProfileConsumerIdentityNameType : IProductsCreditProfileConsumerIdentityNameType
    {
        public ProductsCreditProfileConsumerIdentityNameType()
        {
        }

        public ProductsCreditProfileConsumerIdentityNameType(Proxy.PersonalReportResponse.ProductsCreditProfileConsumerIdentityNameType productsCreditProfileConsumerIdentityNameType)
        {
            if (productsCreditProfileConsumerIdentityNameType != null)
            {
                Code = productsCreditProfileConsumerIdentityNameType.code;
            }
        }

        public string Code { get; set; }
    }
}