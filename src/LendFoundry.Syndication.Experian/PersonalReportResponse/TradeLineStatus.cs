namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class TradeLineStatus : ITradeLineStatus
    {
        public TradeLineStatus()
        {
        }

        public TradeLineStatus(string code, string value)
        {
            if (!string.IsNullOrWhiteSpace(code))
            {
                Code = code;
            }
            if (!string.IsNullOrWhiteSpace(value))
            {
                Value = value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}