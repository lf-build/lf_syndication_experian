﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ITradeLineEcoa
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}