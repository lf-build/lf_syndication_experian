﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class DirectCheck : IDirectCheck
    {
        public DirectCheck()
        {

        }

        public DirectCheck(Proxy.PersonalReportResponse.DirectCheck directCheck)
        {
            if (directCheck != null)
            {
                SubscriberNumber = directCheck.SubscriberNumber;
                SubscriberName = directCheck.SubscriberName;
                SubscriberAddress = directCheck.SubscriberAddress;
                SubscriberCity = directCheck.SubscriberCity;
                SubscriberState = directCheck.SubscriberState;
                SubscriberZip = directCheck.SubscriberZip;
                SubscriberPhone = directCheck.SubscriberPhone;
            }
        }

        public string SubscriberNumber { get; set; }
        public string SubscriberName { get; set; }
        public string SubscriberAddress { get; set; }
        public string SubscriberCity { get; set; }
        public string SubscriberState { get; set; }
        public string SubscriberZip { get; set; }
        public string SubscriberPhone { get; set; }
    }
}
