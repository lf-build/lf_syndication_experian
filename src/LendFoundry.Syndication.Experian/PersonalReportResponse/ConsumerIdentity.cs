using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class ConsumerIdentity : IConsumerIdentity
    {
        public ConsumerIdentity()
        {
        }

        public ConsumerIdentity(Proxy.PersonalReportResponse.ProductsCreditProfileConsumerIdentity consumerIdentity)
        {
            if (consumerIdentity != null)
            {
                if (consumerIdentity.Name != null)
                    Name = new ConsumerIdentityName(consumerIdentity.Name);
               
                if (consumerIdentity.Phone != null)
                    Phone =
                        new ConsumerIdentityPhone(consumerIdentity.Phone);
                Yob = consumerIdentity.YOB;
                Dob = consumerIdentity.DOB;

            }
        }

        [JsonConverter(typeof(InterfaceConverter<IConsumerIdentityName, ConsumerIdentityName>))]
        public IConsumerIdentityName Name { get; set; }

        public string Yob { get; set; }
        public string Dob { get; set; }


        [JsonConverter(typeof(InterfaceConverter<IConsumerIdentityPhone, ConsumerIdentityPhone>))]
        public IConsumerIdentityPhone Phone { get; set; }
    }
}