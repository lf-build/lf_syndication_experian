﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IConsumerAssistanceReferralAddress
    {
        string CityStateZip { get; set; }
        string OfficeName { get; set; }
        string Phone { get; set; }
        string PoBox { get; set; }
        string StreetName { get; set; }
    }
}