﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IFraudServicesType
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}