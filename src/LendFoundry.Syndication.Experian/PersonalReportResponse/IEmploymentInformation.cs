﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IEmploymentInformation
    {
        string AddressExtraLine { get; set; }
        string AddressFirstLine { get; set; }
        string AddressSecondLine { get; set; }
        string FirstReportedDate { get; set; }
        string LastUpdatedDate { get; set; }
        string Name { get; set; }
        IOrigination Origination { get; set; }
        string Zip { get; set; }
    }
}