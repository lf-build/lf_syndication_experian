﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IProductsCreditProfileDirectCheck
    {
        string SubscriberAddress { get; set; }
        string SubscriberCity { get; set; }
        string SubscriberName { get; set; }
        string SubscriberNumber { get; set; }
        string SubscriberPhone { get; set; }
        string SubscriberState { get; set; }
        string SubscriberZip { get; set; }
    }
}