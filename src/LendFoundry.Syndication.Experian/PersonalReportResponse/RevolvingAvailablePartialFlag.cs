namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class RevolvingAvailablePartialFlag : IRevolvingAvailablePartialFlag
    {
        public RevolvingAvailablePartialFlag()
        {
        }

        public RevolvingAvailablePartialFlag(Proxy.PersonalReportResponse.ProductsCreditProfileProfileSummaryRevolvingAvailablePartialFlag revolvingAvailablePartialFlag)
        {
            if (revolvingAvailablePartialFlag != null)
            {
                Code = revolvingAvailablePartialFlag.code;
                Value = revolvingAvailablePartialFlag.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}