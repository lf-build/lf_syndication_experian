namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class MonthlyPaymentPartialFlag : IMonthlyPaymentPartialFlag
    {
        public MonthlyPaymentPartialFlag()
        {
        }

        public MonthlyPaymentPartialFlag(Proxy.PersonalReportResponse.ProductsCreditProfileProfileSummaryMonthlyPaymentPartialFlag monthlyPaymentPartialFlag)
        {
            if (monthlyPaymentPartialFlag != null)
            {
                Code = monthlyPaymentPartialFlag.code;
                Value = monthlyPaymentPartialFlag.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}