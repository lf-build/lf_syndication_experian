﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IInquiryType
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}