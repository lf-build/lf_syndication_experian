﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IPaymentDataAccountType
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}