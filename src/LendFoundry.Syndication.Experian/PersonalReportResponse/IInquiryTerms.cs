﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IInquiryTerms
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}