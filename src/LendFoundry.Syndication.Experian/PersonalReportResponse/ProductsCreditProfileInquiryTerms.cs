namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class InquiryTerms : IInquiryTerms
    {
        public InquiryTerms()
        {
        }

        public InquiryTerms(Proxy.PersonalReportResponse.ProductsCreditProfileInquiryTerms inquiryTerms)
        {
            if (inquiryTerms != null)
            {
                Code = inquiryTerms.code;
            }
        }

        public string Code { get; set; }
        public string Value { get; set; }
    }
}