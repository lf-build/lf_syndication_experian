﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ISocialErrorCode
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}