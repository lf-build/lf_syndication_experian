﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IConsumerIdentity
    {
        IConsumerIdentityName Name { get; set; }
        IConsumerIdentityPhone Phone { get; set; }
        string Yob { get; set; }
        string Dob { get; set; }

    }
}