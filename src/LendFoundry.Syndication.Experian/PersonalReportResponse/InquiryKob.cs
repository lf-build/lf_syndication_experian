namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class InquiryKob : IInquiryKob
    {
        public InquiryKob()
        {
        }

        public InquiryKob(Proxy.PersonalReportResponse.ProductsCreditProfileInquiryKOB inquiryKob)
        {
            if (inquiryKob != null)
            {
                Code = inquiryKob.code;
            }
        }

        public string Code { get; set; }
    }
}