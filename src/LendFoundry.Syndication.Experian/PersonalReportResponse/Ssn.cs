using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Experian.Proxy.PersonalReportResponse;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class Ssn : ISsn
    {
        public Ssn()
        {
        }

        public Ssn(ProductsCreditProfileSSN productsCreditProfileSsn)
        {
            if (productsCreditProfileSsn != null)
            {
                Number = productsCreditProfileSsn.Number;
                if (productsCreditProfileSsn.VariationIndicator != null)
                    VariationIndicator =
                        new SsnVariationIndicator(productsCreditProfileSsn.VariationIndicator);
            }
        }

        [JsonConverter(typeof(InterfaceConverter<ISsnVariationIndicator, SsnVariationIndicator>))]
        public ISsnVariationIndicator VariationIndicator { get; set; }

        public string Number { get; set; }
    }
}