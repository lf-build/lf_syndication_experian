using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class ProductCreditProfile : IProductCreditProfile
    {
        public ProductCreditProfile()
        {
        }

        public ProductCreditProfile(Proxy.PersonalReportResponse.ProductsCreditProfile productsCreditProfile)
        {
            if (productsCreditProfile != null)
            {
                if (productsCreditProfile.AddressInformation != null)
                    AddressInformation =
                        productsCreditProfile.AddressInformation.Select(
                            p => new AddressInformation(p))
                            .ToList<IAddressInformation>();
                if (productsCreditProfile.ConsumerAssistanceReferralAddress != null)
                    ConsumerAssistanceReferralAddress =
                        new ConsumerAssistanceReferralAddress(
                            productsCreditProfile.ConsumerAssistanceReferralAddress);
                if (productsCreditProfile.ConsumerIdentity != null)
                    ConsumerIdentity =
                        productsCreditProfile.ConsumerIdentity.Select(p => new ConsumerIdentity(p))
                            .ToList<IConsumerIdentity>();
                if (productsCreditProfile.EmploymentInformation != null)
                    EmploymentInformation =
                        productsCreditProfile.EmploymentInformation.Select(
                            p => new EmploymentInformation(p))
                            .ToList<IEmploymentInformation>();
                if (productsCreditProfile.FraudServices != null)
                    FraudServices =
                        productsCreditProfile.FraudServices.Select(p => new FraudServices(p))
                            .ToList<IFraudServices>();
                if (productsCreditProfile.Header != null)
                    Header = new ProfileHeader(productsCreditProfile.Header);
                if (productsCreditProfile.InformationalMessage != null)
                    InformationalMessage =
                        productsCreditProfile.InformationalMessage.Select(
                            p => new InformationalMessage(p))
                            .ToList<IInformationalMessage>();
                if (productsCreditProfile.Inquiry != null)
                    Inquiry =
                        productsCreditProfile.Inquiry.Select(p => new Inquiry(p))
                            .ToList<IInquiry>();
                if (productsCreditProfile.ProcessingMessage != null)
                    ProcessingMessage = new ProcessingMessage(productsCreditProfile.ProcessingMessage);
              
                if (productsCreditProfile.PublicRecord != null)
                    PublicRecord =
                        productsCreditProfile.PublicRecord.Select(p => new PublicRecord(p))
                            .ToList<IPublicRecord>();
                if (productsCreditProfile.RiskModel != null)
                    RiskModel =
                        productsCreditProfile.RiskModel.Select(p => new RiskModel(p))
                            .ToList<IRiskModel>();
                if (productsCreditProfile.SSN != null)
                    Ssn =
                        productsCreditProfile.SSN.Select(p => new Ssn(p))
                            .ToList<ISsn>();

                if (productsCreditProfile.Statement != null)
                    Statement = new Statement(productsCreditProfile.Statement);
                if (productsCreditProfile.TradeLine != null)
                    TradeLine =
                        productsCreditProfile.TradeLine.Select(p => new TradeLine(p))
                            .ToList<ITradeLine>();

                if (productsCreditProfile.ProfileSummary != null)
                    ProfileSummary = new ProfileSummary(productsCreditProfile.ProfileSummary);

                if (productsCreditProfile.PremierAttributes != null)
                    PremierAttributes = new PremierAttributes(productsCreditProfile.PremierAttributes.Attributes);

                if (productsCreditProfile.DirectCheck != null)
                    DirectCheck =
                        productsCreditProfile.DirectCheck.Select(p => new DirectCheck(p))
                            .ToList<IDirectCheck>();
                if (productsCreditProfile.Error != null)
                    Error = new Error(productsCreditProfile.Error);

            }
        }

        [JsonConverter(typeof(InterfaceConverter<IProfileHeader, ProfileHeader>))]
        public IProfileHeader Header { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IRiskModel, RiskModel>))]
        public List<IRiskModel> RiskModel { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISsn, Ssn>))]
        public List<ISsn> Ssn { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IConsumerIdentity, ConsumerIdentity>))]
        public List<IConsumerIdentity> ConsumerIdentity { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddressInformation, AddressInformation>))]
        public List<IAddressInformation> AddressInformation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IEmploymentInformation, EmploymentInformation>))]
        public List<IEmploymentInformation> EmploymentInformation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITradeLine, TradeLine>))]
        public List<ITradeLine> TradeLine { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IConsumerAssistanceReferralAddress, ConsumerAssistanceReferralAddress>))]
        public IConsumerAssistanceReferralAddress ConsumerAssistanceReferralAddress { get; set; }

     
        [JsonConverter(typeof(InterfaceListConverter<IPublicRecord, PublicRecord>))]
        public List<IPublicRecord> PublicRecord { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IInquiry, Inquiry>))]
        public List<IInquiry> Inquiry { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IStatement, Statement>))]
        public IStatement Statement { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IInformationalMessage, InformationalMessage>))]
        public List<IInformationalMessage> InformationalMessage { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IFraudServices, FraudServices>))]
        public List<IFraudServices> FraudServices { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IProfileSummary, ProfileSummary>))]
        public IProfileSummary ProfileSummary { get; set; }


        [JsonConverter(typeof(InterfaceConverter<IPremierAttributes, PremierAttributes>))]
        public IPremierAttributes PremierAttributes { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IProcessingMessage, ProcessingMessage>))]
        public IProcessingMessage ProcessingMessage { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IDirectCheck, DirectCheck>))]
        public List<IDirectCheck> DirectCheck { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IError, Error>))]
        public IError Error { get; set; }
    }
}