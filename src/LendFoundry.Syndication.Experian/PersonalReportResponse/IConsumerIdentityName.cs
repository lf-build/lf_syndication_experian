﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IConsumerIdentityName
    {
        string First { get; set; }
        string Middle { get; set; }
        string Surname { get; set; }
    }
}