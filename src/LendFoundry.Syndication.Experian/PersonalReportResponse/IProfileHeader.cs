﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IProfileHeader
    {
        string ArfVersion { get; set; }
        string Preamble { get; set; }
        string ReferenceNumber { get; set; }
        string ReportDate { get; set; }
        string ReportTime { get; set; }
    }
}