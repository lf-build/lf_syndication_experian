﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ISsn
    {
        string Number { get; set; }
        ISsnVariationIndicator VariationIndicator { get; set; }
    }
}