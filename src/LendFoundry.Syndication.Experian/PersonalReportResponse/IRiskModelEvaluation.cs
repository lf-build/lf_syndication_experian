﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IRiskModelEvaluation
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}