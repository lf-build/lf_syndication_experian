﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IProfileSummary
    {
        string DelinquenciesOver30Days { get; set; }
        string DelinquenciesOver60Days { get; set; }
        string DelinquenciesOver90Days { get; set; }
        string DerogCounter { get; set; }
        string DisputedAccountsExcluded { get; set; }
        string InquiriesDuringLast6Months { get; set; }
        string InstallmentBalance { get; set; }
        string MonthlyPayment { get; set; }
        IMonthlyPaymentPartialFlag MonthlyPaymentPartialFlag { get; set; }
        string NowDelinquentDerog { get; set; }
        string OldestTradeOpenDate { get; set; }
        string PaidAccounts { get; set; }
        string PastDueAmount { get; set; }
        string PublicRecordsCount { get; set; }
        string RealEstateBalance { get; set; }
        string RealEstatePayment { get; set; }
        IRealEstatePaymentPartialFlag RealEstatePaymentPartialFlag { get; set; }
        IRevolvingAvailablePartialFlag RevolvingAvailablePartialFlag { get; set; }
        string RevolvingAvailablePercent { get; set; }
        string RevolvingBalance { get; set; }
        string SatisfactoryAccounts { get; set; }
        string TotalInquiries { get; set; }
        string TotalTradeItems { get; set; }
        string WasDelinquentDerog { get; set; }
    }
}