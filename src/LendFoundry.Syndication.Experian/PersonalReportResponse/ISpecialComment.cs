﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ISpecialComment
    {
        string Code { get; set; }
    }
}