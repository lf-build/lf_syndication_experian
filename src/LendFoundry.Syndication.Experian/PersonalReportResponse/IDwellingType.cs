﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IDwellingType
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}