namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class SpecialComment : ISpecialComment
    {
        public SpecialComment()
        {
        }

        public SpecialComment(string code)
        {
            if (!string.IsNullOrWhiteSpace(code))
                Code = code;
        }

        public string Code { get; set; }
    }
}