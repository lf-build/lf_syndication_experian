﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IHomeOwnership
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}