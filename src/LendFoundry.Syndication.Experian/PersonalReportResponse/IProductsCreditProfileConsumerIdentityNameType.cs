﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IProductsCreditProfileConsumerIdentityNameType
    {
        string Code { get; set; }
    }
}