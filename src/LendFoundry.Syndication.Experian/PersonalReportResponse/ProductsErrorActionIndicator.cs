﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class ProductsErrorActionIndicator : IProductsErrorActionIndicator
    {
        public ProductsErrorActionIndicator()
        {
        }

        public ProductsErrorActionIndicator(Proxy.PersonalReportResponse.ProductsErrorActionIndicator productsErrorActionIndicator)
        {
            if (productsErrorActionIndicator != null)
            {
                Code = productsErrorActionIndicator.code;
                Value = productsErrorActionIndicator.value;
            }
        }

        public string Code { get; set; }
        public string Value { get; set; }
    }
}