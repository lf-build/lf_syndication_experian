﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IPublicRecord
    {
        string Amount { get; set; }
        IPublicRecordBankruptcy Bankruptcy { get; set; }
        string BookPageSequence { get; set; }
        string ConsumerComment { get; set; }
        IPublicRecordCourt Court { get; set; }
        IPublicRecordEcoa PublicRecordEcoa { get; set; }
        IPublicRecordEvaluation PublicRecordEvaluation { get; set; }
        string FilingDate { get; set; }
        string PlaintiffName { get; set; }
        string ReferenceNumber { get; set; }
        IRecordStatus Status { get; set; }
        string StatusDate { get; set; }
        string Designator { get; set; }
    }
}