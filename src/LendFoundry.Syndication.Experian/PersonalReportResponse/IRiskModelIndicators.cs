﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
     interface IRiskModelIndicators
    {
         string AssetInsight { get; set; }
         string BankruptcyPLUS { get; set; }
         string BankruptcyPLUSRescaled { get; set; }
         string BustOutScore { get; set; }
         string CollectScore { get; set; }
         string TotalDebtToIncome { get; set; }
         string FirstMtgDebtToIncome { get; set; }
         string TotalMtgDebtToIncome { get; set; }
         string TotalDebtToIncomeW2 { get; set; }
         string TotalDebtToIncomeJointW2 { get; set; }
         string FirstMtgDebtToIncomeW2 { get; set; }
         string TotalMtgDebtToIncomeW2 { get; set; }
         string FICOAdvanced { get; set; }
         string FICOAdvanced2 { get; set; }
         string FICOAuto2 { get; set; }
         string FICOAuto3 { get; set; }
         string FICO8Auto { get; set; }
         string FICO9Auto { get; set; }
         string FICOBank2 { get; set; }
         string FICOBank3 { get; set; }
         string FICO8Bank { get; set; }
         string FICO9Bank { get; set; }
         string FICOBankruptcy { get; set; }
         string FICOInstall2 { get; set; }
         string FICOInstall3 { get; set; }
         string FICOInsHomeownerF35 { get; set; }
         string FICOInsHomeownerF4 { get; set; }
         string FICOInsPrefAutoGT { get; set; }
         string FICOInsStandardAutoGT { get; set; }
         string FICOInsStandardAutoMin { get; set; }
         string FICOInsNonstandardAuto { get; set; }
         string FICOFinance2 { get; set; }
         string FICOFinance3 { get; set; }
         string FICO2 { get; set; }
         string FICO3 { get; set; }
         string FICO8 { get; set; }
         string FICO9 { get; set; }
         string FraudShield { get; set; }
         string IncomeInsight { get; set; }
         string IncomeInsightW2 { get; set; }
         string NeverPay { get; set; }
         string RecoveryScoreBank { get; set; }
         string RecoveryScoreRetail { get; set; }
         string ROI1Digit { get; set; }
         string ROI3Digit { get; set; }
         string ScorexPLUS { get; set; }
         string ScorexPLUS2 { get; set; }
         string TEC { get; set; }
         string TeleRisk { get; set; }
         string VantageScore { get; set; }
         string VantageScore2 { get; set; }
         string VantageScore3 { get; set; }
         string VantageScore3Positive { get; set; }
         string Auto { get; set; }
         string BankruptcyWatch { get; set; }
         string CreditUnion { get; set; }
         string FICOAuto { get; set; }
         string FICOBank { get; set; }
         string Bankruptcy { get; set; }
         string FICOInstall { get; set; }
         string FICOFinance { get; set; }
         string FICO { get; set; }
         string NationalEquiv { get; set; }
         string NationalRisk { get; set; }
         string Retail { get; set; }         

    }
}