﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class Origination : IOrigination
    {
        public Origination()
        {
        }

        public Origination(
            string code, string value)
        {
            Code = code;
            Value = value;
        }

        public string Code { get; set; }
        public string Value { get; set; }
    }
}