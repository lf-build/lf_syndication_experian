namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class RealEstatePaymentPartialFlag : IRealEstatePaymentPartialFlag
    {
        public RealEstatePaymentPartialFlag()
        {
        }

        public RealEstatePaymentPartialFlag(Proxy.PersonalReportResponse.ProductsCreditProfileProfileSummaryRealEstatePaymentPartialFlag realEstatePaymentPartialFlag)
        {
            if (realEstatePaymentPartialFlag != null)
            {
                Code = realEstatePaymentPartialFlag.code;
                Value = realEstatePaymentPartialFlag.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}