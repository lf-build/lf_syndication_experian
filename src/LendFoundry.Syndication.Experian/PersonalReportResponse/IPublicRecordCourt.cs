﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IPublicRecordCourt
    {
        string Code { get; set; }
        string Name { get; set; }
    }
}