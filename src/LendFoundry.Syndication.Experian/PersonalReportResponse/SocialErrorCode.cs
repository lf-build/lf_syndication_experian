namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class SocialErrorCode : ISocialErrorCode
    {
        public SocialErrorCode()
        {
        }

        public SocialErrorCode(string code, string value)
        {
            Code = code;
            Value = value;
        }

        public string Code { get; set; }
        public string Value { get; set; }
    }
}