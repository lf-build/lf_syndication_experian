﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IAccountType
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}