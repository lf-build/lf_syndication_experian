﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IProductsResponse
    {
        ProductCreditProfile CreditProfile { get; set; }
    }
}