﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IPublicRecordBankruptcyType
    {
        string Code { get; set; }
       
    }
}