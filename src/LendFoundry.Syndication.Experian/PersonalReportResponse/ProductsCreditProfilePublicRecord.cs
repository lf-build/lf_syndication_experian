using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class PublicRecord : IPublicRecord
    {
        public PublicRecord()
        {
        }

        public PublicRecord(Proxy.PersonalReportResponse.ProductsCreditProfilePublicRecord publicRecord)
        {
            if (publicRecord != null)
            {
                Amount = publicRecord.Amount;
                if (publicRecord.Bankruptcy != null)
                    Bankruptcy =
                        new PublicRecordBankruptcy(publicRecord.Bankruptcy);
                BookPageSequence = publicRecord.BookPageSequence;
                ConsumerComment = publicRecord.ConsumerComment;
                if (publicRecord.Court != null)
                    Court = new PublicRecordCourt(publicRecord.Court);
                if (publicRecord.ECOA != null)
                    PublicRecordEcoa = new PublicRecordEcoa(publicRecord.ECOA.code);
                if (publicRecord.Evaluation != null)
                    PublicRecordEvaluation =
                        new PublicRecordEvaluation(publicRecord.Evaluation.code, publicRecord.Evaluation.value);
                FilingDate = publicRecord.FilingDate;
                PlaintiffName = publicRecord.PlaintiffName;
                ReferenceNumber = publicRecord.ReferenceNumber;
                if (publicRecord.Status != null)
                    Status = new RecordStatus(publicRecord.Status.code, publicRecord.Status.value);
                StatusDate = publicRecord.StatusDate;
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IRecordStatus, RecordStatus>))]
        public IRecordStatus Status { get; set; }

        public string StatusDate { get; set; }
        public string FilingDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPublicRecordEvaluation, PublicRecordEvaluation>))]
        public IPublicRecordEvaluation PublicRecordEvaluation { get; set; }

        public string Amount { get; set; }
        public string ConsumerComment { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPublicRecordCourt, PublicRecordCourt>))]
        public IPublicRecordCourt Court { get; set; }

        public string ReferenceNumber { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPublicRecordEcoa, PublicRecordEcoa>))]
        public IPublicRecordEcoa PublicRecordEcoa { get; set; }

        public string BookPageSequence { get; set; }
        public string PlaintiffName { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPublicRecordBankruptcy, PublicRecordBankruptcy>))]
        public IPublicRecordBankruptcy Bankruptcy { get; set; }
        public string Designator { get; set; }
    }
}