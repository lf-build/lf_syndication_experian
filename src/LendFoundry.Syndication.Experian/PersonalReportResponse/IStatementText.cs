﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IStatementText
    {
        string MessageText { get; set; }
    }
}