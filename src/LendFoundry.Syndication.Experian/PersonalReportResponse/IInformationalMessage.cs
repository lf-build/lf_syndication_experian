﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IInformationalMessage
    {
        string MessageNumber { get; set; }
        string MessageText { get; set; }
    }
}