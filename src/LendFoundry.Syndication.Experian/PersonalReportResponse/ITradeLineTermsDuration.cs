﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ITradeLineTermsDuration
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}