using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class Inquiry : IInquiry
    {
        public Inquiry()
        {
        }

        public Inquiry(Proxy.PersonalReportResponse.ProductsCreditProfileInquiry inquiry)
        {
            if (inquiry != null)
            {
                Amount = inquiry.Amount;
                Date = inquiry.Date;
                if (inquiry.KOB != null)
                    Kob = new InquiryKob(inquiry.KOB);
                Subcode = inquiry.Subcode;
                SubscriberDisplayName = inquiry.SubscriberDisplayName;
                if (inquiry.Terms != null)
                    Terms = new InquiryTerms(inquiry.Terms);
                if (inquiry.Type != null)
                    Type = new InquiryType(inquiry.Type);
            }
        }

        public string Date { get; set; }
        public string Amount { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IInquiryType, InquiryType>))]
        public IInquiryType Type { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IInquiryTerms, InquiryTerms>))]
        public IInquiryTerms Terms { get; set; }

        public string Subcode { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IInquiryKob, InquiryKob>))]
        public IInquiryKob Kob { get; set; }

        public string SubscriberDisplayName { get; set; }
    }
}