﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IAddressInformation
    {
        string CensusGeoCode { get; set; }
        string City { get; set; }
        string CountyCode { get; set; }
        IDwellingType DwellingType { get; set; }
        string FirstReportedDate { get; set; }
        IHomeOwnership HomeOwnership { get; set; }
        string LastReportingSubcode { get; set; }
        string LastUpdatedDate { get; set; }
        IOrigination Origination { get; set; }
        string State { get; set; }
        string StreetName { get; set; }
        string StreetPrefix { get; set; }
        string StreetSuffix { get; set; }
        string TimesReported { get; set; }
        string UnitId { get; set; }
        string UnitType { get; set; }
        string Zip { get; set; }
    }
}