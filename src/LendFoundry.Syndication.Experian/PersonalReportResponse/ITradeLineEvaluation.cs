﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ITradeLineEvaluation
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}