namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class Products : IProducts
    {
        public Products()
        {
        }

        public Products(Proxy.PersonalReportResponse.Products products)
        {
            if (products?.CustomSolution != null)
                CreditProfile = new ProductCreditProfile(products.CustomSolution);
        }

        public IProductCreditProfile CreditProfile { get; set; }
    }
}