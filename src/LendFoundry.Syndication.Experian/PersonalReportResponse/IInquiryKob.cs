﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IInquiryKob
    {
        string Code { get; set; }
    }
}