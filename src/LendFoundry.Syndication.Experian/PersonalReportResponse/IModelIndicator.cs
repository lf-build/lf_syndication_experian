﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IModelIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}