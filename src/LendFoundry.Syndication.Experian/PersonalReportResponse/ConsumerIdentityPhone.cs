using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class ConsumerIdentityPhone : IConsumerIdentityPhone
    {
        public ConsumerIdentityPhone()
        {
        }

        public ConsumerIdentityPhone(Proxy.PersonalReportResponse.ProductsCreditProfileConsumerIdentityPhone Phone)
        {
            if (Phone != null)
            {
                Number = Phone.Number;
                Type = new ProductsCreditProfileConsumerIdentityPhoneType(Phone.Type);
                Source = new ProductsCreditProfileConsumerIdentityPhoneSource(Phone.Source);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IProductsCreditProfileConsumerIdentityPhoneType, ProductsCreditProfileConsumerIdentityPhoneType>))]

        public IProductsCreditProfileConsumerIdentityPhoneType Type { get; set; }

        public string Number { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IProductsCreditProfileConsumerIdentityPhoneSource, ProductsCreditProfileConsumerIdentityPhoneSource>))]
        public IProductsCreditProfileConsumerIdentityPhoneSource Source { get; set; }

    }
}