﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IStatementType
    {
        string Code { get; set; }
    }
}