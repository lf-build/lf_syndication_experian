﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class Error : IError
    {
        public Error()
        {

        }

        public Error(Proxy.PersonalReportResponse.Error error)
        {
            if (error != null)
            {
                HostID = error.HostID;
                ApplicationID = error.ApplicationID;
                ReportDate = error.ReportDate;
                ReportTime = error.ReportTime;
                ReportType = error.ReportType;
                Preamble = error.Preamble;
                RegionCode = error.RegionCode;
                ARFVersion = error.ARFVersion;
                Surname = error.Surname;
                FirstName = error.FirstName;
                ErrorCode = error.ErrorCode;
                ModuleID = error.ModuleID;
                ReferenceNumber = error.ReferenceNumber;
                ActionIndicator = new ProductsErrorActionIndicator(error.ActionIndicator);
            }
        }

        public string HostID { get; set; }
        public string ApplicationID { get; set; }
        public string ReportDate { get; set; }
        public string ReportTime { get; set; }
        public string ReportType { get; set; }
        public string Preamble { get; set; }
        public string RegionCode { get; set; }
        public string ARFVersion { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string ErrorCode { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IProductsErrorActionIndicator, ProductsErrorActionIndicator>))]
        public IProductsErrorActionIndicator ActionIndicator { get; set; }
        public string ModuleID { get; set; }
        public string ReferenceNumber { get; set; }
    }
}
