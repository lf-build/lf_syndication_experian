namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class InformationalMessage : IInformationalMessage
    {
        public InformationalMessage()
        {
        }

        public InformationalMessage(Proxy.PersonalReportResponse.ProductsCreditProfileInformationalMessage informationalMessage)
        {
            if (informationalMessage != null)
            {
                MessageNumber = informationalMessage.MessageNumber;
                MessageText = informationalMessage.MessageText;
            }
        }

        public string MessageNumber { get; set; }
        public string MessageText { get; set; }
    }
}