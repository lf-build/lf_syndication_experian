﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ITradeLineEnhancedPaymentData
    {
        IPaymentDataAccountCondition AccountCondition { get; set; }
        IPaymentDataAccountType AccountType { get; set; }
        string InitialPaymentLevelDate { get; set; }
        IPaymentStatus PaymentStatus { get; set; }
        ISpecialComment SpecialComment { get; set; }
    }
}