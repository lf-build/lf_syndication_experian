﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IProcessingMessage
    {
        string ProcessingAction { get; set; }
    }
}