namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class DwellingType : IDwellingType
    {
        public DwellingType()
        {
        }

        public DwellingType(Proxy.PersonalReportResponse.ProductsCreditProfileAddressInformationDwellingType dwellingType)
        {
            if (dwellingType != null)
            {
                Code = dwellingType.code;
                Value = dwellingType.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}