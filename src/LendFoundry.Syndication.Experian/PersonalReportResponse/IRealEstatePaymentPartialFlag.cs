﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IRealEstatePaymentPartialFlag
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}