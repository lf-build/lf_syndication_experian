﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IAddressErrorCode
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}