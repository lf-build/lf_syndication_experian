using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class FraudServices : IFraudServices
    {
        public FraudServices()
        {
        }

        public FraudServices(Proxy.PersonalReportResponse.ProductsCreditProfileFraudServices fraudServices)
        {
            if (fraudServices != null)
            {
                AddressCount = fraudServices.AddressCount;
                AddressDate = fraudServices.AddressDate;
                if (fraudServices.AddressErrorCode != null)
                    AddressErrorCode =
                        new AddressErrorCode(
                            fraudServices.AddressErrorCode.code, fraudServices.AddressErrorCode.value);
                DateOfBirth = fraudServices.DateOfBirth;
                DateOfDeath = fraudServices.DateOfDeath;
                if (fraudServices.SIC != null)
                    Sic = new FraudServicesSic(fraudServices.SIC);
                SocialCount = fraudServices.SocialCount;
                SocialDate = fraudServices.SocialDate;
                if (fraudServices.SocialErrorCode != null)
                    SocialErrorCode =
                        new SocialErrorCode(
                            fraudServices.SocialErrorCode.code, fraudServices.SocialErrorCode.value);
                SsnFirstPossibleIssuanceYear = fraudServices.SSNFirstPossibleIssuanceYear;
                SsnLastPossibleIssuanceYear = fraudServices.SSNLastPossibleIssuanceYear;
                Text = fraudServices.Text;
                if (fraudServices.Type != null)
                    Type = new FraudServicesType(fraudServices.Type);
                Indicator = fraudServices.Indicator;
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IFraudServicesType, FraudServicesType>))]
        public IFraudServicesType Type { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IFraudServicesSic, FraudServicesSic>))]
        public IFraudServicesSic Sic { get; set; }

        public string Text { get; set; }
        public string SocialDate { get; set; }
        public string SocialCount { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISocialErrorCode, SocialErrorCode>))]
        public ISocialErrorCode SocialErrorCode { get; set; }

        public string AddressDate { get; set; }
        public string AddressCount { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddressErrorCode, AddressErrorCode>))]
        public IAddressErrorCode AddressErrorCode { get; set; }

        public string SsnFirstPossibleIssuanceYear { get; set; }

        public string SsnLastPossibleIssuanceYear { get; set; }

        public string DateOfBirth { get; set; }
        public string DateOfDeath { get; set; }

        public string Indicator { get; set; }

    }
}