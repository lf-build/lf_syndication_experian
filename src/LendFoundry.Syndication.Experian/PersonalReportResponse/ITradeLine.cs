﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ITradeLine
    {
        string AccountNumber { get; set; }
        IAccountType AccountType { get; set; }
        List<ITradeLineAmount> Amount { get; set; }
        string AmountPastDue { get; set; }
        string BalanceAmount { get; set; }
        string BalanceDate { get; set; }
        string ConsumerComment { get; set; }
        string DelinquenciesOver30Days { get; set; }
        string DelinquenciesOver60Days { get; set; }
        string DelinquenciesOver90Days { get; set; }
        string DerogCounter { get; set; }
        ITradeLineEcoa Ecoa { get; set; }
        ITradeLineEnhancedPaymentData EnhancedPaymentData { get; set; }
        ITradeLineEvaluation Evaluation { get; set; }
        ITradeLineKob Kob { get; set; }
        string LastPaymentDate { get; set; }
        string MaxDelinquencyDate { get; set; }
        string MonthlyPaymentAmount { get; set; }
        IMonthlyPaymentType MonthlyPaymentType { get; set; }
        string MonthsHistory { get; set; }
        string OpenDate { get; set; }
        ITradeLineOpenOrClosed OpenOrClosed { get; set; }
        string OriginalCreditorName { get; set; }
        string PaymentProfile { get; set; }
        IRevolvingOrInstallment RevolvingOrInstallment { get; set; }
        ITradeLineSpecialComment SpecialComment { get; set; }
        ITradeLineStatus Status { get; set; }
        string StatusDate { get; set; }
        string Subcode { get; set; }
        string SubscriberDisplayName { get; set; }
        ITradeLineTermsDuration TermsDuration { get; set; }
    }
}