namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class PaymentDataAccountCondition : IPaymentDataAccountCondition
    {
        public PaymentDataAccountCondition()
        {
        }

        public PaymentDataAccountCondition(string code)
        {
            if (!string.IsNullOrWhiteSpace(code))
            {
                Code = code;
            }
        }

        public string Code { get; set; }
        public string Value { get; set; }
    }
}