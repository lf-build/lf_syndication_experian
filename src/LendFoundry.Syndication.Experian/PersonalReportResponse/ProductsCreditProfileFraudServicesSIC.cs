namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class FraudServicesSic : IFraudServicesSic
    {
        public FraudServicesSic()
        {
        }

        public FraudServicesSic(Proxy.PersonalReportResponse.ProductsCreditProfileFraudServicesSIC fraudServicesSic)
        {
            if (fraudServicesSic != null)

                Code = fraudServicesSic.code;
        }

        public string Code { get; set; }
    }
}