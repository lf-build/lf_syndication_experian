﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IPublicRecordBankruptcy
    {
        string AdjustmentPercent { get; set; }
        string AssetAmount { get; set; }
        string LiabilitiesAmount { get; set; }
        string RepaymentPercent { get; set; }
        IPublicRecordBankruptcyType Type { get; set; }
    }
}