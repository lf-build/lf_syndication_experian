using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class Statement : IStatement
    {
        public Statement()
        {
        }

        public Statement(Proxy.PersonalReportResponse.ProductsCreditProfileStatement productsCreditProfileStatement)
        {
            if (productsCreditProfileStatement != null)
            {
                DateReported = productsCreditProfileStatement.DateReported;
                if (productsCreditProfileStatement.StatementText != null)
                    StatementText =
                        new StatementText(productsCreditProfileStatement.StatementText.MessageText);
                if (productsCreditProfileStatement.Type != null)
                    Type = new StatementType(productsCreditProfileStatement.Type.code);
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IStatementType, StatementType>))]
        public IStatementType Type { get; set; }

        public string DateReported { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IStatementText, StatementText>))]
        public IStatementText StatementText { get; set; }
    }
}