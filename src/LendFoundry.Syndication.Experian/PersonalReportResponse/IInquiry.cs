﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IInquiry
    {
        string Amount { get; set; }
        string Date { get; set; }
        IInquiryKob Kob { get; set; }
        string Subcode { get; set; }
        string SubscriberDisplayName { get; set; }
        IInquiryTerms Terms { get; set; }
        IInquiryType Type { get; set; }
    }
}