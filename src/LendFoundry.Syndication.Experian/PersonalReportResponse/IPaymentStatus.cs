﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IPaymentStatus
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}