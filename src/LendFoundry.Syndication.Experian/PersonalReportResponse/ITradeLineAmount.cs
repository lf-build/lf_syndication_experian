﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ITradeLineAmount
    {
        ITradeLineAmountQualifier Qualifier { get; set; }
        string Value { get; set; }
    }
}