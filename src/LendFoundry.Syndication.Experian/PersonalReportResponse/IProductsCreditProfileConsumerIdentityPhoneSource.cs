﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IProductsCreditProfileConsumerIdentityPhoneSource
    {
         string Code { get; set; }
         string Value { get; set; }
    }
}