namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class PaymentStatus : IPaymentStatus
    {
        public PaymentStatus()
        {
        }

        public PaymentStatus(string code)
        {
            if (!string.IsNullOrWhiteSpace(code))
                Code = code;
        }

        public string Code { get; set; }
        public string Value { get; set; }
    }
}