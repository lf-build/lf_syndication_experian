﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IRevolvingOrInstallment
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}