﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IMonthlyPaymentPartialFlag
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}