﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IProductsErrorActionIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}
