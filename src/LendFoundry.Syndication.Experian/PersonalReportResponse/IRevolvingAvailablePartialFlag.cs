﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IRevolvingAvailablePartialFlag
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}