﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IProductsCreditProfileEmploymentInformationOrigination
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}