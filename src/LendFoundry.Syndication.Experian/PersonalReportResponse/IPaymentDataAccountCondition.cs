﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IPaymentDataAccountCondition
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}