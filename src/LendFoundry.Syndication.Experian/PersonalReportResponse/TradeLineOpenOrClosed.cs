namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class TradeLineOpenOrClosed : ITradeLineOpenOrClosed
    {
        public TradeLineOpenOrClosed()
        {
        }

        public TradeLineOpenOrClosed(string code, string value)
        {
            if (!string.IsNullOrWhiteSpace(code))
            {
                Code = code;
            }
            if (!string.IsNullOrWhiteSpace(value))
            {
                Value = value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}