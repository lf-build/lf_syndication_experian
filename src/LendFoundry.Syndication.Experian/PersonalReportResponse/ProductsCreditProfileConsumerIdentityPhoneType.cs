namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class ProductsCreditProfileConsumerIdentityPhoneSource : IProductsCreditProfileConsumerIdentityPhoneSource
    {
        public ProductsCreditProfileConsumerIdentityPhoneSource()
        {
        }

        public ProductsCreditProfileConsumerIdentityPhoneSource(Proxy.PersonalReportResponse.ProductsCreditProfileConsumerIdentityPhoneSource productsCreditProfileConsumerIdentityPhoneSource)
        {
            if (productsCreditProfileConsumerIdentityPhoneSource != null)
            {
                Code = productsCreditProfileConsumerIdentityPhoneSource.code;
                Value = productsCreditProfileConsumerIdentityPhoneSource.value;
            }
        }

        public string Code { get; set; }
        public string Value { get; set; }

    }
}