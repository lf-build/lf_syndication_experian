namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class ProductsCreditProfileConsumerIdentityPhoneType : IProductsCreditProfileConsumerIdentityPhoneType
    {
        public ProductsCreditProfileConsumerIdentityPhoneType()
        {
        }

        public ProductsCreditProfileConsumerIdentityPhoneType(Proxy.PersonalReportResponse.ProductsCreditProfileConsumerIdentityPhoneType productsCreditProfileConsumerIdentityPhoneType)
        {
            if (productsCreditProfileConsumerIdentityPhoneType != null)
            {
                Code = productsCreditProfileConsumerIdentityPhoneType.code;
                Value = productsCreditProfileConsumerIdentityPhoneType.value;
            }
        }

        public string Code { get; set; }
        public string Value { get; set; }

    }
}