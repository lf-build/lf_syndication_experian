﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IRiskModel
    {
        IRiskModelEvaluation Evaluation { get; set; }
        IModelIndicator ModelIndicator { get; set; }
        string Score { get; set; }
        string ScoreFactorCodeFour { get; set; }
        string ScoreFactorCodeOne { get; set; }
        string ScoreFactorCodeThree { get; set; }
        string ScoreFactorCodeTwo { get; set; }
        string ScoreFactorCodeFourDesc { get; set; }
        string ScoreFactorCodeOneDesc { get; set; }
        string ScoreFactorCodeThreeDesc { get; set; }
        string ScoreFactorCodeTwoDesc { get; set; }
        string ScorePercentile { get; set; }
    }
}