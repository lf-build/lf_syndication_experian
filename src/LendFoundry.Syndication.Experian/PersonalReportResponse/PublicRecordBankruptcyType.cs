using LendFoundry.Foundation.Lookup;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class PublicRecordBankruptcyType : IPublicRecordBankruptcyType
    {
        public PublicRecordBankruptcyType()
        {
        }

        public PublicRecordBankruptcyType(Proxy.PersonalReportResponse.ProductsCreditProfilePublicRecordBankruptcyType productsCreditProfilePublicRecordBankruptcyType)
        {
            if (productsCreditProfilePublicRecordBankruptcyType != null)
            {

                Code = productsCreditProfilePublicRecordBankruptcyType.code;
            }
        }

        public string Code { get; set; }
       
    }
}