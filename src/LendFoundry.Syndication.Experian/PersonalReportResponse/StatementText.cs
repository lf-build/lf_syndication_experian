namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class StatementText : IStatementText
    {
        public StatementText()
        {
        }

        public StatementText(string messageText)
        {
            if (!string.IsNullOrWhiteSpace(messageText))
                MessageText = messageText;
        }

        public string MessageText { get; set; }
    }
}