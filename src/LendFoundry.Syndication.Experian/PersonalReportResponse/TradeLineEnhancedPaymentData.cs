using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class TradeLineEnhancedPaymentData : ITradeLineEnhancedPaymentData
    {
        public TradeLineEnhancedPaymentData()
        {
        }

        public TradeLineEnhancedPaymentData(Proxy.PersonalReportResponse.ProductsCreditProfileTradeLineEnhancedPaymentData productsCreditProfileTradeLineEnhancedPaymentData)
        {
            if (productsCreditProfileTradeLineEnhancedPaymentData != null)
            {
                if (productsCreditProfileTradeLineEnhancedPaymentData.AccountCondition != null)
                    AccountCondition =
                        new PaymentDataAccountCondition(
                            productsCreditProfileTradeLineEnhancedPaymentData.AccountCondition.code);
                if (productsCreditProfileTradeLineEnhancedPaymentData.AccountType != null)
                    AccountType =
                        new PaymentDataAccountType(
                            productsCreditProfileTradeLineEnhancedPaymentData.AccountType.code);
                InitialPaymentLevelDate = productsCreditProfileTradeLineEnhancedPaymentData.InitialPaymentLevelDate;
                if (productsCreditProfileTradeLineEnhancedPaymentData.PaymentStatus != null)
                    PaymentStatus =
                        new PaymentStatus(
                            productsCreditProfileTradeLineEnhancedPaymentData.PaymentStatus.code);
                if (productsCreditProfileTradeLineEnhancedPaymentData.SpecialComment != null)
                    SpecialComment =
                        new SpecialComment(
                            productsCreditProfileTradeLineEnhancedPaymentData.SpecialComment.code);
            }
        }

        public string InitialPaymentLevelDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPaymentDataAccountCondition, PaymentDataAccountCondition>))]
        public IPaymentDataAccountCondition AccountCondition { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPaymentStatus, PaymentStatus>))]
        public IPaymentStatus PaymentStatus { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPaymentDataAccountType, PaymentDataAccountType>))]
        public IPaymentDataAccountType AccountType { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISpecialComment, SpecialComment>))]
        public ISpecialComment SpecialComment { get; set; }
    }
}