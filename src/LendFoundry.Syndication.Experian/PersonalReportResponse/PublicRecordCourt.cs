namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class PublicRecordCourt : IPublicRecordCourt
    {
        public PublicRecordCourt()
        {
        }

        public PublicRecordCourt(Proxy.PersonalReportResponse.ProductsCreditProfilePublicRecordCourt productsCreditProfilePublicRecordCourt)
        {
            if (productsCreditProfilePublicRecordCourt != null)
            {
                Code = productsCreditProfilePublicRecordCourt.code;
                Name = productsCreditProfilePublicRecordCourt.name;
            }
        }

        public string Code { get; set; }

        public string Name { get; set; }
    }
}