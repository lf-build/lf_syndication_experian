﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IError
    {
        string HostID { get; set; }
        string ApplicationID { get; set; }
        string ReportDate { get; set; }
        string ReportTime { get; set; }
        string ReportType { get; set; }
        string Preamble { get; set; }
        string RegionCode { get; set; }
        string ARFVersion { get; set; }
        string Surname { get; set; }
        string FirstName { get; set; }
        string ErrorCode { get; set; }
        IProductsErrorActionIndicator ActionIndicator { get; set; }
        string ModuleID { get; set; }
        string ReferenceNumber { get; set; }
    }
}
