﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ITradeLineStatus
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}