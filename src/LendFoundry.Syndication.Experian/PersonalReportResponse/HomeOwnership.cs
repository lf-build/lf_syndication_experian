namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class HomeOwnership : IHomeOwnership
    {
        public HomeOwnership()
        {
        }

        public HomeOwnership(Proxy.PersonalReportResponse.ProductsCreditProfileAddressInformationHomeOwnership homeOwnership)
        {
            if (homeOwnership != null)
            {
                Code = homeOwnership.code;
                Value = homeOwnership.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}