using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class ProfileSummary : IProfileSummary
    {
        public ProfileSummary()
        {
        }

        public ProfileSummary(Proxy.PersonalReportResponse.ProductsCreditProfileProfileSummary profileSummary)
        {
            if (profileSummary != null)
            {
                DelinquenciesOver30Days = profileSummary.DelinquenciesOver30Days;
                DelinquenciesOver60Days = profileSummary.DelinquenciesOver60Days;
                DelinquenciesOver90Days = profileSummary.DelinquenciesOver90Days;
                DerogCounter = profileSummary.DerogCounter;
                DisputedAccountsExcluded = profileSummary.DisputedAccountsExcluded;
                InquiriesDuringLast6Months = profileSummary.InquiriesDuringLast6Months;
                InstallmentBalance = profileSummary.InstallmentBalance;
                MonthlyPayment = profileSummary.MonthlyPayment;
                if (profileSummary.MonthlyPaymentPartialFlag != null)
                    MonthlyPaymentPartialFlag =
                        new MonthlyPaymentPartialFlag(
                            profileSummary.MonthlyPaymentPartialFlag);
                NowDelinquentDerog = profileSummary.NowDelinquentDerog;
                OldestTradeOpenDate = profileSummary.OldestTradeOpenDate;
                PaidAccounts = profileSummary.PaidAccounts;
                PastDueAmount = profileSummary.PastDueAmount;
                PublicRecordsCount = profileSummary.PublicRecordsCount;
                RealEstateBalance = profileSummary.RealEstateBalance;
                RealEstatePayment = profileSummary.RealEstatePayment;
                if (profileSummary.RealEstatePaymentPartialFlag != null)
                    RealEstatePaymentPartialFlag =
                        new RealEstatePaymentPartialFlag(
                            profileSummary.RealEstatePaymentPartialFlag);
                if (profileSummary.RevolvingAvailablePartialFlag != null)
                    RevolvingAvailablePartialFlag =
                        new RevolvingAvailablePartialFlag(
                            profileSummary.RevolvingAvailablePartialFlag);
                RevolvingAvailablePercent = profileSummary.RevolvingAvailablePercent;
                RevolvingBalance = profileSummary.RevolvingBalance;
                SatisfactoryAccounts = profileSummary.SatisfactoryAccounts;
                TotalInquiries = profileSummary.TotalInquiries;
                TotalTradeItems = profileSummary.TotalTradeItems;
                WasDelinquentDerog = profileSummary.WasDelinquentDerog;
            }
        }

        public string DisputedAccountsExcluded { get; set; }
        public string PublicRecordsCount { get; set; }
        public string InstallmentBalance { get; set; }
        public string RealEstateBalance { get; set; }
        public string RevolvingBalance { get; set; }
        public string PastDueAmount { get; set; }
        public string MonthlyPayment { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IMonthlyPaymentPartialFlag, MonthlyPaymentPartialFlag>))]
        public IMonthlyPaymentPartialFlag MonthlyPaymentPartialFlag { get; set; }

        public string RealEstatePayment { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IRealEstatePaymentPartialFlag, RealEstatePaymentPartialFlag>))]
        public IRealEstatePaymentPartialFlag RealEstatePaymentPartialFlag
        {
            get; set;
        }

        public string RevolvingAvailablePercent { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IRevolvingAvailablePartialFlag, RevolvingAvailablePartialFlag>))]
        public IRevolvingAvailablePartialFlag RevolvingAvailablePartialFlag
        {
            get;
            set;
        }

        public string TotalInquiries { get; set; }
        public string InquiriesDuringLast6Months { get; set; }
        public string TotalTradeItems { get; set; }
        public string PaidAccounts { get; set; }
        public string SatisfactoryAccounts { get; set; }
        public string NowDelinquentDerog { get; set; }
        public string WasDelinquentDerog { get; set; }
        public string OldestTradeOpenDate { get; set; }
        public string DelinquenciesOver30Days { get; set; }
        public string DelinquenciesOver60Days { get; set; }
        public string DelinquenciesOver90Days { get; set; }
        public string DerogCounter { get; set; }
    }
}