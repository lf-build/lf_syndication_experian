﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IConsumerIdentityPhone
    {
        IProductsCreditProfileConsumerIdentityPhoneType Type { get; set; }

         string Number { get; set; }
        IProductsCreditProfileConsumerIdentityPhoneSource Source { get; set; }
    }
}