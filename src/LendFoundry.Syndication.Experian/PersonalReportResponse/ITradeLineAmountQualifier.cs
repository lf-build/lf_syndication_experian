﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ITradeLineAmountQualifier
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}