﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ITradeLineKob
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}