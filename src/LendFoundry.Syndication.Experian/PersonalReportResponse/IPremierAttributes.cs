﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IPremierAttributes
    {
        List<IAttributes> Attributes { get; set; }
    }
}