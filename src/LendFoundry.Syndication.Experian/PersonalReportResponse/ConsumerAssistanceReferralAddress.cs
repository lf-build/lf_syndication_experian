namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class ConsumerAssistanceReferralAddress : IConsumerAssistanceReferralAddress
    {
        public ConsumerAssistanceReferralAddress()
        {
        }

        public ConsumerAssistanceReferralAddress(Proxy.PersonalReportResponse.ProductsCreditProfileConsumerAssistanceReferralAddress consumerAssistanceReferralAddress)
        {
            if (consumerAssistanceReferralAddress != null)
            {
                CityStateZip = consumerAssistanceReferralAddress.CityStateZip;
                OfficeName = consumerAssistanceReferralAddress.OfficeName;
                Phone = consumerAssistanceReferralAddress.Phone;
                PoBox = consumerAssistanceReferralAddress.POBox;
                StreetName = consumerAssistanceReferralAddress.StreetName;
            }
        }

        public string OfficeName { get; set; }
        public string StreetName { get; set; }

        public string PoBox { get; set; }

        public string CityStateZip { get; set; }
        public string Phone { get; set; }
    }
}