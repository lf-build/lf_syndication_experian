using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class PublicRecordBankruptcy : IPublicRecordBankruptcy
    {
        public PublicRecordBankruptcy()
        {
        }

        public PublicRecordBankruptcy(Proxy.PersonalReportResponse.ProductsCreditProfilePublicRecordBankruptcy publicRecordBankruptcy)
        {
            if (publicRecordBankruptcy != null)
            {
                AdjustmentPercent = publicRecordBankruptcy.AdjustmentPercent;
                AssetAmount = publicRecordBankruptcy.AssetAmount;
                LiabilitiesAmount = publicRecordBankruptcy.LiabilitiesAmount;
                RepaymentPercent = publicRecordBankruptcy.RepaymentPercent;
                if (publicRecordBankruptcy.Type != null)
                    Type =
                        new PublicRecordBankruptcyType(
                            publicRecordBankruptcy.Type);
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IPublicRecordBankruptcyType, PublicRecordBankruptcyType>))]
        public IPublicRecordBankruptcyType Type { get; set; }

        public string AssetAmount { get; set; }
        public string LiabilitiesAmount { get; set; }
        public string RepaymentPercent { get; set; }
        public string AdjustmentPercent { get; set; }
    }
}