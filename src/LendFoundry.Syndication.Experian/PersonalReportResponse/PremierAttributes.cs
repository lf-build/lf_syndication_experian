﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class PremierAttributes:IPremierAttributes
    {
        public PremierAttributes()
        {
                
        }
        public PremierAttributes(Proxy.PersonalReportResponse.Attributes attributes)
        {
            if (attributes != null)
                Attributes =
                    attributes.Attribute.Select(p => new Attributes(p))
                        .ToList<IAttributes>();
        }


        [JsonConverter(typeof(InterfaceListConverter<IAttributes, Attributes>))]
        public List<IAttributes> Attributes { get; set; }
    }
}