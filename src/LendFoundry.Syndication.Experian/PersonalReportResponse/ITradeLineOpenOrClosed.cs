﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ITradeLineOpenOrClosed
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}