﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface ISsnVariationIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}