namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class SsnVariationIndicator : ISsnVariationIndicator
    {
        public SsnVariationIndicator()
        {
        }

        public SsnVariationIndicator(Proxy.PersonalReportResponse.ProductsCreditProfileSSNVariationIndicator productsCreditProfileSsnVariationIndicator)
        {
            if (productsCreditProfileSsnVariationIndicator != null)
            {
                Code = productsCreditProfileSsnVariationIndicator.code;
                Value = productsCreditProfileSsnVariationIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}