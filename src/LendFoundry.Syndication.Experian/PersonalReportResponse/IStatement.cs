﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IStatement
    {
        string DateReported { get; set; }
        IStatementText StatementText { get; set; }
        IStatementType Type { get; set; }
    }
}