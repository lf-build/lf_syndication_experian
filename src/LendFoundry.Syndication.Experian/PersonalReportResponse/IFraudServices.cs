﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IFraudServices
    {
        string AddressCount { get; set; }
        string AddressDate { get; set; }
        IAddressErrorCode AddressErrorCode { get; set; }
        string DateOfBirth { get; set; }
        string DateOfDeath { get; set; }
        IFraudServicesSic Sic { get; set; }
        string SocialCount { get; set; }
        string SocialDate { get; set; }
        ISocialErrorCode SocialErrorCode { get; set; }
        string SsnFirstPossibleIssuanceYear { get; set; }
        string SsnLastPossibleIssuanceYear { get; set; }
        string Text { get; set; }
        IFraudServicesType Type { get; set; }
         string Indicator { get; set; }

    }
}