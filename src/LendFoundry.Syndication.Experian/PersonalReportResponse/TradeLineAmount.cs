using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class TradeLineAmount : ITradeLineAmount
    {
        public TradeLineAmount()
        {
        }

        public TradeLineAmount(Proxy.PersonalReportResponse.ProductsCreditProfileTradeLineAmount productsCreditProfileTradeLineAmount)
        {
            if (productsCreditProfileTradeLineAmount != null)
            {
                if (productsCreditProfileTradeLineAmount.Qualifier != null)
                    Qualifier =
                        new TradeLineAmountQualifier(productsCreditProfileTradeLineAmount.Qualifier.code, productsCreditProfileTradeLineAmount.Qualifier.Value);
                Value = productsCreditProfileTradeLineAmount.Value;
            }
        }

        [JsonConverter(typeof(InterfaceConverter<ITradeLineAmountQualifier, TradeLineAmountQualifier>))]
        public ITradeLineAmountQualifier Qualifier { get; set; }

        public string Value { get; set; }
    }
}