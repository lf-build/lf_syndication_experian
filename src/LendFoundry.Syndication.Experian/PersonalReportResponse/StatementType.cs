namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class StatementType : IStatementType
    {
        public StatementType()
        {
        }

        public StatementType(string code)
        {
            if (!string.IsNullOrWhiteSpace(code))

                Code = code;
        }

        public string Code { get; set; }
    }
}