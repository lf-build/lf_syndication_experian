﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IFraudServicesSic
    {
        string Code { get; set; }
    }
}