namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class PaymentDataAccountType : IPaymentDataAccountType
    {
        public PaymentDataAccountType()
        {
        }

        public PaymentDataAccountType(string code)
        {
            if (!string.IsNullOrWhiteSpace(code))
            {
                Code = code;
            }
        }

        public string Code { get; set; }
        public string Value { get; set; }
    }
}