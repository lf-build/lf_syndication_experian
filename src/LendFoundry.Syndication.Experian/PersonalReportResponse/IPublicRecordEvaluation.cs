﻿namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public interface IPublicRecordEvaluation
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}