namespace LendFoundry.Syndication.Experian.PersonalReportResponse
{
    public class ProductsCreditProfileDirectCheck : IProductsCreditProfileDirectCheck
    {
        public ProductsCreditProfileDirectCheck()
        {
        }

        public ProductsCreditProfileDirectCheck(Proxy.PersonalReportResponse.ProductsCreditProfileDirectCheck productsCreditProfileDirectCheck)
        {
            if (productsCreditProfileDirectCheck != null)
            {
                SubscriberAddress = productsCreditProfileDirectCheck.SubscriberAddress;
                SubscriberCity = productsCreditProfileDirectCheck.SubscriberCity;
                SubscriberName = productsCreditProfileDirectCheck.SubscriberName;
                SubscriberNumber = productsCreditProfileDirectCheck.SubscriberNumber;
                SubscriberPhone = productsCreditProfileDirectCheck.SubscriberPhone;
                SubscriberState = productsCreditProfileDirectCheck.SubscriberState;
                SubscriberZip = productsCreditProfileDirectCheck.SubscriberZip;
            }
        }

        public string SubscriberNumber { get; set; }
        public string SubscriberName { get; set; }
        public string SubscriberAddress { get; set; }
        public string SubscriberCity { get; set; }
        public string SubscriberState { get; set; }
        public string SubscriberZip { get; set; }
        public string SubscriberPhone { get; set; }
    }
}