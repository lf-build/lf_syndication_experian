﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.Experian
{
    /// <summary>
    /// Class DatamerchException.
    /// </summary>
    /// <seealso cref="System.Exception" />
    [Serializable]
    public class ExperianException : Exception
    {
        #region Public Constructors

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ExperianException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExperianException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public ExperianException(string message) : this(null, message, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExperianException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="message">The message.</param>
        public ExperianException(string errorCode, string message) : this(errorCode, message, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExperianException" /> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public ExperianException(string message, Exception innerException) : this(null, message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExperianException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public ExperianException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }

        #endregion Public Constructors

        #region Protected Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ExperianException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        protected ExperianException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        #endregion Protected Constructors

        #region Public Properties

        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>The error code.</value>
        public string ErrorCode { get; set; }

        #endregion Public Properties
    }
}