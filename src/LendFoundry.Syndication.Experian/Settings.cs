﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Syndication.Experian
{
    public class Settings
    {        
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "experian";
        public static string Schedule => Environment.GetEnvironmentVariable($"{ServiceName}_TASK_SCHEDULE") ?? "0 0 12 1/1 * ? *";
    }
}