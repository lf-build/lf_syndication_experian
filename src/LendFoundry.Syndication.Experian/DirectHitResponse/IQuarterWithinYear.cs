namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IQuarterWithinYear
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}