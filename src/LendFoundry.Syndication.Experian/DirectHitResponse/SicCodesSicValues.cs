namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class SicCodesSicValues : ISicCodesSicValues
    {
        public SicCodesSicValues()
        {
        }

        public SicCodesSicValues(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileSICCodesSICValues sicCodesSicValues)
        {
            if (sicCodesSicValues != null)
            {
                Code = sicCodesSicValues.code;
                Value = sicCodesSicValues.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}