namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class PaymentTrendIndicator : IPaymentTrendIndicator
    {
        public PaymentTrendIndicator()
        {
        }

        public PaymentTrendIndicator(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileExecutiveSummaryPaymentTrendIndicator paymentTrendIndicator)
        {
            if (paymentTrendIndicator != null)
            {
                Code = paymentTrendIndicator.code;
                Value = paymentTrendIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}