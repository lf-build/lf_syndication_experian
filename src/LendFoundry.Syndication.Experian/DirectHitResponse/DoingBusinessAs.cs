namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class DoingBusinessAs : IDoingBusinessAs
    {
        public DoingBusinessAs()
        {
        }

        public DoingBusinessAs(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileDoingBusinessAs doingBusinessAs)
        {
            if (doingBusinessAs != null)

                DbaName = doingBusinessAs.DBAName;
        }

        public string DbaName { get; set; }
    }
}