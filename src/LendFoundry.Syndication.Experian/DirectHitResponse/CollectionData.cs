using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class CollectionData : ICollectionData
    {
        public CollectionData()
        {
        }

        public CollectionData(
            Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileCollectionData collectionData)
        {
            if (collectionData != null)
            {
                DatePlacedForCollection = collectionData.DatePlacedForCollection;
                DateClosed = collectionData.DateClosed;
                AmountPlacedForCollection = collectionData.AmountPlacedForCollection;
                AmountPaid = collectionData.AmountPaid;
                if (collectionData.AccountStatus != null)
                    AccountStatus =
                        collectionData.AccountStatus.Select(
                            p => new AccountStatus(p))
                            .ToList<IAccountStatus>();
                if (collectionData.CollectionAgencyInfo != null)
                    CollectionAgencyInfo =
                        collectionData.CollectionAgencyInfo.Select(
                            p => new CollectionAgencyInfo(p))
                            .ToList<ICollectionAgencyInfo>();
            }
        }

        public string DatePlacedForCollection { get; set; }
        public string DateClosed { get; set; }
        public string AmountPlacedForCollection { get; set; }
        public string AmountPaid { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAccountStatus, AccountStatus>))]
        public List<IAccountStatus> AccountStatus { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICollectionAgencyInfo, CollectionAgencyInfo>))]
        public List<ICollectionAgencyInfo> CollectionAgencyInfo { get; set; }
    }
}