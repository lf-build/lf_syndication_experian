namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IModelInformation
    {
        string ModelCode { get; set; }
        string ModelTitle { get; set; }
        string CustomModelCode { get; set; }
    }
}