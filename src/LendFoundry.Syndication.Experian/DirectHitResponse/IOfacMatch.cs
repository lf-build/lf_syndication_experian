namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IOfacMatch
    {
        string Code { get; set; }
    }
}