using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IProfileInquiry
    {
        string InquiryBusinessCategory { get; set; }
        List<IInquiryCount> InquiryCount { get; set; }
    }
}