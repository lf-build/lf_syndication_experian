namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class PubliclyHeldCompany : IPubliclyHeldCompany
    {
        public PubliclyHeldCompany()
        {
        }

        public PubliclyHeldCompany(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationPubliclyHeldCompany publiclyHeldCompany)
        {
            if (publiclyHeldCompany != null)

                Code = publiclyHeldCompany.code;
        }

        public string Code { get; set; }
    }
}