namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IPaymentIndicator
    {
        string Code { get; set; }
    }
}