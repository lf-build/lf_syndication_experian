namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IOfacMatchCode
    {
        string Code { get; set; }
    }
}