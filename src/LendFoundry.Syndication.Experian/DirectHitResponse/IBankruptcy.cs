using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IBankruptcy
    {
        string DateFiled { get; set; }
        string DocumentNumber { get; set; }
        string FilingLocation { get; set; }
        string Owner { get; set; }
        string LiabilityAmount { get; set; }
        string AssetAmount { get; set; }
        string ExemptAmount { get; set; }
        string CustomerDispute { get; set; }
        List<ILegalType> LegalType { get; set; }
        List<ILegalAction> LegalAction { get; set; }
        List<IStatement> Statement { get; set; }
    }
}