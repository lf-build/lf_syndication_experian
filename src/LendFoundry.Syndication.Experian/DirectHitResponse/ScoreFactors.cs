using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class ScoreFactors : IScoreFactors
    {
        public ScoreFactors()
        {
        }

        public ScoreFactors(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileScoreFactors scoreFactors)
        {
            if (scoreFactors != null)
            {
                ModelCode = scoreFactors.ModelCode;
                if (scoreFactors.ScoreFactor != null)
                    ScoreFactor =
                        scoreFactors.ScoreFactor.Select(p => new ScoreFactor(p))
                            .ToList<IScoreFactor>();
            }
        }

        public string ModelCode { get; set; }

        public List<IScoreFactor> ScoreFactor { get; set; }
    }
}