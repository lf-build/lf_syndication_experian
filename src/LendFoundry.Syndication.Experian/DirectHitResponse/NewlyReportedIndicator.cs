namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class NewlyReportedIndicator : INewlyReportedIndicator
    {
        public NewlyReportedIndicator()
        {
        }

        public NewlyReportedIndicator(Proxy.DirectHitReportResponse.NewlyReportedIndicator newlyReportedIndicator)
        {
            if (newlyReportedIndicator != null)
                Code = newlyReportedIndicator.code;
        }

        public string Code { get; set; }
    }
}