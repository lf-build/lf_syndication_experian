using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class RecentHighCredit : IRecentHighCredit
    {
        public RecentHighCredit()
        {
        }

        public RecentHighCredit(Proxy.DirectHitReportResponse.RecentHighCredit recentHighCredit)
        {
            if (recentHighCredit != null)
            {
                Amount = recentHighCredit.Amount;
                if (recentHighCredit.Modifier != null)
                    Modifier = recentHighCredit.Modifier.Select(p => new Modifier(p)).ToList<IModifier>();
            }
        }

        public string Amount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IModifier, Modifier>))]
        public List<IModifier> Modifier { get; set; }
    }
}