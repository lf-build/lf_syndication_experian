using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class UccFilingsCollateralCodesCollateral : IUccFilingsCollateralCodesCollateral
    {
        public UccFilingsCollateralCodesCollateral()
        {
        }

        public UccFilingsCollateralCodesCollateral(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileUCCFilingsCollateralCodesCollateral uccFilingsCollateralCodesCollateral)
        {
            if (uccFilingsCollateralCodesCollateral?.Collateral != null)
                Collateral =
                    uccFilingsCollateralCodesCollateral.Collateral.Select(
                        p => new UccFilingsCollateralCodesCollateralValues(p))
                        .ToList<IUccFilingsCollateralCodesCollateralValues>();
        }

        public List<IUccFilingsCollateralCodesCollateralValues> Collateral { get; set; }
    }
}