namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ICorporateLink
    {
        string CorporateLinkageIndicator { get; set; }
    }
}