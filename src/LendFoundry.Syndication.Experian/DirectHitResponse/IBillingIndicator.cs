namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IBillingIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}