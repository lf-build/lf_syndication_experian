namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class BusinessVictimStatementIndicator : IBusinessVictimStatementIndicator
    {
        public BusinessVictimStatementIndicator()
        {
        }

        public BusinessVictimStatementIndicator(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryBusinessVictimStatementIndicator businessVictimStatementIndicator)
        {
            if (businessVictimStatementIndicator != null)
            {
                Code = businessVictimStatementIndicator.code;
                Value = businessVictimStatementIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}