namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ILinkageRecordType
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}