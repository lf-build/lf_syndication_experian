using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class JudgmentOrAttachmentLien : IJudgmentOrAttachmentLien
    {
        public JudgmentOrAttachmentLien()
        {
        }

        public JudgmentOrAttachmentLien(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileJudgmentOrAttachmentLien judgmentOrAttachmentLien)
        {
            if (judgmentOrAttachmentLien != null)
            {
                DateFiled = judgmentOrAttachmentLien.DateFiled;
                DocumentNumber = judgmentOrAttachmentLien.DocumentNumber;
                FilingLocation = judgmentOrAttachmentLien.FilingLocation;
                LiabilityAmount = judgmentOrAttachmentLien.LiabilityAmount;
                PlaintiffName = judgmentOrAttachmentLien.PlaintiffName;
                if (judgmentOrAttachmentLien.LegalType != null)
                    LegalType =
                        judgmentOrAttachmentLien.LegalType.Select(p => new LegalType(p))
                            .ToList<ILegalType>();
                if (judgmentOrAttachmentLien.LegalAction != null)
                    LegalAction =
                        judgmentOrAttachmentLien.LegalAction.Select(p => new LegalAction(p))
                            .ToList<ILegalAction>();
            }
        }

        public string DateFiled { get; set; }
        public string DocumentNumber { get; set; }
        public string FilingLocation { get; set; }
        public string LiabilityAmount { get; set; }
        public string PlaintiffName { get; set; }

        public List<ILegalType> LegalType { get; set; }

        public List<ILegalAction> LegalAction { get; set; }
    }
}