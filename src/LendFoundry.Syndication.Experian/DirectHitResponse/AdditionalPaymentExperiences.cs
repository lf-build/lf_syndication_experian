using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class AdditionalPaymentExperiences : IAdditionalPaymentExperiences
    {
        public AdditionalPaymentExperiences()
        {
        }

        public AdditionalPaymentExperiences(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileAdditionalPaymentExperiences additionalPaymentExperiences)
        {
            if (additionalPaymentExperiences != null)
            {
                BusinessCategory = additionalPaymentExperiences.BusinessCategory;
                DateReported = additionalPaymentExperiences.DateReported;
                DateLastActivity = additionalPaymentExperiences.DateLastActivity;
                Terms = additionalPaymentExperiences.Terms;
                CurrentPercentage = additionalPaymentExperiences.CurrentPercentage;
                Dbt30 = additionalPaymentExperiences.DBT30;
                Dbt60 = additionalPaymentExperiences.DBT60;
                Dbt90 = additionalPaymentExperiences.DBT90;
                Dbt90Plus = additionalPaymentExperiences.DBT90Plus;
                Comments = additionalPaymentExperiences.Comments;
                if (additionalPaymentExperiences.RecentHighCredit != null)
                    RecentHighCredit =
                        additionalPaymentExperiences.RecentHighCredit.Select(p => new RecentHighCredit(p))
                            .ToList<IRecentHighCredit>();
                if (additionalPaymentExperiences.AccountBalance != null)
                    AccountBalance =
                        additionalPaymentExperiences.AccountBalance.Select(p => new AccountBalance(p))
                            .ToList<IAccountBalance>();
                if (additionalPaymentExperiences.TradeLineFlag != null)
                    TradeLineFlag =
                        additionalPaymentExperiences.TradeLineFlag.Select(p => new TradeLineFlag(p))
                            .ToList<ITradeLineFlag>();
                if (additionalPaymentExperiences.NewlyReportedIndicator != null)
                    NewlyReportedIndicator =
                        additionalPaymentExperiences.NewlyReportedIndicator.Select(p => new NewlyReportedIndicator(p))
                            .ToList<INewlyReportedIndicator>();
            }
        }

        public string BusinessCategory { get; set; }
        public string DateReported { get; set; }
        public string DateLastActivity { get; set; }
        public string Terms { get; set; }
        public string CurrentPercentage { get; set; }
        public string Dbt30 { get; set; }

        public string Dbt60 { get; set; }

        public string Dbt90 { get; set; }

        public string Dbt90Plus { get; set; }

        public string Comments { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IRecentHighCredit, RecentHighCredit>))]
        public List<IRecentHighCredit> RecentHighCredit { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAccountBalance, AccountBalance>))]
        public List<IAccountBalance> AccountBalance { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITradeLineFlag, TradeLineFlag>))]
        public List<ITradeLineFlag> TradeLineFlag { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<INewlyReportedIndicator, NewlyReportedIndicator>))]
        public List<INewlyReportedIndicator> NewlyReportedIndicator { get; set; }
    }
}