using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IExpandedBusinessNameAndAddress
    {
        string ExperianBin { get; set; }
        string ProfileDate { get; set; }
        string ProfileTime { get; set; }
        string TerminalNumber { get; set; }
        string BusinessName { get; set; }
        string StreetAddress { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zip { get; set; }
        string ZipExtension { get; set; }
        string PhoneNumber { get; set; }
        string TaxId { get; set; }
        string WebsiteUrl { get; set; }
        List<IProfileType> ProfileType { get; set; }

        List<IMatchingBranchAddress>
             MatchingBranchAddress
        { get; set; }

        List<ICorporateLink> CorporateLink
        {
            get; set;
        }
    }
}