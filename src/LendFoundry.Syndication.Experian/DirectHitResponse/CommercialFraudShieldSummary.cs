using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class CommercialFraudShieldSummary : ICommercialFraudShieldSummary
    {
        public CommercialFraudShieldSummary()
        {
        }

        public CommercialFraudShieldSummary(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileCommercialFraudShieldSummary commercialFraudShieldSummary)
        {
            if (commercialFraudShieldSummary != null)
            {
                BusinessBin = commercialFraudShieldSummary.BusinessBIN;
                if (commercialFraudShieldSummary.MatchingBusinessIndicator != null)
                    MatchingBusinessIndicator =
                        commercialFraudShieldSummary.MatchingBusinessIndicator.Select(
                            p => new MatchingBusinessIndicator(p)).ToList<IMatchingBusinessIndicator>();
                if (commercialFraudShieldSummary.ActiveBusinessIndicator != null)
                    ActiveBusinessIndicator =
                        commercialFraudShieldSummary.ActiveBusinessIndicator.Select(
                            p => new ActiveBusinessIndicator(p))
                            .ToList<IActiveBusinessIndicator>();

                if (commercialFraudShieldSummary.OFACMatchCode != null)
                    OfacMatchCode =
                        commercialFraudShieldSummary.OFACMatchCode.Select(
                            p => new OfacMatchCode(p))
                            .ToList<IOfacMatchCode>();

                if (commercialFraudShieldSummary.BusinessVictimStatementIndicator != null)
                    BusinessVictimStatementIndicator =
                        commercialFraudShieldSummary.BusinessVictimStatementIndicator.Select(
                            p =>
                                new BusinessVictimStatementIndicator(p))
                            .ToList<IBusinessVictimStatementIndicator>();

                if (commercialFraudShieldSummary.BusinessRiskTriggersIndicator != null)
                    BusinessRiskTriggersIndicator =
                        commercialFraudShieldSummary.BusinessRiskTriggersIndicator.Select(
                            p => new BusinessRiskTriggersIndicator(p))
                            .ToList<IBusinessRiskTriggersIndicator>();

                if (commercialFraudShieldSummary.NameAddressVerificationIndicator != null)
                    NameAddressVerificationIndicator =
                        commercialFraudShieldSummary.NameAddressVerificationIndicator.Select(
                            p =>
                                new NameAddressVerificationIndicator(p))
                            .ToList<INameAddressVerificationIndicator>
                            ();
            }
        }

        public string BusinessBin { get; set; }

        public List<IMatchingBusinessIndicator> MatchingBusinessIndicator { get; set; }

        public List<IActiveBusinessIndicator> ActiveBusinessIndicator { get; set; }

        public List<IOfacMatchCode> OfacMatchCode { get; set; }

        public List<IBusinessVictimStatementIndicator> BusinessVictimStatementIndicator { get; set; }

        public List<IBusinessRiskTriggersIndicator> BusinessRiskTriggersIndicator { get; set; }

        public List<INameAddressVerificationIndicator> NameAddressVerificationIndicator { get; set; }
    }
}