namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IBusinessVictimStatementIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}