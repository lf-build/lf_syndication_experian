﻿namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class Modifier : IModifier
    {
        public Modifier()
        {
        }

        public Modifier(Proxy.DirectHitReportResponse.Modifier modifier)
        {
            if (modifier != null)
                Code = modifier.code;
        }

        public string Code { get; set; }
    }
}