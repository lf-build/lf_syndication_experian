namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IInputSummary
    {
        string SubscriberNumber { get; set; }
        string InquiryTransactionNumber { get; set; }
        string CpuVersion { get; set; }
        string Inquiry { get; set; }
    }
}