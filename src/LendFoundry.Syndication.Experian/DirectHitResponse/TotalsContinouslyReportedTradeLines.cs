using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class TotalsContinouslyReportedTradeLines : ITotalsContinouslyReportedTradeLines
    {
        public TotalsContinouslyReportedTradeLines()
        {
        }

        public TotalsContinouslyReportedTradeLines(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfilePaymentTotalsContinouslyReportedTradeLines totalsContinouslyReportedTradeLines)
        {
            if (totalsContinouslyReportedTradeLines != null)
            {
                NumberOfLines = totalsContinouslyReportedTradeLines.NumberOfLines;
                Dbt = totalsContinouslyReportedTradeLines.DBT;
                CurrentPercentage = totalsContinouslyReportedTradeLines.CurrentPercentage;
                Dbt30 = totalsContinouslyReportedTradeLines.DBT30;
                Dbt60 = totalsContinouslyReportedTradeLines.DBT60;
                Dbt90 = totalsContinouslyReportedTradeLines.DBT90;
                if (totalsContinouslyReportedTradeLines.TotalHighCreditAmount != null)
                    TotalHighCreditAmount =
                        totalsContinouslyReportedTradeLines.TotalHighCreditAmount.Select(
                            p => new TotalHighCreditAmount(p)).ToList<ITotalHighCreditAmount>();
                if (totalsContinouslyReportedTradeLines.TotalAccountBalance != null)
                    TotalAccountBalance =
                        totalsContinouslyReportedTradeLines.TotalAccountBalance.Select(p => new TotalAccountBalance(p))
                            .ToList<ITotalAccountBalance>();
            }
        }

        public string NumberOfLines { get; set; }

        public string Dbt { get; set; }

        public string CurrentPercentage { get; set; }

        public string Dbt30 { get; set; }

        public string Dbt60 { get; set; }

        public string Dbt90 { get; set; }

        public string Dbt120 { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalHighCreditAmount, TotalHighCreditAmount>))]
        public List<ITotalHighCreditAmount> TotalHighCreditAmount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalAccountBalance, TotalAccountBalance>))]
        public List<ITotalAccountBalance> TotalAccountBalance { get; set; }
    }
}