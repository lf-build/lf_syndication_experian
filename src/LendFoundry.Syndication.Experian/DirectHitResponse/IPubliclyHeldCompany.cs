namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IPubliclyHeldCompany
    {
        string Code { get; set; }
    }
}