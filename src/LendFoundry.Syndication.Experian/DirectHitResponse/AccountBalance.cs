using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class AccountBalance : IAccountBalance
    {
        public AccountBalance()
        {
        }

        public AccountBalance(Proxy.DirectHitReportResponse.AccountBalance accountBalance)
        {
            if (accountBalance != null)
            {
                Amount = accountBalance.Amount;
                if (accountBalance.Modifier != null)
                    Modifier = accountBalance.Modifier.Select(p => new Modifier(p)).ToList<IModifier>();
            }
        }

        public string Amount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IModifier, Modifier>))]
        public List<IModifier> Modifier { get; set; }
    }
}