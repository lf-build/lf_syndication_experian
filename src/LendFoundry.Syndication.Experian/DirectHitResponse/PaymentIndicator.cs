namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class PaymentIndicator : IPaymentIndicator
    {
        public PaymentIndicator()
        {
        }

        public PaymentIndicator(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileTradePaymentExperiencesPaymentIndicator paymentIndicator)
        {
            if (paymentIndicator != null)

                Code = paymentIndicator.code;
        }

        public string Code { get; set; }
    }
}