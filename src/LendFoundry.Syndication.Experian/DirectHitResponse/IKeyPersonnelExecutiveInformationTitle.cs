namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IKeyPersonnelExecutiveInformationTitle
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}