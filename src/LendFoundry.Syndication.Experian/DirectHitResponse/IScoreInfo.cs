namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IScoreInfo
    {
        string Score { get; set; }
    }
}