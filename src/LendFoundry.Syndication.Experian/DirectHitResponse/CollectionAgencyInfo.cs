namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class CollectionAgencyInfo : ICollectionAgencyInfo
    {
        public CollectionAgencyInfo()
        {
        }

        public CollectionAgencyInfo(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileCollectionDataCollectionAgencyInfo collectionAgencyInfo)
        {
            if (collectionAgencyInfo != null)
            {
                AgencyName = collectionAgencyInfo.AgencyName;
                PhoneNumber = collectionAgencyInfo.PhoneNumber;
            }
        }

        public string AgencyName { get; set; }
        public string PhoneNumber { get; set; }
    }
}