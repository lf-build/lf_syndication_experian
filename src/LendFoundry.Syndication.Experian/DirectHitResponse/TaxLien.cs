using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class TaxLien : ITaxLien
    {
        public TaxLien()
        {
        }

        public TaxLien(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileTaxLien taxLien)
        {
            if (taxLien != null)
            {
                DateFiled = taxLien.DateFiled;
                DocumentNumber = taxLien.DocumentNumber;
                FilingLocation = taxLien.FilingLocation;
                Owner = taxLien.Owner;
                LiabilityAmount = taxLien.LiabilityAmount;
                if (taxLien.LegalType != null)
                    LegalType =
                        taxLien.LegalType.Select(p => new LegalType(p)).ToList<ILegalType>();
                if (taxLien.LegalAction != null)
                    LegalAction =
                        taxLien.LegalAction.Select(p => new LegalAction(p)).ToList<ILegalAction>();
            }
        }

        public string DateFiled { get; set; }
        public string DocumentNumber { get; set; }
        public string FilingLocation { get; set; }
        public string Owner { get; set; }
        public string LiabilityAmount { get; set; }

        public List<ILegalType> LegalType { get; set; }

        public List<ILegalAction> LegalAction { get; set; }
    }
}