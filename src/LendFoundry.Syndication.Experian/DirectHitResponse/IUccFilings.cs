using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IUccFilings
    {
        string DateFiled { get; set; }
        string DocumentNumber { get; set; }
        string FilingLocation { get; set; }
        string SecuredParty { get; set; }
        List<ILegalType> LegalType { get; set; }
        List<ILegalAction> LegalAction { get; set; }
        List<IUccFilingsOriginalUccFilingsInfo> OriginalUccFilingsInfo { get; set; }
        IUccFilingsCollateralCodesCollateral CollateralCodes { get; set; }
    }
}