using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class CurrentTotalAccountBalance : ICurrentTotalAccountBalance
    {
        public CurrentTotalAccountBalance()
        {
        }

        public CurrentTotalAccountBalance(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileExecutiveSummaryCurrentTotalAccountBalance currentTotalAccountBalance)
        {
            if (currentTotalAccountBalance != null)
            {
                Amount = currentTotalAccountBalance.Amount;
                if (currentTotalAccountBalance.Modifier != null)
                    Modifier = currentTotalAccountBalance.Modifier.Select(p => new Modifier(p)).ToList<IModifier>();
            }
        }

        public string Amount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IModifier, Modifier>))]
        public List<IModifier> Modifier { get; set; }
    }
}