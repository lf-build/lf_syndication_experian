namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IScoreFactor
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}