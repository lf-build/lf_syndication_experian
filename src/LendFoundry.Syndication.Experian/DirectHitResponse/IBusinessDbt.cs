namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IBusinessDbt
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}