namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class CorporateLink : ICorporateLink
    {
        public CorporateLink()
        {
        }

        public CorporateLink(string corporateLinkageIndicator)
        {
            if (!string.IsNullOrWhiteSpace(corporateLinkageIndicator))
                CorporateLinkageIndicator = corporateLinkageIndicator;
        }

        public string CorporateLinkageIndicator { get; set; }
    }
}