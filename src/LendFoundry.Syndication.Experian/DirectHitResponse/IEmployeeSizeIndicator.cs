namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IEmployeeSizeIndicator
    {
        string Code { get; set; }
    }
}