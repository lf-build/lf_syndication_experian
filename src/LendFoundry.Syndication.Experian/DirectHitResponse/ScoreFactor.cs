namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class ScoreFactor : IScoreFactor
    {
        public ScoreFactor()
        {
        }

        public ScoreFactor(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileScoreFactorsScoreFactor scoreFactor)
        {
            if (scoreFactor != null)
            {
                Code = scoreFactor.code;
                Value = scoreFactor.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}