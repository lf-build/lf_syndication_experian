using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IScoreFactors
    {
        string ModelCode { get; set; }
        List<IScoreFactor> ScoreFactor { get; set; }
    }
}