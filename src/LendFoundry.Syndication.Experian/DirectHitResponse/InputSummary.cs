namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class InputSummary : IInputSummary
    {
        public InputSummary()
        {
        }

        public InputSummary(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileInputSummary inputSummary)
        {
            if (inputSummary != null)
            {
                SubscriberNumber = inputSummary.SubscriberNumber;
                InquiryTransactionNumber = inputSummary.InquiryTransactionNumber;
                CpuVersion = inputSummary.CPUVersion;
                Inquiry = inputSummary.Inquiry;
            }
        }

        public string SubscriberNumber { get; set; }
        public string InquiryTransactionNumber { get; set; }

        public string CpuVersion { get; set; }

        public string Inquiry { get; set; }
    }
}