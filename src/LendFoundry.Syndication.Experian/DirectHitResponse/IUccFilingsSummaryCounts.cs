using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IUccFilingsSummaryCounts
    {
        string UccFilingsTotal { get; set; }

        List<ISummaryCountsMostRecent6Months> MostRecent6Months { get; set; }

        List<ISummaryCountsPrevious6Months> Previous6Months { get; set; }
    }
}