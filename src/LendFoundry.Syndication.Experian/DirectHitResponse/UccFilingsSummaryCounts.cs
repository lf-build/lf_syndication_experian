using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class UccFilingsSummaryCounts : IUccFilingsSummaryCounts
    {
        public UccFilingsSummaryCounts()
        {
        }

        public UccFilingsSummaryCounts(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileUCCFilingsSummaryCounts uccFilingsSummaryCounts)
        {
            if (uccFilingsSummaryCounts != null)
            {
                if (uccFilingsSummaryCounts.MostRecent6Months != null)
                    MostRecent6Months =
                        uccFilingsSummaryCounts.MostRecent6Months.Select(
                            p => new SummaryCountsMostRecent6Months(p))
                            .ToList<ISummaryCountsMostRecent6Months>();
                if (uccFilingsSummaryCounts.Previous6Months != null)
                    Previous6Months =
                        uccFilingsSummaryCounts.Previous6Months.Select(
                            p => new SummaryCountsPrevious6Months(p))
                            .ToList<ISummaryCountsPrevious6Months>();

                UccFilingsTotal = uccFilingsSummaryCounts.UCCFilingsTotal;
            }
        }

        public string UccFilingsTotal { get; set; }

        public List<ISummaryCountsMostRecent6Months> MostRecent6Months { get; set; }

        public List<ISummaryCountsPrevious6Months> Previous6Months { get; set; }
    }
}