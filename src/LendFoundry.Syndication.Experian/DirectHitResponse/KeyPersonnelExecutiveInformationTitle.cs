namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class KeyPersonnelExecutiveInformationTitle : IKeyPersonnelExecutiveInformationTitle
    {
        public KeyPersonnelExecutiveInformationTitle()
        {
        }

        public KeyPersonnelExecutiveInformationTitle(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformationTitle productsPremierProfileKeyPersonnelExecutiveInformationTitle)
        {
            if (productsPremierProfileKeyPersonnelExecutiveInformationTitle != null)
            {
                Code = productsPremierProfileKeyPersonnelExecutiveInformationTitle.code;
                Value = productsPremierProfileKeyPersonnelExecutiveInformationTitle.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}