namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface INaicsCodesNaicsValues
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}