using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class LowestTotalAccountBalance : ILowestTotalAccountBalance
    {
        public LowestTotalAccountBalance()
        {
        }

        public LowestTotalAccountBalance(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileExecutiveSummaryLowestTotalAccountBalance lowestTotalAccountBalance)
        {
            if (lowestTotalAccountBalance != null)
            {
                Amount = lowestTotalAccountBalance.Amount;
                if (lowestTotalAccountBalance.Modifier != null)
                    Modifier =
                        lowestTotalAccountBalance.Modifier.Select(
                            p => new Modifier(p)).ToList<IModifier>();
            }
        }

        public string Amount { get; set; }

        public List<IModifier> Modifier { get; set; }
    }
}