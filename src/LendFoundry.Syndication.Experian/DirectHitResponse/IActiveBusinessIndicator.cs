namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IActiveBusinessIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}