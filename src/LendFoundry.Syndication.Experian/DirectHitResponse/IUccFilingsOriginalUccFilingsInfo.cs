using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IUccFilingsOriginalUccFilingsInfo
    {
        string FilingState { get; set; }
        string DocumentNumber { get; set; }
        string DateFiled { get; set; }
        List<ILegalAction> LegalAction { get; set; }
    }
}