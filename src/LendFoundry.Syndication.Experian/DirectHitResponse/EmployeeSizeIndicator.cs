namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class EmployeeSizeIndicator : IEmployeeSizeIndicator
    {
        public EmployeeSizeIndicator()
        {
        }

        public EmployeeSizeIndicator(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileBusinessFactsEmployeeSizeIndicator employeeSizeIndicator)
        {
            if (employeeSizeIndicator != null)

                Code = employeeSizeIndicator.code;
        }

        public string Code { get; set; }
    }
}