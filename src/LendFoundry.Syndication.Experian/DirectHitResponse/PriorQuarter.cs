using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class PriorQuarter : IPriorQuarter
    {
        public PriorQuarter()
        {
        }

        public PriorQuarter(Proxy.DirectHitReportResponse.PriorQuarter priorQuarter)
        {
            if (priorQuarter != null)
            {
                Date = priorQuarter.Date;
                Dbt = priorQuarter.DBT;
                CurrentPercentage = priorQuarter.CurrentPercentage;
                Dbt30 = priorQuarter.DBT30;
                Dbt60 = priorQuarter.DBT60;
                Dbt90 = priorQuarter.DBT90;
                Dbt120 = priorQuarter.DBT120;
                YearOfQuarter = priorQuarter.YearOfQuarter;
                Quarter = priorQuarter.Quarter;
                Score = priorQuarter.Score;
                if (priorQuarter.TotalAccountBalance != null)
                    TotalAccountBalance =
                        priorQuarter.TotalAccountBalance.Select(p => new TotalAccountBalance(p))
                            .ToList<ITotalAccountBalance>();
                if (priorQuarter.QuarterWithinYear != null)
                    QuarterWithinYear =
                        priorQuarter.QuarterWithinYear.Select(p => new QuarterWithinYear(p))
                            .ToList<IQuarterWithinYear>();
            }
        }

        public string Date { get; set; }

        public string Dbt { get; set; }

        public string CurrentPercentage { get; set; }

        public string Dbt30 { get; set; }

        public string Dbt60 { get; set; }

        public string Dbt90 { get; set; }

        public string Dbt120 { get; set; }
        public string YearOfQuarter { get; set; }

        public string Quarter { get; set; }
        public string Score { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalAccountBalance, TotalAccountBalance>))]
        public List<ITotalAccountBalance> TotalAccountBalance { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IQuarterWithinYear, QuarterWithinYear>))]
        public List<IQuarterWithinYear> QuarterWithinYear { get; set; }
    }
}