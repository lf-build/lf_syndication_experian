using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class QuarterlyPaymentTrends : IQuarterlyPaymentTrends
    {
        public QuarterlyPaymentTrends()
        {
        }

        public QuarterlyPaymentTrends(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileQuarterlyPaymentTrends quarterlyPaymentTrends)
        {
            if (quarterlyPaymentTrends != null)
            {
                if (quarterlyPaymentTrends.MostRecentQuarter != null)
                    MostRecentQuarter =
                        quarterlyPaymentTrends.MostRecentQuarter.Select(p => new MostRecentQuarter(p))
                            .ToList<IMostRecentQuarter>();
                if (quarterlyPaymentTrends.PriorQuarter != null)
                    PriorQuarter =
                        quarterlyPaymentTrends.PriorQuarter.Select(p => new PriorQuarter(p)).ToList<IPriorQuarter>();
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IMostRecentQuarter, MostRecentQuarter>))]
        public List<IMostRecentQuarter> MostRecentQuarter { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPriorQuarter, PriorQuarter>))]
        public List<IPriorQuarter> PriorQuarter { get; set; }
    }
}