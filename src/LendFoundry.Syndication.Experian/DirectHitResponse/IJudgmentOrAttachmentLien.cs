using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IJudgmentOrAttachmentLien
    {
        string DateFiled { get; set; }
        string DocumentNumber { get; set; }
        string FilingLocation { get; set; }
        string LiabilityAmount { get; set; }
        string PlaintiffName { get; set; }
        List<ILegalType> LegalType { get; set; }
        List<ILegalAction> LegalAction { get; set; }
    }
}