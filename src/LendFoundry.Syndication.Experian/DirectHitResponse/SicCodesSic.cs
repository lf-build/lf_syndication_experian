namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class SicCodesSic : ISicCodesSic
    {
        public SicCodesSic()
        {
        }

        public SicCodesSic(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileSICCodesSIC sicCodesSic)
        {
            if (sicCodesSic?.SIC != null)
                Sic = new SicCodesSicValues(sicCodesSic.SIC);
        }

        public ISicCodesSicValues Sic { get; set; }
    }
}