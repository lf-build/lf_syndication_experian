namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ISummaryCountsMostRecent6Months
    {
        string StartDate { get; set; }
        string FilingsTotal { get; set; }
        string FilingsWithDerogatoryCollateral { get; set; }
        string ReleasesAndTerminationsTotal { get; set; }
        string ContinuationsTotal { get; set; }
        string AmendedAndAssignedTotal { get; set; }
    }
}