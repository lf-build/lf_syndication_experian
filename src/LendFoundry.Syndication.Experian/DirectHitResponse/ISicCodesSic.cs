namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ISicCodesSic
    {
        ISicCodesSicValues Sic { get; set; }
    }
}