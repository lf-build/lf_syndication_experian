namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class CorporateLinkageReturnLimitExceeded : ICorporateLinkageReturnLimitExceeded
    {
        public CorporateLinkageReturnLimitExceeded()
        {
        }

        public CorporateLinkageReturnLimitExceeded(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileCorporateLinkageReturnLimitExceeded corporateLinkageReturnLimitExceeded)
        {
            if (corporateLinkageReturnLimitExceeded != null)
            {
                Code = corporateLinkageReturnLimitExceeded.code;
            }
        }

        public string Code { get; set; }
    }
}