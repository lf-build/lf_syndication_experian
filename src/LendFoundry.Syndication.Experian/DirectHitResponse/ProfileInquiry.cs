using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class ProfileInquiry : IProfileInquiry
    {
        public ProfileInquiry()
        {
        }

        public ProfileInquiry(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileInquiry profileInquiry)
        {
            if (profileInquiry != null)
            {
                InquiryBusinessCategory = profileInquiry.InquiryBusinessCategory;
                if (profileInquiry.InquiryCount != null)
                    InquiryCount =
                        profileInquiry.InquiryCount.Select(p => new InquiryCount(p))
                            .ToList<IInquiryCount>();
            }
        }

        public string InquiryBusinessCategory { get; set; }

        public List<IInquiryCount> InquiryCount { get; set; }
    }
}