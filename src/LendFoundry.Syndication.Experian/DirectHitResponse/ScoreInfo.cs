namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class ScoreInfo : IScoreInfo
    {
        public ScoreInfo()
        {
        }

        public ScoreInfo(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationScoreInfo scoreInfo)
        {
            if (scoreInfo != null)

                Score = scoreInfo.Score;
        }

        public string Score { get; set; }
    }
}