using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IPremierProfile
    {
        List<IInputSummary> InputSummary { get; set; }
        List<IExpandedCreditSummary> ExpandedCreditSummary { get; set; }

        List<IExpandedBusinessNameAndAddress> ExpandedBusinessNameAndAddress { get; set; }

        List<IDoingBusinessAs> DoingBusinessAs { get; set; }
        List<IExecutiveSummary> ExecutiveSummary { get; set; }
        List<ICollectionData> CollectionData { get; set; }
        List<ITradePaymentExperiences> TradePaymentExperiences { get; set; }

        List<IAdditionalPaymentExperiences> AdditionalPaymentExperiences { get; set; }

        List<IPaymentTotals> PaymentTotals { get; set; }
        List<IPaymentTrends> PaymentTrends { get; set; }
        List<IIndustryPaymentTrends> IndustryPaymentTrends { get; set; }
        List<IQuarterlyPaymentTrends> QuarterlyPaymentTrends { get; set; }
        List<ITaxLien> TaxLien { get; set; }
        List<IJudgmentOrAttachmentLien> JudgmentOrAttachmentLien { get; set; }
        List<IUccFilingsSummaryCounts> UccFilingsSummaryCounts { get; set; }
        List<IUccFilings> UccFilings { get; set; }

        List<IKeyPersonnelExecutiveInformation> KeyPersonnelExecutiveInformation { get; set; }

        List<ICorporateLinkage> CorporateLinkage { get; set; }
        List<IBusinessFacts> BusinessFacts { get; set; }
        List<ISicCodesSic> SicCodes { get; set; }
        List<INaicsCodesNaics> NaicsCodes { get; set; }
        List<ICompetitors> Competitors { get; set; }

        List<ICommercialFraudShieldSummary> CommercialFraudShieldSummary { get; set; }

        List<IProfileInquiry> Inquiry { get; set; }

        List<IIntelliscoreScoreInformation> IntelliscoreScoreInformation { get; set; }

        List<IScoreFactors> ScoreFactors { get; set; }
        List<IScoreTrendsCreditLimit> ScoreTrendsCreditLimit { get; set; }
        List<IBillingIndicator> BillingIndicator { get; set; }
        IProcessingMessage ProcessingMessage { get; set; }
        List<IBankruptcy> Bankruptcy { get; set; }
    }
}