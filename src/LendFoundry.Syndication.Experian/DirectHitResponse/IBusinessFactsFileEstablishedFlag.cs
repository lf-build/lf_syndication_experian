namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IBusinessFactsFileEstablishedFlag
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}