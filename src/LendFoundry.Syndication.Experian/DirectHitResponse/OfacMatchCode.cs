namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class OfacMatchCode : IOfacMatchCode
    {
        public OfacMatchCode()
        {
        }

        public OfacMatchCode(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryOFACMatchCode ofacMatchCode)
        {
            if (ofacMatchCode != null)

                Code = ofacMatchCode.code;
        }

        public string Code { get; set; }
    }
}