namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IUccFilingsCollateralCodesCollateralValues
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}