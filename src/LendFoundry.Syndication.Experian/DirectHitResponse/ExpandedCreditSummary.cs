using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class ExpandedCreditSummary : IExpandedCreditSummary
    {
        public ExpandedCreditSummary()
        {
        }

        public ExpandedCreditSummary(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileExpandedCreditSummary expandedCreditSummary)
        {
            if (expandedCreditSummary != null)
            {
                BankruptcyFilingCount = expandedCreditSummary.BankruptcyFilingCount;
                TaxLienFilingCount = expandedCreditSummary.TaxLienFilingCount;
                OldestTaxLienDate = expandedCreditSummary.OldestTaxLienDate;
                MostRecentTaxLienDate = expandedCreditSummary.MostRecentTaxLienDate;
                JudgmentFilingCount = expandedCreditSummary.JudgmentFilingCount;
                OldestJudgmentDate = expandedCreditSummary.OldestJudgmentDate;
                MostRecentJudgmentDate = expandedCreditSummary.MostRecentJudgmentDate;
                CollectionCount = expandedCreditSummary.CollectionCount;
                CollectionBalance = expandedCreditSummary.CollectionBalance;
                CollectionCountPast24Months = expandedCreditSummary.CollectionCountPast24Months;
                LegalBalance = expandedCreditSummary.LegalBalance;
                UccFilings = expandedCreditSummary.UCCFilings;
                UccDerogCount = expandedCreditSummary.UCCDerogCount;
                CurrentAccountBalance = expandedCreditSummary.CurrentAccountBalance;
                CurrentTradelineCount = expandedCreditSummary.CurrentTradelineCount;
                MonthlyAverageDbt = expandedCreditSummary.MonthlyAverageDBT;
                HighestDbt6Months = expandedCreditSummary.HighestDBT6Months;
                HighestDbt5Quarters = expandedCreditSummary.HighestDBT5Quarters;
                ActiveTradelineCount = expandedCreditSummary.ActiveTradelineCount;
                AllTradelineBalance = expandedCreditSummary.AllTradelineBalance;
                AllTradelineCount = expandedCreditSummary.AllTradelineCount;
                AverageBalance5Quarters = expandedCreditSummary.AverageBalance5Quarters;
                SingleHighCredit = expandedCreditSummary.SingleHighCredit;
                LowBalance6Months = expandedCreditSummary.LowBalance6Months;
                HighBalance6Months = expandedCreditSummary.HighBalance6Months;
                OldestCollectionDate = expandedCreditSummary.OldestCollectionDate;
                MostRecentCollectionDate = expandedCreditSummary.MostRecentCollectionDate;
                CurrentDbt = expandedCreditSummary.CurrentDBT;
                OldestUccFilingDate = expandedCreditSummary.OldestUCCFilingDate;
                MostRecentUccFilingDate = expandedCreditSummary.MostRecentUCCFilingDate;
                JudgmentFlag = expandedCreditSummary.JudgmentFlag;
                TaxLienFlag = expandedCreditSummary.TaxLienFlag;
                TradeCollectionCount = expandedCreditSummary.TradeCollectionCount;
                TradeCollectionBalance = expandedCreditSummary.TradeCollectionBalance;
                OpenCollectionCount = expandedCreditSummary.OpenCollectionCount;
                OpenCollectionBalance = expandedCreditSummary.OpenCollectionBalance;
                MostRecentBankruptcyDate = expandedCreditSummary.MostRecentBankruptcyDate;
                if (expandedCreditSummary.OFACMatch != null)
                    OfacMatch =
                        expandedCreditSummary.OFACMatch.Select(
                            p => new OfacMatch(p.code))
                            .ToList<IOfacMatch>();
                if (expandedCreditSummary.VictimStatement != null)
                    VictimStatement =
                        expandedCreditSummary.VictimStatement.Select(
                            p => new VictimStatement(p.code, p.Value))
                            .ToList<IVictimStatement>();
            }
        }

        public string BankruptcyFilingCount { get; set; }
        public string TaxLienFilingCount { get; set; }
        public string OldestTaxLienDate { get; set; }
        public string MostRecentTaxLienDate { get; set; }
        public string JudgmentFilingCount { get; set; }
        public string OldestJudgmentDate { get; set; }
        public string MostRecentJudgmentDate { get; set; }
        public string CollectionCount { get; set; }
        public string CollectionBalance { get; set; }
        public string CollectionCountPast24Months { get; set; }
        public string LegalBalance { get; set; }

        public string UccFilings { get; set; }

        public string UccDerogCount { get; set; }

        public string CurrentAccountBalance { get; set; }
        public string CurrentTradelineCount { get; set; }

        public string MonthlyAverageDbt { get; set; }

        public string HighestDbt6Months { get; set; }

        public string HighestDbt5Quarters { get; set; }

        public string ActiveTradelineCount { get; set; }
        public string AllTradelineBalance { get; set; }
        public string AllTradelineCount { get; set; }
        public string AverageBalance5Quarters { get; set; }
        public string SingleHighCredit { get; set; }
        public string LowBalance6Months { get; set; }
        public string HighBalance6Months { get; set; }
        public string OldestCollectionDate { get; set; }
        public string MostRecentCollectionDate { get; set; }

        public string CurrentDbt { get; set; }

        public string OldestUccFilingDate { get; set; }

        public string MostRecentUccFilingDate { get; set; }

        public string JudgmentFlag { get; set; }
        public string TaxLienFlag { get; set; }
        public string TradeCollectionCount { get; set; }
        public string TradeCollectionBalance { get; set; }
        public string OpenCollectionCount { get; set; }
        public string OpenCollectionBalance { get; set; }
        public string MostRecentBankruptcyDate { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IOfacMatch, OfacMatch>))]
        public List<IOfacMatch> OfacMatch { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IVictimStatement, VictimStatement>))]
        public List<IVictimStatement> VictimStatement
        {
            get;
            set;
        }
    }
}