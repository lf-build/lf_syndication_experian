namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class SummaryCountsMostRecent6Months : ISummaryCountsMostRecent6Months
    {
        public SummaryCountsMostRecent6Months()
        {
        }

        public SummaryCountsMostRecent6Months(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileUCCFilingsSummaryCountsMostRecent6Months summaryCountsMostRecent6Months)
        {
            if (summaryCountsMostRecent6Months != null)
            {
                AmendedAndAssignedTotal = summaryCountsMostRecent6Months.AmendedAndAssignedTotal;
                ContinuationsTotal = summaryCountsMostRecent6Months.ContinuationsTotal;
                FilingsTotal = summaryCountsMostRecent6Months.FilingsTotal;
                FilingsWithDerogatoryCollateral = summaryCountsMostRecent6Months.FilingsWithDerogatoryCollateral;
                ReleasesAndTerminationsTotal = summaryCountsMostRecent6Months.ReleasesAndTerminationsTotal;
                StartDate = summaryCountsMostRecent6Months.StartDate;
            }
        }

        public string StartDate { get; set; }
        public string FilingsTotal { get; set; }
        public string FilingsWithDerogatoryCollateral { get; set; }
        public string ReleasesAndTerminationsTotal { get; set; }
        public string ContinuationsTotal { get; set; }
        public string AmendedAndAssignedTotal { get; set; }
    }
}