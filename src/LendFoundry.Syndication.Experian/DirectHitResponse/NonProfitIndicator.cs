namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class NonProfitIndicator : INonProfitIndicator
    {
        public NonProfitIndicator()
        {
        }

        public NonProfitIndicator(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileBusinessFactsNonProfitIndicator nonProfitIndicator)
        {
            if (nonProfitIndicator != null)
            {
                Code = nonProfitIndicator.code;
                Value = nonProfitIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}