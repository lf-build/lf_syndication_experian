namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class MatchingBranchAddress : IMatchingBranchAddress
    {
        public MatchingBranchAddress()
        {
        }

        public MatchingBranchAddress(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddressMatchingBranchAddress matchingBranchAddress)
        {
            if (matchingBranchAddress != null)
            {
                MatchingBranchBin = matchingBranchAddress.MatchingBranchBIN;
                MatchingStreetAddress = matchingBranchAddress.MatchingStreetAddress;
                MatchingCity = matchingBranchAddress.MatchingCity;
                MatchingState = matchingBranchAddress.MatchingState;
                MatchingZip = matchingBranchAddress.MatchingZip;
                MatchingZipExtension = matchingBranchAddress.MatchingZipExtension;
            }
        }

        public string MatchingBranchBin { get; set; }

        public string MatchingStreetAddress { get; set; }
        public string MatchingCity { get; set; }
        public string MatchingState { get; set; }
        public string MatchingZip { get; set; }
        public string MatchingZipExtension { get; set; }
    }
}