namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IVictimStatement
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}