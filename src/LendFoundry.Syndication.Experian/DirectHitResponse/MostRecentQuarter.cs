using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class MostRecentQuarter : IMostRecentQuarter
    {
        public MostRecentQuarter()
        {
        }

        public MostRecentQuarter(Proxy.DirectHitReportResponse.MostRecentQuarter mostRecentQuarter)
        {
            if (mostRecentQuarter != null)
            {
                Dbt = mostRecentQuarter.DBT;
                CurrentPercentage = mostRecentQuarter.CurrentPercentage;
                Dbt30 = mostRecentQuarter.DBT30;
                Dbt60 = mostRecentQuarter.DBT60;
                Dbt90 = mostRecentQuarter.DBT90;
                Dbt120 = mostRecentQuarter.DBT120;
                YearOfQuarter = mostRecentQuarter.YearOfQuarter;
                Quarter = mostRecentQuarter.Quarter;
                Score = mostRecentQuarter.Score;
                if (mostRecentQuarter.TotalAccountBalance != null)
                    TotalAccountBalance =
                        mostRecentQuarter.TotalAccountBalance.Select(p => new TotalAccountBalance(p))
                            .ToList<ITotalAccountBalance>();
                if (mostRecentQuarter.QuarterWithinYear != null)
                    QuarterWithinYear =
                        mostRecentQuarter.QuarterWithinYear.Select(p => new QuarterWithinYear(p))
                            .ToList<IQuarterWithinYear>();
            }
        }

        public string Dbt { get; set; }

        public string CurrentPercentage { get; set; }

        public string Dbt30 { get; set; }

        public string Dbt60 { get; set; }

        public string Dbt90 { get; set; }

        public string Dbt120 { get; set; }

        public string YearOfQuarter { get; set; }

        public string Quarter { get; set; }
        public string Score { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalAccountBalance, TotalAccountBalance>))]
        public List<ITotalAccountBalance> TotalAccountBalance { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IQuarterWithinYear, QuarterWithinYear>))]
        public List<IQuarterWithinYear> QuarterWithinYear { get; set; }
    }
}