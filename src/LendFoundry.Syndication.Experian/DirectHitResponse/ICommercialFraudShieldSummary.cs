using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ICommercialFraudShieldSummary
    {
        string BusinessBin { get; set; }
        List<IMatchingBusinessIndicator> MatchingBusinessIndicator { get; set; }

        List<IActiveBusinessIndicator> ActiveBusinessIndicator { get; set; }

        List<IOfacMatchCode> OfacMatchCode { get; set; }

        List<IBusinessVictimStatementIndicator> BusinessVictimStatementIndicator { get; set; }

        List<IBusinessRiskTriggersIndicator> BusinessRiskTriggersIndicator { get; set; }

        List<INameAddressVerificationIndicator> NameAddressVerificationIndicator { get; set; }
    }
}