namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IMatchingBusinessIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}