namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IInquiryCount
    {
        string Date { get; set; }
        string Count { get; set; }
    }
}