namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ISalesIndicator
    {
        string Code { get; set; }
    }
}