namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IProfileType
    {
        string Code { get; set; }
    }
}