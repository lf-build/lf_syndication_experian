using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class TotalsNewlyReportedTradeLines : ITotalsNewlyReportedTradeLines
    {
        public TotalsNewlyReportedTradeLines()
        {
        }

        public TotalsNewlyReportedTradeLines(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfilePaymentTotalsNewlyReportedTradeLines totalsNewlyReportedTradeLines)
        {
            if (totalsNewlyReportedTradeLines != null)
            {
                NumberOfLines = totalsNewlyReportedTradeLines.NumberOfLines;
                Dbt = totalsNewlyReportedTradeLines.DBT;
                CurrentPercentage = totalsNewlyReportedTradeLines.CurrentPercentage;
                Dbt30 = totalsNewlyReportedTradeLines.DBT30;
                Dbt60 = totalsNewlyReportedTradeLines.DBT60;
                Dbt90 = totalsNewlyReportedTradeLines.DBT90;
                Dbt120 = totalsNewlyReportedTradeLines.DBT120;
                if (totalsNewlyReportedTradeLines.TotalHighCreditAmount != null)
                    TotalHighCreditAmount =
                        totalsNewlyReportedTradeLines.TotalHighCreditAmount.Select(
                            p => new TotalHighCreditAmount(p)).ToList<ITotalHighCreditAmount>();
                if (totalsNewlyReportedTradeLines.TotalAccountBalance != null)
                    TotalAccountBalance =
                        totalsNewlyReportedTradeLines.TotalAccountBalance.Select(
                            p => new TotalAccountBalance(p)).ToList<ITotalAccountBalance>();
            }
        }

        public string NumberOfLines { get; set; }

        public string Dbt { get; set; }

        public string CurrentPercentage { get; set; }

        public string Dbt30 { get; set; }

        public string Dbt60 { get; set; }

        public string Dbt90 { get; set; }

        public string Dbt120 { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalHighCreditAmount, TotalHighCreditAmount>))]
        public List<ITotalHighCreditAmount> TotalHighCreditAmount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalAccountBalance, TotalAccountBalance>))]
        public List<ITotalAccountBalance> TotalAccountBalance { get; set; }
    }
}