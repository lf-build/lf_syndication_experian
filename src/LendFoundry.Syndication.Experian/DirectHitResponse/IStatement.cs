namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IStatement
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}