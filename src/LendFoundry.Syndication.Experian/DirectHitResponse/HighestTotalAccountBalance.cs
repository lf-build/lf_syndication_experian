using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class HighestTotalAccountBalance : IHighestTotalAccountBalance
    {
        public HighestTotalAccountBalance()
        {
        }

        public HighestTotalAccountBalance(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileExecutiveSummaryHighestTotalAccountBalance highestTotalAccountBalance)
        {
            if (highestTotalAccountBalance != null)
            {
                Amount = highestTotalAccountBalance.Amount;
                if (highestTotalAccountBalance.Modifier != null)
                    Modifier = highestTotalAccountBalance.Modifier.Select(p => new Modifier(p)).ToList<IModifier>();
            }
        }

        public string Amount { get; set; }

        public List<IModifier> Modifier { get; set; }
    }
}