namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class BusinessTypeIndicator : IBusinessTypeIndicator
    {
        public BusinessTypeIndicator()
        {
        }

        public BusinessTypeIndicator(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileBusinessFactsBusinessTypeIndicator businessTypeIndicator)
        {
            if (businessTypeIndicator != null)
            {
                Code = businessTypeIndicator.code;
                Value = businessTypeIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}