namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class YearsInBusinessIndicator : IYearsInBusinessIndicator
    {
        public YearsInBusinessIndicator()
        {
        }

        public YearsInBusinessIndicator(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileBusinessFactsYearsInBusinessIndicator yearsInBusinessIndicator)
        {
            if (yearsInBusinessIndicator != null)
            {
                Code = yearsInBusinessIndicator.code;
                Value = yearsInBusinessIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}