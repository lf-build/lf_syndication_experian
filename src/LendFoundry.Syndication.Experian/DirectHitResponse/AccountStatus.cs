namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class AccountStatus : IAccountStatus
    {
        public AccountStatus()
        {
        }

        public AccountStatus(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileCollectionDataAccountStatus accountStatus)
        {
            if (accountStatus != null)
            {
                Code = accountStatus.code;
                Value = accountStatus.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}