using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IUccFilingsCollateralCodesCollateral
    {
        List<IUccFilingsCollateralCodesCollateralValues> Collateral { get; set; }
    }
}