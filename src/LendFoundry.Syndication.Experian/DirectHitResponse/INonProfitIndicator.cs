namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface INonProfitIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}