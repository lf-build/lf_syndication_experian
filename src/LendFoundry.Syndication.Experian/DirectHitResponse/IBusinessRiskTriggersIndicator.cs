namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IBusinessRiskTriggersIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}