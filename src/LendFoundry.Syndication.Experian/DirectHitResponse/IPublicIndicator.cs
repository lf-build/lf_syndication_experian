namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IPublicIndicator
    {
        string Code { get; set; }
    }
}