﻿namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IModifier
    {
        string Code { get; set; }
    }
}