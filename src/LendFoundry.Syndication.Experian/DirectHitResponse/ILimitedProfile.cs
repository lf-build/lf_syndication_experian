namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ILimitedProfile
    {
        string Code { get; set; }
    }
}