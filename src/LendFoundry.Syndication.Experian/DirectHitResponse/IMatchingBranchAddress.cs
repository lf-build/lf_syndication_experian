namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IMatchingBranchAddress
    {
        string MatchingBranchBin { get; set; }
        string MatchingStreetAddress { get; set; }
        string MatchingCity { get; set; }
        string MatchingState { get; set; }
        string MatchingZip { get; set; }
        string MatchingZipExtension { get; set; }
    }
}