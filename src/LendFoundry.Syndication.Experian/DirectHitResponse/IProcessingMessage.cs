namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IProcessingMessage
    {
        string ProcessingAction { get; set; }
    }
}