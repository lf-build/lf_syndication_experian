namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class BusinessFactsFileEstablishedFlag : IBusinessFactsFileEstablishedFlag
    {
        public BusinessFactsFileEstablishedFlag()
        {
        }

        public BusinessFactsFileEstablishedFlag(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileBusinessFactsFileEstablishedFlag businessFactsFileEstablishedFlag)
        {
            if (businessFactsFileEstablishedFlag != null)
            {
                Code = businessFactsFileEstablishedFlag.code;
                Value = businessFactsFileEstablishedFlag.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}