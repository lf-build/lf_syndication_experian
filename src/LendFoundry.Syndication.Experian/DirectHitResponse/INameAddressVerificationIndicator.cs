namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface INameAddressVerificationIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}