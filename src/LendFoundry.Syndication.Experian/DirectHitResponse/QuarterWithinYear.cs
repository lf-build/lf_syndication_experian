namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class QuarterWithinYear : IQuarterWithinYear
    {
        public QuarterWithinYear()
        {
        }

        public QuarterWithinYear(Proxy.DirectHitReportResponse.QuarterWithinYear quarterWithinYear)
        {
            if (quarterWithinYear != null)
            {
                Code = quarterWithinYear.code;
                Value = quarterWithinYear.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}