namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IIndustryPaymentComparison
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}