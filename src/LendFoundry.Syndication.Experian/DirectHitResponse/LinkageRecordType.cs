namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class LinkageRecordType : ILinkageRecordType
    {
        public LinkageRecordType()
        {
        }

        public LinkageRecordType(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileCorporateLinkageLinkageRecordType linkageRecordType)
        {
            if (linkageRecordType != null)
            {
                Code = linkageRecordType.code;
                Value = linkageRecordType.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}