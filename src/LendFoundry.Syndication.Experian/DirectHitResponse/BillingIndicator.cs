namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class BillingIndicator : IBillingIndicator
    {
        public BillingIndicator()
        {
        }

        public BillingIndicator(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileBillingIndicator billingIndicator)
        {
            if (billingIndicator != null)
            {
                Code = billingIndicator.code;
                Value = billingIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}