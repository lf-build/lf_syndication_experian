namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class NaicsCodesNaics : INaicsCodesNaics
    {
        public NaicsCodesNaics()
        {
        }

        public NaicsCodesNaics(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileNAICSCodesNAICS naicsCodesNaics)
        {
            if (naicsCodesNaics?.NAICS != null)
                Naics = new NaicsCodesNaicsValues(naicsCodesNaics.NAICS);
        }

        public INaicsCodesNaicsValues Naics { get; set; }
    }
}