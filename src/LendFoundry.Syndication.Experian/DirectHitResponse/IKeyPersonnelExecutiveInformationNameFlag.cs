namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IKeyPersonnelExecutiveInformationNameFlag
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}