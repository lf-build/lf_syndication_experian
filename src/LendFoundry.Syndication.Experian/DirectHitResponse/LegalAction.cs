namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class LegalAction : ILegalAction
    {
        public LegalAction()
        {
        }

        public LegalAction(Proxy.DirectHitReportResponse.LegalAction legalAction)
        {
            if (legalAction != null)
            {
                Code = legalAction.code;
                Value = legalAction.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}