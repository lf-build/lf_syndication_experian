namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class TradeLineFlag : ITradeLineFlag
    {
        public TradeLineFlag()
        {
        }

        public TradeLineFlag(Proxy.DirectHitReportResponse.TradeLineFlag tradeLineFlag)
        {
            if (tradeLineFlag != null)
                Code = tradeLineFlag.code;
        }

        public string Code { get; set; }
    }
}