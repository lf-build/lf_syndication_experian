namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ICorporateLinkageReturnLimitExceeded
    {
        string Code { get; set; }
    }
}