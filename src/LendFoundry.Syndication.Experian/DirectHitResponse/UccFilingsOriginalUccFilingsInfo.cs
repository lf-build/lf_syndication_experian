using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class UccFilingsOriginalUccFilingsInfo : IUccFilingsOriginalUccFilingsInfo
    {
        public UccFilingsOriginalUccFilingsInfo(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileUCCFilingsOriginalUCCFilingsInfo uccFilingsOriginalUccFilingsInfo)
        {
            if (uccFilingsOriginalUccFilingsInfo != null)
            {
                DateFiled = uccFilingsOriginalUccFilingsInfo.DateFiled;
                DocumentNumber = uccFilingsOriginalUccFilingsInfo.DocumentNumber;
                FilingState = uccFilingsOriginalUccFilingsInfo.FilingState;
                if (uccFilingsOriginalUccFilingsInfo.LegalAction != null)
                    LegalAction =
                        uccFilingsOriginalUccFilingsInfo.LegalAction.Select(p => new LegalAction(p))
                            .ToList<ILegalAction>();
            }
        }

        public string FilingState { get; set; }
        public string DocumentNumber { get; set; }
        public string DateFiled { get; set; }

        public List<ILegalAction> LegalAction { get; set; }
    }
}