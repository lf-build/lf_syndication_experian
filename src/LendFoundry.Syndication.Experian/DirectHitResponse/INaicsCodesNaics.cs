namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface INaicsCodesNaics
    {
        INaicsCodesNaicsValues Naics { get; set; }
    }
}