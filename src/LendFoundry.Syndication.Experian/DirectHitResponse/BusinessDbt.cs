namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class BusinessDbt : IBusinessDbt
    {
        public BusinessDbt()
        {
        }

        public BusinessDbt(string code, string value)
        {
            if (!string.IsNullOrWhiteSpace(code))
                Code = code;
            if (!string.IsNullOrWhiteSpace(value))
                Value = value;
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}