using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IQuarterlyPaymentTrends
    {
        List<IMostRecentQuarter> MostRecentQuarter { get; set; }
        List<IPriorQuarter> PriorQuarter { get; set; }
    }
}