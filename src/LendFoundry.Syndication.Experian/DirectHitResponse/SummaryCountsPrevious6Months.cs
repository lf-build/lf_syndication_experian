namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class SummaryCountsPrevious6Months : ISummaryCountsPrevious6Months
    {
        public SummaryCountsPrevious6Months()
        {
        }

        public SummaryCountsPrevious6Months(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileUCCFilingsSummaryCountsPrevious6Months summaryCountsPrevious6Months)
        {
            if (summaryCountsPrevious6Months != null)
            {
                AmendedAndAssignedTotal = summaryCountsPrevious6Months.AmendedAndAssignedTotal;
                ContinuationsTotal = summaryCountsPrevious6Months.ContinuationsTotal;
                FilingsTotal = summaryCountsPrevious6Months.FilingsTotal;
                FilingsWithDerogatoryCollateral = summaryCountsPrevious6Months.FilingsWithDerogatoryCollateral;
                ReleasesAndTerminationsTotal = summaryCountsPrevious6Months.ReleasesAndTerminationsTotal;
                StartDate = summaryCountsPrevious6Months.StartDate;
            }
        }

        public string StartDate { get; set; }
        public string FilingsTotal { get; set; }
        public string FilingsWithDerogatoryCollateral { get; set; }
        public string ReleasesAndTerminationsTotal { get; set; }
        public string ContinuationsTotal { get; set; }
        public string AmendedAndAssignedTotal { get; set; }
    }
}