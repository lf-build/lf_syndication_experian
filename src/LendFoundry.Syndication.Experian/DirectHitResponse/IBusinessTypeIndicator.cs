namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IBusinessTypeIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}