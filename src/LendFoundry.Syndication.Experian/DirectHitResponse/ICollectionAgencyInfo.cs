namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ICollectionAgencyInfo
    {
        string AgencyName { get; set; }
        string PhoneNumber { get; set; }
    }
}