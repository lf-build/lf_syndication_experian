namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IAccountStatus
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}