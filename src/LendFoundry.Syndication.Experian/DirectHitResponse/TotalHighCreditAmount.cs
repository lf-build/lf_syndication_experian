using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class TotalHighCreditAmount : ITotalHighCreditAmount
    {
        public TotalHighCreditAmount()
        {
        }

        public TotalHighCreditAmount(Proxy.DirectHitReportResponse.TotalHighCreditAmount totalHighCreditAmount)
        {
            if (totalHighCreditAmount != null)
            {
                Amount = totalHighCreditAmount.Amount;
                if (totalHighCreditAmount.Modifier != null)
                    Modifier = totalHighCreditAmount.Modifier.Select(p => new Modifier(p)).ToList<IModifier>();
            }
        }

        public string Amount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IModifier, Modifier>))]
        public List<IModifier> Modifier { get; set; }
    }
}