using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ITotalsAdditionalTradeLines
    {
        string NumberOfLines { get; set; }
        string CurrentPercentage { get; set; }
        string Dbt30 { get; set; }
        string Dbt60 { get; set; }
        string Dbt90 { get; set; }
        string Dbt120 { get; set; }
        List<ITotalHighCreditAmount> TotalHighCreditAmount { get; set; }
        List<ITotalAccountBalance> TotalAccountBalance { get; set; }
    }
}