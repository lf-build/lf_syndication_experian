namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IPaymentTrendIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}