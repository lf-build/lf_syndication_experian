namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class BusinessRiskTriggersIndicator : IBusinessRiskTriggersIndicator
    {
        public BusinessRiskTriggersIndicator()
        {
        }

        public BusinessRiskTriggersIndicator(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryBusinessRiskTriggersIndicator businessRiskTriggersIndicator)
        {
            if (businessRiskTriggersIndicator != null)
            {
                Code = businessRiskTriggersIndicator.code;
                Value = businessRiskTriggersIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}