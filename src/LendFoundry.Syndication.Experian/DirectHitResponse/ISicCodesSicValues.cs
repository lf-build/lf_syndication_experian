namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ISicCodesSicValues
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}