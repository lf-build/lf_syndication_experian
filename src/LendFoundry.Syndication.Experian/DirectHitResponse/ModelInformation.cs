namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class ModelInformation : IModelInformation
    {
        public ModelInformation()
        {
        }

        public ModelInformation(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationModelInformation modelInformation)
        {
            if (modelInformation != null)
            {
                ModelCode = modelInformation.ModelCode;
                ModelTitle = modelInformation.ModelTitle;
                CustomModelCode = modelInformation.CustomModelCode;
            }
        }

        public string ModelCode { get; set; }
        public string ModelTitle { get; set; }
        public string CustomModelCode { get; set; }
    }
}