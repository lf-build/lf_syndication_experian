namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class InquiryCount : IInquiryCount
    {
        public InquiryCount()
        {
        }

        public InquiryCount(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileInquiryInquiryCount inquiryCount)
        {
            if (inquiryCount != null)
            {
                Date = inquiryCount.Date;
                Count = inquiryCount.Count;
            }
        }

        public string Date { get; set; }
        public string Count { get; set; }
    }
}