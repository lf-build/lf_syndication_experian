namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class MatchingBusinessIndicator : IMatchingBusinessIndicator
    {
        public MatchingBusinessIndicator()
        {
        }

        public MatchingBusinessIndicator(Proxy.DirectHitReportResponse.MatchingBusinessIndicator matchingBusinessIndicator)
        {
            if (matchingBusinessIndicator != null)
            {
                Code = matchingBusinessIndicator.code;
                Value = matchingBusinessIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}