namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class ActiveBusinessIndicator : IActiveBusinessIndicator
    {
        public ActiveBusinessIndicator()
        {
        }

        public ActiveBusinessIndicator(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryActiveBusinessIndicator activeBusinessIndicator)
        {
            if (activeBusinessIndicator != null)
            {
                Code = activeBusinessIndicator.code;
                Value = activeBusinessIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}