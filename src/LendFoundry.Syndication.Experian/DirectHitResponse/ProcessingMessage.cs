namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class ProcessingMessage : IProcessingMessage
    {
        public ProcessingMessage()
        {
        }

        public ProcessingMessage(Proxy.DirectHitReportResponse.ProcessingMessage processingMessage)
        {
            if (processingMessage != null)
                ProcessingAction = processingMessage.ProcessingAction;
        }

        public string ProcessingAction { get; set; }
    }
}