using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IAdditionalPaymentExperiences
    {
        string BusinessCategory { get; set; }
        string DateReported { get; set; }
        string DateLastActivity { get; set; }
        string Terms { get; set; }
        string CurrentPercentage { get; set; }
        string Dbt30 { get; set; }
        string Dbt60 { get; set; }
        string Dbt90 { get; set; }
        string Dbt90Plus { get; set; }
        string Comments { get; set; }
        List<IRecentHighCredit> RecentHighCredit { get; set; }
        List<IAccountBalance> AccountBalance { get; set; }
        List<ITradeLineFlag> TradeLineFlag { get; set; }
        List<INewlyReportedIndicator> NewlyReportedIndicator { get; set; }
    }
}