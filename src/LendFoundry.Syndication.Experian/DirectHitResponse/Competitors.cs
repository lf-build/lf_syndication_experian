namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class Competitors : ICompetitors
    {
        public Competitors()
        {
        }

        public Competitors(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileCompetitors competitors)
        {
            if (competitors != null)
                CompetitorName = competitors.CompetitorName;
        }

        public string CompetitorName { get; set; }
    }
}