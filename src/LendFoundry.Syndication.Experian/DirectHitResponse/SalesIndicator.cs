namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class SalesIndicator : ISalesIndicator
    {
        public SalesIndicator()
        {
        }

        public SalesIndicator(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileBusinessFactsSalesIndicator salesIndicator)
        {
            if (salesIndicator != null)

                Code = salesIndicator.code;
        }

        public string Code { get; set; }
    }
}