namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IYearsInBusinessIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}