using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class ScoreTrendsCreditLimit : IScoreTrendsCreditLimit
    {
        public ScoreTrendsCreditLimit()
        {
        }

        public ScoreTrendsCreditLimit(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileScoreTrendsCreditLimit scoreTrendsCreditLimit)
        {
            if (scoreTrendsCreditLimit != null)
            {
                CreditLimitAmount = scoreTrendsCreditLimit.CreditLimitAmount;
                if (scoreTrendsCreditLimit.MostRecentQuarter != null)
                    MostRecentQuarter =
                        scoreTrendsCreditLimit.MostRecentQuarter.Select(p => new MostRecentQuarter(p))
                            .ToList<IMostRecentQuarter>();
                if (scoreTrendsCreditLimit.PriorQuarter != null)
                    PriorQuarter =
                        scoreTrendsCreditLimit.PriorQuarter.Select(p => new PriorQuarter(p)).ToList<IPriorQuarter>();
            }
        }

        public string CreditLimitAmount { get; set; }

        public List<IMostRecentQuarter> MostRecentQuarter { get; set; }

        public List<IPriorQuarter> PriorQuarter { get; set; }
    }
}