namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class LimitedProfile : ILimitedProfile
    {
        public LimitedProfile()
        {
        }

        public LimitedProfile(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationLimitedProfile limitedProfile)
        {
            if (limitedProfile != null)

                Code = limitedProfile.code;
        }

        public string Code { get; set; }
    }
}