namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ITradeLineFlag
    {
        string Code { get; set; }
    }
}