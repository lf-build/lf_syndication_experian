namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ILegalType
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}