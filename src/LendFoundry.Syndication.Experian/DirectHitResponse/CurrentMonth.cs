using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class CurrentMonth : ICurrentMonth
    {
        public CurrentMonth()
        {
        }

        public CurrentMonth(Proxy.DirectHitReportResponse.CurrentMonth currentMonth)
        {
            if (currentMonth != null)
            {
                CurrentPercentage = currentMonth.CurrentPercentage;
                Date = currentMonth.Date;
                Dbt = currentMonth.DBT;
                Dbt120 = currentMonth.DBT120;
                Dbt30 = currentMonth.DBT30;
                Dbt60 = currentMonth.DBT60;
                Dbt90 = currentMonth.DBT90;
                if (currentMonth.TotalAccountBalance != null)
                    TotalAccountBalance =
                        currentMonth.TotalAccountBalance.Select(p => new TotalAccountBalance(p))
                            .ToList<ITotalAccountBalance>();
            }
        }

        public string Date { get; set; }

        public string Dbt { get; set; }

        public string CurrentPercentage { get; set; }

        public string Dbt30 { get; set; }

        public string Dbt60 { get; set; }

        public string Dbt90 { get; set; }

        public string Dbt120 { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalAccountBalance, TotalAccountBalance>))]
        public List<ITotalAccountBalance> TotalAccountBalance { get; set; }
    }
}