namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface INewlyReportedIndicator
    {
        string Code { get; set; }
    }
}