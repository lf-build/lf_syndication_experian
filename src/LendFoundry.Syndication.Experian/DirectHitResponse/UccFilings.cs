using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class UccFilings : IUccFilings
    {
        public UccFilings()
        {
        }

        public UccFilings(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileUCCFilings uccFilings)
        {
            if (uccFilings != null)
            {
                DateFiled = uccFilings.DateFiled;
                DocumentNumber = uccFilings.DocumentNumber;
                FilingLocation = uccFilings.FilingLocation;
                SecuredParty = uccFilings.SecuredParty;
                if (uccFilings.LegalType != null)
                    LegalType =
                        uccFilings.LegalType.Select(p => new LegalType(p)).ToList<ILegalType>();
                if (uccFilings.LegalAction != null)
                    LegalAction =
                        uccFilings.LegalAction.Select(p => new LegalAction(p))
                            .ToList<ILegalAction>();

                if (uccFilings.OriginalUCCFilingsInfo != null)
                    OriginalUccFilingsInfo =
                        uccFilings.OriginalUCCFilingsInfo.Select(
                            p => new UccFilingsOriginalUccFilingsInfo(p))
                            .ToList<IUccFilingsOriginalUccFilingsInfo>();
                if (uccFilings.CollateralCodes != null)
                    CollateralCodes =
                        new UccFilingsCollateralCodesCollateral(
                            uccFilings.CollateralCodes);
            }
        }

        public string DateFiled { get; set; }
        public string DocumentNumber { get; set; }
        public string FilingLocation { get; set; }
        public string SecuredParty { get; set; }

        public List<ILegalType> LegalType { get; set; }

        public List<ILegalAction> LegalAction { get; set; }

        public List<IUccFilingsOriginalUccFilingsInfo> OriginalUccFilingsInfo
        {
            get;
            set;
        }

        public IUccFilingsCollateralCodesCollateral CollateralCodes { get; set; }
    }
}