namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ILegalAction
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}