namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class PublicIndicator : IPublicIndicator
    {
        public PublicIndicator()
        {
        }

        public PublicIndicator(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileBusinessFactsPublicIndicator publicIndicator)
        {
            if (publicIndicator != null)

                Code = publicIndicator.code;
        }

        public string Code { get; set; }
    }
}