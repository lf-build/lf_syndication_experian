using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class PaymentTrends : IPaymentTrends
    {
        public PaymentTrends()
        {
        }

        public PaymentTrends(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfilePaymentTrends paymentTrends)
        {
            if (paymentTrends != null)
            {
                if (paymentTrends.CurrentMonth != null)
                    CurrentMonth =
                        paymentTrends.CurrentMonth.Select(p => new CurrentMonth(p))
                            .ToList<ICurrentMonth>();
                if (paymentTrends.PriorMonth != null)
                    PriorMonth =
                        paymentTrends.PriorMonth.Select(p => new PriorMonth(p))
                            .ToList<IPriorMonth>();
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<ICurrentMonth, CurrentMonth>))]
        public List<ICurrentMonth> CurrentMonth { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPriorMonth, PriorMonth>))]
        public List<IPriorMonth> PriorMonth { get; set; }
    }
}