namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class UccFilingsCollateralCodesCollateralValues : IUccFilingsCollateralCodesCollateralValues
    {
        public UccFilingsCollateralCodesCollateralValues()
        {
        }

        public UccFilingsCollateralCodesCollateralValues(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileUCCFilingsCollateralCodesCollateralValues uccFilingsCollateralCodesCollateralValues)
        {
            if (uccFilingsCollateralCodesCollateralValues != null)
            {
                Code = uccFilingsCollateralCodesCollateralValues.code;
                Value = uccFilingsCollateralCodesCollateralValues.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}