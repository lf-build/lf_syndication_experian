namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IDoingBusinessAs
    {
        string DbaName { get; set; }
    }
}