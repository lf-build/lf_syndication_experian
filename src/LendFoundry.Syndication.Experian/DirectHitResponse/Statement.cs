namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class Statement : IStatement
    {
        public Statement()
        {
        }

        public Statement(Proxy.DirectHitReportResponse.Statement statement)
        {
            if (statement != null)
            {
                Code = statement.code;
                Value = statement.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}