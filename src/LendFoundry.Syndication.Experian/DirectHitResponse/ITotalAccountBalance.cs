using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ITotalAccountBalance
    {
        string Amount { get; set; }
        List<IModifier> Modifier { get; set; }
    }
}