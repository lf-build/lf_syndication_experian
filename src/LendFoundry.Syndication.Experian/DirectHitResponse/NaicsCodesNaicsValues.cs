namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public class NaicsCodesNaicsValues : INaicsCodesNaicsValues
    {
        public NaicsCodesNaicsValues()
        {
        }

        public NaicsCodesNaicsValues(Proxy.DirectHitReportResponse.NetConnectResponseProductsPremierProfileNAICSCodesNAICSValues naicsCodesNaicsValues)
        {
            if (naicsCodesNaicsValues != null)
            {
                Code = naicsCodesNaicsValues.code;
                Value = naicsCodesNaicsValues.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}