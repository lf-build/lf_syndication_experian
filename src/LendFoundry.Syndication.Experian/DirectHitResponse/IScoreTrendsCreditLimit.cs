using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IScoreTrendsCreditLimit
    {
        string CreditLimitAmount { get; set; }
        List<IMostRecentQuarter> MostRecentQuarter { get; set; }
        List<IPriorQuarter> PriorQuarter { get; set; }
    }
}