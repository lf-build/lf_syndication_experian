using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface IKeyPersonnelExecutiveInformation
    {
        string Name { get; set; }
        List<IKeyPersonnelExecutiveInformationNameFlag> NameFlag { get; set; }
        List<IKeyPersonnelExecutiveInformationTitle> Title { get; set; }
    }
}