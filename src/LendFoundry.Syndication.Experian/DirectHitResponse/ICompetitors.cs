namespace LendFoundry.Syndication.Experian.DirectHitReportResponse
{
    public interface ICompetitors
    {
        string CompetitorName { get; set; }
    }
}