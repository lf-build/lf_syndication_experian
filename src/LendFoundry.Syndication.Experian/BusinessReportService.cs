﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Experian.Events;
using LendFoundry.Syndication.Experian.Proxy;
using LendFoundry.Syndication.Experian.Proxy.SearchBusinessResponse;
using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian
{
    public class BusinessReportService : IBusinessReportService
    {
        public BusinessReportService(IExperianConfiguration reportConfiguration, IBusinessReportProxy businessReportProxy, IEventHubClient eventHub, ILookupService lookup, ILogger logger, ITenantTime tenantTime)
        {
            if (reportConfiguration == null)
                throw new ArgumentNullException(nameof(reportConfiguration));
            if (businessReportProxy == null)
                throw new ArgumentNullException(nameof(businessReportProxy));
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));
            if (lookup == null)
                throw new ArgumentNullException(nameof(lookup));
            ReportConfiguration = reportConfiguration.businessReportConfiguration;
            BusinessReportProxy = businessReportProxy;
            EventHub = eventHub;
            Lookup = lookup;
            Logger = logger;
            TenantTime = tenantTime;
        }
        private ILogger Logger { get; }
        private IBusinessReportConfiguration ReportConfiguration { get; }
        private IBusinessReportProxy BusinessReportProxy { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }

        private ITenantTime TenantTime { get; }

        public async Task<ISearchBusinessResponse> GetBusinessSearch(string entityType, string entityId, IBusinessSearchRequest searchRequest)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            EnsureBusinessSearchRequestIsValid(searchRequest);

            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                var searchBusinessProxyRequest = new Proxy.SearchBusinessRequest.NetConnectRequest(ReportConfiguration, searchRequest);
                Logger.Info("Started Execution for GetBusinessSearch Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "ReferenceNumber" + referencenumber + " TypeOf ExperianReport: BusinessReport");
                NetConnectResponse searchBusinessProxyResponse = BusinessReportProxy.GetBusinessSearch(searchBusinessProxyRequest);
                if (!string.IsNullOrWhiteSpace(searchBusinessProxyResponse?.ErrorTag))
                {
                    throw new ExperianException(
                        $"Net connect response with error. CompletionCode {searchBusinessProxyResponse.CompletionCode}. - ErrorMessage: {searchBusinessProxyResponse.ErrorMessage ?? ""} - ErrorTag: {searchBusinessProxyResponse.ErrorTag}");
                }
                var result = new Response.SearchBusinessResponse(searchBusinessProxyResponse);

                await EventHub.Publish(new ExperianBusinessSearchPerformed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = searchRequest,
                    ReferenceNumber = referencenumber
                });
                Logger.Info("Completed Execution for GetBusinessSearch Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "ReferenceNumber" + referencenumber + " TypeOf ExperianReport: BusinessReport");
                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetBusinessSearch Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "ReferenceNumber" + referencenumber + " TypeOf ExperianReport: BusinessReport" + "Exception" + exception.Message);
                await EventHub.Publish(new ExperianBusinessSearchFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.InnerException != null ? exception.InnerException.Message : exception.Message,
                    Request = searchRequest,
                    ReferenceNumber = referencenumber
                });
                throw;
            }
        }
        public async Task<IBusinessReportResponse> GetBusinessReport(string entityType, string entityId, IBusinessReportRequest reportRequest)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            EnsureBusinessReportRequestIsValid(reportRequest);

            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {

                var businessReportRequest = new Proxy.BusinessReportRequest.NetConnectRequest(ReportConfiguration, reportRequest);
                Logger.Info("Started Execution for GetBusinessReport Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "ReferenceNumber" + referencenumber + " TypeOf ExperianReport: BusinessReport");

                string businessReportXmlResponse = BusinessReportProxy.GetBusinessReport(businessReportRequest);
                Proxy.BusinessReportResponse.NetConnectResponse businessReportResponse = XmlSerialization.Deserialize<Proxy.BusinessReportResponse.NetConnectResponse>(businessReportXmlResponse);
                if (!string.IsNullOrWhiteSpace(businessReportResponse?.ErrorTag))
                {
                    throw new ExperianException(
                        $"Net connect response with error. CompletionCode {businessReportResponse.CompletionCode}. - ErrorMessage: {businessReportResponse.ErrorMessage ?? ""} - ErrorTag: {businessReportResponse.ErrorTag}");
                }
                var result = new Response.BusinessReportResponse(businessReportResponse);

                await EventHub.Publish(new ExperianBusinessReportFetched
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response= result,
                    ResponseObject=businessReportXmlResponse,
                    Request = reportRequest,
                    ReferenceNumber = referencenumber
                });
                Logger.Info("Completed Execution for GetBusinessReport Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "ReferenceNumber" + referencenumber + " TypeOf ExperianReport: BusinessReport");
                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Proceesing GetBusinessReport Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "ReferenceNumber" + referencenumber + " TypeOf ExperianReport: BusinessReport" + "Exception" + exception.Message);
                await EventHub.Publish(new ExperianBusinessReportFailed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.InnerException != null ? exception.InnerException.Message : exception.Message,
                    Request = reportRequest,
                    ReferenceNumber = referencenumber
                });
                throw;
            }
        }


        public async Task<string> ResetPassword()
        {
            var response = await RequestNewPassword();
            if (string.IsNullOrEmpty(response))
            {
                await SetPassword(response);
            }
            else
                throw new ExperianException("Empty password received from RequestNewPassword method For Experian Business");
            return response;

        }

        public async Task<IDirectHitReportResponse> GetDirectHitReport(string entityType, string entityId, IDirectHitReportRequest directhitRequest)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            EnsureDirectHitRequestIsValid(directhitRequest);

            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                var directedReportRequest = new Proxy.DirectHitReportRequest.NetConnectRequest(ReportConfiguration, directhitRequest);

                Logger.Info("Started Execution for GetDirectHitReport Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "ReferenceNumber" + referencenumber + " TypeOf ExperianReport: BusinessReport");

                Proxy.DirectHitReportResponse.NetConnectResponse directhitReportResponse = BusinessReportProxy.GetDirectHitReport(directedReportRequest);
                if (!string.IsNullOrWhiteSpace(directhitReportResponse?.ErrorTag))
                {
                    throw new ExperianException(
                            $"Net connect response with error. CompletionCode {directhitReportResponse.CompletionCode}. - ErrorMessage: {directhitReportResponse.ErrorMessage ?? ""} - ErrorTag: {directhitReportResponse.ErrorTag}");
                }
                var result = new Response.DirectHitReportResponse(directhitReportResponse);

                await EventHub.Publish(new ExperianDirecthitReportFetched
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = directhitRequest,
                    ReferenceNumber = referencenumber
                });
                Logger.Info("Completed Execution for GetDirectHitReport Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "ReferenceNumber" + referencenumber + " TypeOf ExperianReport: BusinessReport");
                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Proceesing GetDirectHitReport Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "ReferenceNumber" + referencenumber + " TypeOf ExperianReport: BusinessReport" + "Exception" + exception.Message);
                await EventHub.Publish(new ExperianDirecthitReportFailed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.InnerException != null ? exception.InnerException.Message : exception.Message,
                    Request = directhitRequest,
                    ReferenceNumber = referencenumber
                });
                throw;
            }
        }
        
        private async Task<string> RequestNewPassword()
        {
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                Logger.Info("Started Execution for RequestNewPassword Request Info: Date & Time:" + TenantTime.Now + "ReferenceNumber" + referencenumber);
                var response = BusinessReportProxy.RequestNewPassword();
                await EventHub.Publish(new NewPasswordRequested
                {
                    Response = response,
                    Request = "requestNewPassword",
                    ReferenceNumber = referencenumber
                });
                Logger.Info("Completed Execution for RequestNewPassword Request Info: Date & Time:" + TenantTime.Now + "ReferenceNumber" + referencenumber);
                return response;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Proceesing RequestNewPassword Date & Time:" + TenantTime.Now + " MethodName:" + "RequestNewPassword" + "ReferenceNumber" + referencenumber + "Exception" + exception.Message);
                throw;
            }


        }

        private async Task<string> SetPassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException(nameof(password));
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                Logger.Info("Started Execution for SetPassword Request Info: Date & Time:" + TenantTime.Now + "ReferenceNumber" + referencenumber);
                var response = BusinessReportProxy.ResetPassword(password);
                await EventHub.Publish(new ResetPasswordRequested
                {

                    Response = response,
                    Request = password,
                    ReferenceNumber = referencenumber
                });
                Logger.Info("Completed Execution for SetPassword Request Info: Date & Time:" + TenantTime.Now + "ReferenceNumber" + referencenumber);
                return response;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Proceesing SetPassword Date & Time:" + TenantTime.Now + " MethodName:" + "SetPassword" + "ReferenceNumber" + referencenumber + "Exception" + exception.Message);
                throw;
            }


        }
        private static void EnsureDirectHitRequestIsValid(IDirectHitReportRequest directhitRequest)
        {
            if (directhitRequest == null)
                throw new ArgumentNullException(nameof(directhitRequest));
            if (string.IsNullOrWhiteSpace(directhitRequest.BusinessName))
                throw new ArgumentException("BusinessName is required", nameof(directhitRequest.BusinessName));

            if (string.IsNullOrWhiteSpace(directhitRequest.City))
                throw new ArgumentException("City is required", nameof(directhitRequest.City));

            if (string.IsNullOrWhiteSpace(directhitRequest.Street))
                throw new ArgumentException("Street is required", nameof(directhitRequest.Street));

            if (string.IsNullOrWhiteSpace(directhitRequest.State))
                throw new ArgumentException("State is required", nameof(directhitRequest.State));

            if (string.IsNullOrWhiteSpace(directhitRequest.Zip))
                throw new ArgumentException("Zip is required", nameof(directhitRequest.Zip));

            if (!Regex.IsMatch(directhitRequest.Zip, "^([0-9]+)$"))
                throw new ArgumentException("Invalid Zip Code");
        }
        private static void EnsureBusinessReportRequestIsValid(IBusinessReportRequest reportRequest)
        {
            if (reportRequest == null)
                throw new ArgumentNullException(nameof(reportRequest));

            if (string.IsNullOrWhiteSpace(reportRequest.TransactionNumber))
                throw new ArgumentException("TransactionNumber is required",
                    nameof(reportRequest.TransactionNumber));

            if (string.IsNullOrWhiteSpace(reportRequest.BisListNumber))
                throw new ArgumentException("BISListNumber is required", nameof(reportRequest.BisListNumber));
        }
        private static void EnsureBusinessSearchRequestIsValid(IBusinessSearchRequest searchRequest)
        {
            if (searchRequest == null)
                throw new ArgumentNullException(nameof(searchRequest));

            if (string.IsNullOrWhiteSpace(searchRequest.BusinessName))
                throw new ArgumentException("BusinessName is required", nameof(searchRequest.BusinessName));

            if (string.IsNullOrWhiteSpace(searchRequest.City))
                throw new ArgumentException("City is required", nameof(searchRequest.City));

            if (string.IsNullOrWhiteSpace(searchRequest.Street))
                throw new ArgumentException("Street is required", nameof(searchRequest.Street));

            if (string.IsNullOrWhiteSpace(searchRequest.State))
                throw new ArgumentException("State is required", nameof(searchRequest.State));

            if (string.IsNullOrWhiteSpace(searchRequest.Zip))
                throw new ArgumentException("Zip is required", nameof(searchRequest.Zip));

            if (!Regex.IsMatch(searchRequest.Zip, "^([0-9]+)$"))
                throw new ArgumentException("Invalid Zip Code");
        }
        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
      
    }
   
}