﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Experian.Events;
using LendFoundry.Syndication.Experian.Proxy;
using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NetConnectRequest = LendFoundry.Syndication.Experian.Proxy.PersonalReportRequest.NetConnectRequest;

namespace LendFoundry.Syndication.Experian
{
    public class PersonalReportService : IPersonalReportService
    {
        public PersonalReportService(IExperianConfiguration reportConfiguration,
            IPersonalReportProxy personalReportProxy, IEventHubClient eventHub, ILookupService lookup, ILogger logger, ITenantTime tenantTime)
        {
            if (reportConfiguration == null)
                throw new ArgumentNullException(nameof(reportConfiguration));
            if (personalReportProxy == null)
                throw new ArgumentNullException(nameof(personalReportProxy));
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));
            if (lookup == null)
                throw new ArgumentNullException(nameof(lookup));
            EventHub = eventHub;
            Lookup = lookup;
            PersonalReportConfiguration = reportConfiguration.personalReportConfiguration;
            PersonalReportProxy = personalReportProxy;
            Logger = logger;
            TenantTime = tenantTime;
        }

        private IPersonalReportConfiguration PersonalReportConfiguration { get; }
        private IPersonalReportProxy PersonalReportProxy { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        private ILogger Logger { get; }
        private ITenantTime TenantTime { get; }

        public async Task<IPersonalReportResponse> GetPersonalReportResponse(string entityType, string entityId, IPersonalReportRequest personalReportRequest, bool isHardpull = false)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            entityType = EnsureEntityType(entityType);

            EnsureRequestIsValid(personalReportRequest);
            personalReportRequest.Street = personalReportRequest.Street.Replace("," , " ");
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                var personalReportRequestProxy = new NetConnectRequest(PersonalReportConfiguration, personalReportRequest);
                if (isHardpull == true)
                {
                    if (string.IsNullOrWhiteSpace(PersonalReportConfiguration.HardPullSubCode))
                    {
                        throw new ExperianException($"Personal Report Configuration HardPullSubCode Not Found.");
                    }
                    else
                    {
                        personalReportRequestProxy.Request.Products.CustomSolution.Subscriber.SubCode = PersonalReportConfiguration.HardPullSubCode;
                    }
                }
                Logger.Info($"Started Execution for Get Personal Report Request Info: Date & Time:{TenantTime.Now} EntityType:{entityType} EntityId:{entityId}ReferenceNumber{referencenumber} TypeOf ExperianReport: PersonalReport");

                var personalReportResponseProxy = PersonalReportProxy.GetPersonalReport(personalReportRequestProxy);

                if (!string.IsNullOrWhiteSpace(personalReportResponseProxy?.ErrorTag))
                {
                    throw new ExperianException($"Net connect response with error. CompletionCode {personalReportResponseProxy.CompletionCode}. - ErrorMessage: {personalReportResponseProxy.ErrorMessage } - ErrorTag: {personalReportResponseProxy.ErrorTag}");
                }

                var personalReportResponse = new Response.PersonalReportResponse(personalReportResponseProxy);
                var result = new Response.PersonalReportResponse();
                if (personalReportResponse != null && personalReportResponse.ProductCreditProfile != null)
                {
                    result = ExperianCodeValueMapping(personalReportResponse);
                }
                if (isHardpull == true)
                {
                    await EventHub.Publish(new ExperianPersonalHardPullReportFetched
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = result,
                        Request = personalReportRequest,
                        ReferenceNumber = referencenumber
                    });
                }
                else
                {
                    await EventHub.Publish(new ExperianPersonalReportFetched
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = result,
                        Request = personalReportRequest,
                        ReferenceNumber = referencenumber
                    });
                }
                Logger.Info($"Completed Execution for Get Personal Report Request Info: Date & Time:{TenantTime.Now} EntityType:{entityType} EntityId:{entityId} ReferenceNumber {referencenumber} TypeOf ExperianReport: PersonalReport");

                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetPersonalReportResponse Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "ReferenceNumber" + referencenumber + " TypeOf ExperianReport: PersonalReport error" + exception);
                await EventHub.Publish(new ExperianPersonalReportFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.InnerException != null ? exception.InnerException.Message : exception.Message,
                    Request = personalReportRequest,
                    ReferenceNumber = referencenumber
                });
                return null;
            }
        }

        private Response.PersonalReportResponse ExperianCodeValueMapping(Response.PersonalReportResponse personalReportResponse)
        {
            if (personalReportResponse.ProductCreditProfile.PublicRecord != null && personalReportResponse.ProductCreditProfile.PublicRecord.Count > 0)
            {
                var publicRecordStatusCodeDesignator = Lookup.GetLookupEntries("publicRecordStatusCodeDesignator");
                var ecoaCode = Lookup.GetLookupEntries("ECOACode");
                foreach (var publicRecord in personalReportResponse.ProductCreditProfile.PublicRecord)
                {
                    if (publicRecord.Status.Code != null)
                    {                        
                        publicRecord.Designator = publicRecordStatusCodeDesignator.Where(x => x.Key.ToUpper() == publicRecord.Status.Code.ToUpper()).Select(x => x.Value).FirstOrDefault();
                    }
                    if (publicRecord.PublicRecordEcoa != null && publicRecord.PublicRecordEcoa.Code != null)
                    {                        
                        publicRecord.PublicRecordEcoa.Value = ecoaCode.Where(x => x.Key.ToUpper() == publicRecord.PublicRecordEcoa.Code.ToUpper()).Select(x => x.Value).FirstOrDefault();
                    }
                }
            }
            if (personalReportResponse.ProductCreditProfile.TradeLine != null && personalReportResponse.ProductCreditProfile.TradeLine.Count > 0)
            {
                var accountConditionAndPaymentStatus = Lookup.GetLookupEntries("accountConditionAndPaymentStatus");
                var accountPurposeType = Lookup.GetLookupEntries("accountPurposeType");

                foreach (var tradeLine in personalReportResponse.ProductCreditProfile.TradeLine)
                {                    
                    if (tradeLine.EnhancedPaymentData != null
                        && tradeLine.EnhancedPaymentData.AccountCondition != null
                        && tradeLine.EnhancedPaymentData.AccountCondition.Code != null)
                    {
                        tradeLine.EnhancedPaymentData.AccountCondition.Value = accountConditionAndPaymentStatus.Where(x => x.Key.ToUpper() == tradeLine.EnhancedPaymentData.AccountCondition.Code.ToUpper()).Select(x => x.Value).FirstOrDefault();
                    }
                    if (tradeLine.EnhancedPaymentData != null
                        && tradeLine.EnhancedPaymentData.AccountType.Code != null)
                    {
                        tradeLine.EnhancedPaymentData.AccountType.Value = accountPurposeType.Where(x => x.Key.ToUpper() == tradeLine.EnhancedPaymentData.AccountType.Code.ToUpper()).Select(x => x.Value).FirstOrDefault();
                    }
                    if (tradeLine.EnhancedPaymentData != null
                       && tradeLine.EnhancedPaymentData.PaymentStatus.Code != null)
                    {
                        tradeLine.EnhancedPaymentData.PaymentStatus.Value = accountConditionAndPaymentStatus.Where(x => x.Key.ToUpper() == tradeLine.EnhancedPaymentData.PaymentStatus.Code.ToUpper()).Select(x => x.Value).FirstOrDefault();
                    }
                }
            }
            if (personalReportResponse.ProductCreditProfile.Inquiry != null && personalReportResponse.ProductCreditProfile.Inquiry.Count > 0)
            {
                var fraudIndicators = Lookup.GetLookupEntries("inquiryTerms");
                foreach (var inquiry in personalReportResponse.ProductCreditProfile.Inquiry)
                {
                    if (inquiry.Terms.Code != null)
                    {
                        inquiry.Terms.Value = fraudIndicators.Where(x => x.Key.ToUpper() == inquiry.Terms.Code.ToUpper()).Select(x => x.Value).FirstOrDefault();
                    }
                }
            }
            if (personalReportResponse.ProductCreditProfile.RiskModel != null && personalReportResponse.ProductCreditProfile.RiskModel.Count > 0)
            {
                var vantageScore = Lookup.GetLookupEntries("vantageScore");
                var fraudShieldScorePlus = Lookup.GetLookupEntries("FraudShieldScorePlus");
                var fico8 = Lookup.GetLookupEntries("Fico8");
                var bankruptcyPlus = Lookup.GetLookupEntries("BankruptcyPlus");

                foreach (var riskModel in personalReportResponse.ProductCreditProfile.RiskModel)
                {                    
                    if (riskModel.ModelIndicator.Code != null)
                    {
                        var scoreFactorValue = new Dictionary<string, string>();
                        if (riskModel.ModelIndicator.Code.Trim() == "2")
                        {
                            scoreFactorValue = fraudShieldScorePlus;
                        }
                        else if (riskModel.ModelIndicator.Code.Trim().ToUpper() == "V3")
                        {
                            scoreFactorValue = vantageScore;
                        }
                        else if (riskModel.ModelIndicator.Code.Trim().ToUpper() == "BP")
                        {
                            scoreFactorValue = bankruptcyPlus;
                        }
                        else if (riskModel.ModelIndicator.Code.Trim().ToUpper() == "AF")
                        {
                            scoreFactorValue = fico8;
                        }
                        if (riskModel.ScoreFactorCodeOne != null && scoreFactorValue.Count() > 0)
                        {
                            riskModel.ScoreFactorCodeOneDesc = scoreFactorValue.Where(x => x.Key.ToUpper() == riskModel.ScoreFactorCodeOne.ToUpper()).Select(x => x.Value).FirstOrDefault();
                        }
                        if (riskModel.ScoreFactorCodeTwo != null && scoreFactorValue.Count() > 0)
                        {
                            riskModel.ScoreFactorCodeTwoDesc = scoreFactorValue.Where(x => x.Key.ToUpper() == riskModel.ScoreFactorCodeTwo.ToUpper()).Select(x => x.Value).FirstOrDefault();
                        }
                        if (riskModel.ScoreFactorCodeThree != null && scoreFactorValue.Count() > 0)
                        {
                            riskModel.ScoreFactorCodeThreeDesc = scoreFactorValue.Where(x => x.Key.ToUpper() == riskModel.ScoreFactorCodeThree.ToUpper()).Select(x => x.Value).FirstOrDefault();
                        }
                        if (riskModel.ScoreFactorCodeFour != null && scoreFactorValue.Count() > 0)
                        {
                            riskModel.ScoreFactorCodeFourDesc = scoreFactorValue.Where(x => x.Key.ToUpper() == riskModel.ScoreFactorCodeFour.ToUpper()).Select(x => x.Value).FirstOrDefault();
                        }
                    }
                }
            }
            return personalReportResponse;
        }

        public async Task<string> ResetPassword()
        {
            var response = await RequestNewPassword();
            if (string.IsNullOrEmpty(response))
            {
                await SetPassword(response);
            }
            else
                throw new ExperianException("Empty password received from RequestNewPassword method For Experian Personal");
            return response;

        }
        private static void EnsureRequestIsValid(IPersonalReportRequest personalReportRequest)
        {
            if (personalReportRequest == null)
                throw new ArgumentNullException(nameof(personalReportRequest));

            if (string.IsNullOrWhiteSpace(personalReportRequest.Firstname))
                throw new ArgumentException("Firstname is required", nameof(personalReportRequest.Firstname));

            if (!Regex.IsMatch(personalReportRequest.Firstname, "^([a-zA-Z\\s]+)$"))
                throw new ArgumentException("Invalid FirstName", nameof(personalReportRequest.Firstname));

            if (string.IsNullOrWhiteSpace(personalReportRequest.Lastname))
                throw new ArgumentException("Lastname is required", nameof(personalReportRequest.Lastname));

            if (!Regex.IsMatch(personalReportRequest.Lastname, "^([a-zA-Z\\s]+)$"))
                throw new ArgumentException("Invalid LastName", nameof(personalReportRequest.Lastname));

            if (string.IsNullOrWhiteSpace(personalReportRequest.Street))
                throw new ArgumentException("Street is required", nameof(personalReportRequest.Street));

            if (string.IsNullOrWhiteSpace(personalReportRequest.Zip))
                throw new ArgumentException("Zip is required", nameof(personalReportRequest.Zip));

            if (!Regex.IsMatch(personalReportRequest.Zip, "^([0-9]+)$"))
                throw new ArgumentException("Invalid Zip Code");

            if (string.IsNullOrWhiteSpace(personalReportRequest.Ssn))
                throw new ArgumentException("Ssn is required", nameof(personalReportRequest.Ssn));

            if (!Regex.IsMatch(personalReportRequest.Ssn, "^([0-9]{9}|[0-9]{3}-[0-9]{2}-[0-9]{4}|[0-9]+)$"))
                throw new ArgumentException("Invalid Ssn Number format");

            if (personalReportRequest.Ssn.Replace("-", "").Length > 9)
                throw new ArgumentException("Ssn should not more than 9 digits", nameof(personalReportRequest.Ssn));
        }
        private async Task<string> RequestNewPassword()
        {
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                Logger.Info("Started Execution for RequestPersonalPassword Request Info: Date & Time:" + TenantTime.Now + "ReferenceNumber" + referencenumber + "TypeOf ExperianReport: PersonalReport");
                var response = PersonalReportProxy.RequestNewPassword();
                await EventHub.Publish(new NewPasswordRequested
                {
                    Response = response,
                    Request = "requestNewPassword",
                    ReferenceNumber = referencenumber
                });
                Logger.Info("Completed Execution for RequestPersonalPassword Request Info: Date & Time:" + TenantTime.Now + "ReferenceNumber" + referencenumber + "TypeOf ExperianReport: PersonalReport");
                return response;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Proceesing RequestPersonalPassword Date & Time:" + TenantTime.Now + " MethodName:" + "RequestNewPassword" + "ReferenceNumber" + referencenumber + "Exception" + exception.Message + "TypeOf ExperianReport: PersonalReport");
                throw;
            }

        }

        private async Task<string> SetPassword(string password)
        {
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                if (string.IsNullOrWhiteSpace(password))
                    throw new ArgumentNullException(nameof(password));
                Logger.Info("Started Execution for SetPassword Request Info: Date & Time:" + TenantTime.Now + "ReferenceNumber" + referencenumber + "TypeOf ExperianReport: PersonalReport");
                var response = PersonalReportProxy.ResetPassword(password);
                await EventHub.Publish(new ResetPasswordRequested
                {
                    Response = response,
                    Request = password,
                    ReferenceNumber = referencenumber
                });
                Logger.Info("Completed Execution for SetPassword Request Info: Date & Time:" + TenantTime.Now + "ReferenceNumber" + referencenumber + "TypeOf ExperianReport: PersonalReport");
                return response;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Proceesing SetPassword Date & Time:" + TenantTime.Now + " MethodName:" + "SetPassword" + "ReferenceNumber" + referencenumber + "Exception" + exception.Message + "TypeOf ExperianReport: PersonalReport");
                throw;
            }


        }
        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}