using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IIntelliscoreScoreInformation
    {
        string Filler { get; set; }
        string ProfileNumber { get; set; }
        string PercentileRanking { get; set; }
        string Action { get; set; }
        string RiskClass { get; set; }

        List<IPubliclyHeldCompany> PubliclyHeldCompany { get; set; }

        List<ILimitedProfile> LimitedProfile { get; set; }

        List<IScoreInfo> ScoreInfo { get; set; }

        List<IModelInformation> ModelInformation { get; set; }
    }
}