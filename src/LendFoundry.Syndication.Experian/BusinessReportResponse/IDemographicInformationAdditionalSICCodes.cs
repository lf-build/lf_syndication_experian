namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 
	public interface IDemographicInformationAdditionalSICCodes {
		 string SICCode { get; set; }
	}
	
	}