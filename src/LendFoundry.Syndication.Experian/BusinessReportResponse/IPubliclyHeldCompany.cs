namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IPubliclyHeldCompany
    {
        string Code { get; set; }
    }
}