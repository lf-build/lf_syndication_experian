using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class NaicsCodesNaics : INaicsCodesNaics
    {
        public NaicsCodesNaics()
        {
        }

        public NaicsCodesNaics(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileNAICSCodesNAICS naicsCodesNaics)
        {
            if (naicsCodesNaics?.NAICS != null)
                Naics = new NaicsCodesNaicsValues(naicsCodesNaics.NAICS);
        }

        [JsonConverter(typeof(InterfaceConverter<INaicsCodesNaicsValues, NaicsCodesNaicsValues>))]
        public INaicsCodesNaicsValues Naics { get; set; }
    }
}