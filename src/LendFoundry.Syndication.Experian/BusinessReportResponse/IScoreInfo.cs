namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IScoreInfo
    {
        string Score { get; set; }
    }
}