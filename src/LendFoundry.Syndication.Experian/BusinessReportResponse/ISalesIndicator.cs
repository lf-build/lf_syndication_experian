namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ISalesIndicator
    {
        string Code { get; set; }
    }
}