namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class BillingIndicator : IBillingIndicator
    {
        public BillingIndicator()
        {
        }

        public BillingIndicator(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileBillingIndicator billingIndicator)
        {
            if (billingIndicator != null)
            {
                Code = billingIndicator.code;
                Value = billingIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}