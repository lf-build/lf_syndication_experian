using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class UccFilingsCollateralCodesCollateral : IUccFilingsCollateralCodesCollateral
    {
        public UccFilingsCollateralCodesCollateral()
        {
        }

        public UccFilingsCollateralCodesCollateral(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileUCCFilingsCollateralCodesCollateral uccFilingsCollateralCodesCollateral)
        {
            if (uccFilingsCollateralCodesCollateral?.Collateral != null)
                Collateral =
                    uccFilingsCollateralCodesCollateral.Collateral.Select(
                        p => new UccFilingsCollateralCodesCollateralValues(p))
                        .ToList<IUccFilingsCollateralCodesCollateralValues>();
        }
        [JsonConverter(typeof(InterfaceListConverter<IUccFilingsCollateralCodesCollateralValues, UccFilingsCollateralCodesCollateralValues>))]
        public List<IUccFilingsCollateralCodesCollateralValues> Collateral { get; set; }
    }
}