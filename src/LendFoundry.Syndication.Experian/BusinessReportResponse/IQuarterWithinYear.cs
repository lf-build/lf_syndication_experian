namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IQuarterWithinYear
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}