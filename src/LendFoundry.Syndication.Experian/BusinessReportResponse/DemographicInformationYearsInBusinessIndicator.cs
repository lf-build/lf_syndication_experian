namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 	public class DemographicInformationYearsInBusinessIndicator : IDemographicInformationYearsInBusinessIndicator {

	     public DemographicInformationYearsInBusinessIndicator()
	     {
	         
	     }

	     public DemographicInformationYearsInBusinessIndicator(Proxy.BusinessReportResponse.DemographicInformationYearsInBusinessIndicator yearsInBusinessIndicator)
	     {
	         if (yearsInBusinessIndicator!=null)
	         {
	             Code = yearsInBusinessIndicator.Code;
	             Text = yearsInBusinessIndicator.Text;
	         }
	     }
		public string Code { get; set; }
		public string Text { get; set; }
	}
	
}