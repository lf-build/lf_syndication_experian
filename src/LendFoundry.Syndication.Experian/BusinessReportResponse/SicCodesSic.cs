using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class SicCodesSic : ISicCodesSic
    {
        public SicCodesSic()
        {
        }

        public SicCodesSic(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileSICCodesSIC sicCodesSic)
        {
            if (sicCodesSic?.SIC != null)
                Sic = new SicCodesSicValues(sicCodesSic.SIC);
        }

        [JsonConverter(typeof(InterfaceConverter<ISicCodesSicValues, SicCodesSicValues>))]
        public ISicCodesSicValues Sic { get; set; }
    }
}