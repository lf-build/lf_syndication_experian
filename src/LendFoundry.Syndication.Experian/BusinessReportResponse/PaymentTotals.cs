using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class PaymentTotals : IPaymentTotals
    {
        public PaymentTotals()
        {
        }

        public PaymentTotals(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfilePaymentTotals paymentTotals)
        {
            if (paymentTotals != null)
            {
                if (paymentTotals.NewlyReportedTradeLines != null)
                    NewlyReportedTradeLines =
                        paymentTotals.NewlyReportedTradeLines.Select(
                            p => new TotalsNewlyReportedTradeLines(p))
                            .ToList<ITotalsNewlyReportedTradeLines>();
                if (paymentTotals.ContinouslyReportedTradeLines != null)
                    ContinouslyReportedTradeLines =
                        paymentTotals.ContinouslyReportedTradeLines.Select(
                            p => new TotalsContinouslyReportedTradeLines(p))
                            .ToList<ITotalsContinouslyReportedTradeLines>();
                if (paymentTotals.CombinedTradeLines != null)
                    CombinedTradeLines =
                        paymentTotals.CombinedTradeLines.Select(
                            p => new TotalsCombinedTradeLines(p))
                            .ToList<ITotalsCombinedTradeLines>();
                if (paymentTotals.AdditionalTradeLines != null)
                    AdditionalTradeLines =
                        paymentTotals.AdditionalTradeLines.Select(
                            p => new TotalsAdditionalTradeLines(p))
                            .ToList<ITotalsAdditionalTradeLines>();
                if (paymentTotals.TradeLines != null)
                    TradeLines =
                        paymentTotals.TradeLines.Select(p => new TotalsTradeLines(p))
                            .ToList<ITotalsTradeLines>();
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<ITotalsNewlyReportedTradeLines, TotalsNewlyReportedTradeLines>))]
        public List<ITotalsNewlyReportedTradeLines> NewlyReportedTradeLines { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalsContinouslyReportedTradeLines, TotalsContinouslyReportedTradeLines>))]
        public List<ITotalsContinouslyReportedTradeLines> ContinouslyReportedTradeLines { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalsCombinedTradeLines, TotalsCombinedTradeLines>))]
        public List<ITotalsCombinedTradeLines> CombinedTradeLines { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalsAdditionalTradeLines, TotalsAdditionalTradeLines>))]
        public List<ITotalsAdditionalTradeLines> AdditionalTradeLines { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalsTradeLines, TotalsTradeLines>))]
        public List<ITotalsTradeLines> TradeLines { get; set; }
    }
}