namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ICollectionAgencyInfo
    {
        string AgencyName { get; set; }
        string PhoneNumber { get; set; }
    }
}