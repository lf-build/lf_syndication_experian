namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 
	public class DemographicInformationModifier:IDemographicInformationModifier {

	    public DemographicInformationModifier()
	    {
	        
	    }

	    public DemographicInformationModifier(Proxy.BusinessReportResponse.DemographicInformationModifier modifier)
	    {
	        if (modifier!=null)
	        {
	        Code = modifier.Code;

            }
        }
		public string Code { get; set; }
	}
}