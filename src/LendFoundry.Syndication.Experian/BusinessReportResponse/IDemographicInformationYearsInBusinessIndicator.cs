namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 	public interface  IDemographicInformationYearsInBusinessIndicator {
		 string Code { get; set; }
		 string Text { get; set; }
	}
	
}