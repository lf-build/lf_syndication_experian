namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ILinkageRecordType
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}