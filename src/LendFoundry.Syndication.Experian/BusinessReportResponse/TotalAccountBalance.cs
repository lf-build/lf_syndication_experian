using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class TotalAccountBalance : ITotalAccountBalance
    {
        public TotalAccountBalance()
        {
        }

        public TotalAccountBalance(Proxy.BusinessReportResponse.TotalAccountBalance totalAccountBalance)
        {
            if (totalAccountBalance != null)
            {
                Amount = totalAccountBalance.Amount;
                if (totalAccountBalance.Modifier != null)
                    Modifier = totalAccountBalance.Modifier.Select(p => new Modifier(p)).ToList<IModifier>();
            }
        }

        public string Amount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IModifier, Modifier>))]
        public List<IModifier> Modifier { get; set; }
    }
}