namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class PubliclyHeldCompany : IPubliclyHeldCompany
    {
        public PubliclyHeldCompany()
        {
        }

        public PubliclyHeldCompany(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationPubliclyHeldCompany publiclyHeldCompany)
        {
            if (publiclyHeldCompany != null)

                Code = publiclyHeldCompany.code;
        }

        public string Code { get; set; }
    }
}