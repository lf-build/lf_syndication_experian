namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class KeyPersonnelExecutiveInformationNameFlag : IKeyPersonnelExecutiveInformationNameFlag
    {
        public KeyPersonnelExecutiveInformationNameFlag()
        {
        }

        public KeyPersonnelExecutiveInformationNameFlag(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformationNameFlag keyPersonnelExecutiveInformationNameFlag)
        {
            if (keyPersonnelExecutiveInformationNameFlag != null)
            {
                Code = keyPersonnelExecutiveInformationNameFlag.code;
                Value = keyPersonnelExecutiveInformationNameFlag.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}