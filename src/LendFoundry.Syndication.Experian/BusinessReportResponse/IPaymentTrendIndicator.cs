namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IPaymentTrendIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}