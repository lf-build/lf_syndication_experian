namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class LegalAction : ILegalAction
    {
        public LegalAction()
        {
        }

        public LegalAction(Proxy.BusinessReportResponse.LegalAction legalAction)
        {
            if (legalAction != null)
            {
                Code = legalAction.code;
                Value = legalAction.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}