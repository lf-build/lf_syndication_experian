using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IBusinessFacts
    {
        string FileEstablishedDate { get; set; }
        string YearsInBusiness { get; set; }
        string SalesRevenue { get; set; }
        string EmployeeSize { get; set; }
        string StateOfIncorporation { get; set; }
        string DateOfIncorporation { get; set; }
        List<IBusinessFactsFileEstablishedFlag> FileEstablishedFlag { get; set; }
        List<IBusinessTypeIndicator> BusinessTypeIndicator { get; set; }

        List<IYearsInBusinessIndicator> YearsInBusinessIndicator { get; set; }

        List<ISalesIndicator> SalesIndicator { get; set; }
        List<IEmployeeSizeIndicator> EmployeeSizeIndicator { get; set; }
        List<IPublicIndicator> PublicIndicator { get; set; }
        List<INonProfitIndicator> NonProfitIndicator { get; set; }
    }
}