namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 
	public class DemographicInformationNetWorthIndicator:IDemographicInformationNetWorthIndicator {
	    public DemographicInformationNetWorthIndicator()
	    {
	        
	    }

	    public DemographicInformationNetWorthIndicator(Proxy.BusinessReportResponse.DemographicInformationNetWorthIndicator netWorthIndicator)
	    {
	        if (netWorthIndicator!=null)
	        {
	            Code = netWorthIndicator.Code;
	        }
	    }
		public string Code { get; set; }
	}
}