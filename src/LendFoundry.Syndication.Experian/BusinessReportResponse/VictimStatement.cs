namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class VictimStatement : IVictimStatement
    {
        public VictimStatement()
        {
        }

        public VictimStatement(string code, string value)
        {
            if (!string.IsNullOrWhiteSpace(code))
                Code = code;
            if (!string.IsNullOrWhiteSpace(value))
                Value = value;
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}