namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ILegalType
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}