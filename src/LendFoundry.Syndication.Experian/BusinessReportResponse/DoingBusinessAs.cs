namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class DoingBusinessAs : IDoingBusinessAs
    {
        public DoingBusinessAs()
        {
        }

        public DoingBusinessAs(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileDoingBusinessAs doingBusinessAs)
        {
            if (doingBusinessAs != null)

                DbaName = doingBusinessAs.DBAName;
        }

        public string DbaName { get; set; }
    }
}