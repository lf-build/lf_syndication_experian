using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class PremierProfile : IPremierProfile
    {
        public PremierProfile()
        {
        }

        public PremierProfile(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfile productsPremierProfile)
        {
            if (productsPremierProfile != null)
            {
                if (productsPremierProfile.InputSummary != null)
                    InputSummary =
                        productsPremierProfile.InputSummary.Select(p => new InputSummary(p))
                            .ToList<IInputSummary>();
                if (productsPremierProfile.ExpandedCreditSummary != null)
                    ExpandedCreditSummary =
                        productsPremierProfile.ExpandedCreditSummary.Select(
                            p => new ExpandedCreditSummary(p))
                            .ToList<IExpandedCreditSummary>();
                if (productsPremierProfile.ExpandedBusinessNameAndAddress != null)
                    ExpandedBusinessNameAndAddress =
                        productsPremierProfile.ExpandedBusinessNameAndAddress.Select(
                            p => new ExpandedBusinessNameAndAddress(p))
                            .ToList<IExpandedBusinessNameAndAddress>();
                if (productsPremierProfile.DoingBusinessAs != null)
                    DoingBusinessAs =
                        productsPremierProfile.DoingBusinessAs.Select(p => new DoingBusinessAs(p))
                            .ToList<IDoingBusinessAs>();
                if (productsPremierProfile.ExecutiveSummary != null)
                    ExecutiveSummary =
                        productsPremierProfile.ExecutiveSummary.Select(
                            p => new ExecutiveSummary(p))
                            .ToList<IExecutiveSummary>();
                if (productsPremierProfile.CollectionData != null)
                    CollectionData =
                        productsPremierProfile.CollectionData.Select(p => new CollectionData(p))
                            .ToList<ICollectionData>();
                if (productsPremierProfile.TradePaymentExperiences != null)
                    TradePaymentExperiences =
                        productsPremierProfile.TradePaymentExperiences.Select(
                            p => new TradePaymentExperiences(p))
                            .ToList<ITradePaymentExperiences>();
                if (productsPremierProfile.AdditionalPaymentExperiences != null)
                    AdditionalPaymentExperiences =
                        productsPremierProfile.AdditionalPaymentExperiences.Select(
                            p => new AdditionalPaymentExperiences(p))
                            .ToList<IAdditionalPaymentExperiences>();
                if (productsPremierProfile.PaymentTotals != null)
                    PaymentTotals =
                        productsPremierProfile.PaymentTotals.Select(p => new PaymentTotals(p))
                            .ToList<IPaymentTotals>();
                if (productsPremierProfile.PaymentTrends != null)
                    PaymentTrends =
                        productsPremierProfile.PaymentTrends.Select(p => new PaymentTrends(p))
                            .ToList<IPaymentTrends>();
                if (productsPremierProfile.IndustryPaymentTrends != null)
                    IndustryPaymentTrends =
                        productsPremierProfile.IndustryPaymentTrends.Select(
                            p => new IndustryPaymentTrends(p))
                            .ToList<IIndustryPaymentTrends>();
                if (productsPremierProfile.QuarterlyPaymentTrends != null)
                    QuarterlyPaymentTrends =
                        productsPremierProfile.QuarterlyPaymentTrends.Select(
                            p => new QuarterlyPaymentTrends(p))
                            .ToList<IQuarterlyPaymentTrends>();
                if (productsPremierProfile.TaxLien != null)
                    TaxLien =
                        productsPremierProfile.TaxLien.Select(p => new TaxLien(p))
                            .ToList<ITaxLien>();
                if (productsPremierProfile.JudgmentOrAttachmentLien != null)
                    JudgmentOrAttachmentLien =
                        productsPremierProfile.JudgmentOrAttachmentLien.Select(
                            p => new JudgmentOrAttachmentLien(p))
                            .ToList<IJudgmentOrAttachmentLien>();
                if (productsPremierProfile.UCCFilingsSummaryCounts != null)
                    UccFilingsSummaryCounts =
                        productsPremierProfile.UCCFilingsSummaryCounts.Select(
                            p => new UccFilingsSummaryCounts(p))
                            .ToList<IUccFilingsSummaryCounts>();
                if (productsPremierProfile.UCCFilings != null)
                    UccFilings =
                        productsPremierProfile.UCCFilings.Select(p => new UccFilings(p))
                            .ToList<IUccFilings>();
                ;
                if (productsPremierProfile.CorporateRegistration != null)
                    CorporateRegistration= new CorporateRegistration(productsPremierProfile.CorporateRegistration);

                if (productsPremierProfile.DemographicInformation!=null)
                    DemographicInformation=new DemographicInformation(productsPremierProfile.DemographicInformation);

                if (productsPremierProfile.KeyPersonnelExecutiveInformation != null)
                    KeyPersonnelExecutiveInformation =
                        productsPremierProfile.KeyPersonnelExecutiveInformation.Select(
                            p => new KeyPersonnelExecutiveInformation(p))
                            .ToList<IKeyPersonnelExecutiveInformation>();
                ;
                if (productsPremierProfile.CorporateLinkage != null)
                    CorporateLinkage =
                        productsPremierProfile.CorporateLinkage.Select(
                            p => new CorporateLinkage(p))
                            .ToList<ICorporateLinkage>();
                ;
                if (productsPremierProfile.BusinessFacts != null)
                    BusinessFacts =
                        productsPremierProfile.BusinessFacts.Select(p => new BusinessFacts(p))
                            .ToList<IBusinessFacts>();
                ;
                if (productsPremierProfile.SICCodes != null)
                    SicCodes =
                        productsPremierProfile.SICCodes.Select(p => new SicCodesSic(p))
                            .ToList<ISicCodesSic>();
                ;
                if (productsPremierProfile.NAICSCodes != null)
                    NaicsCodes =
                        productsPremierProfile.NAICSCodes.Select(p => new NaicsCodesNaics(p))
                            .ToList<INaicsCodesNaics>();
                ;
                if (productsPremierProfile.Competitors != null)
                    Competitors =
                        productsPremierProfile.Competitors.Select(p => new Competitors(p))
                            .ToList<ICompetitors>();
                if (productsPremierProfile.ExecutiveElements != null)
                    ExecutiveElements = new ExecutiveElements(productsPremierProfile.ExecutiveElements);
                     
                ;
                if (productsPremierProfile.CommercialFraudShieldSummary != null)
                    CommercialFraudShieldSummary =
                        productsPremierProfile.CommercialFraudShieldSummary.Select(
                            p => new CommercialFraudShieldSummary(p))
                            .ToList<ICommercialFraudShieldSummary>();
                ;
                if (productsPremierProfile.Inquiry != null)
                    Inquiry =
                        productsPremierProfile.Inquiry.Select(p => new ProfileInquiry(p))
                            .ToList<IProfileInquiry>();
                ;
                if (productsPremierProfile.IntelliscoreScoreInformation != null)
                    IntelliscoreScoreInformation =
                        productsPremierProfile.IntelliscoreScoreInformation.Select(
                            p => new IntelliscoreScoreInformation(p))
                            .ToList<IIntelliscoreScoreInformation>();
                ;
                if (productsPremierProfile.ScoreFactors != null)
                    ScoreFactors =
                        productsPremierProfile.ScoreFactors.Select(p => new ScoreFactors(p))
                            .ToList<IScoreFactors>();
                ;
                if (productsPremierProfile.ScoreTrendsCreditLimit != null)
                    ScoreTrendsCreditLimit =
                        productsPremierProfile.ScoreTrendsCreditLimit.Select(
                            p => new ScoreTrendsCreditLimit(p))
                            .ToList<IScoreTrendsCreditLimit>();
                ;
                if (productsPremierProfile.BillingIndicator != null)
                    BillingIndicator =
                        productsPremierProfile.BillingIndicator.Select(
                            p => new BillingIndicator(p))
                            .ToList<IBillingIndicator>();
                ;
                if (productsPremierProfile.ProcessingMessage != null)
                    ProcessingMessage = new ProcessingMessage(productsPremierProfile.ProcessingMessage);
                if (productsPremierProfile.Bankruptcy != null)
                    Bankruptcy =
                        productsPremierProfile.Bankruptcy.Select(p => new Bankruptcy(p))
                            .ToList<IBankruptcy>();
                ;
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IInputSummary, InputSummary>))]
        public List<IInputSummary> InputSummary { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IExpandedCreditSummary, ExpandedCreditSummary>))]
        public List<IExpandedCreditSummary> ExpandedCreditSummary { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IExpandedBusinessNameAndAddress, ExpandedBusinessNameAndAddress>))]
        public List<IExpandedBusinessNameAndAddress> ExpandedBusinessNameAndAddress { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IDoingBusinessAs, DoingBusinessAs>))]
        public List<IDoingBusinessAs> DoingBusinessAs { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IExecutiveSummary, ExecutiveSummary>))]
        public List<IExecutiveSummary> ExecutiveSummary { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICollectionData, CollectionData>))]
        public List<ICollectionData> CollectionData { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IExecutiveElements, ExecutiveElements>))]
        public IExecutiveElements ExecutiveElements { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ITradePaymentExperiences, TradePaymentExperiences>))]
        public List<ITradePaymentExperiences> TradePaymentExperiences { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAdditionalPaymentExperiences, AdditionalPaymentExperiences>))]
        public List<IAdditionalPaymentExperiences> AdditionalPaymentExperiences { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPaymentTotals, PaymentTotals>))]
        public List<IPaymentTotals> PaymentTotals { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPaymentTrends, PaymentTrends>))]
        public List<IPaymentTrends> PaymentTrends { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IIndustryPaymentTrends, IndustryPaymentTrends>))]
        public List<IIndustryPaymentTrends> IndustryPaymentTrends { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IQuarterlyPaymentTrends, QuarterlyPaymentTrends>))]
        public List<IQuarterlyPaymentTrends> QuarterlyPaymentTrends { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITaxLien, TaxLien>))]
        public List<ITaxLien> TaxLien { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IJudgmentOrAttachmentLien, JudgmentOrAttachmentLien>))]
        public List<IJudgmentOrAttachmentLien> JudgmentOrAttachmentLien { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IUccFilingsSummaryCounts, UccFilingsSummaryCounts>))]
        public List<IUccFilingsSummaryCounts> UccFilingsSummaryCounts { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IUccFilings, UccFilings>))]
        public List<IUccFilings> UccFilings { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ICorporateRegistration, CorporateRegistration>))]
        public ICorporateRegistration CorporateRegistration { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDemographicInformation, DemographicInformation>))]
        public IDemographicInformation DemographicInformation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IKeyPersonnelExecutiveInformation, KeyPersonnelExecutiveInformation>))]
        public List<IKeyPersonnelExecutiveInformation> KeyPersonnelExecutiveInformation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICorporateLinkage, CorporateLinkage>))]
        public List<ICorporateLinkage> CorporateLinkage { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBusinessFacts, BusinessFacts>))]
        public List<IBusinessFacts> BusinessFacts { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISicCodesSic, SicCodesSic>))]
        public List<ISicCodesSic> SicCodes { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<INaicsCodesNaics, NaicsCodesNaics>))]
        public List<INaicsCodesNaics> NaicsCodes { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICompetitors, Competitors>))]
        public List<ICompetitors> Competitors { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICommercialFraudShieldSummary, CommercialFraudShieldSummary>))]
        public List<ICommercialFraudShieldSummary> CommercialFraudShieldSummary { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IProfileInquiry, ProfileInquiry>))]
        public List<IProfileInquiry> Inquiry { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IIntelliscoreScoreInformation, IntelliscoreScoreInformation>))]
        public List<IIntelliscoreScoreInformation> IntelliscoreScoreInformation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IScoreFactors, ScoreFactors>))]
        public List<IScoreFactors> ScoreFactors { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IScoreTrendsCreditLimit, ScoreTrendsCreditLimit>))]
        public List<IScoreTrendsCreditLimit> ScoreTrendsCreditLimit { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBillingIndicator, BillingIndicator>))]
        public List<IBillingIndicator> BillingIndicator { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IProcessingMessage, ProcessingMessage>))]
        public IProcessingMessage ProcessingMessage { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankruptcy, Bankruptcy>))]
        public List<IBankruptcy> Bankruptcy { get; set; }
    }
}