namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface INewlyReportedIndicator
    {
        string Code { get; set; }
    }
}