namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 
		public class DemographicInformationProfitLossIndicator:IDemographicInformationProfitLossIndicator {
		    public DemographicInformationProfitLossIndicator()
		    {
		        
		    }

		    public DemographicInformationProfitLossIndicator(Proxy.BusinessReportResponse.DemographicInformationProfitLossIndicator profitLossIndicator)
		    {
		        if (profitLossIndicator!=null)
		        {
		            Code = profitLossIndicator.Code;

		        }
		    }
		public string Code { get; set; }
	}

}