﻿

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    	public interface IDemographicInformationBusinessType
		{
		 string Code { get; set; }
		 string Text { get; set; }
	}
}
