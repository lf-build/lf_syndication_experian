namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class NewlyReportedIndicator : INewlyReportedIndicator
    {
        public NewlyReportedIndicator()
        {
        }

        public NewlyReportedIndicator(Proxy.BusinessReportResponse.NewlyReportedIndicator newlyReportedIndicator)
        {
            if (newlyReportedIndicator != null)
                Code = newlyReportedIndicator.code;
        }

        public string Code { get; set; }
    }
}