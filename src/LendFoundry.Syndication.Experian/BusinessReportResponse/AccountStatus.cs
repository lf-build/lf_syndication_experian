namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class AccountStatus : IAccountStatus
    {
        public AccountStatus()
        {
        }

        public AccountStatus(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileCollectionDataAccountStatus accountStatus)
        {
            if (accountStatus != null)
            {
                Code = accountStatus.code;
                Value = accountStatus.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}