namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class NameAddressVerificationIndicator : INameAddressVerificationIndicator
    {
        public NameAddressVerificationIndicator()
        {
        }

        public NameAddressVerificationIndicator(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryNameAddressVerificationIndicator nameAddressVerificationIndicator)
        {
            if (nameAddressVerificationIndicator != null)
            {
                Code = nameAddressVerificationIndicator.code;
                Value = nameAddressVerificationIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}