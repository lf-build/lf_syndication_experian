using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class TotalsCombinedTradeLines : ITotalsCombinedTradeLines
    {
        public TotalsCombinedTradeLines()
        {
        }

        public TotalsCombinedTradeLines(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfilePaymentTotalsCombinedTradeLines totalsCombinedTradeLines)
        {
            if (totalsCombinedTradeLines != null)
            {
                NumberOfLines = totalsCombinedTradeLines.NumberOfLines;
                Dbt = totalsCombinedTradeLines.DBT;
                CurrentPercentage = totalsCombinedTradeLines.CurrentPercentage;
                Dbt30 = totalsCombinedTradeLines.DBT30;
                Dbt60 = totalsCombinedTradeLines.DBT60;
                Dbt90 = totalsCombinedTradeLines.DBT90;
                Dbt120 = totalsCombinedTradeLines.DBT120;
                if (totalsCombinedTradeLines.TotalHighCreditAmount != null)
                    TotalHighCreditAmount =
                        totalsCombinedTradeLines.TotalHighCreditAmount.Select(
                            p => new TotalHighCreditAmount(p)).ToList<ITotalHighCreditAmount>();
                if (totalsCombinedTradeLines.TotalAccountBalance != null)
                    TotalAccountBalance =
                        totalsCombinedTradeLines.TotalAccountBalance.Select(
                            p => new TotalAccountBalance(p)).ToList<ITotalAccountBalance>();
            }
        }

        public string NumberOfLines { get; set; }

        public string Dbt { get; set; }

        public string CurrentPercentage { get; set; }

        public string Dbt30 { get; set; }

        public string Dbt60 { get; set; }

        public string Dbt90 { get; set; }

        public string Dbt120 { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalHighCreditAmount, TotalHighCreditAmount>))]
        public List<ITotalHighCreditAmount> TotalHighCreditAmount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalAccountBalance, TotalAccountBalance>))]
        public List<ITotalAccountBalance> TotalAccountBalance { get; set; }
    }
}