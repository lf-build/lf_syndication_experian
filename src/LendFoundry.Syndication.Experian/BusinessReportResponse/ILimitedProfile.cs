namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ILimitedProfile
    {
        string Code { get; set; }
    }
}