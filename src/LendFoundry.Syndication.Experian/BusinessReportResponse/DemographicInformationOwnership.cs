﻿

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    	public class DemographicInformationOwnership : IDemographicInformationOwnership
		{
		    public DemographicInformationOwnership()
		    {
		        
		    }

		    public DemographicInformationOwnership(Proxy.BusinessReportResponse.DemographicInformationOwnership informationOwnership)
		    {
		        if (informationOwnership!=null)
		        {
		            Code = informationOwnership.Code;
		        }
		    }
		  public string Code { get; set; }
	    }
}
