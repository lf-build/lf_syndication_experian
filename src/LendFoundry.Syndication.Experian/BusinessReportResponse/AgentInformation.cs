﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class AgentInformation : IAgentInformation
    {
        public AgentInformation()
        {
            
        }

        public AgentInformation(Proxy.BusinessReportResponse.AgentInformation agentInformation)
        {
            if (agentInformation != null)
            {
                Name = agentInformation.Name;
                StreetAddress = agentInformation.StreetAddress;
                City = agentInformation.City;
                State = agentInformation.State;
            }
        }
        public string Name { get; set; }

        public string StreetAddress { get; set; }

        public string City { get; set; }

        public string State { get; set; }
    }
}
