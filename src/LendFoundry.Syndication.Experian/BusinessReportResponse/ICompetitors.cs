namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ICompetitors
    {
        string CompetitorName { get; set; }
    }
}