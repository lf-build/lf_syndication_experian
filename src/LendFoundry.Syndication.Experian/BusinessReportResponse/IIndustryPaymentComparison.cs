namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IIndustryPaymentComparison
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}