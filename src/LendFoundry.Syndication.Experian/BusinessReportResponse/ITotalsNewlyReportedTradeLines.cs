using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ITotalsNewlyReportedTradeLines
    {
        string NumberOfLines { get; set; }
        string Dbt { get; set; }
        string CurrentPercentage { get; set; }
        string Dbt30 { get; set; }
        string Dbt60 { get; set; }
        string Dbt90 { get; set; }
        string Dbt120 { get; set; }
        List<ITotalHighCreditAmount> TotalHighCreditAmount { get; set; }
        List<ITotalAccountBalance> TotalAccountBalance { get; set; }
    }
}