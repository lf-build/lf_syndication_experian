namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface INaicsCodesNaics
    {
        INaicsCodesNaicsValues Naics { get; set; }
    }
}