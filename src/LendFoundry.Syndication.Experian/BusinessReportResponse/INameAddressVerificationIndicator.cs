namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface INameAddressVerificationIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}