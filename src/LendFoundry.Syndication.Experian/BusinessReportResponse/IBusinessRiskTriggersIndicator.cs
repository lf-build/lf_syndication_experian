namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IBusinessRiskTriggersIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}