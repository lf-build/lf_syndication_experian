using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class TotalsAdditionalTradeLines : ITotalsAdditionalTradeLines
    {
        public TotalsAdditionalTradeLines()
        {
        }

        public TotalsAdditionalTradeLines(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfilePaymentTotalsAdditionalTradeLines totalsAdditionalTradeLines)
        {
            if (totalsAdditionalTradeLines != null)
            {
                NumberOfLines = totalsAdditionalTradeLines.NumberOfLines;
                CurrentPercentage = totalsAdditionalTradeLines.CurrentPercentage;
                Dbt30 = totalsAdditionalTradeLines.DBT30;
                Dbt60 = totalsAdditionalTradeLines.DBT60;
                Dbt90 = totalsAdditionalTradeLines.DBT90;
                Dbt120 = totalsAdditionalTradeLines.DBT120;
                if (totalsAdditionalTradeLines.TotalHighCreditAmount != null)
                    TotalHighCreditAmount =
                        totalsAdditionalTradeLines.TotalHighCreditAmount.Select(p => new TotalHighCreditAmount(p))
                            .ToList<ITotalHighCreditAmount>();
                if (totalsAdditionalTradeLines.TotalAccountBalance != null)
                    TotalAccountBalance =
                        totalsAdditionalTradeLines.TotalAccountBalance.Select(p => new TotalAccountBalance(p))
                            .ToList<ITotalAccountBalance>();
            }
        }

        public string NumberOfLines { get; set; }
        public string CurrentPercentage { get; set; }

        public string Dbt30 { get; set; }

        public string Dbt60 { get; set; }

        public string Dbt90 { get; set; }

        public string Dbt120 { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalHighCreditAmount, TotalHighCreditAmount>))]
        public List<ITotalHighCreditAmount> TotalHighCreditAmount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalAccountBalance, TotalAccountBalance>))]
        public List<ITotalAccountBalance> TotalAccountBalance { get; set; }
    }
}