namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class Statement : IStatement
    {
        public Statement()
        {
        }

        public Statement(Proxy.BusinessReportResponse.Statement statement)
        {
            if (statement != null)
            {
                Code = statement.code;
                Value = statement.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}