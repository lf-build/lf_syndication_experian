using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class LowestTotalAccountBalance : ILowestTotalAccountBalance
    {
        public LowestTotalAccountBalance()
        {
        }

        public LowestTotalAccountBalance(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileExecutiveSummaryLowestTotalAccountBalance lowestTotalAccountBalance)
        {
            if (lowestTotalAccountBalance != null)
            {
                Amount = lowestTotalAccountBalance.Amount;
                if (lowestTotalAccountBalance.Modifier != null)
                    Modifier =
                        lowestTotalAccountBalance.Modifier.Select(
                            p => new Modifier(p)).ToList<IModifier>();
            }
        }

        public string Amount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IModifier, Modifier>))]
        public List<IModifier> Modifier { get; set; }
    }
}