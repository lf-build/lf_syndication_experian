using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IUccFilingsSummaryCounts
    {
        string UccFilingsTotal { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISummaryCountsMostRecent6Months, SummaryCountsMostRecent6Months>))]
        List<ISummaryCountsMostRecent6Months> MostRecent6Months { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISummaryCountsPrevious6Months, SummaryCountsPrevious6Months>))]
        List<ISummaryCountsPrevious6Months> Previous6Months { get; set; }
    }
}