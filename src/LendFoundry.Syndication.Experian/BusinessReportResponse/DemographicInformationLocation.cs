﻿

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    	public class DemographicInformationLocation:IDemographicInformationLocation 
		{
		    public DemographicInformationLocation()
		    {
		        
		    }

		    public DemographicInformationLocation(Proxy.BusinessReportResponse.DemographicInformationLocation location)
		    {
		        if (location!=null)
		        {
		            Code = location.Code;
		        }
		    }
		public string Code { get; set; }
	}

}
