namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
  public interface IDemographicInformationSalesIndicator {
		 string Code { get; set; }
	}
	
}