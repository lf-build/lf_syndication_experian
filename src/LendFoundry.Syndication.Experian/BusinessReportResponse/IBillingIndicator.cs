namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IBillingIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}