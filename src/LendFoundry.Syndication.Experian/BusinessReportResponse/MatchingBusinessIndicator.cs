namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class MatchingBusinessIndicator : IMatchingBusinessIndicator
    {
        public MatchingBusinessIndicator()
        {
        }

        public MatchingBusinessIndicator(Proxy.BusinessReportResponse.MatchingBusinessIndicator matchingBusinessIndicator)
        {
            if (matchingBusinessIndicator != null)
            {
                Code = matchingBusinessIndicator.code;
                Value = matchingBusinessIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}