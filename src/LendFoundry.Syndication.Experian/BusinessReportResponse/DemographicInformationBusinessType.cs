﻿

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    	public class DemographicInformationBusinessType:IDemographicInformationBusinessType
		{
		    public DemographicInformationBusinessType()
		    {
		        
		    }

		    public DemographicInformationBusinessType(Proxy.BusinessReportResponse.DemographicInformationBusinessType businessType)
		    {
		        if (businessType!=null)
		        {
		            Code = businessType.Code;
		            Text = businessType.Text;
		        }
		    }
		public string Code { get; set; }
		public string Text { get; set; }
	}
}
