namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class ScoreInfo : IScoreInfo
    {
        public ScoreInfo()
        {
        }

        public ScoreInfo(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationScoreInfo scoreInfo)
        {
            if (scoreInfo != null)

                Score = scoreInfo.Score;
        }

        public string Score { get; set; }
    }
}