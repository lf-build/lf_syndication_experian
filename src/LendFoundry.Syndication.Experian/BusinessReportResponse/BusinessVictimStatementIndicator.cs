namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class BusinessVictimStatementIndicator : IBusinessVictimStatementIndicator
    {
        public BusinessVictimStatementIndicator()
        {
        }

        public BusinessVictimStatementIndicator(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryBusinessVictimStatementIndicator businessVictimStatementIndicator)
        {
            if (businessVictimStatementIndicator != null)
            {
                Code = businessVictimStatementIndicator.code;
                Value = businessVictimStatementIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}