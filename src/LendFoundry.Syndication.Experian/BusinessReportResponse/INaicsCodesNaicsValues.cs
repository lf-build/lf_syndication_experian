namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface INaicsCodesNaicsValues
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}