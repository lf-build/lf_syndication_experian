﻿

using LendFoundry.Foundation.Client;
using NATS.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class DemographicInformation : IDemographicInformation
    {
        public DemographicInformation()
        {
            
        }

        public DemographicInformation(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileDemographicInformation demographicInformation)
        {
            if (demographicInformation!=null)
            {
                PrimarySICCode = demographicInformation.PrimarySICCode;
                SICDescription = demographicInformation.SICDescription;
                SecondarySICCode = demographicInformation.SecondarySICCode;
                SecondarySICDescription = demographicInformation.SecondarySICDescription;
                if (demographicInformation.AdditionalSICCodes !=null)
                {
                 AdditionalSICCodes=new DemographicInformationAdditionalSICCodes(demographicInformation.AdditionalSICCodes);
                }
                if (demographicInformation.YearsInBusinessIndicator !=null)
                {
                    YearsInBusinessIndicator=new DemographicInformationYearsInBusinessIndicator(demographicInformation.YearsInBusinessIndicator);
                }
                YearsInBusinessOrLowRange = demographicInformation.YearsInBusinessOrLowRange;
                HighRangeYears = demographicInformation.HighRangeYears;
                if (demographicInformation.SalesIndicator!=null)
                {
                    SalesIndicator = new DemographicInformationSalesIndicator(demographicInformation.SalesIndicator);
                }
                SalesRevenueOrLowRange = demographicInformation.SalesRevenueOrLowRange;
                HighRangeOfSales = demographicInformation.HighRangeOfSales;
                if (demographicInformation.ProfitLossIndicator!=null)
                {
                    ProfitLossIndicator =new DemographicInformationProfitLossIndicator(demographicInformation.ProfitLossIndicator);
                }
                if (demographicInformation.ProfitLossCode!=null)
                {
                    ProfitLossCode =new DemographicInformationProfitLossCode(demographicInformation.ProfitLossCode);
                }
                ProfitAmountOrLowRange = demographicInformation.ProfitAmountOrLowRange;
                HighRangeOfProfit = demographicInformation.HighRangeOfProfit;
                if (demographicInformation.NetWorthIndicator!=null)
                {
                    NetWorthIndicator =new DemographicInformationNetWorthIndicator(demographicInformation.NetWorthIndicator);
                }
                if (demographicInformation.NetWorthAmountOrLowRange!=null)
                {
                    NetWorthAmountOrLowRange =new DemographicInformationNetWorthAmountOrLowRange(demographicInformation.NetWorthAmountOrLowRange);
                }
                if (demographicInformation.HighRangeOrNetWorth!=null)
                {
                    HighRangeOrNetWorth = new DemographicInformationHighRangeOrNetWorth(demographicInformation.HighRangeOrNetWorth);
                }
                if (demographicInformation.EmployeeIndicator!=null)
                {
                    EmployeeIndicator =new DemographicInformationEmployeeIndicator(demographicInformation.EmployeeIndicator);
                }
                EmployeeSizeOrLowRange = demographicInformation.EmployeeSizeOrLowRange;
                HighEmployeeRange = demographicInformation.HighEmployeeRange;
                InBuildingDate = demographicInformation.InBuildingDate;
                BuildingSize = demographicInformation.BuildingSize;

                if (demographicInformation.BuildingOwnership!=null)
                {
                    BuildingOwnership = new DemographicInformationBuildingOwnership(demographicInformation.BuildingOwnership);
                }
                if (demographicInformation.Ownership!=null)
                {
                    Ownership = new DemographicInformationOwnership(demographicInformation.Ownership);
                }
                if (demographicInformation.Location!=null)
                {
                    Location = new DemographicInformationLocation(demographicInformation.Location);
                }
                if (demographicInformation.BusinessType!=null)
                {
                    BusinessType = new DemographicInformationBusinessType(demographicInformation.BusinessType);
                }
                CustomerCount = demographicInformation.CustomerCount;
                DateFounded = demographicInformation.DateFounded;

            }
        }
        public string PrimarySICCode { get; set; }
        public string SICDescription { get; set; }
        public string SecondarySICCode { get; set; }
        public string SecondarySICDescription { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDemographicInformationAdditionalSICCodes, DemographicInformationAdditionalSICCodes>))]
        public IDemographicInformationAdditionalSICCodes AdditionalSICCodes { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDemographicInformationYearsInBusinessIndicator,DemographicInformationYearsInBusinessIndicator>))]
        public IDemographicInformationYearsInBusinessIndicator YearsInBusinessIndicator { get; set; }
        public string YearsInBusinessOrLowRange { get; set; }
        public string HighRangeYears { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDemographicInformationSalesIndicator,DemographicInformationSalesIndicator>))]
        public IDemographicInformationSalesIndicator SalesIndicator { get; set; }
        public string SalesRevenueOrLowRange { get; set; }
        public string HighRangeOfSales { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDemographicInformationProfitLossIndicator,DemographicInformationProfitLossIndicator>))]
        public IDemographicInformationProfitLossIndicator ProfitLossIndicator { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDemographicInformationProfitLossCode,DemographicInformationProfitLossCode>))]
        public IDemographicInformationProfitLossCode ProfitLossCode { get; set; }
        public string ProfitAmountOrLowRange { get; set; }
        public string HighRangeOfProfit { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDemographicInformationNetWorthIndicator,DemographicInformationNetWorthIndicator>))]

        public IDemographicInformationNetWorthIndicator NetWorthIndicator { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDemographicInformationNetWorthAmountOrLowRange,DemographicInformationNetWorthAmountOrLowRange>))]
        public IDemographicInformationNetWorthAmountOrLowRange NetWorthAmountOrLowRange { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDemographicInformationHighRangeOrNetWorth, DemographicInformationHighRangeOrNetWorth>))]
        public IDemographicInformationHighRangeOrNetWorth HighRangeOrNetWorth { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDemographicInformationEmployeeIndicator,DemographicInformationEmployeeIndicator>))]
        public IDemographicInformationEmployeeIndicator EmployeeIndicator { get; set; }
        public string EmployeeSizeOrLowRange { get; set; }
        public string HighEmployeeRange { get; set; }
        public string InBuildingDate { get; set; }
        public string BuildingSize { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDemographicInformationBuildingOwnership,DemographicInformationBuildingOwnership>))]
        public IDemographicInformationBuildingOwnership BuildingOwnership { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDemographicInformationOwnership,DemographicInformationOwnership>))]
        public IDemographicInformationOwnership Ownership { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDemographicInformationLocation,DemographicInformationLocation>))]
        public IDemographicInformationLocation Location { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDemographicInformationBusinessType, DemographicInformationBusinessType>))]
        public IDemographicInformationBusinessType BusinessType { get; set; }
        public string CustomerCount { get; set; }
        public string DateFounded { get; set; }
    }
}
