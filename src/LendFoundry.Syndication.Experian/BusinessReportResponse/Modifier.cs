﻿namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class Modifier : IModifier
    {
        public Modifier()
        {
        }

        public Modifier(Proxy.BusinessReportResponse.Modifier modifier)
        {
            if (modifier != null)
                Code = modifier.code;
        }

        public string Code { get; set; }
    }
}