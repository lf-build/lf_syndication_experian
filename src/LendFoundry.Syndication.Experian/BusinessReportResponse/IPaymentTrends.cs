using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IPaymentTrends
    {
        List<ICurrentMonth> CurrentMonth { get; set; }
        List<IPriorMonth> PriorMonth { get; set; }
    }
}