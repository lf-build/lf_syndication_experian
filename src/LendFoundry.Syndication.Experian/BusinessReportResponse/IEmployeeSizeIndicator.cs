namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IEmployeeSizeIndicator
    {
        string Code { get; set; }
    }
}