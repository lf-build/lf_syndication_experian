namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 
	public class DemographicInformationEmployeeIndicator:IDemographicInformationEmployeeIndicator {
	    public DemographicInformationEmployeeIndicator()
	    {
	        
	    }

	    public DemographicInformationEmployeeIndicator(Proxy.BusinessReportResponse.DemographicInformationEmployeeIndicator employeeIndicator)
	    {
	        if (employeeIndicator!=null)
	        {
	            Code = employeeIndicator.Code;
	        }
	    }
		public string Code { get; set; }
	}
}