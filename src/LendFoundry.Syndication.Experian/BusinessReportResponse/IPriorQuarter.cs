using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IPriorQuarter
    {
        string Date { get; set; }
        string Dbt { get; set; }
        string CurrentPercentage { get; set; }
        string Dbt30 { get; set; }
        string Dbt60 { get; set; }
        string Dbt90 { get; set; }
        string Dbt120 { get; set; }
        string YearOfQuarter { get; set; }
        string Quarter { get; set; }
        string Score { get; set; }
        List<ITotalAccountBalance> TotalAccountBalance { get; set; }
        List<IQuarterWithinYear> QuarterWithinYear { get; set; }
    }
}