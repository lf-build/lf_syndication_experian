using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class TradePaymentExperiences : ITradePaymentExperiences
    {
        public TradePaymentExperiences()
        {
        }

        public TradePaymentExperiences(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileTradePaymentExperiences tradePaymentExperiences)
        {
            if (tradePaymentExperiences != null)
            {
                BusinessCategory = tradePaymentExperiences.BusinessCategory;
                DateReported = tradePaymentExperiences.DateReported;
                DateLastActivity = tradePaymentExperiences.DateLastActivity;
                Terms = tradePaymentExperiences.Terms;
                CurrentPercentage = tradePaymentExperiences.CurrentPercentage;
                Dbt30 = tradePaymentExperiences.DBT30;
                Dbt60 = tradePaymentExperiences.DBT60;
                Dbt90 = tradePaymentExperiences.DBT90;
                Dbt90Plus = tradePaymentExperiences.DBT90Plus;
                Comments = tradePaymentExperiences.Comments;
                if (tradePaymentExperiences.PaymentIndicator != null)
                    PaymentIndicator =
                        tradePaymentExperiences.PaymentIndicator.Select(
                            p => new PaymentIndicator(p))
                            .ToList<IPaymentIndicator>();
                if (tradePaymentExperiences.RecentHighCredit != null)
                    RecentHighCredit =
                        tradePaymentExperiences.RecentHighCredit.Select(p => new RecentHighCredit(p))
                            .ToList<IRecentHighCredit>();
                if (tradePaymentExperiences.AccountBalance != null)
                    AccountBalance =
                        tradePaymentExperiences.AccountBalance.Select(p => new AccountBalance(p))
                            .ToList<IAccountBalance>();
                if (tradePaymentExperiences.TradeLineFlag != null)
                    TradeLineFlag =
                        tradePaymentExperiences.TradeLineFlag.Select(p => new TradeLineFlag(p)).ToList<ITradeLineFlag>();
                if (tradePaymentExperiences.NewlyReportedIndicator != null)
                    NewlyReportedIndicator =
                        tradePaymentExperiences.NewlyReportedIndicator.Select(p => new NewlyReportedIndicator(p))
                            .ToList<INewlyReportedIndicator>();
            }
        }

        public string BusinessCategory { get; set; }
        public string DateReported { get; set; }
        public string DateLastActivity { get; set; }
        public string Terms { get; set; }
        public string CurrentPercentage { get; set; }

        public string Dbt30 { get; set; }

        public string Dbt60 { get; set; }

        public string Dbt90 { get; set; }

        public string Dbt90Plus { get; set; }

        public string Comments { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPaymentIndicator,PaymentIndicator>))]
        public List<IPaymentIndicator> PaymentIndicator
        {
            get;
            set;
        }

        [JsonConverter(typeof(InterfaceListConverter<IRecentHighCredit,RecentHighCredit>))]
        public List<IRecentHighCredit> RecentHighCredit { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAccountBalance,AccountBalance>))]
        public List<IAccountBalance> AccountBalance { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITradeLineFlag,TradeLineFlag>))]
        public List<ITradeLineFlag> TradeLineFlag { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<INewlyReportedIndicator, NewlyReportedIndicator>))]
        public List<INewlyReportedIndicator> NewlyReportedIndicator { get; set; }
    }
}