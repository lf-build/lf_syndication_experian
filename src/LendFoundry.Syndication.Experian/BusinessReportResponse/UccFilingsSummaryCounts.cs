using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class UccFilingsSummaryCounts : IUccFilingsSummaryCounts
    {
        public UccFilingsSummaryCounts()
        {
        }

        public UccFilingsSummaryCounts(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileUCCFilingsSummaryCounts uccFilingsSummaryCounts)
        {
            if (uccFilingsSummaryCounts != null)
            {
                if (uccFilingsSummaryCounts.MostRecent6Months != null)
                    MostRecent6Months =
                        uccFilingsSummaryCounts.MostRecent6Months.Select(
                            p => new SummaryCountsMostRecent6Months(p))
                            .ToList<ISummaryCountsMostRecent6Months>();
                if (uccFilingsSummaryCounts.Previous6Months != null)
                    Previous6Months =
                        uccFilingsSummaryCounts.Previous6Months.Select(
                            p => new SummaryCountsPrevious6Months(p))
                            .ToList<ISummaryCountsPrevious6Months>();

                UccFilingsTotal = uccFilingsSummaryCounts.UCCFilingsTotal;
            }
        }

        public string UccFilingsTotal { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISummaryCountsMostRecent6Months, SummaryCountsMostRecent6Months>))]
        public List<ISummaryCountsMostRecent6Months> MostRecent6Months { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISummaryCountsPrevious6Months, SummaryCountsPrevious6Months>))]
        public List<ISummaryCountsPrevious6Months> Previous6Months { get; set; }
    }
}