namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class QuarterWithinYear : IQuarterWithinYear
    {
        public QuarterWithinYear()
        {
        }

        public QuarterWithinYear(Proxy.BusinessReportResponse.QuarterWithinYear quarterWithinYear)
        {
            if (quarterWithinYear != null)
            {
                Code = quarterWithinYear.code;
                Value = quarterWithinYear.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}