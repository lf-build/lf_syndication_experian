namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 
		public interface IDemographicInformationProfitLossIndicator {
		 string Code { get; set; }
	}

}