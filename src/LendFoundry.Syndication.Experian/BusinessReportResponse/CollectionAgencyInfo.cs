namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class CollectionAgencyInfo : ICollectionAgencyInfo
    {
        public CollectionAgencyInfo()
        {
        }

        public CollectionAgencyInfo(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileCollectionDataCollectionAgencyInfo collectionAgencyInfo)
        {
            if (collectionAgencyInfo != null)
            {
                AgencyName = collectionAgencyInfo.AgencyName;
                PhoneNumber = collectionAgencyInfo.PhoneNumber;
            }
        }

        public string AgencyName { get; set; }
        public string PhoneNumber { get; set; }
    }
}