﻿namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IModifier
    {
        string Code { get; set; }
    }
}