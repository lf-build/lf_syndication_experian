using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class IntelliscoreScoreInformation : IIntelliscoreScoreInformation
    {
        public IntelliscoreScoreInformation()
        {
        }

        public IntelliscoreScoreInformation(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileIntelliscoreScoreInformation intelliscoreScoreInformation)
        {
            if (intelliscoreScoreInformation != null)
            {
                Filler = intelliscoreScoreInformation.Filler;
                ProfileNumber = intelliscoreScoreInformation.ProfileNumber;
                PercentileRanking = intelliscoreScoreInformation.PercentileRanking;
                Action = intelliscoreScoreInformation.Action;
                RiskClass = intelliscoreScoreInformation.RiskClass;
                if (intelliscoreScoreInformation.PubliclyHeldCompany != null)
                    PubliclyHeldCompany =
                        intelliscoreScoreInformation.PubliclyHeldCompany.Select(
                            p => new PubliclyHeldCompany(p))
                            .ToList<IPubliclyHeldCompany>();
                if (intelliscoreScoreInformation.LimitedProfile != null)
                    LimitedProfile =
                        intelliscoreScoreInformation.LimitedProfile.Select(
                            p => new LimitedProfile(p))
                            .ToList<ILimitedProfile>();

                if (intelliscoreScoreInformation.ScoreInfo != null)
                    ScoreInfo =
                        intelliscoreScoreInformation.ScoreInfo.Select(
                            p => new ScoreInfo(p))
                            .ToList<IScoreInfo>();

                if (intelliscoreScoreInformation.ModelInformation != null)
                    ModelInformation =
                        intelliscoreScoreInformation.ModelInformation.Select(
                            p => new ModelInformation(p))
                            .ToList<IModelInformation>();
            }
        }

        public string Filler { get; set; }
        public string ProfileNumber { get; set; }
        public string PercentileRanking { get; set; }
        public string Action { get; set; }
        public string RiskClass { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPubliclyHeldCompany, PubliclyHeldCompany>))]

        public List<IPubliclyHeldCompany>
            PubliclyHeldCompany
        { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ILimitedProfile, LimitedProfile>))]
        public List<ILimitedProfile> LimitedProfile
        {
            get; set;
        }

        [JsonConverter(typeof(InterfaceListConverter<IScoreInfo, ScoreInfo>))]
        public List<IScoreInfo> ScoreInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IModelInformation, ModelInformation>))]
        public List<IModelInformation> ModelInformation
        {
            get; set;
        }
    }
}