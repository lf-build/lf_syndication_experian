namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IScoreFactor
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}