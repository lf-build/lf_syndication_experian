namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class ScoreFactor : IScoreFactor
    {
        public ScoreFactor()
        {
        }

        public ScoreFactor(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileScoreFactorsScoreFactor scoreFactor)
        {
            if (scoreFactor != null)
            {
                Code = scoreFactor.code;
                Value = scoreFactor.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}