using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ICorporateLinkage
    {
        string LinkageRecordBin { get; set; }
        string LinkageCompanyName { get; set; }
        string LinkageCompanyCity { get; set; }
        string LinkageCompanyState { get; set; }
        string LinkageCountryCode { get; set; }
        string LinkageCompanyAddress { get; set; }

        List<ILinkageRecordType> LinkageRecordType { get; set; }

        List<ICorporateLinkageReturnLimitExceeded> ReturnLimitExceeded { get; set; }

        List<IMatchingBusinessIndicator> MatchingBusinessIndicator { get; set; }
    }
}