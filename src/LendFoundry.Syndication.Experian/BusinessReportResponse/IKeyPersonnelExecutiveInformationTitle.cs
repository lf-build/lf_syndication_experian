namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IKeyPersonnelExecutiveInformationTitle
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}