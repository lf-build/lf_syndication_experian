namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IOfacMatchCode
    {
        string Code { get; set; }
    }
}