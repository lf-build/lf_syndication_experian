using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IPaymentTotals
    {
        List<ITotalsNewlyReportedTradeLines> NewlyReportedTradeLines { get; set; }

        List<ITotalsContinouslyReportedTradeLines> ContinouslyReportedTradeLines { get; set; }

        List<ITotalsCombinedTradeLines> CombinedTradeLines { get; set; }
        List<ITotalsAdditionalTradeLines> AdditionalTradeLines { get; set; }
        List<ITotalsTradeLines> TradeLines { get; set; }
    }
}