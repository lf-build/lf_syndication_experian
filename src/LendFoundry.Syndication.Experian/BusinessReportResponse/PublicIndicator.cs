namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class PublicIndicator : IPublicIndicator
    {
        public PublicIndicator()
        {
        }

        public PublicIndicator(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileBusinessFactsPublicIndicator publicIndicator)
        {
            if (publicIndicator != null)

                Code = publicIndicator.code;
        }

        public string Code { get; set; }
    }
}