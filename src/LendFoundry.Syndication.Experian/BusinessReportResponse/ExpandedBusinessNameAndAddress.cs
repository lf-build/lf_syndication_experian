using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class ExpandedBusinessNameAndAddress : IExpandedBusinessNameAndAddress
    {
        public ExpandedBusinessNameAndAddress()
        {
        }

        public ExpandedBusinessNameAndAddress(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileExpandedBusinessNameAndAddress expandedBusinessNameAndAddress)
        {
            if (expandedBusinessNameAndAddress != null)
            {
                ExperianBin = expandedBusinessNameAndAddress.ExperianBIN;
                ProfileDate = expandedBusinessNameAndAddress.ProfileDate;
                ProfileTime = expandedBusinessNameAndAddress.ProfileTime;
                TerminalNumber = expandedBusinessNameAndAddress.TerminalNumber;
                BusinessName = expandedBusinessNameAndAddress.BusinessName;
                StreetAddress = expandedBusinessNameAndAddress.StreetAddress;
                City = expandedBusinessNameAndAddress.City;
                State = expandedBusinessNameAndAddress.State;
                Zip = expandedBusinessNameAndAddress.Zip;
                ZipExtension = expandedBusinessNameAndAddress.ZipExtension;
                PhoneNumber = expandedBusinessNameAndAddress.PhoneNumber;
                TaxId = expandedBusinessNameAndAddress.TaxID;
                WebsiteUrl = expandedBusinessNameAndAddress.WebsiteURL;
                if (expandedBusinessNameAndAddress.ProfileType != null)
                    ProfileType = expandedBusinessNameAndAddress.ProfileType.Select(p => new ProfileType(p.code)).ToList<IProfileType>();
                if (expandedBusinessNameAndAddress.MatchingBranchAddress != null)
                    MatchingBranchAddress = expandedBusinessNameAndAddress.MatchingBranchAddress.Select(p => new MatchingBranchAddress(p)).ToList<IMatchingBranchAddress>();
                if (expandedBusinessNameAndAddress.CorporateLink != null)
                    CorporateLink = expandedBusinessNameAndAddress.CorporateLink.Select(p => new CorporateLink(p.CorporateLinkageIndicator)).ToList<ICorporateLink>();
            }
        }

        public string ExperianBin { get; set; }

        public string ProfileDate { get; set; }
        public string ProfileTime { get; set; }
        public string TerminalNumber { get; set; }
        public string BusinessName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ZipExtension { get; set; }
        public string PhoneNumber { get; set; }

        public string TaxId { get; set; }

        public string WebsiteUrl { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IProfileType, ProfileType>))]
        public List<IProfileType> ProfileType { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IMatchingBranchAddress,MatchingBranchAddress>))]
        public List<IMatchingBranchAddress> MatchingBranchAddress { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICorporateLink,CorporateLink>))]
        public List<ICorporateLink> CorporateLink
        {
            get; set;
        }
    }
}