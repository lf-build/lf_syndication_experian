using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class ScoreFactors : IScoreFactors
    {
        public ScoreFactors()
        {
        }

        public ScoreFactors(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileScoreFactors scoreFactors)
        {
            if (scoreFactors != null)
            {
                ModelCode = scoreFactors.ModelCode;
                if (scoreFactors.ScoreFactor != null)
                    ScoreFactor =
                        scoreFactors.ScoreFactor.Select(p => new ScoreFactor(p))
                            .ToList<IScoreFactor>();
            }
        }

        public string ModelCode { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IScoreFactor, ScoreFactor>))]
        public List<IScoreFactor> ScoreFactor { get; set; }
    }
}