﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class ExecutiveElements : IExecutiveElements
    {
        public ExecutiveElements()
        {
        }

        public ExecutiveElements(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileExecutiveElements executiveElements)
        {
            if (executiveElements != null)
            {
                BankruptcyCount = executiveElements.BankruptcyCount;
                TaxLienCount = executiveElements.TaxLienCount;
                EarliestTaxLienDate = executiveElements.EarliestTaxLienDate;
                MostRecentTaxLienDate = executiveElements.MostRecentTaxLienDate;
                JudgmentCount = executiveElements.JudgmentCount;
                EarliestJudgmentDate = executiveElements.EarliestJudgmentDate;
                MostRecentJudgmentDate = executiveElements.MostRecentJudgmentDate;
                CollectionCount = executiveElements.CollectionCount;
                LegalBalance = executiveElements.LegalBalance;
                UCCFilings = executiveElements.UCCFilings;
                UCCDerogatoryCount = executiveElements.UCCDerogatoryCount;
                CurrentAccountBalance = executiveElements.CurrentAccountBalance;
                CurrentTradelineCount = executiveElements.CurrentTradelineCount;
                MonthlyAverageDBT = executiveElements.MonthlyAverageDBT;
                HighestDBT6Months = executiveElements.HighestDBT6Months;
                HighestDBT5Quarters = executiveElements.HighestDBT5Quarters;
                ActiveTradelineCount = executiveElements.ActiveTradelineCount;
                AllTradelineBalance = executiveElements.AllTradelineBalance;
                AllTradelineCount = executiveElements.AllTradelineCount;
                AverageBalance5Quarters = executiveElements.AverageBalance5Quarters;
                SingleLineHighCredit = executiveElements.SingleLineHighCredit;
                LowBalance6Months = executiveElements.LowBalance6Months;
                HighBalance6Months = executiveElements.HighBalance6Months;
                EarliestCollectionDate = executiveElements.EarliestCollectionDate;
                MostRecentCollectionDate = executiveElements.MostRecentCollectionDate;
                CurrentDBT = executiveElements.CurrentDBT;
                YearofIncorporation = executiveElements.YearofIncorporation;
                TaxID = executiveElements.TaxID;
                JudgmentFlag = executiveElements.JudgmentFlag;
                TaxLienFlag = executiveElements.TaxLienFlag;

            }
        }

        public string BankruptcyCount { get; set; }
        public string TaxLienCount { get; set; }
        public string EarliestTaxLienDate { get; set; }
        public string MostRecentTaxLienDate { get; set; }
        public string JudgmentCount { get; set; }
        public string EarliestJudgmentDate { get; set; }
        public string MostRecentJudgmentDate { get; set; }
        public string CollectionCount { get; set; }
        public string LegalBalance { get; set; }
        public string UCCFilings { get; set; }
        public string UCCDerogatoryCount { get; set; }
        public string CurrentAccountBalance { get; set; }
        public string CurrentTradelineCount { get; set; }
        public string MonthlyAverageDBT { get; set; }
        public string HighestDBT6Months { get; set; }
        public string HighestDBT5Quarters { get; set; }
        public string ActiveTradelineCount { get; set; }
        public string AllTradelineBalance { get; set; }
        public string AllTradelineCount { get; set; }
        public string AverageBalance5Quarters { get; set; }
        public string SingleLineHighCredit { get; set; }
        public string LowBalance6Months { get; set; }
        public string HighBalance6Months { get; set; }
        public string EarliestCollectionDate { get; set; }
        public string MostRecentCollectionDate { get; set; }
        public string CurrentDBT { get; set; }
        public string YearofIncorporation { get; set; }
        public string TaxID { get; set; }
        public string JudgmentFlag { get; set; }
        public string TaxLienFlag { get; set; }

    }
}