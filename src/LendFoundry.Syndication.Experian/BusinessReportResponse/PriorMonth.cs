using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class PriorMonth : IPriorMonth
    {
        public PriorMonth()
        {
        }

        public PriorMonth(Proxy.BusinessReportResponse.PriorMonth priorMonth)
        {
            if (priorMonth != null)
            {
                Date = priorMonth.Date;
                Dbt = priorMonth.DBT;
                CurrentPercentage = priorMonth.CurrentPercentage;
                Dbt30 = priorMonth.DBT30;
                Dbt60 = priorMonth.DBT60;
                Dbt90 = priorMonth.DBT90;
                Dbt120 = priorMonth.DBT120;
                if (priorMonth.TotalAccountBalance != null)
                    TotalAccountBalance =
                        priorMonth.TotalAccountBalance.Select(p => new TotalAccountBalance(p))
                            .ToList<ITotalAccountBalance>();
            }
        }

        public string Date { get; set; }

        public string Dbt { get; set; }

        public string CurrentPercentage { get; set; }

        public string Dbt30 { get; set; }

        public string Dbt60 { get; set; }

        public string Dbt90 { get; set; }

        public string Dbt120 { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalAccountBalance, TotalAccountBalance>))]
        public List<ITotalAccountBalance> TotalAccountBalance { get; set; }
    }
}