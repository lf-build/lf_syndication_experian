namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class CorporateLinkageReturnLimitExceeded : ICorporateLinkageReturnLimitExceeded
    {
        public CorporateLinkageReturnLimitExceeded()
        {
        }

        public CorporateLinkageReturnLimitExceeded(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileCorporateLinkageReturnLimitExceeded corporateLinkageReturnLimitExceeded)
        {
            if (corporateLinkageReturnLimitExceeded != null)
            {
                Code = corporateLinkageReturnLimitExceeded.code;
            }
        }

        public string Code { get; set; }
    }
}