using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 
	public class DemographicInformationHighRangeOrNetWorth:IDemographicInformationHighRangeOrNetWorth {

	    public DemographicInformationHighRangeOrNetWorth()
	    {
	        
	    }

	    public DemographicInformationHighRangeOrNetWorth(Proxy.BusinessReportResponse.DemographicInformationHighRangeOrNetWorth highRangeOrNetWorth)
	    {
	        if (highRangeOrNetWorth!=null)
	        {
	            Modifier=new DemographicInformationModifier(highRangeOrNetWorth.Modifier);
	            Amount = highRangeOrNetWorth.Amount;
	        }
	    }
		[JsonConverter(typeof(InterfaceConverter<IDemographicInformationModifier, DemographicInformationModifier>))]
		public IDemographicInformationModifier Modifier { get; set; }
		public string Amount { get; set; }
	}
}

