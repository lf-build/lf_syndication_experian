namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class EmployeeSizeIndicator : IEmployeeSizeIndicator
    {
        public EmployeeSizeIndicator()
        {
        }

        public EmployeeSizeIndicator(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileBusinessFactsEmployeeSizeIndicator employeeSizeIndicator)
        {
            if (employeeSizeIndicator != null)

                Code = employeeSizeIndicator.code;
        }

        public string Code { get; set; }
    }
}