namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ICorporateLink
    {
        string CorporateLinkageIndicator { get; set; }
    }
}