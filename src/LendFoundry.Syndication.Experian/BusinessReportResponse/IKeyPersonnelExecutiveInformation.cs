using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IKeyPersonnelExecutiveInformation
    {
        string Name { get; set; }

        List<IKeyPersonnelExecutiveInformationNameFlag> NameFlag { get; set; }

        List<IKeyPersonnelExecutiveInformationTitle> Title { get; set; }
    }
}