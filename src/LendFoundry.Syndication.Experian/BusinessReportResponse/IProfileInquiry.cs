using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IProfileInquiry
    {
        string InquiryBusinessCategory { get; set; }
        List<IInquiryCount> InquiryCount { get; set; }
    }
}