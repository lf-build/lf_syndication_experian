﻿

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    	public interface  IDemographicInformationBuildingOwnership
		{
		 string Code { get; set; }
	}
}
