namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class ProcessingMessage : IProcessingMessage
    {
        public ProcessingMessage()
        {
        }

        public ProcessingMessage(Proxy.BusinessReportResponse.ProcessingMessage processingMessage)
        {
            if (processingMessage != null)
                ProcessingAction = processingMessage.ProcessingAction;
        }

        public string ProcessingAction { get; set; }
    }
}