namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IAccountStatus
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}