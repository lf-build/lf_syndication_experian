using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IIndustryPaymentTrends
    {
        string Sic { get; set; }
        List<ICurrentMonth> CurrentMonth { get; set; }
        List<IPriorMonth> PriorMonth { get; set; }
    }
}