namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
  public class DemographicInformationSalesIndicator:IDemographicInformationSalesIndicator {
      public DemographicInformationSalesIndicator()
      {
          
      }

      public DemographicInformationSalesIndicator(Proxy.BusinessReportResponse.DemographicInformationSalesIndicator salesIndicator)
      {
          if (salesIndicator!=null)
          {
              Code = salesIndicator.Code;
          }
      }
		public string Code { get; set; }
	}
	
}