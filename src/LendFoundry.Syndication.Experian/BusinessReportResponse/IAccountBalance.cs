﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IAccountBalance
    {
        string Amount { get; set; }
        List<IModifier> Modifier { get; set; }
    }
}