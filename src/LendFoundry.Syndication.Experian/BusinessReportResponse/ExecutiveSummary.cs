using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class ExecutiveSummary : IExecutiveSummary
    {
        public ExecutiveSummary()
        {
        }

        public ExecutiveSummary(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileExecutiveSummary executiveSummary)
        {
            if (executiveSummary != null)
            {
                PredictedDbt = executiveSummary.PredictedDBT;
                PredictedDbtDate = executiveSummary.PredictedDBTDate;
                IndustryDbt = executiveSummary.IndustryDBT;
                AllIndustryDbt = executiveSummary.AllIndustryDBT;
                HighCreditAmountExtended = executiveSummary.HighCreditAmountExtended;
                MedianCreditAmountExtended = executiveSummary.MedianCreditAmountExtended;
                IndustryDescription = executiveSummary.IndustryDescription;
                CommonTerms = executiveSummary.CommonTerms;
                CommonTerms2 = executiveSummary.CommonTerms2;
                CommonTerms3 = executiveSummary.CommonTerms3;
                if (executiveSummary.BusinessDBT != null)
                    BusinessDbt =
                        executiveSummary.BusinessDBT.Select(
                            p => new BusinessDbt(p.code, p.Value))
                            .ToList<IBusinessDbt>();
                if (executiveSummary.LowestTotalAccountBalance != null)
                    LowestTotalAccountBalance =
                        executiveSummary.LowestTotalAccountBalance.Select(
                            p => new LowestTotalAccountBalance(p))
                            .ToList<ILowestTotalAccountBalance>();
                if (executiveSummary.HighCreditAmountExtended != null)
                    HighestTotalAccountBalance =
                        executiveSummary.HighestTotalAccountBalance.Select(
                            p => new HighestTotalAccountBalance(p))
                            .ToList<IHighestTotalAccountBalance>();
                if (executiveSummary.CurrentTotalAccountBalance != null)
                    CurrentTotalAccountBalance =
                        executiveSummary.CurrentTotalAccountBalance.Select(
                            p => new CurrentTotalAccountBalance(p))
                            .ToList<ICurrentTotalAccountBalance>();
                if (executiveSummary.IndustryDBT != null)
                    IndustryPaymentComparison =
                        executiveSummary.IndustryPaymentComparison.Select(
                            p => new IndustryPaymentComparison(p))
                            .ToList<IIndustryPaymentComparison>();
                if (executiveSummary.PaymentTrendIndicator != null)
                    PaymentTrendIndicator =
                        executiveSummary.PaymentTrendIndicator.Select(
                            p => new PaymentTrendIndicator(p))
                            .ToList<IPaymentTrendIndicator>();
            }
        }

        public string PredictedDbt { get; set; }

        public string PredictedDbtDate { get; set; }

        public string IndustryDbt { get; set; }

        public string AllIndustryDbt { get; set; }

        public string HighCreditAmountExtended { get; set; }
        public string MedianCreditAmountExtended { get; set; }
        public string IndustryDescription { get; set; }
        public string CommonTerms { get; set; }
        public string CommonTerms2 { get; set; }
        public string CommonTerms3 { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBusinessDbt,BusinessDbt>))]
        public List<IBusinessDbt> BusinessDbt { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ILowestTotalAccountBalance, LowestTotalAccountBalance>))]
        public List<ILowestTotalAccountBalance> LowestTotalAccountBalance
        { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IHighestTotalAccountBalance, HighestTotalAccountBalance>))]
        public List<IHighestTotalAccountBalance> HighestTotalAccountBalance
        { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICurrentTotalAccountBalance, CurrentTotalAccountBalance>))]
        public List<ICurrentTotalAccountBalance> CurrentTotalAccountBalance
        { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IIndustryPaymentComparison, IndustryPaymentComparison>))]
        public List<IIndustryPaymentComparison> IndustryPaymentComparison
        { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPaymentTrendIndicator,PaymentTrendIndicator>))]
        public List<IPaymentTrendIndicator> PaymentTrendIndicator
        {
            get; set;
        }
    }
}