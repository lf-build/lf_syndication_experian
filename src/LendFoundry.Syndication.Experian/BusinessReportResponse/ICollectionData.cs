using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ICollectionData
    {
        string DatePlacedForCollection { get; set; }
        string DateClosed { get; set; }
        string AmountPlacedForCollection { get; set; }
        string AmountPaid { get; set; }
        List<IAccountStatus> AccountStatus { get; set; }
        List<ICollectionAgencyInfo> CollectionAgencyInfo { get; set; }
    }
}