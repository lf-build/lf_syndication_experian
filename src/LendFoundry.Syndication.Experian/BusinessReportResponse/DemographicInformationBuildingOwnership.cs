﻿

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    	public class DemographicInformationBuildingOwnership : IDemographicInformationBuildingOwnership
		{
		    public DemographicInformationBuildingOwnership()
		    {
		        
		    }

		    public DemographicInformationBuildingOwnership(Proxy.BusinessReportResponse.DemographicInformationBuildingOwnership buildingOwnership)
		    {
		        if (buildingOwnership!=null)
		        {
		            Code = buildingOwnership.Code;

		        }
		    }
		public string Code { get; set; }
	}
}
