namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IProcessingMessage
    {
        string ProcessingAction { get; set; }
    }
}