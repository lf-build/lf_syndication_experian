namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface INonProfitIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}