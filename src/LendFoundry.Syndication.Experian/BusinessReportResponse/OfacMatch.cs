namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class OfacMatch : IOfacMatch
    {
        public OfacMatch()
        {
        }

        public OfacMatch(string code)
        {
            if (!string.IsNullOrWhiteSpace(code))
                Code = code;
        }

        public string Code { get; set; }
    }
}