﻿using LendFoundry.Syndication.Experian.Proxy.BusinessReportResponse;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class CorporateRegistration: ICorporateRegistration
    {

        public CorporateRegistration()
        {
            

        }

        public CorporateRegistration(NetConnectResponseProductsPremierProfileCorporateRegistration corporateInformation)
        {
            if (corporateInformation!=null)
            {
                StateOfOrigin = corporateInformation.StateOfOrigin;
                OriginalFilingDate = corporateInformation.OriginalFilingDate;
                RecentFilingDate = corporateInformation.RecentFilingDate;
                IncorporatedDate = corporateInformation.IncorporatedDate;
                BusinessType = new BusinessType(corporateInformation.BusinessType);
                StatusFlag = new StatusFlag(corporateInformation.StatusFlag);
                StatusDescription = corporateInformation.StatusDescription;
                ProfitFlag = new ProfitFlag(corporateInformation.ProfitFlag);
                CharterNumber = corporateInformation.CharterNumber;
                AgentInformation = new AgentInformation(corporateInformation.AgentInformation);
            }

        }
        public string StateOfOrigin { get; set; }

        public string OriginalFilingDate { get; set; }

        public string RecentFilingDate { get; set; }

        public string IncorporatedDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IBusinessType, BusinessType>))]
        public IBusinessType BusinessType { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IStatusFlag, StatusFlag>))]
        public IStatusFlag StatusFlag { get; set; }

        public string StatusDescription { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IProfitFlag, ProfitFlag>))]
        public IProfitFlag ProfitFlag { get; set; }

        public string CharterNumber { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAgentInformation, AgentInformation>))]
        public IAgentInformation AgentInformation { get; set; }
    }
}
