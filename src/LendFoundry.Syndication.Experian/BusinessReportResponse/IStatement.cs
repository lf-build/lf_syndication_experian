namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IStatement
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}