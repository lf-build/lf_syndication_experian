namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class SicCodesSicValues : ISicCodesSicValues
    {
        public SicCodesSicValues()
        {
        }

        public SicCodesSicValues(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileSICCodesSICValues sicCodesSicValues)
        {
            if (sicCodesSicValues != null)
            {
                Code = sicCodesSicValues.code;
                Value = sicCodesSicValues.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}