namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 
	public interface IDemographicInformationNetWorthIndicator {
		 string Code { get; set; }
	}
}