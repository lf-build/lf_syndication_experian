namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IBusinessFactsFileEstablishedFlag
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}