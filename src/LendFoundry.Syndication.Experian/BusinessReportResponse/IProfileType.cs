namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IProfileType
    {
        string Code { get; set; }
    }
}