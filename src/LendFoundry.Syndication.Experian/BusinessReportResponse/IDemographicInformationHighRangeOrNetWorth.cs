
namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 
	public interface IDemographicInformationHighRangeOrNetWorth {
		
		
		 IDemographicInformationModifier Modifier { get; set; }
		 string Amount { get; set; }
	}
}

