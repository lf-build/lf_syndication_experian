namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class IndustryPaymentComparison : IIndustryPaymentComparison
    {
        public IndustryPaymentComparison()
        {
        }

        public IndustryPaymentComparison(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileExecutiveSummaryIndustryPaymentComparison industryPaymentComparison)
        {
            if (industryPaymentComparison != null)
            {
                Code = industryPaymentComparison.code;
                Value = industryPaymentComparison.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}