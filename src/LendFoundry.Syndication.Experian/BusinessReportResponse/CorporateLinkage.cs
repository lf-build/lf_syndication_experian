using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class CorporateLinkage : ICorporateLinkage
    {
        public CorporateLinkage()
        {
        }

        public CorporateLinkage(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileCorporateLinkage corporateLinkage)
        {
            if (corporateLinkage != null)
            {
                LinkageCompanyAddress = corporateLinkage.LinkageCompanyAddress;
                LinkageCompanyCity = corporateLinkage.LinkageCompanyCity;
                LinkageCompanyName = corporateLinkage.LinkageCompanyName;
                LinkageCompanyState = corporateLinkage.LinkageCompanyState;
                LinkageCountryCode = corporateLinkage.LinkageCountryCode;
                LinkageRecordBin = corporateLinkage.LinkageRecordBIN;
                if (corporateLinkage.LinkageRecordType != null)
                    LinkageRecordType =
                        corporateLinkage.LinkageRecordType.Select(
                            p => new LinkageRecordType(p))
                            .ToList<ILinkageRecordType>();
                if (corporateLinkage.MatchingBusinessIndicator != null)
                    MatchingBusinessIndicator =
                        corporateLinkage.MatchingBusinessIndicator.Select(p => new MatchingBusinessIndicator(p))
                            .ToList<IMatchingBusinessIndicator>();

                if (corporateLinkage.ReturnLimitExceeded != null)
                    ReturnLimitExceeded =
                        corporateLinkage.ReturnLimitExceeded.Select(
                            p => new CorporateLinkageReturnLimitExceeded(p))
                            .ToList<ICorporateLinkageReturnLimitExceeded>();
            }
        }

        public string LinkageRecordBin { get; set; }

        public string LinkageCompanyName { get; set; }
        public string LinkageCompanyCity { get; set; }
        public string LinkageCompanyState { get; set; }
        public string LinkageCountryCode { get; set; }
        public string LinkageCompanyAddress { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ILinkageRecordType, LinkageRecordType>))]
        public List<ILinkageRecordType> LinkageRecordType { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICorporateLinkageReturnLimitExceeded, CorporateLinkageReturnLimitExceeded>))]
        public List<ICorporateLinkageReturnLimitExceeded> ReturnLimitExceeded { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IMatchingBusinessIndicator, MatchingBusinessIndicator>))]
        public List<IMatchingBusinessIndicator> MatchingBusinessIndicator { get; set; }
    }
}