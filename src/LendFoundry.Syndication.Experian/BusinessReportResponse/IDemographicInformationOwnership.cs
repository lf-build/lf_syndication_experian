﻿

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    	public interface IDemographicInformationOwnership
    {
		 string Code { get; set; }
	}

}
