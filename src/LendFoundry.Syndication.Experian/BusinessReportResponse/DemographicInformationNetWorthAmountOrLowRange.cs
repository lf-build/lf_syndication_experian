using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 public class DemographicInformationNetWorthAmountOrLowRange :IDemographicInformationNetWorthAmountOrLowRange {

     public DemographicInformationNetWorthAmountOrLowRange()
     {
         
     }

     public DemographicInformationNetWorthAmountOrLowRange(Proxy.BusinessReportResponse.DemographicInformationNetWorthAmountOrLowRange netWorthAmountOrLowRange)
     {
         if (netWorthAmountOrLowRange!=null)
         {
             if (netWorthAmountOrLowRange.Modifier!=null)
             {
                 Modifier=new DemographicInformationModifier(netWorthAmountOrLowRange.Modifier);
             }
             Amount = netWorthAmountOrLowRange.Amount;
         }
     }
		[JsonConverter(typeof(InterfaceConverter<IDemographicInformationModifier, DemographicInformationModifier>))]
		public IDemographicInformationModifier Modifier { get; set; }
		public string Amount { get; set; }
	}
	
}