namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IYearsInBusinessIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}