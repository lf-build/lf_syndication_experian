namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ILegalAction
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}