﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IDemographicInformation
    {
         string PrimarySICCode { get; set; }
         string SICDescription { get; set; }
         string SecondarySICCode { get; set; }
         string SecondarySICDescription { get; set; }
         IDemographicInformationAdditionalSICCodes AdditionalSICCodes { get; set; }
         IDemographicInformationYearsInBusinessIndicator YearsInBusinessIndicator { get; set; }
         string YearsInBusinessOrLowRange { get; set; }
         string HighRangeYears { get; set; }
         IDemographicInformationSalesIndicator SalesIndicator { get; set; }
         string SalesRevenueOrLowRange { get; set; }
         string HighRangeOfSales { get; set; }
         IDemographicInformationProfitLossIndicator ProfitLossIndicator { get; set; }
         IDemographicInformationProfitLossCode ProfitLossCode { get; set; }
         string ProfitAmountOrLowRange { get; set; }
         string HighRangeOfProfit { get; set; }
         IDemographicInformationNetWorthIndicator NetWorthIndicator { get; set; }
         IDemographicInformationNetWorthAmountOrLowRange NetWorthAmountOrLowRange { get; set; }
         IDemographicInformationHighRangeOrNetWorth HighRangeOrNetWorth { get; set; }
         IDemographicInformationEmployeeIndicator EmployeeIndicator { get; set; }
         string EmployeeSizeOrLowRange { get; set; }
         string HighEmployeeRange { get; set; }
         string InBuildingDate { get; set; }
         string BuildingSize { get; set; }
         IDemographicInformationBuildingOwnership BuildingOwnership { get; set; }
         IDemographicInformationOwnership Ownership { get; set; }
         IDemographicInformationLocation Location { get; set; }
         IDemographicInformationBusinessType BusinessType { get; set; }
         string CustomerCount { get; set; }
         string DateFounded { get; set; }
    }
}
