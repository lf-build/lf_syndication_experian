namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class LimitedProfile : ILimitedProfile
    {
        public LimitedProfile()
        {
        }

        public LimitedProfile(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileIntelliscoreScoreInformationLimitedProfile limitedProfile)
        {
            if (limitedProfile != null)

                Code = limitedProfile.code;
        }

        public string Code { get; set; }
    }
}