namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class TradeLineFlag : ITradeLineFlag
    {
        public TradeLineFlag()
        {
        }

        public TradeLineFlag(Proxy.BusinessReportResponse.TradeLineFlag tradeLineFlag)
        {
            if (tradeLineFlag != null)
                Code = tradeLineFlag.code;
        }

        public string Code { get; set; }
    }
}