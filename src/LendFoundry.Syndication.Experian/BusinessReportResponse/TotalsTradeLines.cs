using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class TotalsTradeLines : ITotalsTradeLines
    {
        public TotalsTradeLines()
        {
        }

        public TotalsTradeLines(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfilePaymentTotalsTradeLines totalsTradeLines)
        {
            if (totalsTradeLines != null)
            {
                NumberOfLines = totalsTradeLines.NumberOfLines;
                CurrentPercentage = totalsTradeLines.CurrentPercentage;
                Dbt30 = totalsTradeLines.DBT30;
                Dbt60 = totalsTradeLines.DBT60;
                Dbt90 = totalsTradeLines.DBT90;
                Dbt120 = totalsTradeLines.DBT120;
                if (totalsTradeLines.TotalHighCreditAmount != null)
                    TotalHighCreditAmount =
                        totalsTradeLines.TotalHighCreditAmount.Select(p => new TotalHighCreditAmount(p))
                            .ToList<ITotalHighCreditAmount>();
                if (totalsTradeLines.TotalAccountBalance != null)
                    TotalAccountBalance =
                        totalsTradeLines.TotalAccountBalance.Select(p => new TotalAccountBalance(p))
                            .ToList<ITotalAccountBalance>();
            }
        }

        public string NumberOfLines { get; set; }
        public string CurrentPercentage { get; set; }

        public string Dbt30 { get; set; }

        public string Dbt60 { get; set; }

        public string Dbt90 { get; set; }

        public string Dbt120 { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalHighCreditAmount, TotalHighCreditAmount>))]
        public List<ITotalHighCreditAmount> TotalHighCreditAmount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ITotalAccountBalance, TotalAccountBalance>))]
        public List<ITotalAccountBalance> TotalAccountBalance { get; set; }
    }
}