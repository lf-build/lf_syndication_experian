namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class Competitors : ICompetitors
    {
        public Competitors()
        {
        }

        public Competitors(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileCompetitors competitors)
        {
            if (competitors != null)
                CompetitorName = competitors.CompetitorName;
        }

        public string CompetitorName { get; set; }
    }
}