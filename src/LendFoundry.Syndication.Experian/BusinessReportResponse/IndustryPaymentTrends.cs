using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class IndustryPaymentTrends : IIndustryPaymentTrends
    {
        public IndustryPaymentTrends()
        {
        }

        public IndustryPaymentTrends(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileIndustryPaymentTrends industryPaymentTrends)
        {
            if (industryPaymentTrends != null)
            {
                Sic = industryPaymentTrends.SIC;
                if (industryPaymentTrends.CurrentMonth != null)
                    CurrentMonth =
                        industryPaymentTrends.CurrentMonth.Select(p => new CurrentMonth(p))
                            .ToList<ICurrentMonth>();
                if (industryPaymentTrends.PriorMonth != null)
                    PriorMonth =
                        industryPaymentTrends.PriorMonth.Select(p => new PriorMonth(p))
                            .ToList<IPriorMonth>();
            }
        }

        public string Sic { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICurrentMonth, CurrentMonth>))]
        public List<ICurrentMonth> CurrentMonth { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPriorMonth, PriorMonth>))]
        public List<IPriorMonth> PriorMonth { get; set; }
    }
}