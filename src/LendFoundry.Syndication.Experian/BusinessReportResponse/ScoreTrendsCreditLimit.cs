using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class ScoreTrendsCreditLimit : IScoreTrendsCreditLimit
    {
        public ScoreTrendsCreditLimit()
        {
        }

        public ScoreTrendsCreditLimit(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileScoreTrendsCreditLimit scoreTrendsCreditLimit)
        {
            if (scoreTrendsCreditLimit != null)
            {
                CreditLimitAmount = scoreTrendsCreditLimit.CreditLimitAmount;
                if (scoreTrendsCreditLimit.MostRecentQuarter != null)
                    MostRecentQuarter =
                        scoreTrendsCreditLimit.MostRecentQuarter.Select(p => new MostRecentQuarter(p))
                            .ToList<IMostRecentQuarter>();
                if (scoreTrendsCreditLimit.PriorQuarter != null)
                    PriorQuarter =
                        scoreTrendsCreditLimit.PriorQuarter.Select(p => new PriorQuarter(p)).ToList<IPriorQuarter>();
            }
        }

        public string CreditLimitAmount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IMostRecentQuarter, MostRecentQuarter>))]
        public List<IMostRecentQuarter> MostRecentQuarter { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPriorQuarter, PriorQuarter>))]
        public List<IPriorQuarter> PriorQuarter { get; set; }
    }
}