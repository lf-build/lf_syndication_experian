namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IDoingBusinessAs
    {
        string DbaName { get; set; }
    }
}