using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class BusinessFacts : IBusinessFacts
    {
        public BusinessFacts()
        {
        }

        public BusinessFacts(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileBusinessFacts businessFacts)
        {
            if (businessFacts != null)
            {
                FileEstablishedDate = businessFacts.FileEstablishedDate;
                YearsInBusiness = businessFacts.YearsInBusiness;
                SalesRevenue = businessFacts.SalesRevenue;
                EmployeeSize = businessFacts.EmployeeSize;
                StateOfIncorporation = businessFacts.StateOfIncorporation;
                DateOfIncorporation = businessFacts.DateOfIncorporation;
                if (businessFacts.FileEstablishedFlag != null)
                    FileEstablishedFlag =
                        businessFacts.FileEstablishedFlag.Select(
                            p => new BusinessFactsFileEstablishedFlag(p))
                            .ToList<IBusinessFactsFileEstablishedFlag>();
                if (businessFacts.BusinessTypeIndicator != null)
                    BusinessTypeIndicator =
                        businessFacts.BusinessTypeIndicator.Select(
                            p => new BusinessTypeIndicator(p))
                            .ToList<IBusinessTypeIndicator>();

                if (businessFacts.YearsInBusinessIndicator != null)
                    YearsInBusinessIndicator =
                        businessFacts.YearsInBusinessIndicator.Select(
                            p => new YearsInBusinessIndicator(p))
                            .ToList<IYearsInBusinessIndicator>();

                if (businessFacts.SalesIndicator != null)
                    SalesIndicator =
                        businessFacts.SalesIndicator.Select(
                            p => new SalesIndicator(p))
                            .ToList<ISalesIndicator>();

                if (businessFacts.EmployeeSizeIndicator != null)
                    EmployeeSizeIndicator =
                        businessFacts.EmployeeSizeIndicator.Select(
                            p => new EmployeeSizeIndicator(p))
                            .ToList<IEmployeeSizeIndicator>();

                if (businessFacts.PublicIndicator != null)
                    PublicIndicator =
                        businessFacts.PublicIndicator.Select(
                            p => new PublicIndicator(p))
                            .ToList<IPublicIndicator>();

                if (businessFacts.NonProfitIndicator != null)
                    NonProfitIndicator =
                        businessFacts.NonProfitIndicator.Select(
                            p => new NonProfitIndicator(p))
                            .ToList<INonProfitIndicator>();
            }
        }

        public string FileEstablishedDate { get; set; }
        public string YearsInBusiness { get; set; }
        public string SalesRevenue { get; set; }
        public string EmployeeSize { get; set; }
        public string StateOfIncorporation { get; set; }
        public string DateOfIncorporation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBusinessFactsFileEstablishedFlag, BusinessFactsFileEstablishedFlag>))]
        public List<IBusinessFactsFileEstablishedFlag> FileEstablishedFlag { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBusinessTypeIndicator, BusinessTypeIndicator>))]
        public List<IBusinessTypeIndicator> BusinessTypeIndicator { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IYearsInBusinessIndicator, YearsInBusinessIndicator>))]
        public List<IYearsInBusinessIndicator> YearsInBusinessIndicator { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISalesIndicator, SalesIndicator>))]
        public List<ISalesIndicator> SalesIndicator { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IEmployeeSizeIndicator, EmployeeSizeIndicator>))]
        public List<IEmployeeSizeIndicator> EmployeeSizeIndicator { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPublicIndicator, PublicIndicator>))]
        public List<IPublicIndicator> PublicIndicator { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<INonProfitIndicator, NonProfitIndicator>))]
        public List<INonProfitIndicator> NonProfitIndicator { get; set; }
    }
}