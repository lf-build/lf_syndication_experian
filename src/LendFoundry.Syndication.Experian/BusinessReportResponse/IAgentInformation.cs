﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IAgentInformation
    {
       string Name { get; set; }
    
       string StreetAddress { get; set; }
       
       string City { get; set; }
       
       string State { get; set; }
	}
}