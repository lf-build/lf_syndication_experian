namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class OfacMatchCode : IOfacMatchCode
    {
        public OfacMatchCode()
        {
        }

        public OfacMatchCode(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryOFACMatchCode ofacMatchCode)
        {
            if (ofacMatchCode != null)

                Code = ofacMatchCode.code;
        }

        public string Code { get; set; }
    }
}