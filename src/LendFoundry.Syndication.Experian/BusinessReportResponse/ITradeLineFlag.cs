namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ITradeLineFlag
    {
        string Code { get; set; }
    }
}