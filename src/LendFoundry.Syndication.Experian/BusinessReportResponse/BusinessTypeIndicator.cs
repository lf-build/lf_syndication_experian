namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class BusinessTypeIndicator : IBusinessTypeIndicator
    {
        public BusinessTypeIndicator()
        {
        }

        public BusinessTypeIndicator(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileBusinessFactsBusinessTypeIndicator businessTypeIndicator)
        {
            if (businessTypeIndicator != null)
            {
                Code = businessTypeIndicator.code;
                Value = businessTypeIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}