namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IBusinessDbt
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}