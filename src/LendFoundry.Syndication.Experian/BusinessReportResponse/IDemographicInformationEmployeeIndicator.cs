namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 
	public interface IDemographicInformationEmployeeIndicator {
		 string Code { get; set; }
	}
}