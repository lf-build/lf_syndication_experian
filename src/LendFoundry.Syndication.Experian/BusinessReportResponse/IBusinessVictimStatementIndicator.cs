namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IBusinessVictimStatementIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}