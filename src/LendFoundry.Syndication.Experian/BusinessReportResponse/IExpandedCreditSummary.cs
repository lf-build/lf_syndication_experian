using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IExpandedCreditSummary
    {
        string BankruptcyFilingCount { get; set; }
        string TaxLienFilingCount { get; set; }
        string OldestTaxLienDate { get; set; }
        string MostRecentTaxLienDate { get; set; }
        string JudgmentFilingCount { get; set; }
        string OldestJudgmentDate { get; set; }
        string MostRecentJudgmentDate { get; set; }
        string CollectionCount { get; set; }
        string CollectionBalance { get; set; }
        string CollectionCountPast24Months { get; set; }
        string LegalBalance { get; set; }
        string UccFilings { get; set; }
        string UccDerogCount { get; set; }
        string CurrentAccountBalance { get; set; }
        string CurrentTradelineCount { get; set; }
        string MonthlyAverageDbt { get; set; }
        string HighestDbt6Months { get; set; }
        string HighestDbt5Quarters { get; set; }
        string ActiveTradelineCount { get; set; }
        string AllTradelineBalance { get; set; }
        string AllTradelineCount { get; set; }
        string AverageBalance5Quarters { get; set; }
        string SingleHighCredit { get; set; }
        string LowBalance6Months { get; set; }
        string HighBalance6Months { get; set; }
        string OldestCollectionDate { get; set; }
        string MostRecentCollectionDate { get; set; }
        string CurrentDbt { get; set; }
        string OldestUccFilingDate { get; set; }
        string MostRecentUccFilingDate { get; set; }
        string JudgmentFlag { get; set; }
        string TaxLienFlag { get; set; }
        string TradeCollectionCount { get; set; }
        string TradeCollectionBalance { get; set; }
        string OpenCollectionCount { get; set; }
        string OpenCollectionBalance { get; set; }
        string MostRecentBankruptcyDate { get; set; }
        List<IOfacMatch> OfacMatch { get; set; }
        List<IVictimStatement> VictimStatement { get; set; }
    }
}