﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ICorporateRegistration
    {
         string StateOfOrigin { get; set; }

         string OriginalFilingDate { get; set; }

         string RecentFilingDate { get; set; }

         string IncorporatedDate { get; set; }

         IBusinessType BusinessType { get; set; }

         IStatusFlag StatusFlag { get; set; }

         string StatusDescription { get; set; }

         IProfitFlag ProfitFlag { get; set; }

         string CharterNumber { get; set; }

         IAgentInformation AgentInformation { get; set; }
    }
}
