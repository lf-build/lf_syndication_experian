using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IQuarterlyPaymentTrends
    {
        List<IMostRecentQuarter> MostRecentQuarter { get; set; }
        List<IPriorQuarter> PriorQuarter { get; set; }
    }
}