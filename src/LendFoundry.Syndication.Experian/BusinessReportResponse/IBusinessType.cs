﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IBusinessType
    {
         string Code { get; set; }

         string Text { get; set; }
    }
}
