using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ITaxLien
    {
        string DateFiled { get; set; }
        string DocumentNumber { get; set; }
        string FilingLocation { get; set; }
        string Owner { get; set; }
        string LiabilityAmount { get; set; }
        List<ILegalType> LegalType { get; set; }
        List<ILegalAction> LegalAction { get; set; }
    }
}