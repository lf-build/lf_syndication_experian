namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class BusinessFactsFileEstablishedFlag : IBusinessFactsFileEstablishedFlag
    {
        public BusinessFactsFileEstablishedFlag()
        {
        }

        public BusinessFactsFileEstablishedFlag(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileBusinessFactsFileEstablishedFlag businessFactsFileEstablishedFlag)
        {
            if (businessFactsFileEstablishedFlag != null)
            {
                Code = businessFactsFileEstablishedFlag.code;
                Value = businessFactsFileEstablishedFlag.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}