namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IMatchingBusinessIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}