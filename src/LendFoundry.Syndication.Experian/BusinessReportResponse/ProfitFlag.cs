﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class ProfitFlag :IProfitFlag
    {
        public ProfitFlag()
        {
            
        }

        public ProfitFlag(Proxy.BusinessReportResponse.ProfitFlag profitFlag)
        {
            if (profitFlag !=null)
            {
                Code = profitFlag.Code;
                Text = profitFlag.Text;
            }
        }
        public string Code { get; set; }

        public string Text { get; set; }
    }
}
