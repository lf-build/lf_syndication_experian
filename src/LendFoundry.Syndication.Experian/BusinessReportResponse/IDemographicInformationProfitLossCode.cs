namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 	public interface IDemographicInformationProfitLossCode {
		 string Code { get; set; }
	}
	
}