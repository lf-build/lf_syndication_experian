namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ISicCodesSic
    {
        ISicCodesSicValues Sic { get; set; }
    }
}