namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class BusinessRiskTriggersIndicator : IBusinessRiskTriggersIndicator
    {
        public BusinessRiskTriggersIndicator()
        {
        }

        public BusinessRiskTriggersIndicator(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryBusinessRiskTriggersIndicator businessRiskTriggersIndicator)
        {
            if (businessRiskTriggersIndicator != null)
            {
                Code = businessRiskTriggersIndicator.code;
                Value = businessRiskTriggersIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}