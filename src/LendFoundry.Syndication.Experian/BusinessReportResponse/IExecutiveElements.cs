﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IExecutiveElements
    {
         string BankruptcyCount { get; set; }
         string TaxLienCount { get; set; }
         string EarliestTaxLienDate { get; set; }
         string MostRecentTaxLienDate { get; set; }
         string JudgmentCount { get; set; }
         string EarliestJudgmentDate { get; set; }
         string MostRecentJudgmentDate { get; set; }
         string CollectionCount { get; set; }
         string LegalBalance { get; set; }
         string UCCFilings { get; set; }
         string UCCDerogatoryCount { get; set; }
         string CurrentAccountBalance { get; set; }
         string CurrentTradelineCount { get; set; }
         string MonthlyAverageDBT { get; set; }
         string HighestDBT6Months { get; set; }
         string HighestDBT5Quarters { get; set; }
         string ActiveTradelineCount { get; set; }
         string AllTradelineBalance { get; set; }
         string AllTradelineCount { get; set; }
         string AverageBalance5Quarters { get; set; }
         string SingleLineHighCredit { get; set; }
         string LowBalance6Months { get; set; }
         string HighBalance6Months { get; set; }
         string EarliestCollectionDate { get; set; }
         string MostRecentCollectionDate { get; set; }
         string CurrentDBT { get; set; }
         string YearofIncorporation { get; set; }
         string TaxID { get; set; }
         string JudgmentFlag { get; set; }
         string TaxLienFlag { get; set; }
    }
}
