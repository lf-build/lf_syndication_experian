namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IKeyPersonnelExecutiveInformationNameFlag
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}