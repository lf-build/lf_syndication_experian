namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class ActiveBusinessIndicator : IActiveBusinessIndicator
    {
        public ActiveBusinessIndicator()
        {
        }

        public ActiveBusinessIndicator(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileCommercialFraudShieldSummaryActiveBusinessIndicator activeBusinessIndicator)
        {
            if (activeBusinessIndicator != null)
            {
                Code = activeBusinessIndicator.code;
                Value = activeBusinessIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}