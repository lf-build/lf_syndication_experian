using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IExecutiveSummary
    {
        string PredictedDbt { get; set; }
        string PredictedDbtDate { get; set; }
        string IndustryDbt { get; set; }
        string AllIndustryDbt { get; set; }
        string HighCreditAmountExtended { get; set; }
        string MedianCreditAmountExtended { get; set; }
        string IndustryDescription { get; set; }
        string CommonTerms { get; set; }
        string CommonTerms2 { get; set; }
        string CommonTerms3 { get; set; }
        List<IBusinessDbt> BusinessDbt { get; set; }

        List<ILowestTotalAccountBalance> LowestTotalAccountBalance { get; set; }

        List<IHighestTotalAccountBalance> HighestTotalAccountBalance { get; set; }

        List<ICurrentTotalAccountBalance> CurrentTotalAccountBalance { get; set; }

        List<IIndustryPaymentComparison> IndustryPaymentComparison { get; set; }

        List<IPaymentTrendIndicator> PaymentTrendIndicator { get; set; }
    }
}