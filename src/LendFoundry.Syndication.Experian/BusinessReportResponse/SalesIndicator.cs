namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class SalesIndicator : ISalesIndicator
    {
        public SalesIndicator()
        {
        }

        public SalesIndicator(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileBusinessFactsSalesIndicator salesIndicator)
        {
            if (salesIndicator != null)

                Code = salesIndicator.code;
        }

        public string Code { get; set; }
    }
}