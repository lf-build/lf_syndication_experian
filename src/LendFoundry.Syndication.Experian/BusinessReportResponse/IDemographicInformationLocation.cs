﻿

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    	public interface IDemographicInformationLocation 
		{
		 string Code { get; set; }
	}

}
