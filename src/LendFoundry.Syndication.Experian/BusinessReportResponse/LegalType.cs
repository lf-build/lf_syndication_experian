namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class LegalType : ILegalType
    {
        public LegalType()
        {
        }

        public LegalType(Proxy.BusinessReportResponse.LegalType legalType)
        {
            if (legalType != null)
            {
                Code = legalType.code;
                Value = legalType.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}