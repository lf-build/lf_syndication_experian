using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class UccFilingsOriginalUccFilingsInfo : IUccFilingsOriginalUccFilingsInfo
    {
        public UccFilingsOriginalUccFilingsInfo(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileUCCFilingsOriginalUCCFilingsInfo uccFilingsOriginalUccFilingsInfo)
        {
            if (uccFilingsOriginalUccFilingsInfo != null)
            {
                DateFiled = uccFilingsOriginalUccFilingsInfo.DateFiled;
                DocumentNumber = uccFilingsOriginalUccFilingsInfo.DocumentNumber;
                FilingState = uccFilingsOriginalUccFilingsInfo.FilingState;
                if (uccFilingsOriginalUccFilingsInfo.LegalAction != null)
                    LegalAction =
                        uccFilingsOriginalUccFilingsInfo.LegalAction.Select(p => new LegalAction(p))
                            .ToList<ILegalAction>();
            }
        }

        public string FilingState { get; set; }
        public string DocumentNumber { get; set; }
        public string DateFiled { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILegalAction, LegalAction>))]
        public List<ILegalAction> LegalAction { get; set; }
    }
}