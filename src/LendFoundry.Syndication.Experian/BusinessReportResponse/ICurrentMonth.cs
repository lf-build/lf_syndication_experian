﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ICurrentMonth
    {
        string CurrentPercentage { get; set; }
        string Date { get; set; }
        string Dbt { get; set; }
        string Dbt120 { get; set; }
        string Dbt30 { get; set; }
        string Dbt60 { get; set; }
        string Dbt90 { get; set; }
        List<ITotalAccountBalance> TotalAccountBalance { get; set; }
    }
}