using System.Collections.Generic;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IUccFilingsCollateralCodesCollateral
    {
        List<IUccFilingsCollateralCodesCollateralValues> Collateral { get; set; }
    }
}