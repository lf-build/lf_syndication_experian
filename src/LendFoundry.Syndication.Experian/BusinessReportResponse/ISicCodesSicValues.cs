namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ISicCodesSicValues
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}