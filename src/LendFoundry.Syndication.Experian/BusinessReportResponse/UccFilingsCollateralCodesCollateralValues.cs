namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class UccFilingsCollateralCodesCollateralValues : IUccFilingsCollateralCodesCollateralValues
    {
        public UccFilingsCollateralCodesCollateralValues()
        {
        }

        public UccFilingsCollateralCodesCollateralValues(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileUCCFilingsCollateralCodesCollateralValues uccFilingsCollateralCodesCollateralValues)
        {
            if (uccFilingsCollateralCodesCollateralValues != null)
            {
                Code = uccFilingsCollateralCodesCollateralValues.code;
                Value = uccFilingsCollateralCodesCollateralValues.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}