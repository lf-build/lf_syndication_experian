using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class KeyPersonnelExecutiveInformation : IKeyPersonnelExecutiveInformation
    {
        public KeyPersonnelExecutiveInformation()
        {
        }

        public KeyPersonnelExecutiveInformation(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileKeyPersonnelExecutiveInformation keyPersonnelExecutiveInformation)
        {
            if (keyPersonnelExecutiveInformation != null)
            {
                Name = keyPersonnelExecutiveInformation.Name;
                if (keyPersonnelExecutiveInformation.NameFlag != null)
                    NameFlag =
                        keyPersonnelExecutiveInformation.NameFlag.Select(
                            p => new KeyPersonnelExecutiveInformationNameFlag(p))
                            .ToList<IKeyPersonnelExecutiveInformationNameFlag>();
                if (keyPersonnelExecutiveInformation.Title != null)
                    Title =
                        keyPersonnelExecutiveInformation.Title.Select(
                            p => new KeyPersonnelExecutiveInformationTitle(p))
                            .ToList<IKeyPersonnelExecutiveInformationTitle>();
            }
        }

        public string Name { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IKeyPersonnelExecutiveInformationNameFlag, KeyPersonnelExecutiveInformationNameFlag>))]
        public List<IKeyPersonnelExecutiveInformationNameFlag> NameFlag { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IKeyPersonnelExecutiveInformationTitle, KeyPersonnelExecutiveInformationTitle>))]
        public List<IKeyPersonnelExecutiveInformationTitle> Title { get; set; }
    }
}