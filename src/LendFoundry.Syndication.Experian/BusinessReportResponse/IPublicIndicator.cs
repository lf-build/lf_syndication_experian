namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IPublicIndicator
    {
        string Code { get; set; }
    }
}