namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IActiveBusinessIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}