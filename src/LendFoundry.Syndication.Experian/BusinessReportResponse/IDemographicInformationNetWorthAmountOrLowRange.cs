
namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 public interface IDemographicInformationNetWorthAmountOrLowRange {
	 
		
		 IDemographicInformationModifier Modifier { get; set; }
		 string Amount { get; set; }
	}
	
}