namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 	public class DemographicInformationProfitLossCode:IDemographicInformationProfitLossCode {
	     public DemographicInformationProfitLossCode()
	     {
	         
	     }

	     public DemographicInformationProfitLossCode(Proxy.BusinessReportResponse.DemographicInformationProfitLossCode profitLossCode)
	     {
	         
	         if (profitLossCode!=null)
	         {
	             Code = profitLossCode.Code;
	         }

         }
		public string Code { get; set; }
	}
	
}