using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class Bankruptcy : IBankruptcy
    {
        public Bankruptcy()
        {
        }

        public Bankruptcy(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileBankruptcy bankruptcy)
        {
            if (bankruptcy != null)
            {
                DateFiled = bankruptcy.DateFiled;
                DocumentNumber = bankruptcy.DocumentNumber;
                FilingLocation = bankruptcy.FilingLocation;
                Owner = bankruptcy.Owner;
                LiabilityAmount = bankruptcy.LiabilityAmount;
                AssetAmount = bankruptcy.AssetAmount;
                ExemptAmount = bankruptcy.ExemptAmount;
                CustomerDispute = bankruptcy.CustomerDispute;
                if (bankruptcy.LegalType != null)
                    LegalType = bankruptcy.LegalType.Select(p => new LegalType(p)).ToList<ILegalType>();
                if (bankruptcy.LegalAction != null)
                    LegalAction = bankruptcy.LegalAction.Select(p => new LegalAction(p)).ToList<ILegalAction>();
                if (bankruptcy.Statement != null)
                    Statement = bankruptcy.Statement.Select(p => new Statement(p)).ToList<IStatement>();
            }
        }

        public string DateFiled { get; set; }
        public string DocumentNumber { get; set; }
        public string FilingLocation { get; set; }
        public string Owner { get; set; }
        public string LiabilityAmount { get; set; }
        public string AssetAmount { get; set; }
        public string ExemptAmount { get; set; }
        public string CustomerDispute { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ILegalType, LegalType>))]
        public List<ILegalType> LegalType { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ILegalAction, LegalAction>))]
        public List<ILegalAction> LegalAction { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IStatement, Statement>))]
        public List<IStatement> Statement { get; set; }
    }
}