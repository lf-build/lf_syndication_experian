using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class ProfileInquiry : IProfileInquiry
    {
        public ProfileInquiry()
        {
        }

        public ProfileInquiry(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileInquiry profileInquiry)
        {
            if (profileInquiry != null)
            {
                InquiryBusinessCategory = profileInquiry.InquiryBusinessCategory;
                if (profileInquiry.InquiryCount != null)
                    InquiryCount =
                        profileInquiry.InquiryCount.Select(p => new InquiryCount(p))
                            .ToList<IInquiryCount>();
            }
        }

        public string InquiryBusinessCategory { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IInquiryCount, InquiryCount>))]
        public List<IInquiryCount> InquiryCount { get; set; }
    }
}