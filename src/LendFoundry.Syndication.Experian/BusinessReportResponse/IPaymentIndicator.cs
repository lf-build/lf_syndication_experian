namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IPaymentIndicator
    {
        string Code { get; set; }
    }
}