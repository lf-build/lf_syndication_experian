namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IUccFilingsCollateralCodesCollateralValues
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}