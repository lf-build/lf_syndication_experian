namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class PaymentTrendIndicator : IPaymentTrendIndicator
    {
        public PaymentTrendIndicator()
        {
        }

        public PaymentTrendIndicator(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileExecutiveSummaryPaymentTrendIndicator paymentTrendIndicator)
        {
            if (paymentTrendIndicator != null)
            {
                Code = paymentTrendIndicator.code;
                Value = paymentTrendIndicator.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}