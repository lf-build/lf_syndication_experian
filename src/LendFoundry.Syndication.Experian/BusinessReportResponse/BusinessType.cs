﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class BusinessType:IBusinessType
    {
        public BusinessType()
        {
            
        }

        public BusinessType(Proxy.BusinessReportResponse.BusinessType businessType)
        {
            if (businessType!=null)
            {
                Code = businessType.Code;
                Text = businessType.Text;
            }
        }
        public string Code { get; set; }

        public string Text { get; set; }
    }
}
