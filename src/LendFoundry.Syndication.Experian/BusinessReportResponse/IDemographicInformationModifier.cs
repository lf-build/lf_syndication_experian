namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 
	public interface IDemographicInformationModifier {
		 string Code { get; set; }
	}
}