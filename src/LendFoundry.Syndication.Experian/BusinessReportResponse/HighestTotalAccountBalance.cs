using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class HighestTotalAccountBalance : IHighestTotalAccountBalance
    {
        public HighestTotalAccountBalance()
        {
        }

        public HighestTotalAccountBalance(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileExecutiveSummaryHighestTotalAccountBalance highestTotalAccountBalance)
        {
            if (highestTotalAccountBalance != null)
            {
                Amount = highestTotalAccountBalance.Amount;
                if (highestTotalAccountBalance.Modifier != null)
                    Modifier = highestTotalAccountBalance.Modifier.Select(p => new Modifier(p)).ToList<IModifier>();
            }
        }

        public string Amount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IModifier, Modifier>))]
        public List<IModifier> Modifier { get; set; }
    }
}