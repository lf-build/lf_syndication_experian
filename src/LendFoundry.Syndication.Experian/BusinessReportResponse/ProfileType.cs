namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class ProfileType : IProfileType
    {
        public ProfileType()
        {
        }

        public ProfileType(string code)
        {
            if (!string.IsNullOrWhiteSpace(code))
                Code = code;
        }

        public string Code { get; set; }
    }
}