﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class StatusFlag : IStatusFlag
    {
        public StatusFlag()
        {
            
        }

        public StatusFlag(Proxy.BusinessReportResponse.StatusFlag  statusFlag)
        {
            if (statusFlag!=null)
            {
                Code = statusFlag.Code;
                Text = statusFlag.Text;
            }
        }
        public string Code { get; set; }

        public string Text { get; set; }
    }
}
