namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IOfacMatch
    {
        string Code { get; set; }
    }
}