namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
 
	public class DemographicInformationAdditionalSICCodes:IDemographicInformationAdditionalSICCodes {
	    public DemographicInformationAdditionalSICCodes()
	    {
	        
	    }

	    public DemographicInformationAdditionalSICCodes(Proxy.BusinessReportResponse.DemographicInformationAdditionalSICCodes additionalSICCodes)
	    {
	        if (additionalSICCodes!=null)
	        {
	            SICCode = additionalSICCodes.SICCode;
	        }
	    }
		public string SICCode { get; set; }
	}
	
	}