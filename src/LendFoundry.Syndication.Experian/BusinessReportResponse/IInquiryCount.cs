namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IInquiryCount
    {
        string Date { get; set; }
        string Count { get; set; }
    }
}