namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public class NaicsCodesNaicsValues : INaicsCodesNaicsValues
    {
        public NaicsCodesNaicsValues()
        {
        }

        public NaicsCodesNaicsValues(Proxy.BusinessReportResponse.NetConnectResponseProductsPremierProfileNAICSCodesNAICSValues naicsCodesNaicsValues)
        {
            if (naicsCodesNaicsValues != null)
            {
                Code = naicsCodesNaicsValues.code;
                Value = naicsCodesNaicsValues.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}