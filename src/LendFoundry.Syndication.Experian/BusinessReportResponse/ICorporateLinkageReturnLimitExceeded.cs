namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface ICorporateLinkageReturnLimitExceeded
    {
        string Code { get; set; }
    }
}