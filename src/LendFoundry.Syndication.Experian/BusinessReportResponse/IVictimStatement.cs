namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IVictimStatement
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}