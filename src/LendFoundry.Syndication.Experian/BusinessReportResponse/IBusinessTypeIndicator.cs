namespace LendFoundry.Syndication.Experian.BusinessReportResponse
{
    public interface IBusinessTypeIndicator
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}