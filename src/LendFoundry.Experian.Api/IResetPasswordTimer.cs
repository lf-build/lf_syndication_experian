namespace LendFoundry.Syndication.Experian.Api
{
    public interface IResetPasswordTimer
    {
        void Execute();
    }
}