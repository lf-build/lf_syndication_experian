using System;
using System.Collections.Generic;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using Quartz;
using Quartz.Impl;
using LendFoundry.Tenant.Client;
using System.Text;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Syndication.Experian.Api
{
    public class ResetPasswordTimer : IResetPasswordTimer
    {
        public ResetPasswordTimer(
            ITokenHandler tokenHandler,
            IConfigurationServiceFactory configurationFactory, ILoggerFactory loggerFactory, ITenantServiceFactory tenantServiceFactory,
            IBusinessReportServiceFactory businessReportService,
            IPersonalReportServiceFactory personalReportServiceFactory)
        {
            TokenHandler = tokenHandler;
            ConfigurationFactory = configurationFactory;
            TenantServiceFactory = tenantServiceFactory;
            Schedule = Settings.Schedule;
            BusinessReportService = businessReportService;
            PersonalReportServiceFactory = personalReportServiceFactory;
            LoggerFactory = loggerFactory;
        }


        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private string Schedule { get; }
        private IPersonalReportServiceFactory PersonalReportServiceFactory { get; }
        private IBusinessReportServiceFactory BusinessReportService { get; }
        private ILoggerFactory LoggerFactory { get; }

        public void Execute()
        {
            var scheduler = new StdSchedulerFactory().GetScheduler().Result;
            scheduler.ScheduleJob(CreateJob(), CreateTrigger());
            scheduler.Start();       
        }

        public void OnSchedule()
        {
            var emptyReader = new StaticTokenReader(string.Empty);
            var tenantService = TenantServiceFactory.Create(emptyReader);
            var tenants = tenantService.GetActiveTenants();
            tenants.ForEach(tenant =>
            {

                var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName, null, "system", null);
                var reader = new StaticTokenReader(token.Value);
                var logger = LoggerFactory.Create(NullLogContext.Instance);
                var businessReportService = BusinessReportService.Create(reader, TokenHandler, logger);
                var personalReportService = PersonalReportServiceFactory.Create(reader, TokenHandler, logger);
                var configuration = ConfigurationFactory.Create<ExperianConfiguration>(Settings.ServiceName, reader);
                if (configuration == null)
                {
                    logger.Error("Was not found the Configuration related to Experian");
                    return;
                }
                try
                {
                    var newPasswordForBusiness = businessReportService.ResetPassword().Result;
                    var newPasswordForPersonal = personalReportService.ResetPassword().Result;

                    if (string.IsNullOrEmpty(newPasswordForPersonal) || string.IsNullOrEmpty(newPasswordForBusiness))
                        throw new Exception("New Password for Personal or Business not Found");

                    var configurationdata = configuration.Get();
                    configurationdata.businessReportConfiguration.NetConnectPassword = newPasswordForBusiness;
                    configurationdata.businessReportConfiguration.NetConnectPasswordHash = Hash(newPasswordForBusiness);

                    configurationdata.personalReportConfiguration.NetConnectPassword = newPasswordForPersonal;
                    configurationdata.personalReportConfiguration.NetConnectPasswordHash = Hash(newPasswordForPersonal);
                    configuration.Set(configurationdata);
                }
                catch (Exception ex)
                {
                    logger.Error("Failed attempt to update password.", ex);
                }
            });
        }

        private string Hash(string password)
        {
            var bytes = new UTF8Encoding().GetBytes(password);
            var hashBytes = System.Security.Cryptography.MD5.Create().ComputeHash(bytes);
            return Convert.ToBase64String(hashBytes);
        }

        private IJobDetail CreateJob()
        {
            var data = new Dictionary<string, object>
            {
                {"agent", this}
            } as IDictionary<string, object>;

            return JobBuilder
                .Create<AgentJob>()
                .SetJobData(new JobDataMap(data))
                .Build();
        }

        private ITrigger CreateTrigger()
        {
            var expression = new CronExpression(Schedule);
            var schedule = CronScheduleBuilder.CronSchedule(expression);
            var triggerBuilder = TriggerBuilder.Create().WithSchedule(schedule);
            return triggerBuilder.Build();
        }

        private class AgentJob : IJob
        {
             public Task Execute(IJobExecutionContext context)
            {
                return Task.Run(() =>
                {
                    var agent = context.MergedJobDataMap.Get("agent") as ResetPasswordTimer;
                    agent?.OnSchedule();
                });
            }
        }
    }
}
