﻿using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Experian.Proxy;
using LendFoundry.Tenant.Client;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace LendFoundry.Syndication.Experian.Api
{
    public class Startup
    {
        #region Public Methods

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseHealthCheck();
            app.UseCors(env);
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Experian Service");
            });
            
            app.UseErrorHandling();
            app.UseResetPasswordTimer();
            app.UseMvc();
        }

     
		// This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
                        services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Experian"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Experian.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();         
            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();        
            // interface implements
            services.AddConfigurationService<ExperianConfiguration>(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();         
            services.AddTransient<IResetPasswordTimer, ResetPasswordTimer>();
            services.AddTransient<IPersonalReportServiceFactory, PersonalReportServiceFactory>(); 
            services.AddTransient<IBusinessReportServiceFactory, BusinessReportServiceFactory>();
            services.AddTransient<IPersonalReportProxyFactory, PersonalReportProxyFactory>();
            services.AddTransient<IBusinessReportProxyFactory, BusinessReportProxyFactory>();
            services.AddDependencyServiceUriResolver<ExperianConfiguration>(Settings.ServiceName);
            // Configuration factory
            services.AddTransient<IExperianConfiguration>(p =>
            {
                var configuration = p.GetService<IConfigurationService<ExperianConfiguration>>().Get();                
                return configuration;
            });
            services.AddLookupService();
            services.AddTransient<IBusinessReportProxy, BusinessReportProxy>();
            services.AddTransient<IBusinessReportService, BusinessReportService>();
            services.AddTransient<IPersonalReportProxy, PersonalReportProxy>();
            services.AddTransient<IPersonalReportService, PersonalReportService>();
            services.AddTransient<IProcessProxy, ProcessProxy>();
            services.AddTransient<IProcessProxyFactory, ProcessProxyFactory>();

        }

        #endregion Public Methods
    }
}
