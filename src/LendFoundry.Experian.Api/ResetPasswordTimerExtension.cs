﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Foundation.Services;

namespace LendFoundry.Syndication.Experian.Api
{
    public static class ResetPasswordTimerExtension
    {
        public static void UseResetPasswordTimer(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IResetPasswordTimer>().Execute();
        }
    }
}
