﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using Microsoft.AspNetCore.Mvc;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian.Api.Controller
{
    [Route("")]
    public class BusinessReportApiController : ExtendedController
    {
        public BusinessReportApiController(IBusinessReportService businessReportservice)
        {
            BusinessReportService = businessReportservice;
        }

        private IBusinessReportService BusinessReportService { get; }

        [HttpPost("{entitytype}/{entityid}/business/search")]
        [ProducesResponseType(typeof(ISearchBusinessResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetBusinessSearch(string entityType, string entityId, [FromBody] GetBusinessSearchRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => BusinessReportService.GetBusinessSearch(entityType, entityId, request)));
            });
        }

        [HttpPost("{entitytype}/{entityid}/business/report")]
        [ProducesResponseType(typeof(IBusinessReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetBusinessReport(string entityType, string entityId, [FromBody] GetBusinessReportRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => BusinessReportService.GetBusinessReport(entityType, entityId, request)));
                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpPost("{entitytype}/{entityid}/business/directhitreport")]
        [ProducesResponseType(typeof(IDirectHitReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetDirectHitReport(string entityType, string entityId, [FromBody] GetDirectHitReportRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => BusinessReportService.GetDirectHitReport(entityType, entityId, request)));
                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
        
        //TODO: We need to remove after reset password test
        [HttpPost("reset-password/business")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> ResetPassword()
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => BusinessReportService.ResetPassword()));
                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
    }
}