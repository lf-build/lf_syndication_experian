﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using Microsoft.AspNetCore.Mvc;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;


namespace LendFoundry.Syndication.Experian.Api.Controller
{
    [Route("")]
    public class PersonalReportApiController : ExtendedController
    {
        public PersonalReportApiController(IPersonalReportService personalReportService)
        {
            PersonalReportService = personalReportService;
        }

        private IPersonalReportService PersonalReportService { get; }

        [HttpPost("{entitytype}/{entityid}/personal/report/{ishardpull?}")]
        [ProducesResponseType(typeof(IPersonalReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetPersonalReport(string entityType, string entityId, [FromBody] GetPersonalReportRequest request, bool isHardpull = false)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => PersonalReportService.GetPersonalReportResponse(entityType, entityId, request, isHardpull)));
            });
        }
        [HttpPost("reset-password/personal")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> ResetPassword()
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => PersonalReportService.ResetPassword()));
                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

    }
}