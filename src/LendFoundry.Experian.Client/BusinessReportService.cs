﻿using System;
using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using LendFoundry.Foundation.Client;
using RestSharp;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian.Client
{
    public class BusinessReportService : IBusinessReportService
    {
        public BusinessReportService(IServiceClient client)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IBusinessReportResponse> GetBusinessReport(string entityType, string entityId, IBusinessReportRequest reportRequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/business/report", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(reportRequest);
            return await Client.ExecuteAsync<Response.BusinessReportResponse>(request);
        }

        public async Task<ISearchBusinessResponse> GetBusinessSearch(string entityType, string entityId, IBusinessSearchRequest searchRequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/business/search", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(searchRequest);
            return await Client.ExecuteAsync<Response.SearchBusinessResponse>(request);
        }

        public async Task<IDirectHitReportResponse> GetDirectHitReport(string entityType, string entityId, IDirectHitReportRequest directedRequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/business/directhitreport", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(directedRequest);
            return await Client.ExecuteAsync<Response.DirectHitReportResponse>(request);
        }
        public async Task<string> ResetPassword()
        {
            var request = new RestRequest("reset-password/business", Method.POST);
            return await Client.ExecuteAsync<string>(request);
        }
    }
}