﻿using System;
using LendFoundry.Syndication.Experian.Request;
using LendFoundry.Syndication.Experian.Response;
using LendFoundry.Foundation.Client;
using RestSharp;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Experian.Client
{
    public class PersonalReportService : IPersonalReportService
    {
        public PersonalReportService(IServiceClient client)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IPersonalReportResponse> GetPersonalReportResponse(string entityType, string entityId, IPersonalReportRequest reportRequest, bool isHardpull=false)
        {
            string uri = string.Empty;
            if (isHardpull==true)
                uri = "{entitytype}/{entityid}/personal/report/true";            
            else
                uri = "{entitytype}/{entityid}/personal/report";
            var request = new RestRequest(uri, Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(reportRequest);
            return await Client.ExecuteAsync<Response.PersonalReportResponse>(request);
        }
        public async Task<string> ResetPassword()
        {
            var request = new RestRequest("reset-password/personal", Method.POST);
            return await Client.ExecuteAsync<string>(request);
        }
    }
}