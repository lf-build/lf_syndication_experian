﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Experian.Client
{
    public class PersonalReportClientFactory : IPersonalReportClientFactory
    {
        public PersonalReportClientFactory(IServiceProvider provider, string endPoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endPoint, port).Uri;
        }

        public PersonalReportClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        #region Private Properties
        /// <summary>
        /// Gets the provider.
        /// </summary>
        /// <value>The provider.</value>
        private IServiceProvider Provider { get; }
        private Uri Uri { get; }

        #endregion Private Properties

       public IPersonalReportService Create(ITokenReader tokenReader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(tokenReader, logger).Get("experian");
            }
            var client = Provider.GetServiceClient(tokenReader, uri);
            return new PersonalReportService(client);
        }
    }
}