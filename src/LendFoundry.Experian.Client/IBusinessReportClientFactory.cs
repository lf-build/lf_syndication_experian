﻿using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Experian;

namespace LendFoundry.Syndication.Experian.Client
{
    public interface IBusinessReportClientFactory
    {
        IBusinessReportService Create(ITokenReader tokenReader);
    }
}