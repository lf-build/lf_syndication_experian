﻿using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Experian.Client
{
    public static class BusinessReportServiceClientExtensions
    {
        #region Public Methods

        /// <summary>
        /// Adds the lenddo service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="port">The port.</param>
        /// <returns>IServiceCollection.</returns>
        public static IServiceCollection AddBusinessReportService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IBusinessReportClientFactory>(p => new BusinessReportClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IBusinessReportClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        #endregion Public Methods
    }
}
