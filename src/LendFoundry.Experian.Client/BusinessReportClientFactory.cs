﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Experian.Client
{
    public class BusinessReportClientFactory : IBusinessReportClientFactory
    {
        public BusinessReportClientFactory(IServiceProvider provider,string endPoint,int port)
        {
            Providers = provider;
            Uri = new UriBuilder("http", endPoint, port).Uri;
        }

        public BusinessReportClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Providers = provider;
            Uri = uri;
        }
        #region Private Properties
        /// <summary>
        /// Gets the provider.
        /// </summary>
        /// <value>The provider.</value>
        private IServiceProvider Providers { get; }

        private Uri Uri { get; }

        #endregion Private Properties
        public IBusinessReportService Create(ITokenReader tokenReader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Providers.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Providers.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(tokenReader, logger).Get("experian");
            }
            var client = Providers.GetServiceClient(tokenReader, uri);
            return new BusinessReportService(client);
        }
    }
}
